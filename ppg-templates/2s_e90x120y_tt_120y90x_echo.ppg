// Header section

Clock Frequency = 10 Mhz;
ISA Card Address = 8000;   // VME address of the pulse programmer board (in hex)
Number of Flags = 24;      // Number of output bits

//************************************************************************************************************************************//

//************************************************************************************************************************************//

// !! USER SECTION !! // Definitions specific for T2 visualization and spin echo type ... will flip the measured Z polarization alternately on every measurement  
                      // 
                      // FID+/- visualization sequence= FID_N = RF_delay (+ pi/2x + xyFlip/2y Tau  +           xyFlip/2y + pi/2x + Tmeas          + pi/2-x + xyFlip/2-y Tau  +          xyFLip/2-y + pi/2-x + Tmeas )^N 
					  //                                        msc$ub1/2                 msc&ub1=0/ub2=1                  msc&ub1=1/ub2=1                  msc&ub1=0/ub2=0                    msc&ub1=1/ub2=0
                      //					                                               
					  // There are N bipolar observations of the FID in this case showing +/- representations of the transverse polarization effectively as a function of (0 -> N-1)*2Tau
					  //
                      // Spin Echo visualization = SpE_N = FID_N +       (pi/2x + xyFlip/2-y Tau  +           xyFlip/2-y + pi/2x + Tmeas          + pi/2-x + pi/2-y Tau  +          pi/2-y + pi/2-x + Tmeas  ) + FID_2N
					  //                                                                 msc&ub1=0/ub2=1                   msc&ub1=0/ub2=1                  msc&ub1=0/ub2=0                    msc&ub1=1/ub2=0        
					  // There are another 2N FID observation measurements after the pi pulse 
					  //  
					  // The use of user bit1 is to tag interleaved unequal dwell times pairs during [the T2 depahsing slice (Tau/ub1=0), the Z-pol measurement interval (Tmeas/ub1=1)]
					  // The use of user bit2 is to tag z direction of the polarization measurement, as the sequence is y,-z, y,z  
					  // However, the pulse sequence "effectively" extracts transverse de/rephasing behavious on a time scale in units of Tau, i.e. (0->3N)*Tau  
					  //
					  // the loop labels and numbers are:    label              number       time of one loop unit                  defaults
					  //                                 i) rf_delay          n_rf_delay     2*(d_dwell_z + d_dwell_x_2 + 2*d_frpi_2)
					  //                                ii) FID_measurement   n_FID_delays    "
					  //							   iii) Echo_measurement  n_ECHO_delays   "                                      n_echo_delays=2*n_FID_delays
					  // It is assumed that it the PSMii is being used 1f=x/0def, 3f=y/90deg, 5f=-x/180deg, fRef=-y/270deg are the phases of the RF pulses/gates 
					  // 
  // Delay declarations

//  d_mcs_pulse=0;      // minimal pulse-length, for pulsing  mcs next  // not used in this script
//  d_fsc_pulse=0;      // minimal pulse-length, for pulsing fsc strobe // "
//  d_pulse=0;   		// minimal pulse-length, for pulsing both mac next and fsc strobe // "
  d_rf_halfpi_on=0;     // pi/2 pulse (one hopes the pulse is strong enough to precess all the spins ... rectangular or Hermite pulse shape), 
					 //   must be determined experimentally using 2d mode
  d_rf_xyflip_2on=0; // one half the angle of the "xy plain" flip around the y axis, typically 120 degrees 					 
  d_rf_pi_on=0;      // pi pulse
  d_dwell_z=0;       // dwell time when polarization is along Z (i.e. before every pi_x/2) .. this is the significant measurement, must be <<T1
  d_dwell_x_2=0;     // 1/2 the dwell time when polarization is along Y during the T2 examination interval (i.e. after every pi_x/2) ... this must be  <<T2
  d_dacs=0; 		 // DAC Servicetime

// !! END OF USER SECTION !! //

//************************************************************************************************************************************//

// Bit patterns ATTENTION!!: The bits are adressed from high to low, the position in the sum gives the number of the bit affected.
// READ THE NOTE BELOW ABOUT BIT 3
f_rf0_on       = 1,1;   // bit   1
f_rf0_off      = 0,1;
f_rf90_on      = 1,1;   // bit   2
f_rf90_off     = 0,1;
f_rf180_on     = 1,1;   // bit   3
f_rf180_off    = 0,1;
f_rf270_on     = 1,1;   // bit   4
f_rf270_off    = 0,1;
f_rfgate_on    = 1,1;   // bit   5
f_rfgate_off   = 0,1;
f_rf_on        = 1,1;   // bit   6
f_rf_off       = 0,1;

f_msnp_hi  = 1,1;       // bit   7 next dwelltime multiscaler pulse
f_msnp_lo  = 0,1;

f_counter_e    = 1,1;   // bit   8 multiscaler counter-gate
f_counter_d    = 0,1;

f_userbit1_on  = 1,1;   // bit   9 -> Userbit1
f_userbit1_off = 0,1;

f_userbit2_on  = 1,1;   // bit  10 -> Userbit2
f_userbit2_off = 0,1;

f_freqp_hi     = 1,1;   // bit  11 next FSC freq. ext. strobe
f_freqp_lo     = 0,1;

f_udctrl_u     = 1,1;   // bit  12 Strobe up down control
f_udctrl_d     = 0,1;

f_dacservp_hi  = 1,1;   // bit  13 Dac service pulse
f_dacservp_lo  = 0,1;

f_beam_on      = 1,1;   // bit 14
f_beam_off     = 0,1;

f_pol_pos      = 1,1;   // bit 15
f_pol_neg      = 0,1;

f_fsc_0        = 0,9;
f_fsc_1        = 1,9;

f_fsc_dummy  = 0,9;     // mask for channel 16 - 24 FSC memory select channels

//************************************************************************************************************************************//

// Program //--------------------------------------------------------------------------------------------------------------// Program //

// NOTE: Bit 3 is misbehaving. This program is modified so the rfgate output (bit 5) performs the actions of rf180 (bit 3), and vice versa
// note that the f_msnp pulse is synched with the rf pulse preceeding the gate measurement (to save coding)  
// PreBeam section min number 2  if loop is exectured once. First pulse triggers the next FSC freq strobe 
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;		
  Loop PreBeam N_PreBeam_cycles;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;		
  End Loop PreBeam; 
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;	

// BeamOn section min number 2  if loop is exectured once. 
        d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;		
  Loop BeamOn N_BeamOn_cycles;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;		
  End Loop BeamOn; 
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;		
// Beam is shut off during last d_dwell_z time to make sure the next rf pulse is not applied to any incomming beam 

// Quasi Free Induction decay measurement using   ...      
// (pi/2_x/ pi/2_y Tau pi/2_y pi/2_x Tmeas  pi/2_-x/ pi/2_-y Tau pi/2_-y pi/2_-x Tmeas)^N // preserve timing with the re_delay & beam build up section, rf gate still only with RF pi/2 0 degree (i.e. x) pulses
 		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;		 
  Loop FID_measurement n_FID_cycles;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
  End Loop FID_measurement; 
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		
// pi_y pulse is most simply implemented by removing one pi/2_y pi/2_y sandwich, which can be the first pair in the echo group .... and it is removed by puting these back in, like the rest of the sequence
        // Quasi Echo Free Induction decay measurement using          
// (pi/2_x/ pi/2_y Tau pi/2_y pi/2_x Tmeas  pi/2_-x/ pi/2_-y Tau pi/2_-y pi/2_-x Tmeas)^N // preserve timing with the re_delay & beam build up section, rf gate still only with RF pi/2 0 degree (i.e. x) pulses
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off; 
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_pi_on         f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;    // pi pulse
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;  
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;	 
  Loop Echo_measurement n_ECHO_cycles;
	    d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_on  + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
  End Loop Echo_measurement; 
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_dwell_x_2        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_xyflip_2on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_on  + f_rf180_off + f_rf90_off + f_rf0_off;
		d_rf_halfpi_on        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
		d_dwell_z     	   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
// Add DAQ service time 
        d_dacs             f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
        d_dacs             f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
//  End Program //


