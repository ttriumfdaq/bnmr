// tune_descriptions.js
// 
var txt = 
'{"modes":[' +

        '{"ppgmode":"10","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 10" }' +
         ']},'+

        '{"ppgmode":"1a","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1a" }' +
         ']},'+

        '{"ppgmode":"1b","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1b" }' +
         ']},'+

        '{"ppgmode":"1c","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1c" }' +
         ']},'+

        '{"ppgmode":"1f","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1f" }' +
         ']},'+

        '{"ppgmode":"1g","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1g" }' +
         ']},'+

        '{"ppgmode":"1j","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 1j" }' +
         ']},'+


        '{"ppgmode":"20","tunes":[' +
              '{"tuneName":"last","description":"Description for Tune:last  PPG Mode 20" }' +
         ']},'+

        '{"ppgmode":"2a", "tunes":[' +
              '{"tuneName":"try5","description":"Description for try5 mode 2a" },' +
              '{"tuneName":"try55","description":"Description for try55 mode 2a " },' +
              '{"tuneName":"test","description":"Description for test  mode 2a" },'+
              '{"tuneName":"last","description":"Description for Tune:last  PPG Mode: 2a" }' +
         ']},'+

        '{"ppgmode":"2b","tunes":[' +
              '{"tuneName":"last","description":"Description for Tune:last  PPG Mode: 2b" }' +
         ']},'+

        '{"ppgmode":"2c","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 2c" }' +
         ']},'+

        '{"ppgmode":"2d","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 2d" }' +
         ']},'+

        '{"ppgmode":"2e","tunes":[' +
              '{"tuneName":"last","description":"Description for  Tune:last  PPG Mode 2e" }' +
         ']}'+

']}'

