// psm3_functions.js
//
// the following globals are specific to psm3
 var channel_path = new Array ();
 var num_channels_enabled=0;// default
var channels=["f0 Chan1","f0 Chan2","f0 Chan3","f0 Chan4","f1 REF","none"];
var channel=["f0ch1","f0ch2","f0ch3","f0ch4","f1"];
var fcw_mode=0; // global
// PSM Arrays
var channel_enabled = new Array ();
var enable_quad = new Array ();
// Paths for checkboxes
var channel_path_array   = new Array ();   // psmbox0, psmbox1  channel_enabled checkboxes
var channel_enabled_path = new Array (); // paths for channel enabled
//var path00= channel_path[0] + "/channel enabled"; use channel_enabled_path[i]
//var path01= channel_path[1] + "/channel enabled";
var freq_table_driven=0;
var operating_mode;
var iq_max=512;
var iq_min=-511;

var show_gates; // BOOL
var remember_amplitude = new Array ();

// indices into channels array
var chidx_ref = 4; // "f1"
var chidx_none=5; // "none"
var chan_max=5; // maximum channels (excluding "none")

// PSMIII channel colour arrays
var channel_bgcolours=["lightblue","mediumpurple", "pink","coral","turquoise",];
var channel_colours=["black","white","black","black","black"];
var channel_class= ["f0ch1","f0ch2","f0ch3","f0ch4","f1ref"];
var channel_class_iq=["f0ch1_iq","f0ch2_iq","f0ch3_iq","f0ch4_iq","f1ref_iq"];
var channel_class_b=["f0ch1_b","f0ch2_b","f0ch3_b","f0ch4_b","f1ref_b"];
var channel_class_s=["f0ch1_s","f0ch2_s","f0ch3_s","f0ch4_s","f1ref_s"];
var channel_class_g=["f0ch1_g","f0ch2_g","f0ch3_g","f0ch4_g","f1ref_g"];

var debug_bgcol=["blanchedalmond","yellow", "pink","red"]; // for true_value debug. pink not used.
var eval_counter=0;

// ======================================================================================
//   P S M  TABLE
// ======================================================================================
function build_psm_params()
{ // For PSMIII
    var i;
    var pattern1f = /1f/;  // PPG Mode "1f"
    var pattern2s =/2s/; // PPG Mode 2s
    var text="";
    var text_tmp="";
    var col,cols;
    var paths=new Array(); var values=new Array(); //  arrays for async_odbset
    var index=0; //  index for async_odbset arrays (paths,values)
    var show_max_channels=0;
    var my_class,param_class;

    progressFlag= progress_build_psm;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:lime;font-weight:bold"> build_psm_params: starting</span>'  ; 
    
    //  alert('build_psm_params: starting ')


    param_class="param";
     
    if(document.getElementById("last_selected_showchanA").value =="")
	document.getElementById("last_selected_showchanA").value =0; // "foch1"
    if(document.getElementById("last_selected_showchanB").value =="")
	document.getElementById("last_selected_showchanB").value =chidx_none; // "none"
    
    // Show the channels selected to be displayed (max 2)
    //  alert('add_display_channels: channels to display are A='+document.getElementById("last_selected_showchanA").value+' and  B='+document.getElementById("last_selected_showchanB").value)
    
    show_chanA = document.form1.last_selected_showchanA.value;
    show_chanB = document.form1.last_selected_showchanB.value;
    //  alert('build_psm_parameters: showchanA is '+show_chanA+' and show_chanB is '+show_chanB);
    if(show_chanA < 0 || show_chanB > chidx_none)
	{
	    alert('add_display_channels: illegal value of show_chanA ('+show_chanA+'). Setting show_chanA to '+chidx_none+' or '+channels[chidx_none]);
	    show_chanA = chidx_none;
	    document.form1.last_selected_showchanB.value=show_chanA;
	}
    if(show_chanB < 0 || show_chanB > chidx_none)
	{
	    alert('add_display_channels: illegal value of show_chanA ('+show_chanB+'). Setting show_chanB to '+chidx_none+' or '+channels[chidx_none]);
	    show_chanB = chidx_none;
	    document.form1.last_selected_showchanB.value=show_chanB;
	}
    
    
    num_channels_enabled=0;
    if(!have_psm)
	{
	    if(document.getElementById("PSMparams") !=null)
		{
		    document.getElementById("PSMparams").innerHTML='<tr><td class="allpsmtbg">This mode does not use RF and this is a spacer</td></tr>';
		    document.getElementById("PSMparams").className="psmtbg";
		}
	    else 
		alert('add_display_channels:  document.getElementById("PSMparams") is null')
		    document.getElementById("dbg").innerHTML=""; // debug params
	    
	    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> build_psm_params: and ending "  ;     
	    progressFlag=progress_build_psm_done;
	    return;
	}
    
    // P S M  
    //  alert(' document.getElementById("PSMparams").innerHTML='+ document.getElementById("PSMparams").innerHTML);
    
    //  alert('remember_PSMparams = '+remember_PSMparams)  
    if( document.getElementById("PSMparams").className=="white")
	document.getElementById("PSMparams").className=="psm_title";
    // document.getElementById("PSMparams").innerHTML=remember_PSMparams;
    
    
    
    // All but 3 PPG Modes which use RF  use only one PSM profile (f0ch1) 
    //   ALL  PSMIII channels may be shown
    //   PPG Mode "1f"  may use REF as well 
    //   PPG Mode "2s"   will enable all channels (BNMR only)
    //   PPG Mode "1w"   may enable all channels  (BNMR only)
    
    
    text = '<tbody>';
    text += '<tr align="center">';
    text += '<th class="psm_title" id="psm_title" colspan="4">RF Module  ('+psm_module+') Parameters</th>';
    text +='</tr>';
    
    
    get_psm_data_arrays(); // get the psm data into its arrays - also fills operating_mode
    
    if (pattern_cw.test(operating_mode) && bnmr_expt)
	{
	    fcw_mode=1;
	    param_class="fcw_param";
	}
    
    
    cntr_start=0; // initialize line counter

    // Add enable channels table
    text += '<tr><td colspan=5>';
    text +='<table id="allchan" style="width: 100%;" border="1" cellpadding="1">';
    text +='<tbody>';
    text += '<tr  align="center" >';
    text += '<th class='+param_class+' rowspan=2 style="font-weight:bold;" >Module<br>Operating Mode:</th>';
    text += '<th class='+param_class+' style="font-weight:bold;" >Channels:</th>';
    text += '<th class='+param_class+' colspan=4  style="font-weight:bold;">f0</th>';
    text +='<th class='+param_class+' style="font-weight:bold;" >f1</th>';
    text += '</tr><tr  align="center">';
    text += '<th class='+param_class+'>Enabled: ';
    for(i=0; i<chan_max; i++)
	{
            if(channel_enabled[i])
		my_class="green";
	    else
		my_class="red";
	    
	    if(i==chan_max-1)
		text += '<th class="'+my_class+'">ref';
	    else
		text += '<th class="'+my_class+'">chan'+(i+1)+'</th>';
	}
    text += '</tr><tr  align="center">';
    
    if( (pattern_bnmr.test(operating_mode) && bnmr_expt) || (pattern_bnqr.test(operating_mode) && bnqr_expt))
	{
	    text += '<th class="hdreverse" rowspan=1 style="font-size:120%">'+operating_mode+'</th>'; 
	    text += '<th class='+param_class+' rowspan=2>Amplitude:</th>';
        }
    else if (fcw_mode)
        {  
	    text += '<th class="fcw"  rowspan=1 style="font-size:120%">'+operating_mode+'</th>';
	    text += '<th class="fcw_param" rowspan=2><span style=color:saddlebrown; font-weight:bold;">CW</span> Amplitude:</th>'; 
	}
    else
	{
	    text += '<th class="red" rowspan=1 style="font-size:120%">'+operating_mode+'</th>';
	    text += '<th class='+param_class+' rowspan=2>Amplitude:</th>';
	}
    for(i=0; i<chan_max; i++)
	{
            if(channel_enabled[i] && parseInt(amplitude[i])==0)
		my_class="orange"; // one off
            else if (!channel_enabled[i] && parseInt(amplitude[i])==0)
		my_class="red"; // both off
	    else if (!channel_enabled[i] &&  amplitude[i] > 0)
		{
		    if(rstate==state_stopped)
			my_class="pink";
		    else
			my_class="red"; // should be set to zero at BOR
		}
	    else
		my_class="green";
	    
	    text += '<th class="'+my_class+'">'+amplitude[i]+'</th>';
	}
    text +='</tr></table>';
    text +='</td></tr>';

    text +='<tr><th colspan=6 class="normal"  id="psmerr"></th></tr>';
	
	//	TEMP   if(document.getElementById("PSMparams") !=null)
	//		document.getElementById("PSMparams").innerHTML=text;  // replace <table id="PSMparams" > by text
	//	alert('TEMP got to here')
	

    if(num_channels_enabled == 0)
	{   // no channels enabled
	    // add selection to display channel(s)
            text += add_display_channels();
	    
	    text += '<th  style="vertical-align:top"></th>';
	    text += '<th  style="vertical-align:top"></th>';
	    text += '</tr> '; // PSM row ends
	    text += '<tr style="text-align:centre;"><th></th><th id=maxamp colspan=4></th></tr>';

	} // end of no channels enabled
    else
	{ // at least one channel is enabled
	    text += '<tr style="text-align:centre;"><th></th><th id=maxamp colspan=4></th></tr>';

	    if(!fcw_mode) // Mode 1w is different
		{ // show items common to all channels (idle freq etc)
		    freq_table_driven = check_mode(ppg_mode);  // freq_table-driven for all mode 2 except 20,2d,2g,2s
		    // can still run a modulated pulse (independent of freq table)

		    if(freq_table_driven)
			{   
			    text +='<tr>';
			    // col=2; // maximum columns (only display two channels at a time - was 6) 
		    
			    text +='<td class=param colspan=6>'; 
		    
			    // TABLE for Idle Freq radio buttons 
			    //alert('opened table for idle freq  for channel '+i)
			    text +='<table id="tifreq" style="width: 100%;" border="5" cellpadding="1">';
			    text+= '<tbody>';
			    text +='<tr align="left">';
			    text +='<td  class=psm_info colspan=4>This PPG mode loads a table containing the frequency scan values into the PSM<br>&nbsp</td></tr>';
		    
		    
			    text +='<td id="1star" class=param colspan=2>After last frequency point in scan ...  <span class=asterisk>*</span> </td>';

			    if(rstate==state_stopped)
				{
				    text +='<td class="param">';
				    text +='<input type="radio" name="jiradiogroup" value=0  onClick="set_radio_jump(1)">';
				    text +='<span class="it">Jump to idle frequency</span>';
				    text +='</td><td class=param>';
				    text +='<input type="radio" name="jiradiogroup" value=1  onClick="set_radio_jump(0)">';
				    text +='<span class="it">Stay at final frequency</span>';
				}
			    else
				{ // running
				    text +='<td class="param" colspan=2>';
				    if(jump_to_idle) // bool
					text +='Jump to idle frequency';
				    else
					text +='Stay at final frequency';
				}
		    
			    
			    text +='</td></tr>';
			    
			    if(jump_to_idle) // bool
				{
				    // extra line in table "tifreq"

				    // load_first is global
				    load_first =  get_bool( PSM ["freq sweep load 1st val in idle"]); // ODBGet( load_first_path) // global
				    
				    text +='<tr>';
				    // col = show_max_channels+1;
				    
				    text +='<td class=param colspan=1>Use first frequency point as idle frequency?</td>';
				    text +='<td class=param colspan=2>';
				    
				    if(rstate==state_stopped)
					text +='<input  name="psmbox512"  type="checkbox"  onClick="gbl_code=2; cs_odbset(load_first_path, this.checked?\'1\':\'0\')">';
				    else
					text += load_first;
				    text +='</td>';
				    //  if(show_max_channels > 1) 
				    text +='<td class=param colspan=2></td>';
				    text +='</tr>';
				    text +='</tbody></table>';  // END OF IDLE FREQ TABLE  tifreq
				    text +='</td></tr>';
				    
				    
				    if (pattern_n.test(load_first))
					{
					    text +='<tr><td class="param" colspan=6>';
					    text +='<table style="width: 100%;" border="0" cellpadding="1">'; // Idle/Ref freq table
					    // show idle freq
					    text +='<tr>';
					    text +='<td class=param>Idle frequency (Hz)</td>';
					    text +='<td class=param>';
					    
					    if(rstate==state_stopped)
						{
						    text +='<a href="#" onclick="myODBEdit(idle_freq_path,  idle_freq_hz, update_psm)" >';
						    text +=idle_freq_hz;
						    text +='</a>';
						    text +='' ;
						}
					    else
						text +=idle_freq_hz;
					    text +='</td>';
					    //if(show_max_channels > 1) 
					    //	text +='<td class=param colspan=6></td>';
					    //  text+='</tr>';
					} //  end of not load_first
				    
				} // end of jump-to-idle
			    else
				{ // close table
				    text +='</tbody></table>';  // END OF IDLE FREQ TABLE  tifreq
				    text +='</td></tr>';
				}
			    
			} // end of  freq table-driven
		    else
			{ // NOT freq table-driven
			    col=2; // maximum columns (was 6)
			    text +='<tr>';
			    pattern_scan = /1[abfg]/;  // Type 1 with a frequency scan (NOT 1w, excluded above)
			    if(pattern_scan.test(ppg_mode))
				{
				    //col=4; // fref as well
				    text +='<td class=param colspan='+col+'><i>';
				    text +='<ul>';
				    text +='<li>PPG Parameters for Frequency Scan (Stop/Start/Incr) control the frequency range</li>';
				    if(channel_enabled[chidx_ref]) // f1 reference channel 4
					    text +='<li>If required, enter Reference (Fixed) frequency under Channel '+channel[4]+' below';
				    text +='</li></ul></td></tr>';
				} // end of Type 1 freq scan modes
			    else
				{  // others e.g. 20 with RF -  show Idle/Reference frequency
				    text +='<td class="param" colspan=6>';
				    text +='<table style="width: 100%;" border="0" cellpadding="1">'; // Idle/Ref freq table
				    
				    text +='<td class=param>Frequency (Hz)</td>';
				    text +='<td class=param>';
				    
				    if(rstate==state_stopped)
					{
					    text +='<a href="#" onclick="myODBEdit(idle_freq_path, idle_freq_hz, update_psm )" >';
					    text +=idle_freq_hz;
					    text +='</a>';
					    text +='' ;
					}
				    else
					text +=idle_freq_hz;
				    text +='</td>';
				    
				} // end of no freq scan ppg modes
			    
			} // end of NOT frequency table-driven
		    
	    
		    // Is Reference Channel enabled?
		    if(channel_enabled[chidx_ref]) // f1 reference ch 4
			{  // fref Tuning FReq  
			    text +='<td  class="fref_tun" style="font-weight:normal;">Reference/Tuning Frequency (Hz)</td>';
			    text +='<td class="'+channel_class[chidx_ref]+'">';
			    if(rstate==state_stopped)
				{
				    text +='<a href="#" onclick="myODBEdit( tuning_freq_path, fref_tuning_freq, update_psm )" >';
				    text += fref_tuning_freq;
				    text +='</a>';
				    text +='' ;
				}
			    else // running
				text += fref_tuning_freq;
			    text +='</td>';
			    
			} // end of  (channel_enabled[chidx_ref]) ref enabled
		    else
			text+='<td class=param colspan=2>Reference frequency disabled</td>'; 
		    text+='</tr></table>'; // end of idle/ref freq table
		    
		} // end of items common to all channels
	    // add selection to display channel(s)
	    text += add_display_channels();    
	    // End of common items
	}
    
    
    // Display selected channel(s)
    text +='<tr>';
    
    
    if(show_chanA == chidx_none)
	text+='<th class=param  style="vertical-align:top"></th>';
    else
	for (i=0; i<chan_max; i++)
	    { // chanA first
		if(i==show_chanA)
		    text+=build_one_psm_channel(i,index,paths,values,"A"); 
	    }  // end of for loop
    if(show_chanB == chidx_none)
	text+='<th class=param  style="vertical-align:top"></th>';    
    else
	{
	    for (i=0; i<chan_max; i++)
		{ // chanB second
		    if(i==show_chanB)
			text+=build_one_psm_channel(i,index,paths,values,"B"); 
		}  // end of for loop   
	}  
    text +='</tr><tr>';

    if(fcw_mode)
	{
	    text +='<tr><th colspan=2 class="normal"  id="errmsg">errmsg</th></tr>'; // used by 1w

	    text += '<tr style="text-align:left;"><th colspan=5 style="font-weight:normal; background-color: #c9ffe5;" >';
	    // text +='PPG Parameter x for CW Mode Frequency Scan (Stop/Start/Incr) controls the frequency range';
	    text +='<br><b>Mode 1w:</b><br>The frequency scan is determined by the <b>PPG parameters:</b>';
	    text +='<ul><li>x (Stop/Start/Incr) and</li>';
	    text +='<li>frequency functions f0ch1-4</li></ul>';
	    text +='<br>Frequency functions accept perl <i>eval</i> arithmetic operators. Trig functions take angles in radians.</span>';
	    text +='</th></tr>';
	    text +='<tr><td class=param colspan=2></td>';    

	    text +='<tr><td id=evalfreq class=param>';
            remember_evalfreq='Click <input name="customscript" title="Evaluate frequency scan" value="Evaluate" type="button" onClick="click_eval_button()"> to show frequency scan';
	    if(evalTimerId==0)
	       text +=remember_evalfreq;
            else
	       text+='waiting for eval perlscript...';
	    text +='</td><td></td></tr>';
	}
    
    if(num_channels_enabled > 0)
	{
	    text += '<tr  id="footnotes">';
	    text += '<th colspan="5"></th></tr>'; // my_footnotes
	}
    
     
    text +='</tbody>';
	
    //	 alert('build_psm_params:   Final text is '+text);
    
    if(document.getElementById("PSMparams") !=null)
	document.getElementById("PSMparams").innerHTML=text;  // replace <table id="PSMparams" > by text
    else
	{
	    alert('build_psm_params: error  document.getElementById("PSMparams") is null');
	    return;
	}

    // alert('build_psm_params:   Final text is '+text);

     // alert('build_psm_params: calling initialize_psm_params with  show_chanA='+show_chanA+'show_chanB='+show_chanB )
    initialize_psm_params(show_chanA,show_chanB);

    // Gate Control - show PSM gates checkbox
    if(show_gates)
	initialize_gate_params(show_chanA, show_chanB);
    show_psm_debug_params();
    
    
    if(index > 0) 
	{
	    // alert('build_psm_params: calling async_odbset with index='+index+'; paths='+paths+'; values='+values)
	    async_odbset(paths,values);  // write to odb if needed
	}
    
    //  alert('smb='+ document.getElementById("smb").innerHTML)
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:lime;"> build_psm_params: ending</span>';

    progressFlag=progress_build_psm_done;
    return;   
}

function select_display_chan(item)
{
  var temp;
  // alert('select_display_chan item='+item)
  if(item==1)// channel A
      {
	  //  alert('select_display_chan A:  item='+item+' index = '+document.form1.select_chanA.selectedIndex);
	  // alert('select_display_chan A: ivalue= '+document.form1.select_chanA.options[document.form1.select_chanA.selectedIndex].value);
	  document.form1.last_selected_showchanA.value  = document.form1.select_chanA.selectedIndex;
	  // alert('selected A '+channels[ document.form1.last_selected_showchanA.value]);
      }
  else
      {
	  //  alert('select_display_chan B:  item='+item+' index = '+document.form1.select_chanB.selectedIndex);
	  // alert('select_display_chan B: ivalue= '+document.form1.select_chanB.options[document.form1.select_chanB.selectedIndex].value);
	  document.form1.last_selected_showchanB.value  = document.form1.select_chanB.selectedIndex;
	  // alert('selected B '+channels[ document.form1.last_selected_showchanB.value]);
      }
  gbl_code=update_psm; // 2
  update();
}


function show_psm_debug_params()
{

   var text="";
   var i;
   // alert('show_psm_debug_params starting with gbl_debug='+gbl_debug)
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>show_psm_debug_params</b>: starting "
   // enable_psm_debug_params=get_bool(ODBGet(tpath));
   enable_psm_debug_params = document.form1.tbox0.checked;
   //alert('show_psm_debug_params:  enable_psm_debug_params= '+enable_psm_debug_params);
   if(!enable_psm_debug_params)
   {
       document.getElementById("dbg").innerHTML="";
       if(!gbl_debug) document.getElementById('tdebug').innerHTML=""; // clear debug title
       return;
   }
   document.getElementById('tdebug').innerHTML=remember_tdebug; // show debug title
   text ='<h3>PSM Debug</h3>'
   text+='Idle freq (Hz) = '+idle_freq_hz
   if(show_fref)
       text+='&nbsp&nbsp Ref freq (Hz) = '+ fref_tuning_freq
   if(freq_table_driven)
   {
      text+='<br>Frequency Table-driven;&nbsp&nbsp Freq end sweep jump to idle = '+ jump_to_idle
      text+='&nbsp&nbsp Load first sweep value in idle = '+ load_first
   }
   
   // Debug table
   text+='<table style="width: 100%;" border="1" cellpadding="5" bgcolor=white>'
   text += '<tr align="left">'

   text += '<td class="helplinks">channel</td>'
   text += '<td class="helplinks">index</td>'
   text +='<td class="helplinks">enabled</td><td class="helplinks">enable quad</td>'
       //  text += '<td class="helplinks">simulate single tone</td>' // PSMII
   text += '<td class="helplinks">amplitude</td><td class="helplinks">moduln mode</td><td class="helplinks">bandwidth</td>'
   text += '<td class="helplinks">jump idle iq</td><td class="helplinks">load 1st</td><td class="helplinks">idle i</td>'
   text += '<td class="helplinks">idle q</td><td class="helplinks">load iq file</td><td class="helplinks">set const i</td>'
   text += '<td class="helplinks">const i</td><td class="helplinks">set const q</td><td class="helplinks">const q</td>'
   text += '<td class="helplinks">phase correction</td>'	 
   text += '<td class="helplinks">gate (1=def)</td></tr>'
 								    
       //  var pattern=/freq */;  
       //  var pf
       
   for (i=0; i<chan_max; i++)
   {
       //  pf = channels[i].replace  (pattern,"f"); 
       //  text += '<tr><td>'+pf+'</td>'
    text += '<tr><td>'+channels[i]+'</td>'
    text += '<td>'+i+'</td>'
    text += '<td>'+ channel_enabled[i]+'</td>'
    if(channel_enabled[i])
      {
         text += '<td>'+ enable_quad[i]+'</td>'
        text += '<td>'+ amplitude[i]+'</td>'
        text += '<td>'+ moduln_mode[i]+'</td>'

        text += '<td>'+ bandwidth[i]+'</td>'
        text += '<td>'+ jump_idle_iq[i]+'</td>'
        text += '<td>'+ load_first_iq[i]+'</td>'
        text += '<td>'+ idle_i[i]+'</td>'
        text += '<td>'+ idle_q[i]+'</td>'
        text += '<td>'+ load_iq_file[i]+'</td>'
        text += '<td>'+ set_const_i[i]+'</td>'
        text += '<td>'+ const_i[i]+'</td>'
        text += '<td>'+ set_const_q[i]+'</td>'
        text += '<td>'+ const_q[i]+'</td>'
        text += '<td>'+ phase_correction[i]+'</td>'
      
        text += '<td>'+ gate_control[i]+'</td></tr>'
     }

   }

   text += '</table>'
   document.getElementById("dbg").innerHTML=text;
   show_paths();
  if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending "
}

function show_paths()
{
    var text="";
    text +='<br><h3>PSM Paths</h3>';
    text +='<table>';
    text +='<tr><td>Pathname</td><td>Path</td></tr>';
    text +='<tr><td>psm_path</td><td>'+psm_path+'</td></tr>';
    text +='<tr><td>idle_freq_path</td><td>'+idle_freq_path+'</td></tr>';
    text +='<tr><td>tuning_freq_path</td><td>'+tuning_freq_path+'</td></tr>';

    for (i=0; i<chan_max; i++)
	{
           text+='<tr><td>channel_path['+i+']</td><td>'+channel_path[i]+'</td></tr>';
           text+='<tr><td>channel_enabled_path['+i+']</td><td>'+channel_enabled_path[i]+'</td></tr>';
           text+='<tr><td>amplitude_path['+i+']</td><td>'+amplitude_path[i]+'</td></tr>';
           text+='<tr><td>gate_control_path['+i+']</td><td>'+gate_control_path[i]+'</td></tr>';
           text+='<tr><td>quadrature_path['+i+']</td><td>'+quadrature_path[i]+'</td></tr>';
           text+='<tr><td>mod_path['+i+']</td><td>'+mod_path[i]+'</td></tr>';

	}
       text += '</table>'
   document.getElementById("keysid").innerHTML=text;
}


function get_enabled_channels()
{
    // fill channel_enabled array for all channels
    var i;
    // alert('get_enabled_channels starting with gbl_debug='+gbl_debug)
     if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b>get_enabled_channels</b>: starting'
    channel_enabled[0]= PSM.f0ch1["channel enabled"]; 
    channel_enabled[1]= PSM.f0ch2["channel enabled"]; 
    channel_enabled[2]= PSM.f0ch3["channel enabled"]; 
    channel_enabled[3]= PSM.f0ch4["channel enabled"]; 
    channel_enabled[chidx_ref]= PSM.f1["channel enabled"]; 

    for(i=0; i<chan_max; i++)
    {
        if(channel_enabled[i]==undefined)
           alert('get_channel_enabled:  channel_enabled['+i+'] is undefined' );
        else
	    channel_enabled[i] = get_bool(channel_enabled[i])
     }
  if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b>get_enabled_channel</b> ends with channel_enabled= '+channel_enabled
  return;
}

function assign_channel_paths()
{ // includes  PSM2 functions get_psm_path_arrays() and get_psm_quad_path_arrays
  //  alert('assign_channel_paths for psm3 starting')
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>assign_channel_paths</b>: starting ";

    // common paths
    idle_freq_path = psm_path + "/Idle freq (hz)";
    tuning_freq_path = psm_path +"/fRef Tuning freq (hz)"; // actually f1 tuning frequency

    for(i=0; i<chan_max; i++)
    {
       channel_path[i] = psm_path + '/' +channel[i];
       mod_path[i] = channel_path[i] + "/IQ Modulation";  // mod_path global
       channel_enabled_path[i] = channel_path[i] + "/channel enabled";
       amplitude_path[i] = channel_path[i] + '/scale factor (def 181 max 255)'
       gate_control_path[i] = channel_path[i] + "/gate control";
       quadrature_path[i]= channel_path[i] + '/quadrature modulation mode';

       // channel_path_array[i]= channel_enabled_path[i]; // why two arrays?
       jump_to_idle_iq_path[i]= mod_path[i] + '/jump to idle iq';
        load_iq_file_path[i]= mod_path[i] + '/load i,q pairs file';
        set_const_i_path[i] =  mod_path[i] + '/set constant i value in file';
        set_const_q_path[i] =  mod_path[i] + '/set constant q value in file';
	if(i>0)
	      phase_correction_path[i] = mod_path[i] + '/phase correction (degrees)';
	    


        moduln_mode_path[i] = mod_path[i] +  '/Moduln mode (ln-sech,Hermite)'  // also Wurst for 2w
        bandwidth_path[i] = mod_path[i] + '/requested bandwidth (Hz)'
        idle_i_path[i]= mod_path[i] + '/idle i(-511<=i<=512)'
        idle_q_path[i]= mod_path[i] + '/idle q(-511<=q<=512)'
        const_i_path[i] = mod_path[i] + '/const i(-511<=i<=512)'
        const_q_path[i] = mod_path[i] + '/const q(-511<=q<=512)'
    }


 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (assign_channel_paths)"
}

function get_psm_data_arrays()
{
    //  alert('get_psm_data_arrays: starting')
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_data_arrays for psm3</b>: starting"
   num_channels_enabled = 0;
   var i, CHAN,IQCHAN,CWCHAN;

   idle_freq_hz = parseInt(PSM["idle freq (hz)"]);  // common to all channels
   jump_to_idle = get_bool(PSM["freq end sweep jump to idle"]);
   load_first =  get_bool( PSM ["freq sweep load 1st val in idle"]); 
   fref_tuning_freq = parseInt(PSM["fref tuning freq (hz)"]);
   operating_mode = PSM["operating mode"];

   for (i=0; i<chan_max; i++) 
   {
      if(channel_enabled[i])
	  num_channels_enabled++;      
    
       switch(i)
       {    
  	   case 0:  // f0ch1
 	      CHAN = PSM.f0ch1;
              break;
 	   case 1:  // f0ch2
	      CHAN = PSM.f0ch2;
              break;
 	   case 2:  // f0ch3
	      CHAN = PSM.f0ch3;
              break;
 	   case 3:  // f0ch4
	      CHAN = PSM.f0ch4;
              break;
	   case 4:  // f1
	      CHAN = PSM.f1;
              break;
           default:
	      printf("programming error - illegal PSM channel\n");
              return;
       } // switch

       // Need the amplitude even if channel is disabled   
       amplitude[i] = CHAN["scale factor (def 181 max 255)"];
       if(!channel_enabled[i])
	   continue;  // next channel

       gate_control[i]  = CHAN["gate control"]     


       enable_quad[i] = get_bool( CHAN["quadrature modulation mode"])
	   // if( enable_quad[i]) 
	   //  {      // single tone uses quad mode
          IQCHAN = CHAN["iq modulation"];
          moduln_mode[i]  = IQCHAN["moduln mode (ln-sech,hermite)"]
          bandwidth[i]    =  IQCHAN["requested bandwidth (hz)"] 
          jump_idle_iq[i] = get_bool( IQCHAN["jump to idle iq"])
          load_first_iq[i] = get_bool( IQCHAN["load first val in idle"])
          idle_i[i] =  IQCHAN["idle i(-511<=i<=512)"]
          idle_q[i] =  IQCHAN["idle q(-511<=q<=512)"]
          load_iq_file[i] =get_bool( IQCHAN["load i,q pairs file"])
          set_const_i[i] = get_bool( IQCHAN["set constant i value in file"]) 
          const_i[i] =  IQCHAN["const i(-511<=i<=512)"]
          set_const_q[i] =get_bool(IQCHAN["set constant q value in file"])
          const_q[i] =    IQCHAN["const q(-511<=q<=512)"]
          if(i==0)
	      phase_correction[i]=0;
          else
	      phase_correction[i] =   IQCHAN["phase correction (degrees)"] ;
          
	  // }
   } // for
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_data_arrays</b>: ending"
}

function check_psm_data_arrays()
{
    var i;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>check_psm_data_arrays</b>: starting ";
    
    if( idle_freq_hz == undefined)
	alert(' idle_freq_hz is undefined (  PSM["idle freq (hz)"]) ');
    if(jump_to_idle==undefined)
	alert('jump_to_idle is undefined ( EqData.frontend.hardware.psm["freq end sweep jump to idle"]) ');
    if(load_first==undefined)
	alert('load_first is undefined ( EqData.frontend.hardware.psm["freq sweep load 1st val in idle"]) ');
    if( fref_tuning_freq == undefined)
	alert('fref_tuning_freq is undefined ( EqData.frontend.hardware.psm["fref tuning freq (hz)"])  ');
    
    for(i=0; i<chan_max; i++)
	{
	    if(channel_enabled[i])
		{
		    if( amplitude[i] == undefined)
			alert(' amplitude['+i+'] is undefined ( PSM.'+channel[i]+'["scale factor (def 181 max 255)"]  ) ');
		    if( gate_control[i] == undefined)
			alert(' gate_control['+i+'] is undefined ( PSM.'+channel[i]+'["gate control"]   ) ');
		    if( enable_quad[i] == undefined)
			alert(' enable_quad['+i+'] is undefined ( PSM.'+channel[i]+'["quadrature modulation mode"]  ) ');
		    
		    //  if( enable_quad[i] )   // using single tone mode
		    // {
		    if(moduln_mode[i] == undefined)  // STRING
			alert('moduln_mode['+i+'] is undefined ( PSM.'+channel[i]+'["iq modulation"]["moduln mode (ln-sech,hermite)"]  ) ');
		    
		    if( bandwidth[i] == undefined) // INT
			alert(' bandwidth['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"][""requested bandwidth (hz)"]) ');
		    
		     if( phase_correction[i] == undefined) // float
			alert(' phase_correction['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"][""phase correction (degrees)"]) '); 

		    if( jump_idle_iq[i]== undefined) // BOOL
			alert('jump_idle_iq['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["jump to idle iq"]) ');
		    
		    if(load_first_iq[i] == undefined)  // BOOL
			alert('load_first_iq['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["load first val in idle"]) ');
		    
		    if( idle_i[i] == undefined)
			alert(' idle_i['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["idle i(-511<=i<=512)"]) ');
		    if( idle_q[i] == undefined)
			alert(' idle_q['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["idle q(-511<=q<=512) "]) ');
		    if(  load_iq_file[i]== undefined)  // BOOL
			alert(' load_iq_file['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["load i,q pairs file"]) ');
		    
		    
		    
		    if( set_const_i[i] == undefined)  // BOOL
			alert(' set_const_i['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["set constant i value in file"]) ');
		    else
			set_const_i[i] =get_bool(  set_const_i[i]);
		    
		    if( set_const_q[i] == undefined)  // BOOL
			alert(' set_const_q['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["set constant q value in file"]) ');
		    
		    if( const_i[i] == undefined) // INT
			alert(' const_i['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["const i(-511<=i<=512)"]) ');
		    if( const_q[i] == undefined) // INT
			alert(' const_q['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["const q(-511<=q<=512)"]) ');
		    //  } // enable quad
		}// enable channel
	}// end loop
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending (check_psm_data_arrays)";
}




function build_one_psm_channel(i,index,paths,values,chanAB)
{
    // parameters i=channel index
    //            index=index into paths and values arrays
    //            paths,values are arrays used in call to async_odbset

    var text_tmp="";
    var text="";
    var ibox;
    var pattern2s = /2s/;

    //alert('build_one_psm_channel: starting with i='+i)
    if(gbl_debug)
	document.getElementById('gdebug').innerHTML+='<br><span style="color:green; font-weight:bold"> build_one_psm_channel: starting with i='+i+'</span>'  ; 
    // Heading
    
    // If showing 2 tables, reduce font size 
    show_chanA = document.form1.last_selected_showchanA.value;
    show_chanB = document.form1.last_selected_showchanB.value;
    var ndisplay=0;
    if(show_chanA >= 0 && show_chanA  < chidx_none) ndisplay++;
    if(show_chanB >= 0 && show_chanB  < chidx_none) ndisplay++;
    
    // alert('ndisplay='+ndisplay);
    var fontsiz="";
    if(ndisplay == 2)
        fontsiz="font-size:80%;"; // reduce font size of tables

    text += '<th  style="vertical-align:top">';
    

    text += '<table id="channel'+i+'" style="width: 100%;'+fontsiz+' " border="5" cellpadding="1" bgcolor="white";>' ; // Table for each channel
				// alert('opened psm table for channel '+i)
    text +='<tbody>';
    text +='<tr align="center">';
    text +='<th style="background-color: '+channel_bgcolours[i]+'; color: '+channel_colours[i]+'; " colspan="2">Channel <i>"'+channels[i]+'"</i></th>';
    text +='</tr>';

    text +='<tr> <td width=40% id= "psm_on'+i+'"  class="'+channel_class[i]+'"> Enabled</td>';
    if (rstate == state_stopped) // stopped
    {
	text +='<td class="'+channel_class[i]+'">';
       // use checkboxes psmbox0-4

       ibox=i; // psmbox0-4
       text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="enable_channel('+i+', this.checked?\'1\':\'0\')">';
       text +='</td>';
    }
    else // running
       text +='<td class="'+channel_class[i]+'">'+channel_enabled[i]+'</td>';

  
    if(!channel_enabled[i]) // channel is disabled
	{
	    text +='</tr></tbody></table> '; // channel table ends
	    text +='</th>';
	    return text;
	}

    // This channel is enabled

    convert_all_quad(i); // convert y/n to 1/0 for all Quad boolean parameters for index i
	 

// ================================= ROW A ==================================================
          
  text +='<tr>'    // Amplitude row A
  // check value of Amplitude
  if(amplitude[i] > 255 )
  {
     alert('Maximum value of amplitude is 255');
     paths[index]=amplitude_path[i];  values[index]=255;  //ODBSet(amplitude_path[i], 255)
     index++;
     amplitude[i]=255;
  }
  else if(amplitude[i] < 0 )
  {
     alert('Minimum value of amplitude is 0 ');
     paths[index]=amplitude_path[i]; values[index]=0;  //ODBSet(amplitude_path[i], 0)
     index++;
     amplitude[i]=0;
  }


  if(amplitude[i]==0 )   // enabled with zero amplitude... no RF
      my_class="error";
  else
      my_class=channel_class[i];
 
  
  text +='<td  class="'+my_class+'">Amplitude (0-255) </td>';

  if(rstate==state_stopped)
      {  

  // document.write ('<td class="'+channel_class[i]+'">'
	  text +='<td class="'+my_class+'">';
	  
	  text +='<a href="#" onclick="myODBEdit(amplitude_path['+i+'], amplitude['+i+'], update_psm)"> ';
	  text += amplitude[i];
	  text +='</a>';
	  text +='' ;
      }
  else  // running
      text +='<td class="'+channel_class[i]+'">'+amplitude[i];
  text +='</td></tr>';
      
  if(!fcw_mode)
      { // fcw mode skips the rest of table
      
// ================================= ROW B ==================================================
  
	  text +='<tr>';  // Quad Mode row B

 

	  if(rstate==state_stopped)
	      {
		  text +='<td colspan=2  class="'+channel_class_b[i]+'">';

		  // TABLE for QUAD/SINGLE TONE 
		  //alert('opened  table for quad/single tone channel '+i)
		  text +='<table id="qst'+i+'" style="width: 100%;" border="1" cellpadding="1">';
		  text +='<tbody>';
		  text +='<tr align="left">';
		  
        
		  //Radio quad buttons
		  text +='<td class="'+channel_class_b[i]+'">Enable Quadrature Mode? </td>';
		  text +='<td id="qmb" class="'+channel_class_b[i]+'">';
		  
		  text +='<input type="radio" name="quadradiogroup'+i+'" id="Quad"  onClick="radio_quad(1,'+i+');"></td>';
		  text +='</td>';

		  text +='<td class="'+channel_class_s[i]+'">Enable Single Tone Mode?&nbsp <span class=asterisk>**</span></td>';
		  text +='<td id="smb" class="'+channel_class_s[i]+'">';
		  text +='<input type="radio" name="quadradiogroup'+i+'" id="SingleTone"  onClick="radio_quad(0,'+i+');"></td>';
		  
		  //alert('closing  table for quad/single tone channel '+i)
		  text +='</tr></tbody></table>';  // table id="qst"
		  
		  // ENDED  TABLE for QUAD/SINGLE TONE  
		  
	      }
	  else  // running
	      {  
		  text +='<td class="'+channel_class_b[i]+'">Enable quadrature mode?</td>';
		  text +='<td class="'+channel_class_b[i]+'">'+enable_quad[i];  
	      }
	  
	  
  
	  text +='</td></tr>';
	  
	  // ================================= ROW C ==================================================
	  
	  // can still load an i q table (i.e. run modulated pulse) in single channel mode)
	  
	  if(enable_quad[i])
	      {        
		  
		  text +='<tr>';  // Modulation Mode  row C
		  var lc_moduln_mode = moduln_mode[i].toLowerCase();
		  var pattern_mode_mode;
		  if(pattern_2w.test(ppg_mode))
	    pattern_mod_mode= /ln-sech|hermite|wurst/; // one of these
		  else
		      pattern_mod_mode= /ln-sech|hermite/; // either
		  
		  if (  pattern_mod_mode.test(lc_moduln_mode) )
		      my_class=channel_class[i];
		  else   // shouldn't happen with radiogroup
		      my_class="error";
		  //text +='<td class="'+channel_class[i]+'">Modulation Mode</td>'
		  text +='<td class="'+my_class+'">Modulation Mode</td>';
		  
		  if(rstate==state_stopped)
		      { 
			  //	 alert('creating mmradiogroup'+i)
			  text +='<td class="'+channel_class[i]+'">';
			  text +='<input type="radio" name="mmradiogroup'+i+'" value=0  onClick="cs_odbset(moduln_mode_path['+i+'],ln_sech_string)">';
			  text +='<span class="it">ln-sech </span>';
			  
			  text +='<input type="radio" name="mmradiogroup'+i+'" value=1  onClick="cs_odbset(moduln_mode_path['+i+'],hermite_string)">';
			  text +='<span class="it">Hermite</span>';
			  
			  if (pattern_2w.test(ppg_mode))
			      {
				  text +='<input type="radio" name="mmradiogroup'+i+'" value=2  onClick="cs_odbset(moduln_mode_path['+i+'],wurst_string)">';
				  text +='<span class="it">Wurst</span>';
			      }
			  
			  
		      }
		  else // running
		      text +='<td>'+moduln_mode[i]+'</td>';  
		  
		  text +='</tr>';
		  
		  
		  
		  // ================================= ROW D ==================================================
		  
		  
		  
		  text +='<tr>' ; // Requested Bandwidth  row D
		  
		  text +='<td class="'+channel_class[i]+'">Requested bandwidth (Hz)</td>';
		  
		  if(rstate==state_stopped)
		      { 
			  text +='<td class="'+channel_class[i]+'">';
			  
			  text +='<a href="#" onclick="myODBEdit(bandwidth_path['+i+'], bandwidth['+i+'], update_psm)" >';
			  text +=bandwidth[i];
			  text +='</a>';
			  text +='' ;
			  text +='</td>';
		      }
		  else // running
		      text +='<td class="'+channel_class[i]+'">'+bandwidth[i]+'</td>';  
		  text +='</tr>';
		  
		  
		  // ================================= ROW E ==================================================
		  
		  text +='<tr>';  // Load i,q pairs file  row # E
		  text +='<td class="'+channel_class_b[i]+'">Load i,q pairs file?</td>';
		  
		  
		  // uses checkboxes  psmbox 5,6,7,8,9
		  
		  if(rstate==state_stopped)
		      {  
			  text +='<td class="'+channel_class_b[i]+'">' ;
			  ibox = i + 5; // psmbox 5-9 
			  text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param(load_iq_file_path['+i+'], this.checked?\'1\':\'0\')">';
		      } 
		  else  // running
		      text +='<td class="'+channel_class_b[i]+'">'+unbool(load_iq_file[i]);  
		  
		  text +='</td></tr>';
		  
		  
		  
		  
		  
		  if (load_iq_file[i])
		      {
			  //================================ ROW E.5 ================================
			  
			  if(pattern_2w.test(ppg_mode))
			      { 
				  if(i > 0) // no phase correction for f0ch1
				      {
					  text +='<tr>';  // Phase correction row E.5  
					  text +='<td id="phi'+i+'" class="'+channel_class[i]+'">Phase correction &Phi;&deg;<br><small> (0=no correction)</small>';
					  if(rstate==state_stopped)
					      { 
						  text +='<td class="'+channel_class[i]+'">';
						  
						  text +='<a href="#" onclick="myODBEdit(phase_correction_path['+i+'], phase_correction['+i+'], update_psm)" >';
						  text +=phase_correction[i];
					 text +='</a>';
					 text +='' ;
					 text +='</td>';
					      }
					  else // running
					      text +='<td class="'+channel_class[i]+'">'+phase_correction[i]+'</td>'; 
					  text +='</tr>';
			     
				      } // i>0
			      } // pattern 2w
			  
			  // ================================= ROW F ==================================================
			  text +='<tr>';  // Jump to Idle  row # F.1
			  //  text +='<td colspan=3 class="'+channel_class_b[i]+'">';
			  // text +='<td id="one_star'+i+'"colspan=3  class="'+channel_class_b[i]+'">After last i,q pair ... <span class=asterisk>*</span> <br>';
			  text +='<td id="one_star" colspan=3  class="'+channel_class_b[i]+'">After last i,q pair ... <span class=asterisk>*</span> <br>';
			  
			  // TABLE for Jump to Idle IQ radio buttons
			  //alert('opened  table for Jump to Idle IQ radio buttons  channel '+i)
			  //  text +='<table id="jiiq'+i+'"  style="width: 100%;" border="0" cellpadding="1">';
			  //  text +='<tbody>';
			  // text +='<tr align="left">';
			  // text +='<td id="one_star'+i+'"  class="'+channel_class_b[i]+'">After last i,q pair ... <span class=asterisk>*</span> </td>';
			  
			  //   text +='<tr>';  // Jump to Idle  row # F.2
			  //	     text +='<td class="'+channel_class_b[i]+'">';
			  
			  // uses jiqradiogroupi
			  
			  if(rstate==state_stopped)
			      {   	 
				  text +='<input type="radio" name="jiqradiogroup'+i+'" value=0  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 1)">';
				  text +='<span class="small" >Jump to idle i,q pair</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
				  //   text +='</td><td class="'+channel_class_b[i]+'">';
				  text +='<input type="radio" name="jiqradiogroup'+i+'" value=1  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 0);">';
				  text +='<span class="small">Stay at final i,q pair</span>';
			      }
			  else // running
			      { 
				  if(pattern_y.test(jump_to_idle_iq_path[i]))
				      text +='jump to idle i,q pair';
				  else
				      text +='stay at final i,q pair';
			      }
			  text +='</td></tr>';
			  // text +='</tbody></table>';  // END OF TABLE  jiiq
			  //  text +='</td></tr>';
			  
			  // end of jump to idle i,q
			  
		      } // end of load_iq_file[i]
		  
		  
		  // ================================= ROW G ==================================================
		  
		  
		  if ( (load_iq_file[i] && jump_idle_iq[i] ) || !(load_iq_file[i]))
		      {
			  text +='<tr>';  // Idle i row # G
			  
			  // Check value
			  if(idle_i[i] > iq_max )
			      {  // Set a legal value
				  idle_i[i] =iq_max;
				  paths[index]=idle_i_path[i]; values[index]=iq_max;
				  index++;
			      }
			  else if(idle_i[i] < iq_min )
			      {
				  idle_i[i] = iq_min;
				  paths[index]=idle_i_path[i];
				  values[index]=0;
				  index++;
			      }
			  
			  text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle i ('+iq_min+' <= 0 < '+iq_max+')</td>';
			  
			  if(rstate==state_stopped)
			      {       
				  text +='<td  class="'+channel_class[i]+'">';
				  // alert('idle_i_path[i]='+idle_i_path[i]);
				  text +='<a href="#" onclick="myODBEdit( idle_i_path['+i+'],  idle_i['+i+']), update_psm" >';
				  text +=idle_i[i];
				  text +='</a>';
				  text +='' ;
				  text +='</td>';
			      }
			  else // running
			      text +='<td class="'+channel_class[i]+'">'+idle_i[i]+'</td>';  
			  text +='</tr>';
			  
			  
			  // ================================= ROW H ==================================================
			  
		 
			  text +='<tr>';  // Idle q row # H
			  
			  text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle q  ('+iq_min+' <= 0 < '+iq_max+')</td>';
			  
			  // Check value
			  if(idle_q[i] > iq_max )
			      { // illegal value. Set a legal value
				  idle_q[i] =iq_max;
				  paths[index]=idle_q_path[i];
				  values[index]=iq_max;
				  index++;
			      }
			  
			  else if(idle_q[i] < iq_min )
			      {  // illegal value. Set a legal value
				  idle_q[i]= iq_min;	   
				  paths[index]=idle_q_path[i];
				  values[index]=0;
				  index++;
			      }
			  
			  
			  if(rstate==state_stopped)
			      { 
				  text +='<td class="'+channel_class[i]+'">';
				  text +='<a href="#" onclick="myODBEdit( idle_q_path['+i+'],  idle_q['+i+'], update_psm)" >';
				  text +=idle_q[i];
				  text +='</a>';
				  text +='' ;
				  
				  text +='</td>';
				  
			      }
			  else // running
			      text +='<td class="'+channel_class[i]+'">'+idle_q[i]+'</td>';  
			  text +='</tr>';
			  
		      } // end of  if ( (load_iq_file[i] && jump_idle_iq[i] ) || !(load_iq_file[i]))
	 

		  // ================================= ROW I ==================================================

		  if (load_iq_file[i])
		      {
			  
			  text +='<tr>';  // Set constant i in file  row # I
			  text +='<td class="'+channel_class_iq[i]+'">&nbsp&nbsp Set constant i in file?</td>';
			  
			  // uses checkboxes  psmbox10-14
			  ibox = i + 10 ; 
			  
			  if(rstate==state_stopped)
			      {   
				  text +='<td class="'+channel_class_iq[i]+'">' ;       
				  text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param( set_const_i_path['+i+'], this.checked?\'1\':\'0\')">'	;		             
			      }
			  else // running
			      text +='<td>'+unbool(set_const_i[i]);  
			  text +='</td></tr>';
			  
			  if(set_const_i[i])
			      {
				  text +='<tr>';  // constant i  row # I
				  text +='<td class="'+channel_class[i]+'">Constant i  ('+iq_min+'<= i <='+iq_max+')</td>';
				  
				  // Check value
				  if(const_i[i] > iq_max )
				      {
					  alert('Maximum i value is iq_max');
					  //ODBSet(const_i_path[i], 511)
					  const_i[i] =iq_max;
					  paths[index]=const_i_path[i];
					  values[index]=iq_max;
					  index++;
				      }
				  else if(const_i[i] < iq_min )
				      {
					  alert('Minimum  i value is iq_min');
					  //ODBSet(const_i_path[i], 0)
					  const_i[i] =0;
					  paths[index]=const_i_path[i];
					  values[index]=0;
					  index++;
				      }
				  
				  
				  if(rstate==state_stopped)
				      {       
					  text +='<td class="'+channel_class[i]+'">';
					  
					  text +='<a href="#" onclick="myODBEdit(const_i_path['+i+'],  const_i['+i+'], update_psm )" >';
					  text += const_i[i];
					  text +='</a>';
					  text +='' ;
					  text +='</td>';    
				      }
				  else
				      text +='<td class="'+channel_class[i]+'">'+const_i[i]+'</td>';  
				  text +='</tr>';
			      } // end of set const i
			  
			  // ================================= ROW J ==================================================
			  text +='<tr>';  // Set constant q in file  row # J
			  
			  text +='<td class="'+channel_class_iq[i]+'">&nbsp&nbsp Set constant q in file?</td>';
			  
			  if(rstate==state_stopped)
			      {        
				  text +='<td class="'+channel_class_iq[i]+'">';
				  // uses checkboxes  psmbox15-19
				  ibox = i + 15 ;
				  text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param(set_const_q_path['+i+'], this.checked?\'1\':\'0\')">';
				  
			      }
			  else // running
			      text +='<td class="'+channel_class_iq[i]+'">'+unbool(set_const_q[i]);  
			  text +='</td></tr>';
			  
			  
			  // ================================= ROW K ==================================================
			  
			  if(set_const_q[i])
			      {
				  text +='<tr>';  // constant q  row # K
				  
				  text +='<td class="'+channel_class[i]+'">Constant q ('+iq_min+' <= 0 < '+iq_max+')</td>';
				  
				  // Check value
				  if(const_q[i] > iq_max )
				      {
					  alert('Maximum q value is '+iq_max);
					  // ODBSet(const_q_path[i], 511)
					  const_q[i] =iq_max;
					  paths[index]=const_q_path[i];
					  values[index]=iq_max;
					  index++;
				      }
				  else  if(const_q[i] < iq_min )
				      {
					  alert('Minimum q value is '+iq_min);
					  //ODBSet(const_q_path[i], 0)
					  const_q[i] =iq_min;
					  paths[index]=const_q_path[i];
					  values[index]=iq_min;
					  index++;
				      }
				  if(rstate==state_stopped)
				      {   
					  text +='<td class="'+channel_class[i]+'">';
					  text +='<a href="#" onclick="myODBEdit( const_q_path['+i+'],  const_q['+i+'], update_psm)" >';
					  text += const_q[i];
					  text +='</a>';
					  text +='' ;
				      }
				  else
				      text +='<td class="'+channel_class[i]+'">'+const_q[i];  
				  text +='</td></tr>';
			      } // end set const q
			  
		      } // end load_iq_file
		  
	      } // end iq_modulation mode enabled
	  
	  else
	      { // SINGLE  TONE mode - show  idle i,q pair 
		  // ==============Duplicates ================ ROW G ===========for Single Tone =======================================
		  
		  text +='<tr>';  // message
		  // Check values of idle_i[i] and idle_q[i]
		  var message=new Object();
		  message.myString ='<small>For <b>single tone mode</b> (unmodulated) the Idle i,q pair is set to a pair of constant values. These are usually of the form  (0,'+iq_max+') or ('+iq_min+',0)</small>'
		      if(i > 0)
			  { // no phase adjustment on f0ch1 as this is usually the reference
			      message.myString+='<small> unless adding phase adjustment</small>'
			  }
		  
		  
		  //// text +='<td id="apc" class="'+channel_class[i]+'" colspan=2>'+message.myString+'</td>';
		  var num = check_single_tone_idle_pair(i,index,paths,values,message); // returns number of values added to index
		  index += num; // add
		  
		  text +='<td id="stc" class="'+channel_class[i]+'" colspan=2>'+message.myString+'</td>';
		  
		  text +='<tr>';  // Idle i row # G
		  text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle i </td>'
		      
		      if(rstate==state_stopped)
			  {       
			      text +='<td class="'+channel_class[i]+'">'
				  
				  text +='<a href="#" onclick="myODBEdit( idle_i_path['+i+'],  idle_i['+i+'], update_psm)" >'
				  text +=idle_i[i]
				  text +='</a>';
			      text +='' ;
			      text +='</td>';
			  }
		      else // running
			  text +='<td class="'+channel_class[i]+'">'+idle_i[i]+'</td>';  
		  text +='</tr>'
		      // ==============Duplicates ================ ROW H ===========for Single Tone =======================================
		      
		      text +='<tr>'  // Idle q row # H
		      
		      text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle q </td>'
		      
		      // value checked above
		      if(rstate==state_stopped)
			  { 
			      text +='<td class="'+channel_class[i]+'">'
				  text +='<a href="#" onclick="myODBEdit( idle_q_path['+i+'],  idle_q['+i+'], update_psm)" >'
				  text +=idle_q[i]
				  text +='</a>';
			      text +='' ;
			      
			      text +='</td>';
			      
			  }
		      else // running
			  text +='<td class="'+channel_class[i]+'">'+idle_q[i]+'</td>';  
		  text +='</tr>';
	      } // end of single tone mode

      } // end of not fcw mode

 
  // Gate Control - show PSM gates checkbox
  show_gates = get_bool(show_gate_params); // global
  //alert('build_one_psm_channel: show_gates='+show_gates);
  if(show_gates)
      {      // Display selected channel GATES
	  text+='<th class=param colspan=2  style="vertical-align:top">&nbsp</th></tr>'; // empty line
	  text +='<tr id="psmGates"'+chanAB+'>';
	  text+= show_gate_control(i);
      } 

  text +='</th>';
  text +='</tr></tbody></table>'; // channel table ends
  text +='</th>';
      
  //  alert(' build_one_psm_channel: returning text')
  if(gbl_debug)
      document.getElementById('gdebug').innerHTML+='<br><span style="color:green; font-weight:bold"> build_one_psm_channel: ending';
  return text;
  
}


function  initialize_psm_params(show_chanA,show_chanB)
{
    var i,idx;
    var index; 
    var lc_moduln_mode;
    var amplitude_sum;

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b> initialize_psm_params: starting ";
    //   alert('initialize_psm_params starting')
 
    // alert('initialize_psm_params: show_chanA='+show_chanA+' and show_chanB= '+show_chanB);
    

    amplitude_sum=0;
    check_mode_1w(); // returns if not mode 1w
    
    for(i=0; i<chan_max; i++)
	{
	    switch(i)
		{
		case 0:
		    if(typeof(document.form1.psmbox0) !='undefined')
			document.form1.psmbox0.checked= channel_enabled[i];
		    if(channel_enabled[i])
			    amplitude_sum+=parseInt(amplitude[i]);
		    break;
		case 1:
		    if(typeof(document.form1.psmbox1) !='undefined')
			document.form1.psmbox1.checked= channel_enabled[i];

		    if(channel_enabled[i])
			amplitude_sum+=parseInt(amplitude[i]);
		    break;
		    
		case 2:
		    if(typeof(document.form1.psmbox2) !='undefined')
			document.form1.psmbox2.checked= channel_enabled[i];

		    if(channel_enabled[i])
			amplitude_sum+=parseInt(amplitude[i]);
		    break;
		    
		case 3:
		    if(typeof(document.form1.psmbox3) !='undefined')
			document.form1.psmbox3.checked= channel_enabled[i];

		    if(channel_enabled[i])
			amplitude_sum+=parseInt(amplitude[i]);
		    break;
			
		case 4:
		    if(typeof(document.form1.psmbox4) !='undefined')
			document.form1.psmbox4.checked= channel_enabled[i];
		  
		    if(channel_enabled[i])
			amplitude_sum+=parseInt(amplitude[i]);
		    break;	
		}
	} // for
    
    if(num_channels_enabled < 2)
	document.getElementById("maxamp").innerHTML=""; // clear
    else
	{
	    // alert('amplitude sum ='+amplitude_sum);
	    if(amplitude_sum > 255)
		{
		    if(document.getElementById("maxamp") ==null)
			alert('Did not find element "maxamp"; amplitude sum > 255');
		    else
			{
			    document.getElementById("maxamp").innerHTML= 'Sum of Amplitudes should be < 255 (max)';
			    document.getElementById("maxamp").style.color = "red";
			}
		}
	    else
		document.getElementById("maxamp").innerHTML=""; // clear
	}

     if(num_channels_enabled == 0)
	return;
    
    if(rstate==state_stopped)
	{
	    // only shown if freq_table_driven
	    if(typeof(document.form1.psmbox512) !='undefined')
		document.form1.psmbox512.checked= load_first;  // initialize to the correct value freq sweep load 1st val in idle
	    
	    
	    if(typeof(document.form1.jiradiogroup) !='undefined')
		{
		    
		    document.form1.jiradiogroup[0].checked=  pattern_y.test(jump_to_idle ) ;
		    document.form1.jiradiogroup[1].checked=  pattern_n.test(jump_to_idle);
		}
	  
       
	    for(i=0; i<chan_max; i++)
		{
		    if(!channel_enabled[i])
			continue;

		    if(i != show_chanA  && i != show_chanB)
			continue;
		    
		    if(enable_quad[i])   // quad mode
			index=0;
		    else                 // single tone
			index=1;

		    lc_moduln_mode = moduln_mode[i].toLowerCase();// initialize  modulation mode for each channel

		    switch(i)
			{       
			case 0:      
			    if(typeof(document.form1.quadradiogroup0) !='undefined')
				document.form1.quadradiogroup0[index].checked=1; // initialize  Enable Quad mode ch1
			    // ROW C
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup0) !='undefined')
					{   
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup0[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup0[1].checked= 1;
					    else if (  pattern_wurst.test(lc_moduln_mode))
						{
						    if (pattern_2w.test(ppg_mode))
							document.form1.mmradiogroup0[2].checked= 1;
						    else 
                                                        alert('Wurst modulation mode is not supported in this mode');
						}
					  
					}
				    
				    // ROW E psmbox 5
				    
				    if(typeof(document.form1.psmbox5) !='undefined') // ROW E  
					document.form1.psmbox5.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup0
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup0) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup0[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup0[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    
					    // ROW I   set const I   psmbox10
					    if(typeof(document.form1.psmbox10) !='undefined')
						document.form1.psmbox10.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox15
					    if(typeof(document.form1.psmbox15) !='undefined')
						document.form1.psmbox15.checked= set_const_q[i]; 
					} // end load_iq_file
				} // enable quad
			    break;
			    
			case 1:
			    if(typeof(document.form1.quadradiogroup1) !='undefined')
				document.form1.quadradiogroup1[index].checked=1;  // initialize  Enable Quad mode ch2
			    //   alert('case 1: lc_moduln_mode='+lc_moduln_mode+ 'i='+i+' enable_quad[i]='+enable_quad[i])
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup1) !='undefined')
					{                                                       // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup1[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup1[1].checked= 1;
					    else if (  pattern_wurst.test(lc_moduln_mode))
						{
						    if (pattern_2w.test(ppg_mode))
							document.form1.mmradiogroup1[2].checked= 1;
						    else 
                                                        alert('Wurst modulation mode is not supported in this mode');
						}
					}
				    
				    // ROW E psmbox 6
				    
				    if(typeof(document.form1.psmbox6) !='undefined') // ROW E  
					document.form1.psmbox6.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup1
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup1) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup1[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup1[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox11
					    if(typeof(document.form1.psmbox11) !='undefined')
						document.form1.psmbox11.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox16
					    if(typeof(document.form1.psmbox16) !='undefined')
						document.form1.psmbox16.checked= set_const_q[i]; 
					} // end load_iq_file
				} // enable quad
			    break;
			    
			case 2:
			    if(typeof(document.form1.quadradiogroup2) !='undefined')
				document.form1.quadradiogroup2[index].checked=1;  // initialize  Enable Quad mode ch3
			    //   alert('case 2: lc_moduln_mode='+lc_moduln_mode)
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup2) !='undefined')
					{            
                                           // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup2[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup2[1].checked= 1;
					    else if (  pattern_wurst.test(lc_moduln_mode))
						{
						    if (pattern_2w.test(ppg_mode))
							document.form1.mmradiogroup2[2].checked= 1;
						    else 
                                                        alert('Wurst modulation mode is not supported in this mode');
						}
                                           
					}
				    
				    // ROW E psmbox 7
				    
				    if(typeof(document.form1.psmbox7) !='undefined') // ROW E  
					document.form1.psmbox7.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup2
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup2) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup2[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup2[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox12
					    if(typeof(document.form1.psmbox12) !='undefined')
						document.form1.psmbox12.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox17
					    if(typeof(document.form1.psmbox17) !='undefined')
						document.form1.psmbox17.checked= set_const_q[i]; 
					} // end load_iq_file
				    
				    
				} // enable quad
			    break;
			    
			case 3:
			    if(typeof(document.form1.quadradiogroup3) !='undefined')
				document.form1.quadradiogroup3[index].checked=1;  // initialize  Enable Quad mode ch4
			    //     alert('case 3: lc_moduln_mode='+lc_moduln_mode)
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup3) !='undefined')
					{                                                       // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup3[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup3[1].checked= 1;
                                             
                                            else if (  pattern_wurst.test(lc_moduln_mode))
						{
						    if (pattern_2w.test(ppg_mode))
							document.form1.mmradiogroup3[2].checked= 1;
						    else 
                                                        alert('Wurst modulation mode is not supported in this mode');
						}
					}
				    
				    // ROW E psmbox 8
				    
				    if(typeof(document.form1.psmbox8) !='undefined') // ROW E  
					document.form1.psmbox8.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup3
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup3) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup3[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup3[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox13
					    if(typeof(document.form1.psmbox13) !='undefined')
						document.form1.psmbox13.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox18
					    if(typeof(document.form1.psmbox18) !='undefined')
						document.form1.psmbox18.checked= set_const_q[i]; 
					} // end load_iq_file
				    
				    
				} // enable quad
			    break;
			    
			case 4:
			    if(typeof(document.form1.quadradiogroup4) !='undefined')
				document.form1.quadradiogroup4[index].checked=1;  // initialize  Enable Quad mode f1
			    //   alert('case 4: lc_moduln_mode='+lc_moduln_mode)
			    
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup4) !='undefined')
					{                                                       // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup4[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup4[1].checked= 1;
					    else if (  pattern_wurst.test(lc_moduln_mode))
						{
						    if (pattern_2w.test(ppg_mode))
							document.form1.mmradiogroup4[2].checked= 1;
						    else 
                                                        alert('Wurst modulation mode is not supported in this mode');
						}
					} 
				    
				    // ROW E psmbox 9
				    
				    if(typeof(document.form1.psmbox9) !='undefined') // ROW E  
					document.form1.psmbox9.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup4
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup4) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup4[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup4[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox14
					    if(typeof(document.form1.psmbox14) !='undefined')
						document.form1.psmbox14.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox19
					    if(typeof(document.form1.psmbox19) !='undefined')
						document.form1.psmbox19.checked= set_const_q[i]; 
					} // end load_iq_file
				} // enable quad
			    break;
			} // switch
		    
		    
		} // for loop
	} // state stopped
    

      
    // PSM Footnotes
    if (num_channels_enabled > 0)
	{ // at least one channel is enabled
	    text ='<td class=footnote colspan=5>';
	    
	    if(document.getElementById("one_star") != undefined)   
		{
		    text +='<span class=allwhite><br>hi</span>'; // spacer
		    text +='<span class=asterisk>*</span> if the number of strobes or the gate exceeds the number of values in the table.';
		}
 
	    if(!fcw_mode && ( (show_chanA != chidx_none) || (show_chanB != chidx_none)))
		{
		    text +='<span class=allwhite><br>hi</span>'; // spacer
		    text +='<span class=asterisk>**</span> &nbsp&nbsp';
		    spacer = '<br><span class=allwhite>hi**</span> &nbsp&nbsp'; // spacer
		    text +='Single tone mode is simulated by using Quadrature Mode with Idle i,q pair set to e.g. ('+iq_max+',0)';
		}	
	    text +='</td>'; // footnotes
	    document.getElementById("footnotes").innerHTML=text;
	    
	}
    // if no channels enabled, no footnotes needed; id="footnotes" not written
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="initalize_psm_param: ending "
		     return;
}
function display_gate_control(val)
{
    alert('display_gate_control starting with val='+val);

    if(!val)
	{
	    if(document.getElementById("psmGatesA") !=null)
		document.getElementById("psmGatesA")="";
	    else 
		alert('display_gate_control: psmGatesA not found');
	    if(document.getElementById("psmGatesB") !=null)
		document.getElementById("psmGatesB")="";
	    else 
		alert('display_gate_control: psmGatesB not found');
	}
    else
	reload();

    return;
}

function check_mode_1w()
{
    var i;
    var pattern1w =/1w/;
    var patterne = /empty/;
    var id=new Array();
    var nch; // num enabled channels
    var text;
    // checks enabled channels against functions


    if(!pattern1w.test(ppg_mode))
	return; // nothing to do

    if(gbl_debug)
	document.getElementById('gdebug').innerHTML+='<br><span style="font-weight:bold"> check_mode_1w: starting';
    text="";
    nch=0;
    for(i=0; i<4; i++)
	id[i]='f'+(i+1)+'func';
    // alert('check_mode_1w: id='+id);

    if(document.getElementById("errmsg"))
	document.getElementById("errmsg").innerHTML="hi";
    else
	alert('check_mode_1w: element "errmsg" is missing');

    for(i=0; i<4; i++)
	{
	    if(document.getElementById(id[i]) == undefined)
		alert('check_mode_1w: i='+i+' document.getElementById('+id[i]+') is undefined');
	    else
		{
		    if(channel_enabled[i])
			{ // function should not be "empty" if channel is enabled
			    nch++;
			    if(patterne.test(document.getElementById(id[i]).innerHTML))
				{
				    //alert('pattern empty true for i='+i);
				    document.getElementById(id[i]).style.backgroundColor="red";
				    text+= '<br><span style="color:red;">Disable PSM Ch'+(i+1)+' or supply valid F'+(i+1)+' frequency function</span>';
				}
			}
		    else if(!patterne.test(document.getElementById(id[i]).innerHTML))
			{ // turn bgcolor silver if channel ON and function is not "empty"
			    if(document.getElementById(id[i]) != undefined)
				document.getElementById(id[i]).style.backgroundColor = "silver";

			    text+= '<br><span style="color:grey">Enable PSM Ch'+(i+1)+' or F'+(i+1)+' frequency will be absent   </span>';
			}
		}
	} // for
    if(nch==0)
	text+= '<br>At least one PSM channel must be enabled';

    if(document.getElementById("errmsg") !=null)
	document.getElementById("errmsg").innerHTML=text;
    else
	alert('check_mode_1w: '+text);
   if(gbl_debug)
	document.getElementById('gdebug').innerHTML+='<br><span style="font-weight:bold"> check_mode_1w: ending';
    return;
}
function show_gate_control(i)
{
  var idx;
  var text;

  if(!have_psm)
      return;

  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> show_gate_control: starting with param='+param+'channel i='+i";

  text="";
  // Gate Control for each channel
  //alert(' show_gate_control : starting with  show_gates='+ show_gates)

  // GATES 

  if(show_gates)
      {
	  // If showing 2 tables, reduce font size 
	  show_chanA = document.form1.last_selected_showchanA.value;
	  show_chanB = document.form1.last_selected_showchanB.value;
	  var ndisplay=0;
	  if(show_chanA >= 0 && show_chanA  < chidx_none) ndisplay++;
	  if(show_chanB >= 0 && show_chanB  < chidx_none) ndisplay++;
	  
	  // alert('ndisplay='+ndisplay);
	  var fontsiz="";
	  if(ndisplay == 2)
	      fontsiz="font-size:80%;"; // reduce font size of tables
	  
	  text +='<th colspan=2 style="background-color: '+channel_bgcolours[i]+'; color: '+channel_colours[i]+'; " colspan="2">Gate Control</i></th>';
	  text +='</tr><tr>';
	  
	  // Gates
	  idx =parseInt( gate_control[i]);

	  //  text +='<td colspan=2 class="'+channel_class_g[i]+'">'
text +='<td colspan=2 >'
	      text +='<table style="width: 100%;" border="1" cellpadding="5" >';  // GATE table
	  text +='<tbody>';
	  
	  text +='<tr>';
	  text +='<td colspan=1   class="'+channel_class_g[i]+'" style="color:black"><big>  Front panel gate input</big>';
	  text +='<td colspan=1 class="'+channel_class_g[i]+'" style="color:black"><big>'+idx;
	  if(rstate == ! state_stopped)
	      text +=' ('+gate_states[idx]+')</big></td></tr>';
	  else
	      { // stopped
		  text +='</tr><tr>';			  
		  text +='<td  colspan=1 class="'+channel_class_g[i]+'" style="color:black">';
		  
		  text +='<input name="gateradiogroup'+i+'"  type="radio" value=0 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],0);">';
		  // alert(' show_gate_control: text='+text+ 'gate_control_path[0]='+gate_control_path[0]);
		  // gate_control = 0 (disabled);
		  if(idx==0) // selected state
		      text +='<span class="error">'+gate_states[0]+'</span><br>';
		  else
		      text +=gate_states[0]+'<br>';
		  
		  text +='<input name="gateradiogroup'+i+'"  type="radio" value=1 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],1);">';
		  
		  // gate_control = 1 enabled (default)
		  if(idx==1) // selected state
		     text +='<span style="color: green">'+gate_states[1]+'</span></td>';
		  else
                     text +=gate_states[1]+'<br>';

		  text +='<td  colspan=1 class="'+channel_class_g[i]+'" style="color:black">';
		  text +='<input name="gateradiogroup'+i+'"  type="radio" value=2 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],2);">';
		  
		  // gate_control = 2 (pulse inverted);
		  if(idx==2) 
		      text +='<span class="error">'+gate_states[2]+'</span><br>';
		  else
		      text +=gate_states[2]+'<br>';
		  
		  text +='<input name="gateradiogroup'+i+'"  type="radio" value=3 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],3);">';
		  
		  // gate_control = 3 // ignored (int gate always on)
		  if(idx==3)
		      text +='<span class="error">'+gate_states[3]+'</span>';
		  else
		      text +=gate_states[3];
		  
		  text +='</td></tr>';
	      } // end of stopped
	  text +='</table>';  // end of GATE table
	  text +='</td></tr>';
      } // end of show_gates
  
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> show_gate_control: ending ";
  return text;
}


function initialize_gate_params(show_chanA,show_chanB)
{
    var idx, i;

    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b></b> initialize_gate_params : starting with show_chanA='+show_chanA+' and show_chanB='+show_chanB;

    // alert('initialize_gate_params: starting with show_gates='+show_gates)
    if(rstate==state_stopped && show_gates)
    {
       for(i=0; i<chan_max; i++)
       {

	   if(i==show_chanA || i==show_chanB)
	       { 
		   idx = gate_control[i];
		   idx=parseInt(idx);
        
		   switch(i)
		       {
		       case 0:
			   if(typeof(document.form1.gateradiogroup0) !='undefined')
			       document.form1.gateradiogroup0[idx].checked = 1;
			   break;
		       case 1:
			   if(typeof(document.form1.gateradiogroup1) !='undefined')
			       document.form1.gateradiogroup1[idx].checked = 1;
			   break;
		       case 2:
			   if(typeof(document.form1.gateradiogroup2) !='undefined')
			       document.form1.gateradiogroup2[idx].checked = 1;
			   break;
		       case 3:
			   if(typeof(document.form1.gateradiogroup3) !='undefined')
			       document.form1.gateradiogroup3[idx].checked = 1;
			   break;
		       case 4:
			   if(typeof(document.form1.gateradiogroup4) !='undefined')
			       document.form1.gateradiogroup4[idx].checked = 1;
			   break;
			   
		       } // switch
	       } // if
       } // for 
    } // if rstate...
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="initialize_gate_params ending ";
}	  


function enable_channel(i,val)
{
    // PSMIII  automatically set amplitude to 0 if disabled, to default (181) if enabled

    var paths=new Array();  //  arrays for async_odbset
    var values=new Array();
    // default values of Amplitude
    var amplitude_on;
    var amplitude_off = 0;
    var pattern1w = /1w/;
    var id="";


    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>enable_channel</b>: starting with i="+i;

    if(pattern1w.test(ppg_mode))
       { // Mode 1w (cw) is running
	   id='f'+(i+1)+'func';
	   amplitude_on = 60; // default for cw mode
       }
    else
	amplitude_on = 100; // default for other modes


    i=parseInt(i);
    val=parseInt(val);

    paths[0]=channel_enabled_path[i];
    paths[1]=amplitude_path[i];

    values[0]=val;
  
    if(val)
	{ // turn amplitude ON for enable
	    if(remember_amplitude[i] > 0)
		values[1]=remember_amplitude[i];
	    else    
		values[1]=amplitude_on;
		
	    if(document.getElementById(id) != undefined)  
	       document.getElementById(id).style.backgroundColor ="white"; 
	}
    else
	{
	    values[1]=amplitude_off;
	    remember_amplitude[i]=amplitude[i];
	    if(document.getElementById(id) != undefined)
		document.getElementById(id).style.backgroundColor = "silver";
	}
    // alert(' enable_channel: channel_enabled_path='+channel_enabled_path[i])
 if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br>i='+i+' and channel_enabled_path='+channel_enabled_path[i]
   
    if(i==0) // f0ch1
     document.form1.psmbox0.checked=val; // initialize to correct value
    else if (i==1) //  f0ch2
    document.form1.psmbox1.checked=val; // initialize to correct value
    else if (i==2) //  f0ch3
    document.form1.psmbox2.checked=val; // initialize to correct value
    else if(i==3)//  f0ch4
     document.form1.psmbox3.checked=val; // initialize to correct value
    else if(i==4)//  f1
     document.form1.psmbox4.checked=val; // initialize to correct value

     // ODBSet(channel_enabled_path[i], val);
    gbl_code=2; // for update
    // alert('Calling cs_odbset with i='+i+' and channel_enabled_path[i]='+ channel_enabled_path[i]+' and val= '+val)   
    //    cs_odbset(channel_enabled_path[i], val) 

    // alert('Calling aync_odbset with i='+i+' and paths='+paths+' and values='+values);
    async_odbset(paths,values); 
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending  (enable_channel) with a call to update  (2)"
    update();
 } 


function radio_quad(val,channel_index)
{
    // PSMIII only
    // val = 1 or 0   channel_index=PSM channel index
    var my_path;  
    var paths=new Array();
    var values=new Array();
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>radio_quad</b>: starting ";
    //  alert(' radio_quad: val='+val+' psm_module='+psm_module)
    
    if(val==0)
	document.getElementById('smb').style.backgroundColor="navy"; // signifies clicked
    else
	document.getElementById('qmb').style.backgroundColor="azure"; // signifies clicked
    
    
    if(psm_module=="PSMIII") // PSM Module    psm quad val=1
	{
	    // alert('radio_quad  setting '+quadrature_path[channel_index]+' with val= '+val+'  i = '+i)

	    // Note - PSMIII has no register to set quad/single tone
	    // Assign common paths
	    paths[0]=quadrature_path[channel_index]; 
	    paths[1]=load_iq_file_path[channel_index];

	    // Load defaults for single tone mode
	    if(val==0)
		{ // single tone  modulate with fixed i,q pair or 0,512
		    values[0]=val; 
		    values[1]=0; // don't load i,q file

		    paths[2]=idle_i_path[channel_index]; values[2]=512; //idle i 
		    paths[3]=idle_q_path[channel_index]; values[3]=0; //  idle q  
		    paths[4]=jump_to_idle_iq_path[channel_index];  values[4]=1; // jump_to_idle_iq
		}
	    else
		{  // true quad mode - load an i,q file containing modulation
		    values[0]=val; 
		    values[1]=1; // load i,q file
		    
		    paths[2]=set_const_i_path[channel_index]; values[2]=0; // if using a file, don't set constant i value in file 
		    paths[3]=set_const_q_path[channel_index]; values[3]=0; // if using a file, don't set constant q value in file path
		    paths[4]=jump_to_idle_iq_path[channel_index];  values[4]=0; // jump_to_idle_iq (stay at last i,q, value)
		}

	} // end of PSMIII
    else
	{
	    alert(" radio_quad: unknown psm_module: "+psm_module);
	    return;
	}
    
    //   alert('radio_quad: val='+val+' channel_index= '+channel_index+'  paths= '+paths);
    progressFlag= progress_write_callback;
    mjsonrpc_db_paste(paths,values).then(function(rpc) {
	    var i,len;
	    document.getElementById('readStatus').innerHTML = 'radio_quad:  status= '+rpc.result.status;
	    len=rpc.result.status.length // get status array length
		// alert('radio_quad: length of rpc.result.status='+len)

		for (i=0; i<len; i++)
		    {
			if(rpc.result.status[i] != 1) 
			    alert('radio_quad: status error '+rpc.result.status[i]+' at channel_index '+i);
		    } 
	    document.getElementById('writeStatus').innerHTML='radio_quad: writing paths '+paths+' and values '+values;
	    progressFlag= progress_write_got_callback;
	    gbl_code=2; // for update
	    update(); // rebuild psm table
	}).catch(function(error)
		 { 
		     mjsonrpc_error_alert(error); });

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>radio_quad</b>: ending ";
    return;
}    
   
function check_single_tone_idle_pair(i,index,paths,values,message)
{
    // For single tone mode, i,q pair should be of the form 0, max e.g. 0,512 or -511,0  
    // unless using a phase correction is added.
    // iq_max=512, iq_min=-511
    // i=q=0 is illegal

    var flag;
    var num;
    var idx=index;
   
    flag=mum=0;

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>check_single_tone_idle_pair</b>: starting with index i="+i;
    if(idle_i[i] == 0 && idle_q[i] == 0)
	message.myString='<span class="bolderror" style="font-size:110%">If i and q are both 0, there will be no RF</span>';
	
    // take checking out as we may add a phase correction

    //   else if(idle_i[i] != 0 && idle_q[i] != 0)
    //	message.myString='<span class="boldwarn" style="font-size:100%">One of i or q should be 0 for single tone mode</span>';

    else
	{
	    if( idle_q[i] > iq_max)
		{
		    flag=1;
		    idle_q[i]=iq_max;
		}
	    else if  (idle_q[i] < iq_min)
		{
		    flag=1;
		    idle_q[i]=iq_min;
		}
	    if(flag)
		{ // write new q value
		    paths[idx]=idle_q_path[i]; values[idx]=idle_q[i];
		    idx++;
		    num++;
		    flag=0;
		}
	    

	    
	    if(idle_i[i] > iq_max)
		{
		    idle_i[i]=iq_max;
		    flag=1;
		}
            else if (idle_i[i] < iq_min)
		{
		    flag=1;
		    idle_i[i]=iq_min;
		}
            if(flag)
		{ // write new i value
		    paths[idx]=idle_i_path[i]; values[idx]=idle_i[i];
		    idx++;
                    num++;
		}
	    
	} // end else
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>check_single_tone_idle_pair:</b> returning with num="+num;
    return num;
}

function calculate_2s()
{
    //  alert('calculate_2s starting');
    var e2s_rf_cycle_time, e2s_nprebeam_cycles, e2s_nbeam_cycles, e2s_necho_cycles;
    var max_pi_cn =  3 * e2s_nfid_cycles  -4;

    if(e2s_nfid_cycles < 2 ) 
       alert('Please set number of FID cycles to be at least 2');
    if(e2s_pi_pulse_cycle_num < 3 ) 
       alert('Please set pi cycle number to at least 3');
    else if ( e2s_pi_pulse_cycle_num > max_pi_cn)
       alert('Maximum pi cycle number must be <= '+max_pi_cn+' for '+e2s_nfid_cycles+' FID cycles');
    else
    {
      if(!e2s_select_mode_se2)
         e2s_rf_cycle_time_ms = 2 * e2s_rf_half_pi_on_ms + e2s_xdwell_ms + e2s_zdwell_ms;  // SE1 these are floats
      else
         e2s_rf_cycle_time_ms = 8 * e2s_rf_half_pi_on_ms + 2 * (e2s_xdwell_ms + e2s_zdwell_ms);  // SE2 these are floats
      e2s_nprebeam_cycles =  parseInt(  0.5 + (prebeam_on_ms /  e2s_rf_cycle_time_ms)); // round up
      e2s_nbeam_cycles =     parseInt(  0.5 + (e2c_beam_on_ms /  e2s_rf_cycle_time_ms)); // round up
      e2s_necho_cycles = parseInt( 2 * e2s_nfid_cycles );
    

      e2s_tot_acq_ms  = 3 * e2s_nfid_cycles *  e2s_rf_cycle_time_ms;
  
      alert('RF cycle time (ms)='+ roundup( e2s_rf_cycle_time_ms,4)+' Num Prebeam Cycles= '+ e2s_nprebeam_cycles+ ' Num Beam cycles= '+ e2s_nbeam_cycles +' Num echo cycles= '+  e2s_necho_cycles + ' Total acq time (ms)='+ roundup(  e2s_tot_acq_ms,3));
     }
}


function add_display_channels()
{
    var i,text="";
   
    text='<tr><td class="param">Display Channel A:';
    text+='<select name="select_chanA" onChange="select_display_chan(1)" class="tune">';
    for(i=0; i<6; i++)  // extra value is "none"
	{
	    if (i == document.getElementById("last_selected_showchanA").value) // type=hidden
		sel="selected";
	    else
		sel="";
	    text+='<option value="0" '+sel+' >'+channels[i]+' </option>' ;
	}
    text+='</select></td>';
    
    text+='<td class="param">Display Channel B:';
    text+='<select name="select_chanB" onChange="select_display_chan(2)" class="tune">';
    for(i=0; i<6; i++)  // extra value is "none"
	{
	    if (i == document.getElementById("last_selected_showchanB").value) // type=hidden
		sel="selected";
	    else
		sel="";
	    text+='<option value="0" '+sel+' >'+channels[i]+' </option>' ;
	}
    text+='</select></td>';
    text +='</td></tr>';

    return text;
}

function check_consistency()
{
   var pattern_A = /2c/;
    var pattern_B = /1g|20/;  //  RFon_duration_dt 
    var pattern_C = /1[ab]|2[abde]/; // rf_on_time_ms 
    var pattern1_scan = /1[abfg]/;
    var pattern2_scan = /2[abce]/;
    var pattern2s = /2s/;
    var show_chanA,show_chanB;
    
    
    if(rstate != state_stopped)
	return; // do nothing 
    
    if(!have_psm)
	return; // no rf for this mode

    show_chanA = document.form1.last_selected_showchanA.value;
    show_chanB = document.form1.last_selected_showchanB.value;
    
    if(document.getElementById("psmerr") ==null) 
	{
	    alert(' check_consistency:  document.getElementById("psmerr") is not defined ' );
            return;
	}
    
    // clear error
    document.getElementById("psmerr").className="normal";
    document.getElementById("psmerr").innerHTML=""; // clear errors
    
    if(!channel_enabled[0])
	{ 
	    if( pattern1_scan.test(ppg_mode) || pattern2_scan.test(ppg_mode) || hole_burning_flag )  // a channel ought to be enabled
		{  // one of the PSM channels should be enabled for these modes - usually f0ch1
		    if(num_channels_enabled==0)
			{
			    document.getElementById("psmerr").className="bolderror";
			    document.getElementById("psmerr").innerHTML= "RF must be ENABLED when RFon duration > 0";
			}
		    else
			{
			    document.getElementById("psmerr").className="boldwarn";
			    document.getElementById("psmerr").innerHTML= "PSM f0 Channel 1 is usually used for this mode";
			}
		}
	    if(pattern_B.test(ppg_mode))  // 1g/20
		{
		    if (RFon_duration_dt <= 0) 
			{  // remove error in case it is set
			    if(document.getElementById("RFonDT").className == "error")
				document.getElementById("RFonDT").className="param";
			}
		}
	}
       else
	   { //  RF  is enabled (at least 1 channel) 
	       if (pattern_B.test(ppg_mode))
		   {  // 1g/20
		       // alert('check_consistency:  RFon_duration_dt '+RFon_duration_dt)
		       if (RFon_duration_dt <= 0)
			   {
			       if(document.getElementById("RFonDT") != undefined)
				   document.getElementById("RFonDT").className="error";
			       else 
				   alert('check_consistency: document.getElementById("RFonDT") = '+document.getElementById("RFonDT"));
			       
			       document.getElementById("psmerr").className="bolderror";
			       document.getElementById("psmerr").innerHTML = "RF should be disabled when RFon duration = 0";
			       
			       // if(typeof(document.getElementById("psm_on0")) != null)
			       // document.getElementById("psm_on0").className="error";
			   }
		       else
			   {  // no error
			       if(document.getElementById("RFonDT").className == "error")
				   document.getElementById("RFonDT").className="param";
			       
                               for(i=0; i<chan_max; i++)
				   { // check amplitude is > 0 for all enabled channels
				       if(channel_enabled[i] && amplitude[i]==0 )   // enabled with zero amplitude... no RF
					   {
					       document.getElementById("psmerr").className="bolderror";
					       document.getElementById("psmerr").innerHTML = "RF Amplitude should be set non-zero for enabled channel "+channels[i];
					   }
				   }
			       //      if(typeof(document.getElementById("psm_on0")) != null)
			       //  {	// put this in to check for error; will be null if channel 0 isn't displayed
			       //  if(document.getElementById("psm_on0").className=="error")
			       //   document.getElementById("psm_on0").className= profile_class[0]
			       //  }
			       
			   }
		       
		       
		   }
	       if (pattern_A.test(ppg_mode)  )
		   {   // e2c
		       if( e2c_rf_on_ms <= 0)
			   {
			       if(document.getElementById("e2cRFon") != undefined)
				   document.getElementById("e2cRFon").className="error";
			       //  document.getElementById("psm_on0").className="error";
			   }
		   }
	       if (pattern_C.test(ppg_mode)  )
		   { 
		       if(  rf_on_time_ms <= 0)
			   {
			       if(document.getElementById("RFonms") != undefined)
				   document.getElementById("RFonms").className="error";
			       //document.getElementById("psm_on0").className="error";
			   }
		   }
	       
	   }
}


function click_eval_button()
{

    var pattern0=/empty/i;
    var quote="\"";
    var paths=[
	       "/Customscript/evaluate/f1",
	       "/Customscript/evaluate/f2",
	       "/Customscript/evaluate/f3",
	       "/Customscript/evaluate/f4",
	       "/Custom/hidden/cw eval status",
	       ];			
    var values= new Array();
    
    var func1,func2,func3,func4;
    var myfunc,text;

    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:bold">click_eval_button: starting</span>';
    //alert('click_eval_button: starting ');

    eval_counter=0;  // clear timeout counter
    
    myfunc =  CustomData.hidden.current_mode["f1 frequency function"];
    values[0] = add_quotes(myfunc);
    myfunc =  CustomData.hidden.current_mode["f2 frequency function"];
    values[1] = add_quotes(myfunc);
    myfunc =  CustomData.hidden.current_mode["f3 frequency function"];
    values[2] = add_quotes(myfunc);
    myfunc =  CustomData.hidden.current_mode["f4 frequency function"];
    values[3] = add_quotes(myfunc);
    values[4] = 3;

    gbl_callback_flag=1;
    if(gbl_debug)
	{
	    text='<br> paths='+paths+' values='+values;
	    text+='<br>calling async_odbset with gbl_callback_flag='+gbl_callback_flag;
	    document.getElementById('gdebug').innerHTML+=text;
	}
    async_odbset(paths,values);
    
    document.getElementById('evalfreq').innerHTML="evaluating...";
    document.getElementById('evalfreq').style.backgroundColor="orange";
    	
    // alert("click_eval_button: returning");
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:normal"> click_eval_button: ending</span>';
    return;
}

function add_quotes(mystring)
{
    var quote="\"";
    var str="";

    mystring = mystring.trim();
    // alert('mystring='+mystring);
    if(!mystring)
	str="\"empty\"";  // "empty" in quotes	
    else
	{   // remove any existing quotes at begin and end of string
	    mystring=mystring.replace(/^['"]/,"");
           mystring=mystring.replace(/["']$/,"");
	    str=quote;
	    str=str.concat(mystring);
	    str=str.concat(quote);
	}
    //	alert('returning string:'+str);
    return str; // return quoted string
}


function eval_callback()
{
    // Called by async_odbset as gbl_callback_flag=1
    if(gbl_debug)
	document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:bold">eval_callback: starting</span> and calling custom script \"Evaluate\"';
    //alert('eval_callback: starting custom script');
    ODBcs("Evaluate"); // run custom_script

    if (evalPeriod > 0)  // will check for perlscript done
	{ 
	    if(gbl_debug)
		document.getElementById('gdebug').innerHTML+='<br> setting timeout to call check_eval_done';
	    evalTimerId = setTimeout('check_eval_done()', evalPeriod);
	}
    
   if(gbl_debug)
       document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:normal">eval_callback: ending</span>';
}

function check_eval_done()
{
    // Read /custom/hidden subdir for polling
   if(gbl_debug)
       document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:bold">check_eval_done: starting</span>';

    //    alert('check_eval_done: starting;
    var path=new Array();
    path[0]=hidden_path;

   if(gbl_debug)
       document.getElementById('gdebug').innerHTML+='<br>calling ODBMCopy with path '+path+' and callback to function eval_callback_poll';	  
    ODBMCopy(path, eval_callback_poll, "json");
  if(gbl_debug)
       document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:normal">check_eval_done: ending</span>';
}

function eval_callback_poll(data)
{
    var obj = JSON.parse(data);
    var Hidden=obj[0];
    var text;
    var code = Hidden["cw eval status"];

   if(gbl_debug)
       document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:bold">eval_callback_pol</span>: starting with cw_eval_status code='+code+' and eval_counter='+eval_counter;

    // alert('eval_callback_poll: cw eval status='+code);
    eval_counter++; // counter for timeout

    if(eval_counter > 5 && code != 3)
	{
	   alert('eval_callback_poll: timeout with eval_counter='+eval_counter+'. Script may have failed due to error'); 
	   return;
	}
    document.getElementById('evalfreq').innerHTML="please wait ...";
    switch (code)
	{
	case 0:
	    alert('eval_callback: error - perlscript has not been started');
	    text='error - perlscript has not been started'; 
	    break;
	case 1:
	    //alert('eval_callback: perlscript has been started');
	    text='perlscript has been started';

	    if (evalPeriod > 0)  // check again perlscript done
		evalTimerId = setTimeout('check_eval_done()', evalPeriod);
	    break;
	case 2:
	    alert('eval_callback: perlscript is running');
	    // text='perlscript is running';

	    if (evalPeriod > 0)  // check again perlscript done
		evalTimerId = setTimeout('check_eval_done()', evalPeriod);
	    break;
	case 3: 
	    evalTimerId=0; // clear
	    //alert('eval_callback: perlscript is finished');
            text='perlscript is finished';
	    window.location.href='./eval_freq_1w&';  // Switch to results window
	    break;
	}

    if(gbl_debug)
	{
	    document.getElementById('gdebug').innerHTML+='<br>'+text;
	    document.getElementById('gdebug').innerHTML+='<br><span style="color:fuchsia; font-weight:normal">eval_callback_pol</span>: ending with eval_counter='+eval_counter;
	}
    return;
}