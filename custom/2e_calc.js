// input parameters from user interface & odb  
var mode_path = "/Equipment/FIFO_acq/mode parameters/mode 2e/";

var path_input_rf_on_time =      mode_path+"RF On Time (ms)";
var path_input_num_dt_per_freq = mode_path+"Num dwelltimes per freq";
var path_input_num_postrfbeamon= mode_path+"Num post RFbeamOn dwelltimes";
var path_input_beam_off=         mode_path+"Beam Off time (ms)";
var path_input_num_rfon_delays=  mode_path+"Num RF on Delays (dwelltimes)";


var path_input_freq_start =      mode_path+"Frequency Scan Start (Hz)";
var path_input_freq_stop =       mode_path+"Frequency Scan Stop (Hz)";
var path_input_freq_incr =        mode_path+"Frequency Scan Increment (Hz)";



 
     var input_rf_on_time__ms_ = new Number();  
     var input_e2e_num_dwelltimes_per_freq      = new Number();                                               
     var input_e2e_num_postrfbeamon = new Number();                                                                       
     var input_beam_off_time__ms_  = new Number();  
     var input_num_rfon_delays  = new Number();       

     var input_e2e_num_rf_dwelltimes_per_freq  = new Number(1);       // used for ntuple_depth_bins      

     var input_freq_start_hz = new Number();  
     var input_freq_incr_hz  = new Number();  
     var input_freq_stop_hz  = new Number();  ;


     var output_path = "/Equipment/FIFO_acq/frontend/output/";

                                                                                                            
     var output_e2e_num_beam_off_dwelltimes     = new Number();  
     var output_num_dwell_times = new Number();  
     var output_dwell_time__ms_ = new Number();  
     var output_beam_on_time__ms_  = new Number();  
     var output_num_frequency_steps = new Number();  
     var output_rf_on_time__ms ; // assigned to input_rf_on_time__ms_ = new Number();  
     var  output_rf_off_time__ms_ = new Number();  

var update_time_s = 15; // page update time
var timerID;
var init=0;
var state_stopped=1; // run state
var rfconfig_flag=0;

function initialize()
{
  init=1;
  if(bnmr)
     text=" PPG Mode 2e Calculator for  &beta;-nmr "
  else
      text=" PPG Mode 2e Calculator for  &beta;-nqr "   
  document.getElementById("hdr").innerHTML=text;


 //   alert('initialize: '+ document.form1.nRFondtpf.value+ document.form1.nRFond.value+ document.form1.ndtf.value+ document.form1.nbondt.value)

  // Note  input_e2e_num_rf_dwelltimes_per_freq  =1;
  
   //  document.form1.nRFondtpf.value=  1; // num_rf_dwelltimes_per_freq  
   
  
}

function load()
{
    //alert('load starting')

  if(!init)
  {
      initialize();
      set_load_timer(5); // update in 5s in case waiting for rf_config
  }
  else
      set_load_timer(update_time_s);
  
  get_input_params(); // parameters may have been changed by another 
  get_output_params();
   
 build_inputs();
 
 var text;
 if (rstate == state_stopped) // stopped
     {
        text='Press '
        text+=' <input name="customscript" value="rf_config" type="submit" style="color:firebrick" onClick=test() >'
        text+=' button to <span style="color:red;  font-size:110%;">recalculate</span>, or to ensure calculated values are up to date'
      }	  
 else     // rf_config will have run; no button
    text='Run in progress; calculated values are current'

 document.getElementById("calc").innerHTML=text;
 
 
 write_last_message();
     
 
    build_calculated(); 
    write_date();
 return;
}

function get_input_params()
{
  // alert('get_input_params')
     input_rf_on_time__ms_  =ODBGet(path_input_rf_on_time)  ;  //    assigned to output.dwell_time and output.rf_on_time__ms
                  //    alert(' input_rf_on_time__ms_='+ input_rf_on_time__ms_)
     input_freq_start_hz=ODBGet(path_input_freq_start);
     input_freq_stop_hz=  ODBGet(path_input_freq_stop);

   
     input_freq_incr_hz= ODBGet(path_input_freq_incr);
     
     input_e2e_num_dwelltimes_per_freq   =ODBGet( path_input_num_dt_per_freq);
     input_e2e_num_postrfbeamon        =ODBGet(path_input_num_postrfbeamon);
     input_beam_off_time__ms_ = ODBGet(  path_input_beam_off);
     input_num_rfon_delays = ODBGet(    path_input_num_rfon_delays);

}

function test()
{
    // alert('Please reload page to see new values after rf_config is finished ') 
}


function get_output_params()
{
//   output parameters  (calculated by rf_config )
   var path=output_path+"e2e num beam off dwelltimes";
     output_e2e_num_beam_off_dwelltimes = ODBGet(path);

     path = output_path+ "num dwell times"
     output_num_dwell_times  = ODBGet(path);

     path = output_path+ "dwell time (ms)"
     output_dwell_time__ms_ =  ODBGet(path);

      path = output_path+ "beam on time (ms)"
      output_beam_on_time__ms_ =  ODBGet(path);

      path = output_path+ "num frequency steps"
      output_num_frequency_steps =  ODBGet(path);

      path = output_path+ "rf on time (ms)"
       output_rf_on_time__ms  =  ODBGet(path);


     path = output_path+ "rf off time (ms)"
      output_rf_off_time__ms_   =  ODBGet(path);

}


function set_load_timer(update_time_s)
{
    // alert('set_load_timer: setting timerID .. update_time(sec)= '+update_time_s)
 
  if(timerID)
     clearTimeout(timerID);
  timerID =  setTimeout ('load()', (update_time_s * 1000));

  return;
}

function build_inputs()
{
    var text;     
     

     text="<b>RF on time </b>"
     if(rstate == state_stopped) // only change when run stopped
        text+= '<a href="#" onclick="ODBEdit(path_input_rf_on_time)" >';
     
     text+= input_rf_on_time__ms_;
     if(rstate == state_stopped)
        text+= '</a>';
     text+= '' ;
     text+= ' (ms) '
     text+='<b> = 1 dwelltime</b>'
     text+='<br><small>Since RF is on for ONE dwelltime only (fixed value), RFon time determines the <b>PPG dwelltime<b></small>';
   //   alert('text='+text)
     document.getElementById("inputs").innerHTML=text;


     
     text='';
     text+='<td colspan=1  style="font-weight:normal; background-color: ivory" ></td>'
     text+='<td colspan=1 style="background-color:moccasin; " >'
     text+='<b>Frequency start (Hz)</b> '
     if(rstate == state_stopped) // stopped 
        text+= '<a href="#" onclick="ODBEdit(path_input_freq_start)" >';
     text+= input_freq_start_hz
     if(rstate == state_stopped) // stopped 
        text+= '</a>';
     text+= '</td>' ;
 
     text+='<td colspan=1 style="background-color:moccasin;" >'
     text+="<b>Frequency Increment (Hz)</b> "
     if(rstate == state_stopped) // stopped 
        text+= '<a href="#" onclick="ODBEdit(path_input_freq_incr)" >';
     text+= input_freq_incr_hz
      if(rstate == state_stopped) // stopped 
        text+= '</a>';
     text+= '' ;
     text+= '</td>' ;
     
     text+='<td colspan=1 style="background-color:moccasin;" >'
     text+="<b>Frequency stop (Hz)</b> "
     if(rstate == state_stopped) // stopped 
         text+= '<a href="#" onclick="ODBEdit(path_input_freq_stop)" >';
     text+= input_freq_stop_hz
      if(rstate == state_stopped) // stopped 
        text+= '</a>';
     text+= '' ;
     text+= '</td>' ;
     text+='<td colspan=2  style="font-weight:normal; background-color: ivory" ></td>'
     
     //alert('document.form1.nRFond.value='+document.form1.nRFond.value)
   //   alert('text='+text)
     document.getElementById("myfreq").innerHTML=text;


     text="<b>Post RF Beam off time: </b> "
      if(rstate == state_stopped) // stopped 
         text+= '<a href="#" onclick="ODBEdit(  path_input_beam_off)" >';
     text+=  input_beam_off_time__ms_
      if(rstate == state_stopped) // stopped 
        text+= '</a> ';
     text+= 'ms' ;
     text+= '<br><small>enter time(ms) to keep counting with the beam off (min 2 dwelltimes)</small>'
     document.getElementById("boffms").innerHTML=text;

     // Num RFOn Delays
       text="<b>Num RFOn Delays: </b>"
      if(rstate == state_stopped) // stopped 
         text+= '<a href="#" onclick="ODBEdit(  path_input_num_rfon_delays)" >';
     text+=  input_num_rfon_delays
      if(rstate == state_stopped) // stopped 
         text+= '</a> ';
     text+= '<br>     <small>select delay in <b>dwelltimes</b> before the RF scan starts</small>'     
     document.getElementById("nrfondly").innerHTML=text;


     // Num dwelltimes per freq
       text="<b>Num dwelltimes per frequency: </b> "
      if(rstate == state_stopped) // stopped 
        text+= '<a href="#" onclick="ODBEdit(path_input_num_dt_per_freq )" >';
     text+=  input_e2e_num_dwelltimes_per_freq
      if(rstate == state_stopped) // stopped 
         text+= '</a> ';
     text+= '<br><small>select number of dwelltimes per frequency step, including the 1 dwelltime that the RF is on</small>'
     document.getElementById("ndtf").innerHTML=text; 


  // Num post RFbeamOn dwelltimes
       text="<b>Num post RF BeamOn :</b> "
      if(rstate == state_stopped) // stopped 
         text+= '<a href="#" onclick="ODBEdit(path_input_num_postrfbeamon)" >';
     text+=   input_e2e_num_postrfbeamon
      if(rstate == state_stopped) // stopped 
         text+= '</a> ';
     text+= '<br><small>select number of <b>dwelltimes</b> to keep counting with the beam on, after the RF scan has finished (min 2)</small>'
     document.getElementById("nbondt").innerHTML=text; 

     
}

function build_calculated()
{
  
  
  document.getElementById("cal1").innerHTML= 'Number of frequency steps = '+output_num_frequency_steps;
  document.getElementById("cal2").innerHTML='Beam on time ='+ output_beam_on_time__ms_ +' (ms) '
  document.getElementById("cal3").innerHTML='Number of Beam Off dwelltimes ='+ output_e2e_num_beam_off_dwelltimes
  document.getElementById("cal4").innerHTML='Total number of dwelltimes (or PPG bins) = '+ output_num_dwell_times
  document.getElementById("cal5").innerHTML='Dwell time = '+ output_dwell_time__ms_ +' (ms)';
  document.getElementById("cal6").innerHTML='RF off time = '+ output_rf_off_time__ms_ +' (ms)';
 
 
  
}


function write_last_message()
{
    document.getElementById('lastmsg').innerHTML = 'Last Message: '+ODBGetMsg(1);
}

function reload()
{
   // also called from poll() in params_common.js
   window.location.reload();
}


function get_bool(jval)
{
  var ival;
  var pattern_y = /y/;
  if(pattern_y.test(jval))
    ival=1;
  else
    ival=0;
//  alert('get_bool:  jval= '+jval+ ' returning ival= '+ival)
  return ival;
}


function write_date()
{
  var text;
  var mydate=new Date()
  var time_now = mydate.getTime() /1000 ; // present time in seconds
  var year=mydate.getYear()
  if (year < 1000)
   year+=1900
  var day=mydate.getDay()
  var month=mydate.getMonth()
  var daym=mydate.getDate()
  var hour=mydate.getHours()
  var min=mydate.getMinutes()
  var sec=mydate.getSeconds()
  if (daym<10)
   daym="0"+daym
  if (hour<10)
   hour="0"+hour
  if (min<10)
   min="0"+min
  if (sec<10)
   sec="0"+sec
  var dayarray=new Array("Sun","Mon","Tue","Wed","Thur","Fri","Sat")
  var montharray=new Array("Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec")
  text = 'Page last loaded at '
  text+= dayarray[day]+ " "+ montharray[month]+ " "+ daym+" "+  hour+ ":"+ min+ ":"+ sec+" ", year
  document.getElementById("time").innerHTML = text;
}

