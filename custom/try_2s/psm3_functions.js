// psm3_functions.js
//
// the following globals are specific to psm3
 var channel_path = new Array ();
 var num_channels_enabled=0;// default
var channels=["f0 Chan1","f0 Chan2","f0 Chan3","f0 Chan4","f1 REF","none"];
var channel=["f0ch1","f0ch2","f0ch3","f0ch4","f1"];
// PSM Arrays
var channel_enabled = new Array ();
var enable_quad = new Array ();
// Paths for checkboxes
var channel_path_array   = new Array ();   // psmbox0, psmbox1  channel_enabled checkboxes
var channel_enabled_path = new Array (); // paths for channel enabled
//var path00= channel_path[0] + "/channel enabled"; use channel_enabled_path[i]
//var path01= channel_path[1] + "/channel enabled";
var freq_table_driven=0;
var operating_mode;
var iq_max=512;
var iq_min=-511;
// end

// indices into channels array
var chidx_ref = 4; // "f1"
var chidx_none=5; // "none"
var chan_max=5; // maximum channels (excluding "none")

// PSMIII channel colour arrays
var channel_bgcolours=["lightblue","mediumpurple", "pink","coral","turquoise",];
var channel_colours=["black","white","black","black","black"];
var channel_class= ["f0ch1","f0ch2","f0ch3","f0ch4","f1ref"];
var channel_class_iq=["f0ch1_iq","f0ch2_iq","f0ch3_iq","f0ch4_iq","f1ref_iq"];
var channel_class_b=["f0ch1_b","f0ch2_b","f0ch3_b","f0ch4_b","f1ref_b"];
var channel_class_s=["f0ch1_s","f0ch2_s","f0ch3_s","f0ch4_s","f1ref_s"];
var channel_class_g=["f0ch1_g","f0ch2_g","f0ch3_g","f0ch4_g","f1ref_g"];

var debug_bgcol=["blanchedalmond","yellow", "pink","red"]; // for true_value debug. pink not used.



// ======================================================================================
//   P S M  TABLE
// ======================================================================================
function build_psm_params()
{ // For PSMIII
    var i;
    var pattern1f = /1f/;  // PPG Mode "1f"
    var pattern2s =/2s/; // PPG Mode 2s
    var text="";
    var text_tmp="";
    var col,cols;
    var paths=new Array(); var values=new Array(); //  arrays for async_odbset
    var index=0; //  index for async_odbset arrays (paths,values)
    var show_max_channels=0;
    var my_class;
    progressFlag= progress_build_psm;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:lime;font-weight:bold"> build_psm_params: starting</span>'  ; 
    //  alert('build_psm_params: starting')
    
    
    if(document.getElementById("last_selected_showchanA").value =="")
	document.getElementById("last_selected_showchanA").value =0; // "foch1"
    if(document.getElementById("last_selected_showchanB").value =="")
	document.getElementById("last_selected_showchanB").value =chidx_none; // "none"
    
    // Show the channels selected to be displayed (max 2)
    //  alert('add_display_channels: channels to display are A='+document.getElementById("last_selected_showchanA").value+' and  B='+document.getElementById("last_selected_showchanB").value)
    
    show_chanA = document.form1.last_selected_showchanA.value;
    show_chanB = document.form1.last_selected_showchanB.value;
    //  alert('build_psm_parameters: showchanA is '+show_chanA+' and show_chanB is '+show_chanB);
    if(show_chanA < 0 || show_chanB > chidx_none)
	{
	    alert('add_display_channels: illegal value of show_chanA ('+show_chanA+'). Setting show_chanA to '+chidx_none+' or '+channels[chidx_none]);
	    show_chanA = chidx_none;
	    document.form1.last_selected_showchanB.value=show_chanA;
	}
    if(show_chanB < 0 || show_chanB > chidx_none)
	{
	    alert('add_display_channels: illegal value of show_chanA ('+show_chanB+'). Setting show_chanB to '+chidx_none+' or '+channels[chidx_none]);
	    show_chanB = chidx_none;
	    document.form1.last_selected_showchanB.value=show_chanB;
	}


    num_channels_enabled=0;
    if(!have_psm)
	{
	    if(document.getElementById("PSMparams") !=null)
		{
		    document.getElementById("PSMparams").innerHTML='<tr><td class="allpsmtbg">This mode does not use RF and this is a spacer</td></tr>';
		    document.getElementById("PSMparams").className="psmtbg";
		}
	    else 
		alert('add_display_channels:  document.getElementById("PSMparams") is null')
	    document.getElementById("dbg").innerHTML=""; // debug params
	    
	    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> build_psm_params: and ending "  ;     
	    progressFlag=progress_build_psm_done;
	    return;
	}
    
    // P S M  
    //  alert(' document.getElementById("PSMparams").innerHTML='+ document.getElementById("PSMparams").innerHTML);
    
    //  alert('remember_PSMparams = '+remember_PSMparams)  
    if( document.getElementById("PSMparams").className=="white")
	document.getElementById("PSMparams").className=="psm_title";
    // document.getElementById("PSMparams").innerHTML=remember_PSMparams;
    
    
    
    // All but 2 PPG Modes which use RF  use only one PSM profile (one_f) 
    //   ALL  PSMIII channels may be shown
    //   PPG Mode "1f"  may use REF as well 
    //   PPG Mode "2s"   will enable all channels
    
    
    
    text = '<tbody>';
    text += '<tr align="center">';
    text += '<th class="psm_title" id="psm_title" colspan="4">RF Module  ('+psm_module+') Parameters</th>';
    text +='</tr>';
    
    
    get_psm_data_arrays(); // get the psm data into its arrays
    
    if(ppg_mode == "2s")   
	{
	    text+='<tr><td><a href="/Equipment/FIFO_acq/Frontend/Hardware/PSM/f0ch1?exp='+my_expt+'">f0ch1</a>&nbsp;</td>';
	    text+='<td><a href="/Equipment/FIFO_acq/Frontend/Hardware/PSM/f0ch2?exp='+my_expt+'">f0ch2</a></td></tr>';
	    
	} // end of 2s
    
    
    cntr_start=0; // initialize line counter
    
    
    // Add enable channels table
    text += '<tr><td colspan=5>'
    text +='<table id="allchan" style="width: 100%;" border="1" cellpadding="1">';
    text +='<tbody>';
    text += '<tr  align="center" >';
    text += '<th class="param" rowspan=2 style="font-weight:bold;" >Module<br>Operating Mode:</th>';
    text += '<th class="param" style="font-weight:bold;" >Channels:</th>';
    text += '<th class="param" colspan=4  style="font-weight:bold;">f0</th>';
    text +='<th class="param" style="font-weight:bold;" >f1</th>';
    text += '</tr><tr  align="center">';
    text += '<th class="param">Enabled: ';
    for(i=0; i<chan_max; i++)
	{
            if(channel_enabled[i])
		my_class="green";
	    else
		my_class="red";
	    
	    if(i==chan_max-1)
		text += '<th class="'+my_class+'">ref';
	    else
		text += '<th class="'+my_class+'">chan'+(i+1)+'</th>';
	}
    text += '</tr><tr  align="center">';

    if( (pattern_bnmr.test(operating_mode) && bnmr_expt) || (pattern_bnqr.test(operating_mode) && bnqr_expt))
	text += '<th class="hdreverse" rowspan=1 style="font-size:120%">'+operating_mode+'</th>';
    else
	text += '<th class="red" rowspan=1 style="font-size:120%">'+operating_mode+'</th>';
    text += '<th class="param" rowspan=2>Amplitude:</th>';
    for(i=0; i<chan_max; i++)
	{
            if(channel_enabled[i] && parseInt(amplitude[i])==0)
		my_class="orange"; // one off
            else if (!channel_enabled[i] && parseInt(amplitude[i])==0)
		my_class="red"; // both off
	    else if (!channel_enabled[i] &&  amplitude[i] > 0)
		{
		    if(rstate==state_stopped)
			my_class="pink";
		    else
			my_class="red"; // should be set to zero at BOR
		}
	    else
		my_class="green";

	    text += '<th class="'+my_class+'">'+amplitude[i]+'</th>';
	}
    text +='</tr></table>';
    text +='</td></tr>';

    text +='<tr><th colspan=6 class="normal"  id="psmerr"></th></tr>'

    if(num_channels_enabled == 0)
	{   // no channels enabled
	    // add selection to display channel(s)
            text += add_display_channels();
	    
	    text += '<th  style="vertical-align:top"></th>';
	    text += '<th  style="vertical-align:top"></th>';
	    text += '</tr> '; // PSM row ends
	} // end of no channels enabled
    else
	{ // at least one channel is enabled - show items common to all channels (idle freq etc)
	    freq_table_driven = check_mode(ppg_mode);  // freq table-driven for all mode 2 except 20,2d,2g,2s
	    if(freq_table_driven)
		{   
		    text +='<tr>';
		    // col=2; // maximum columns (only display two channels at a time - was 6) 
		    
		    text +='<td class=param colspan=6>'; 
		    
		    // TABLE for Idle Freq radio buttons 
		    //alert('opened table for idle freq  for channel '+i)
		    text +='<table id="tifreq" style="width: 100%;" border="0" cellpadding="1">';
		    text+= '<tbody>';
		    text +='<tr align="left">';
		    text +='<td  class=psm_info colspan=4>This PPG mode loads a table containing the frequency scan values into the PSM<br>&nbsp</td></tr>';
		    
		    
		    text +='<td id="1star" class=param colspan=2>After last frequency point in scan ...  <span class=asterisk>*</span> </td>';

		    if(rstate==state_stopped)
			{
			    text +='<td class="param">';
			    text +='<input type="radio" name="jiradiogroup" value=0  onClick="set_radio_jump(1)">';
			    text +='<span class="it">Jump to idle frequency</span>';
			    text +='</td><td class=param>';
			    text +='<input type="radio" name="jiradiogroup" value=1  onClick="set_radio_jump(0)">';
			    text +='<span class="it">Stay at final frequency</span>';
			}
		    else
			{ // running
			     text +='<td class="param" colspan=2>';
			    if(jump_to_idle) // bool
				text +='Jump to idle frequency';
			    else
				text +='Stay at final frequency';
			}
		    
		
		    text +='</td></tr>';
	    
		    if(jump_to_idle) // bool
			{
			    // load_first is global
			    load_first =  get_bool( PSM ["freq sweep load 1st val in idle"]); // ODBGet( load_first_path) // global
			    
			    text +='<tr>';
			    // col = show_max_channels+1;
			    
			    text +='<td class=param colspan=1>Use first frequency point as idle frequency?</td>';
			    text +='<td class=param colspan=2>';
			    
			    if(rstate==state_stopped)
				text +='<input  name="psmbox512"  type="checkbox"  onClick="gbl_code=2; cs_odbset(load_first_path, this.checked?\'1\':\'0\')">';
			    else
				text += load_first;
			    text +='</td>';
			    //  if(show_max_channels > 1) 
				text +='<td class=param colspan=2></td>';
			    text +='</tr>';
			    text +='</tbody></table>';  // END OF IDLE FREQ TABLE  tifreq
			    text +='</td></tr>';

		       
			    if (pattern_n.test(load_first))
				{
				    text +='<tr><td class="param" colspan=6>';
				    text +='<table style="width: 100%;" border="0" cellpadding="1">'; // Idle/Ref freq table
				    // show idle freq
				    text +='<tr>';
				    text +='<td class=param>Idle frequency (Hz)</td>';
				    text +='<td class=param>';

				    if(rstate==state_stopped)
					{
					    text +='<a href="#" onclick="myODBEdit(idle_freq_path,  idle_freq_hz, update_psm)" >';
					    text +=idle_freq_hz;
					    text +='</a>';
					    text +='' ;
					}
				    else
					text +=idle_freq_hz;
				    text +='</td>';
				    //if(show_max_channels > 1) 
				    //	text +='<td class=param colspan=6></td>';
				    //  text+='</tr>';
				} //  end of not load_first
			    
			} // end of jump-to-idle
		    
		} // end of  freq table-driven
	    else
		{ // NOT freq table-driven
		    col=2; // maximum columns (was 6)
		    text +='<tr>';
		    pattern_scan = /1[abfg]/;  // Type 1 with a frequency scan
		    if(pattern_scan.test(ppg_mode))
			{
			    if(channel_enabled[chidx_ref]) // f1 reference channel 4
				{
				    //col=4; // fref as well
				    text +='<td class=param colspan='+col+'><i>';
				    text +='<ul>';
				    text +='<li>PPG Parameters for Frequency Scan (Stop/Start/Incr) control the frequency range</li>';
				    text +='<li>If required, enter Reference (Fixed) frequency under Channel '+channel[4]+' below</li>';
				    text +='</i></td></tr>';
				    text +='<tr><td class=param colspan=2></td>'; // ready to add f reference              
				} 
			} // end of freq table-driven modes
		    else
			{  // no loaded freq table
			    text +='<td class="param" colspan=6>';
			    text +='<table style="width: 100%;" border="0" cellpadding="1">'; // Idle/Ref freq table

			    text +='<td class=param>Frequency (Hz)</td>';
			    text +='<td class=param>';
			    
			    if(rstate==state_stopped)
				{
				    text +='<a href="#" onclick="myODBEdit(idle_freq_path, idle_freq_hz, update_psm )" >';
				    text +=idle_freq_hz;
				    text +='</a>';
				    text +='' ;
				}
			    else
				text +=idle_freq_hz;
			    text +='</td>';
			    
			} // end of no freq scan ppg modes
	  
		} // end of NOT frequency table-driven
	
	    
	    if(channel_enabled[chidx_ref]) // f1 reference ch 4
		{  // fref Tuning FReq
		    text +='<td class="fref_tun" style="font-weight:normal;">Reference/Tuning Frequency (Hz)</td>';
		    text +='<td class="'+channel_class[chidx_ref]+'">';
		    if(rstate==state_stopped)
			{
			    text +='<a href="#" onclick="myODBEdit( tuning_freq_path, fref_tuning_freq, update_psm )" >';
			    text += fref_tuning_freq;
			    text +='</a>';
			    text +='' ;
			}
		    else // running
			text += fref_tuning_freq;
		    text +='</td>';
		    
		} // end of  (channel_enabled[chidx_ref]) ref enabled
	    else
			text+='<td class=param colspan=2>reference disabled</td>'; 
	    text+='</tr></table>'; // end of idle/ref freq table
	    
           
	    // add selection to display channel(s)
	    text += add_display_channels();    
	    // End of common items
	}

    // Display selected channel(s)
    text +='<tr>';
    
    if(show_chanA == chidx_none)
	text+='<th class=param  style="vertical-align:top"></th>';
    else
	for (i=0; i<chan_max; i++)
	    { // chanA first
		if(i==show_chanA)
		    text+=build_one_psm_channel(i,index,paths,values); // number of channels enabled is 0
	    }  // end of for loop
    if(show_chanB == chidx_none)
	text+='<th class=param  style="vertical-align:top"></th>';    
    else
	for (i=0; i<chan_max; i++)
	    { // chanB second
		if(i==show_chanB)
		    text+=build_one_psm_channel(i,index,paths,values); // number of channels enabled is 0
	    }  // end of for loop   
    text +='</tr><tr>';


       // GATES 
  text +='<tr id="psmGates"><td>gate control</td>'
  text +='</tr>'
  

	    
    if(num_channels_enabled > 0)
	{
	    text += '<tr  id="footnotes">'
		text += '<th colspan="5"></th></tr>' // my_footnotes
		}
    
    //    text +='<tr><th colspan=2 class="normal"  id="psmerr"></th></tr>'
	text +='</tbody>'
	
	    //	 alert('build_psm_params:   Final text is '+text);
	
	    if(document.getElementById("PSMparams") !=null)
		document.getElementById("PSMparams").innerHTML=text;  // replace <table id="PSMparams" > by text
	    else
		{
		    alert('build_psm_params: error  document.getElementById("PSMparams") is null');
		    return;
		}

    // alert('build_psm_params:   Final text is '+text);

    // alert('build_psm_params: calling show_gate_control with  show_gate_params='+show_gate_params)
     show_gate_control( show_gate_params); 
     // alert('build_psm_params: calling initialize_psm_params with  show_chanA='+show_chanA+'show_chanB='+show_chanB )
     initialize_psm_params(show_chanA,show_chanB)
    show_psm_debug_params();
    
    
    if(index > 0) 
	{
	    // alert('build_psm_params: calling async_odbset with index='+index+'; paths='+paths+'; values='+values)
	    async_odbset(paths,values);  // write to odb if needed
	}
    
    //  alert('smb='+ document.getElementById("smb").innerHTML)
    if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><span style="color:lime;"> build_psm_params: ending</span>';

    progressFlag=progress_build_psm_done;
    return;   
}

function select_display_chan(item)
{
  var temp;
  // alert('select_display_chan item='+item)
  if(item==1)// channel A
      {
	  //  alert('select_display_chan A:  item='+item+' index = '+document.form1.select_chanA.selectedIndex);
	  // alert('select_display_chan A: ivalue= '+document.form1.select_chanA.options[document.form1.select_chanA.selectedIndex].value);
	  document.form1.last_selected_showchanA.value  = document.form1.select_chanA.selectedIndex;
	  // alert('selected A '+channels[ document.form1.last_selected_showchanA.value]);
      }
  else
      {
	  //  alert('select_display_chan B:  item='+item+' index = '+document.form1.select_chanB.selectedIndex);
	  // alert('select_display_chan B: ivalue= '+document.form1.select_chanB.options[document.form1.select_chanB.selectedIndex].value);
	  document.form1.last_selected_showchanB.value  = document.form1.select_chanB.selectedIndex;
	  // alert('selected B '+channels[ document.form1.last_selected_showchanB.value]);
      }
  gbl_code=update_psm; // 2
  update();
}


function show_psm_debug_params()
{

   var text="";
   var i;
   // alert('show_psm_debug_params starting with gbl_debug='+gbl_debug)
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>show_psm_debug_params</b>: starting "
   // enable_psm_debug_params=get_bool(ODBGet(tpath));
   enable_psm_debug_params = document.form1.tbox0.checked;
   //alert('show_psm_debug_params:  enable_psm_debug_params= '+enable_psm_debug_params);
   if(!enable_psm_debug_params)
   {
       document.getElementById("dbg").innerHTML="";
       if(!gbl_debug) document.getElementById('tdebug').innerHTML=""; // clear debug title
       return;
   }
   document.getElementById('tdebug').innerHTML=remember_tdebug; // show debug title
   text ='<h3>PSM Debug</h3>'
   text+='Idle freq (Hz) = '+idle_freq_hz
   if(show_fref)
       text+='&nbsp&nbsp Ref freq (Hz) = '+ fref_tuning_freq
   if(freq_table_driven)
   {
      text+='<br>Frequency Table-driven;&nbsp&nbsp Freq end sweep jump to idle = '+ jump_to_idle
      text+='&nbsp&nbsp Load first sweep value in idle = '+ load_first
   }
   
   // Debug table
   text+='<table style="width: 100%;" border="1" cellpadding="5" bgcolor=white>'
   text += '<tr align="left">'

   text += '<td class="helplinks">channel</td>'
   text += '<td class="helplinks">index</td>'
   text +='<td class="helplinks">enabled</td><td class="helplinks">enable quad</td>'
       //  text += '<td class="helplinks">simulate single tone</td>' // PSMII
   text += '<td class="helplinks">amplitude</td><td class="helplinks">moduln mode</td><td class="helplinks">bandwidth</td>'
   text += '<td class="helplinks">jump idle iq</td><td class="helplinks">load 1st</td><td class="helplinks">idle i</td>'
   text += '<td class="helplinks">idle q</td><td class="helplinks">load iq file</td><td class="helplinks">set const i</td>'
   text += '<td class="helplinks">const i</td><td class="helplinks">set const q</td><td class="helplinks">const q</td>'
   text += '<td class="helplinks">gate (1=def)</td></tr>'
	  								    
       //  var pattern=/freq */;  
       //  var pf
       
   for (i=0; i<chan_max; i++)
   {
       //  pf = channels[i].replace  (pattern,"f"); 
       //  text += '<tr><td>'+pf+'</td>'
    text += '<tr><td>'+channels[i]+'</td>'
    text += '<td>'+i+'</td>'
    text += '<td>'+ channel_enabled[i]+'</td>'
    if(channel_enabled[i])
      {
         text += '<td>'+ enable_quad[i]+'</td>'
        text += '<td>'+ amplitude[i]+'</td>'
        text += '<td>'+ moduln_mode[i]+'</td>'

        text += '<td>'+ bandwidth[i]+'</td>'
        text += '<td>'+ jump_idle_iq[i]+'</td>'
        text += '<td>'+ load_first_iq[i]+'</td>'
        text += '<td>'+ idle_i[i]+'</td>'
        text += '<td>'+ idle_q[i]+'</td>'
        text += '<td>'+ load_iq_file[i]+'</td>'
        text += '<td>'+ set_const_i[i]+'</td>'
        text += '<td>'+ const_i[i]+'</td>'
        text += '<td>'+ set_const_q[i]+'</td>'
        text += '<td>'+ const_q[i]+'</td>'

      
        text += '<td>'+ gate_control[i]+'</td></tr>'
     }

   }

   text += '</table>'
   document.getElementById("dbg").innerHTML=text;
   show_paths();
  if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending "
}

function show_paths()
{
    var text="";
    text +='<br><h3>PSM Paths</h3>';
    text +='<table>';
    text +='<tr><td>Pathname</td><td>Path</td></tr>';
    text +='<tr><td>psm_path</td><td>'+psm_path+'</td></tr>';
    text +='<tr><td>idle_freq_path</td><td>'+idle_freq_path+'</td></tr>';
    text +='<tr><td>tuning_freq_path</td><td>'+tuning_freq_path+'</td></tr>';

    for (i=0; i<chan_max; i++)
	{
           text+='<tr><td>channel_path['+i+']</td><td>'+channel_path[i]+'</td></tr>';
           text+='<tr><td>channel_enabled_path['+i+']</td><td>'+channel_enabled_path[i]+'</td></tr>';
           text+='<tr><td>amplitude_path['+i+']</td><td>'+amplitude_path[i]+'</td></tr>';
           text+='<tr><td>gate_control_path['+i+']</td><td>'+gate_control_path[i]+'</td></tr>';
           text+='<tr><td>quadrature_path['+i+']</td><td>'+quadrature_path[i]+'</td></tr>';
           text+='<tr><td>mod_path['+i+']</td><td>'+mod_path[i]+'</td></tr>';

	}
       text += '</table>'
   document.getElementById("keysid").innerHTML=text;
}


function get_enabled_channels()
{
    // fill channel_enabled array for all channels
    var i;
    // alert('get_enabled_channels starting with gbl_debug='+gbl_debug)
     if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b>get_enabled_channels</b>: starting'
    channel_enabled[0]= PSM.f0ch1["channel enabled"]; 
    channel_enabled[1]= PSM.f0ch2["channel enabled"]; 
    channel_enabled[2]= PSM.f0ch3["channel enabled"]; 
    channel_enabled[3]= PSM.f0ch4["channel enabled"]; 
    channel_enabled[chidx_ref]= PSM.f1["channel enabled"]; 

    for(i=0; i<chan_max; i++)
    {
        if(channel_enabled[i]==undefined)
           alert('get_channel_enabled:  channel_enabled['+i+'] is undefined' );
        else
	    channel_enabled[i] = get_bool(channel_enabled[i])
     }
  if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br><b>get_enabled_channel</b> ends with channel_enabled= '+channel_enabled
  return;
}

function assign_channel_paths()
{ // includes  get_psm_path_arrays() and get_psm_quad_path_arrays
  //  alert('assign_channel_paths for psm3 starting')
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>assign_channel_paths</b>: starting ";

    // common paths
    idle_freq_path = psm_path + "/Idle freq (hz)";
    tuning_freq_path = psm_path +"/fRef Tuning freq (hz)"; // actually f1 tuning frequency

    for(i=0; i<chan_max; i++)
    {
       channel_path[i] = psm_path + '/' +channel[i];
       mod_path[i] = channel_path[i] + "/IQ Modulation";  // mod_path global
       channel_enabled_path[i] = channel_path[i] + "/channel enabled";
       amplitude_path[i] = channel_path[i] + '/scale factor (def 181 max 255)'
       gate_control_path[i] = channel_path[i] + "/gate control";
       quadrature_path[i]= channel_path[i] + '/quadrature modulation mode';

       // channel_path_array[i]= channel_enabled_path[i]; // why two arrays?
       jump_to_idle_iq_path[i]= mod_path[i] + '/jump to idle iq';
        load_iq_file_path[i]= mod_path[i] + '/load i,q pairs file';
        set_const_i_path[i] =  mod_path[i] + '/set constant i value in file';
        set_const_q_path[i] =  mod_path[i] + '/set constant q value in file';
	apply_phase_correction_path = mod_path[i] + '/apply phase correction';

        moduln_mode_path[i] = mod_path[i] +  '/Moduln mode (ln-sech,Hermite)'
        bandwidth_path[i] = mod_path[i] + '/requested bandwidth (Hz)'
        idle_i_path[i]= mod_path[i] + '/idle i(-511<=i<=512)'
        idle_q_path[i]= mod_path[i] + '/idle q(-511<=q<=512)'
        const_i_path[i] = mod_path[i] + '/const i(-511<=i<=512)'
        const_q_path[i] = mod_path[i] + '/const q(-511<=q<=512)'
    }


 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending (assign_channel_paths)"
}

function get_psm_data_arrays()
{
    //  alert('get_psm_data_arrays: starting')
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_data_arrays for psm3</b>: starting"
   num_channels_enabled = 0;
   var i, CHAN,IQCHAN;

   idle_freq_hz = parseInt(PSM["idle freq (hz)"]);  // common to all channels
   jump_to_idle = get_bool(PSM["freq end sweep jump to idle"]);
   load_first =  get_bool( PSM ["freq sweep load 1st val in idle"]); 
   fref_tuning_freq = parseInt(PSM["fref tuning freq (hz)"]);
   operating_mode = PSM["operating mode"];

   for (i=0; i<chan_max; i++) 
   {
      if(channel_enabled[i])
	  num_channels_enabled++;      
    
       switch(i)
       {    
  	   case 0:  // f0ch1
 	      CHAN = PSM.f0ch1;
              break;
 	   case 1:  // f0ch2
	      CHAN = PSM.f0ch2;
              break;
 	   case 2:  // f0ch3
	      CHAN = PSM.f0ch3;
              break;
 	   case 3:  // f0ch4
	      CHAN = PSM.f0ch4;
              break;
	   case 4:  // f1
	      CHAN = PSM.f1;
              break;
           default:
	      printf("programming error - illegal PSM channel\n");
              return;
      }

       // Need the amplitude even if channel is disabled   
       amplitude[i] = CHAN["scale factor (def 181 max 255)"];
       if(!channel_enabled[i])
	   continue;  // next channel

       gate_control[i]  = CHAN["gate control"]     
       enable_quad[i] = get_bool( CHAN["quadrature modulation mode"])
	   // if( enable_quad[i]) 
	   {      // single tone uses quad mode
          IQCHAN = CHAN["iq modulation"];
          moduln_mode[i]  = IQCHAN["moduln mode (ln-sech,hermite)"]
          bandwidth[i]    =  IQCHAN["requested bandwidth (hz)"] 
          jump_idle_iq[i] = get_bool( IQCHAN["jump to idle iq"])
          load_first_iq[i] = get_bool( IQCHAN["load first val in idle"])
          idle_i[i] =  IQCHAN["idle i(-511<=i<=512)"]
          idle_q[i] =  IQCHAN["idle q(-511<=q<=512)"]
          load_iq_file[i] =get_bool( IQCHAN["load i,q pairs file"])
          set_const_i[i] = get_bool( IQCHAN["set constant i value in file"]) 
          const_i[i] =  IQCHAN["const i(-511<=i<=512)"]
          set_const_q[i] =get_bool(IQCHAN["set constant q value in file"])
          const_q[i] =    IQCHAN["const q(-511<=q<=512)"]
          if(i==0)
		  apply_phase_correction[i]=0; // no phase correction for first channel f0ch1
	  else
	        apply_phase_correction[i] = get_bool( IQCHAN["apply phase correction"])
       }
   }
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>get_psm_data_arrays</b>: ending"
}

function check_psm_data_arrays()
{
  var i;
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>check_psm_data_arrays</b>: starting "

  if( idle_freq_hz == undefined)
      alert(' idle_freq_hz is undefined (  PSM["idle freq (hz)"]) ')
  if(jump_to_idle==undefined)
      alert('jump_to_idle is undefined ( EqData.frontend.hardware.psm["freq end sweep jump to idle"]) ')
  if(load_first==undefined)
      alert('load_first is undefined ( EqData.frontend.hardware.psm["freq sweep load 1st val in idle"]) ')
  if( fref_tuning_freq == undefined)
      alert('fref_tuning_freq is undefined ( EqData.frontend.hardware.psm["fref tuning freq (hz)"])  ')
                     
  for(i=0; i<chan_max; i++)
  {
    if(channel_enabled[i])
    {
      if( amplitude[i] == undefined)
	 alert(' amplitude['+i+'] is undefined ( PSM.'+channel[i]+'["scale factor (def 181 max 255)"]  ) ')
      if( gate_control[i] == undefined)
	 alert(' gate_control['+i+'] is undefined ( PSM.'+channel[i]+'["gate control"]   ) ')
      if( enable_quad[i] == undefined)
         alert(' enable_quad['+i+'] is undefined ( PSM.'+channel[i]+'["quadrature modulation mode"]  ) ')

	  //  if( enable_quad[i] )   // using single tone mode
	     // {
         if(moduln_mode[i] == undefined)  // STRING
	     	   alert('moduln_mode['+i+'] is undefined ( PSM.'+channel[i]+'["iq modulation"]["moduln mode (ln-sech,hermite)"]  ) ')
 
	 if( bandwidth[i] == undefined) // INT
	   alert(' bandwidth['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"][""requested bandwidth (hz)"]) ')


	 if( jump_idle_iq[i]== undefined) // BOOL
	   alert('jump_idle_iq['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["jump to idle iq"]) ')

	 if(load_first_iq[i] == undefined)  // BOOL
	  alert('load_first_iq['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["load first val in idle"]) ')
       	
         if( idle_i[i] == undefined)
	   alert(' idle_i['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["idle i(-511<=i<=512)"]) ')
          if( idle_q[i] == undefined)
	    alert(' idle_q['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["idle q(-511<=q<=512) "]) ')
      	if(  load_iq_file[i]== undefined)  // BOOL
	    alert(' load_iq_file['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["load i,q pairs file"]) ')

  

        if( set_const_i[i] == undefined)  // BOOL
	   alert(' set_const_i['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["set constant i value in file"]) ')
        else
	  set_const_i[i] =get_bool(  set_const_i[i])

        if( set_const_q[i] == undefined)  // BOOL
	   alert(' set_const_q['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["set constant q value in file"]) ')
      
       if( const_i[i] == undefined) // INT
          alert(' const_i['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["const i(-511<=i<=512)"]) ')
       if( const_q[i] == undefined) // INT
          alert(' const_q['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["const q(-511<=q<=512)"]) ')

       if( apply_phase_correction[i] == undefined) // BOOL
	   alert(' apply_phase_correction['+i+'] is undefined (  PSM.'+channel[i]+'["iq modulation"]["apply phase correction"]) ')
        else
	    apply_phase_correction[i] =get_bool(  apply_phase_correction[i]);



	      //  } // enable quad

    }// enable channel
  }// end loop
  if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending (check_psm_data_arrays)"
}




function build_one_psm_channel(i,index,paths,values)
{
    // parameters i=channel index
    //            index=index into paths and values arrays
    //            paths,values are arrays used in call to async_odbset

    var text_tmp="";
    var text="";
    var ibox;
    var pattern2s = /2s/;
    //   alert('build_one_psm_channel: starting with i='+i+' channel_bgcolours[i]='+ channel_bgcolours[i])
    // Heading
    text += '<th  style="vertical-align:top">'
    text += '<table id="channel'+i+'" style="width: 100%;" border="5" cellpadding="1" bgcolor="white">'  // Table for each channel
				// alert('opened psm table for channel '+i)
    text +='<tbody>'
    text +='<tr align="center">'
    text +='<th style="background-color: '+channel_bgcolours[i]+'; color: '+channel_colours[i]+'; " colspan="2">Channel <i>"'+channels[i]+'"</i></th>'
    text +='</tr>'


    text +='<tr> <td id= "psm_on'+i+'"  class="'+channel_class[i]+'"> Enabled</td>'
    if (rstate == state_stopped) // stopped
    {
       text +='<td class="'+channel_class[i]+'">'
       // use checkboxes psmbox0-4

       ibox=i; // psmbox0-4
       text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="enable_channel('+i+', this.checked?\'1\':\'0\')">';
       text +='</td>';
    }
    else // running
       text +='<td class="'+channel_class[i]+'">'+channel_enabled[i]+'</td>';

  
    if(!channel_enabled[i]) // channel is disabled
    {
       text +='</tr></tbody></table> ' // channel table ends
       text +='</th>'
       return text;
    }

  // This channel is enabled

  convert_all_quad(i); // convert y/n to 1/0 for all Quad boolean parameters for index i
	 

// ================================= ROW A ==================================================
          
  text +='<tr>'    // Amplitude row A
  // check value of Amplitude
  if(amplitude[i] > 255 )
  {
     alert('Maximum value of amplitude is 255');
     paths[index]=amplitude_path[i];  values[index]=255;  //ODBSet(amplitude_path[i], 255)
     index++;
     amplitude[i]=255;
  }
  else if(amplitude[i] < 0 )
  {
     alert('Minimum value of amplitude is 0 ');
     paths[index]=amplitude_path[i]; values[index]=0;  //ODBSet(amplitude_path[i], 0)
     index++;
     amplitude[i]=0;
  }


	     if(amplitude[i]==0 )   // enabled with zero amplitude... no RF
		       my_class="error";
	      else
     my_class=channel_class[i];
 
  
  text +='<td  class="'+my_class+'">Amplitude (0-255) </td>'

  if(rstate==state_stopped)
  {  

  // document.write ('<td class="'+channel_class[i]+'">'
     text +='<td class="'+my_class+'">'

     text +='<a href="#" onclick="myODBEdit(amplitude_path['+i+'], amplitude['+i+'], update_psm)"> '
     text += amplitude[i];
     text +='</a>';
     text +='' ;
 }
 else  // running
    text +='<td class="'+channel_class[i]+'">'+amplitude[i];
 text +='</td></tr>'
      

// ================================= ROW B ==================================================
  
  text +='<tr>'  // Quad Mode row B

 

  if(rstate==state_stopped)
  {
     text +='<td colspan=2  class="'+channel_class_b[i]+'">'

    // TABLE for QUAD/SINGLE TONE 
     //alert('opened  table for quad/single tone channel '+i)
     text +='<table id="qst'+i+'" style="width: 100%;" border="1" cellpadding="1">'
     text +='<tbody>'
     text +='<tr align="left">'

        
     //Radio quad buttons
     text +='<td class="'+channel_class_b[i]+'">Enable Quadrature Mode? </td>'
     text +='<td id="qmb" class="'+channel_class_b[i]+'">'

     text +='<input type="radio" name="quadradiogroup'+i+'" id="Quad"  onClick="radio_quad(1,'+i+');"></td>'
     text +='</td>'

     text +='<td class="'+channel_class_s[i]+'">Enable Single Tone Mode?&nbsp <span class=asterisk>**</span></td>'
     text +='<td id="smb" class="'+channel_class_s[i]+'">'
     text +='<input type="radio" name="quadradiogroup'+i+'" id="SingleTone"  onClick="radio_quad(0,'+i+');"></td>'

     //alert('closing  table for quad/single tone channel '+i)
     text +='</tr></tbody></table>'  // table id="qst"
 
     // ENDED  TABLE for QUAD/SINGLE TONE  

   }
   else  // running
   {  
      text +='<td class="'+channel_class_b[i]+'">Enable quadrature mode?</td>'
      text +='<td class="'+channel_class_b[i]+'">'+enable_quad[i];  
   }


  
 text +='</td></tr>'
	 
// ================================= ROW C ==================================================

 // do not show most quadrature mode params for single channel mode



       
  if(enable_quad[i])
  {         
     text +='<tr>'  // Modulation Mode  row C
     var lc_moduln_mode = moduln_mode[i].toLowerCase();
     var pattern_mod_mode= /ln-sech|hermite/; // either
      
     if (  pattern_mod_mode.test(lc_moduln_mode) )
        my_class=channel_class[i];
     else   // shouldn't happen with radiogroup
        my_class="error";
          //text +='<td class="'+channel_class[i]+'">Modulation Mode</td>'
        text +='<td class="'+my_class+'">Modulation Mode</td>'

     if(rstate==state_stopped)
     { 
	 //	 alert('creating mmradiogroup'+i)
       text +='<td class="'+channel_class[i]+'">'
		  text +='<input type="radio" name="mmradiogroup'+i+'" value=0  onClick="cs_odbset(moduln_mode_path['+i+'],ln_sech_string)">'
       text +='<span class="it">ln-sech </span>'

       text +='<input type="radio" name="mmradiogroup'+i+'" value=1  onClick="cs_odbset(moduln_mode_path['+i+'],hermite_string)">'
       text +='<span class="it">Hermite</span>'
    }
     else // running
        text +='<td>'+moduln_mode[i]+'</td>';  

     text +='</tr>'


// ================================= ROW D ==================================================

 

     text +='<tr>'  // Requested Bandwidth  row D

     text +='<td class="'+channel_class[i]+'">Requested bandwidth (Hz)</td>'

     if(rstate==state_stopped)
     { 
        text +='<td class="'+channel_class[i]+'">'

        text +='<a href="#" onclick="myODBEdit(bandwidth_path['+i+'], bandwidth['+i+'], update_psm)" >'
        text +=bandwidth[i]
        text +='</a>';
        text +='' ;
        text +='</td>';
     }
     else // running
         text +='<td class="'+channel_class[i]+'">'+bandwidth[i]+'</td>';  
     text +='</tr>'


// ================================= ROW E ==================================================

	 text +='<tr>';  // Load i,q pairs file  row # E
     text +='<td class="'+channel_class_b[i]+'">Load i,q pairs file?</td>';


      // uses checkboxes  psmbox 5,6,7,8,9

     if(rstate==state_stopped)
	 {  
	     text +='<td class="'+channel_class_b[i]+'">' ;
	     ibox = i + 5; // psmbox 5-9 
	     text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param(load_iq_file_path['+i+'], this.checked?\'1\':\'0\')">';
	 } 
     else  // running
	 text +='<td class="'+channel_class_b[i]+'">'+unbool(load_iq_file[i]);  
     
     text +='</td></tr>';
     
     
     // ================================= ROW F ==================================================
     
     
     if (load_iq_file[i])
	 {
	     text +='<tr>';  // Jump to Idle  row # F.1
	     //  text +='<td colspan=3 class="'+channel_class_b[i]+'">';
	     // text +='<td id="one_star'+i+'"colspan=3  class="'+channel_class_b[i]+'">After last i,q pair ... <span class=asterisk>*</span> <br>';
 text +='<td id="one_star" colspan=3  class="'+channel_class_b[i]+'">After last i,q pair ... <span class=asterisk>*</span> <br>';

         // TABLE for Jump to Idle IQ radio buttons
         //alert('opened  table for Jump to Idle IQ radio buttons  channel '+i)
	     //  text +='<table id="jiiq'+i+'"  style="width: 100%;" border="0" cellpadding="1">';
	     //  text +='<tbody>';
	     // text +='<tr align="left">';
	     // text +='<td id="one_star'+i+'"  class="'+channel_class_b[i]+'">After last i,q pair ... <span class=asterisk>*</span> </td>';

 //   text +='<tr>';  // Jump to Idle  row # F.2
 //	     text +='<td class="'+channel_class_b[i]+'">';
	     
	     // uses jiqradiogroupi
	     
	     if(rstate==state_stopped)
		 {   	 
		     text +='<input type="radio" name="jiqradiogroup'+i+'" value=0  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 1)">';
		     text +='<span class="small" >Jump to idle i,q pair</span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
		     //   text +='</td><td class="'+channel_class_b[i]+'">';
		     text +='<input type="radio" name="jiqradiogroup'+i+'" value=1  onClick="set_psm_cb_param(jump_to_idle_iq_path['+i+'], 0);">';
		     text +='<span class="small">Stay at final i,q pair</span>';
		 }
	     else // running
		 { 
		     if(pattern_y.test(jump_to_idle_iq_path[i]))
			 text +='jump to idle i,q pair';
		     else
			 text +='stay at final i,q pair';
		 }
	     text +='</td></tr>';
	     // text +='</tbody></table>';  // END OF TABLE  jiiq
	     //  text +='</td></tr>';
	     
	     // end of jump to idle i,q
	     
	 } // end of load_iq_file[i]
     

 // ================================= ROW G ==================================================


     if ( (load_iq_file[i] && jump_idle_iq[i] ) || !(load_iq_file[i]))
     {
	 text +='<tr>';  // Idle i row # G
	 
	 // Check value
	 if(idle_i[i] > iq_max )
	     {  // Set a legal value
		 idle_i[i] =iq_max;
		 paths[index]=idle_i_path[i]; values[index]=iq_max;
		 index++;
	     }
	 else if(idle_i[i] < iq_min )
	     {
		 idle_i[i] = iq_min;
		 paths[index]=idle_i_path[i];
		 values[index]=0;
		 index++;
	     }

	 text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle i ('+iq_min+' <= 0 < '+iq_max+')</td>';
        
     if(rstate==state_stopped)
     {       
	 text +='<td  class="'+channel_class[i]+'">';
	 // alert('idle_i_path[i]='+idle_i_path[i]);
	 text +='<a href="#" onclick="myODBEdit( idle_i_path['+i+'],  idle_i['+i+']), update_psm" >';
	 text +=idle_i[i];
        text +='</a>';
        text +='' ;
        text +='</td>';
      }
      else // running
        text +='<td class="'+channel_class[i]+'">'+idle_i[i]+'</td>';  
     text +='</tr>';


// ================================= ROW H ==================================================


     text +='<tr>';  // Idle q row # H

     text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle q  ('+iq_min+' <= 0 < '+iq_max+')</td>';

     // Check value
     if(idle_q[i] > iq_max )
	 { // illegal value. Set a legal value
	     idle_q[i] =iq_max;
	     paths[index]=idle_q_path[i];
	     values[index]=iq_max;
	     index++;
	 }

      else if(idle_q[i] < iq_min )
      {  // illegal value. Set a legal value
	  idle_q[i]= iq_min;	   
	  paths[index]=idle_q_path[i];
	  values[index]=0;
	  index++;
      }


      if(rstate==state_stopped)
      { 
         text +='<td class="'+channel_class[i]+'">'
         text +='<a href="#" onclick="myODBEdit( idle_q_path['+i+'],  idle_q['+i+'], update_psm)" >'
         text +=idle_q[i]
         text +='</a>';
         text +='' ;
       
        text +='</td>';
         
      }
      else // running
         text +='<td class="'+channel_class[i]+'">'+idle_q[i]+'</td>';  
      text +='</tr>'

	    } // end of  if ( (load_iq_file[i] && jump_idle_iq[i] ) || !(load_iq_file[i]))


// ================================= ROW I ==================================================

     if (load_iq_file[i])
     {
      
      text +='<tr>'  // Set constant i in file  row # I
      text +='<td class="'+channel_class_iq[i]+'">&nbsp&nbsp Set constant i value in file?</td>'

       // uses checkboxes  psmbox10-14
       ibox = i + 10 ; 

        if(rstate==state_stopped)
        {   
            text +='<td class="'+channel_class_iq[i]+'">'        
	    text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param( set_const_i_path['+i+'], this.checked?\'1\':\'0\')">'			             
         }
         else // running
            text +='<td>'+unbool(set_const_i[i]);  
         text +='</td></tr>'

         if(set_const_i[i])
         {
           text +='<tr>'  // constant i  row # I
           text +='<td class="'+channel_class[i]+'">Constant i  ('+iq_min+'<= i <='+iq_max+')</td>'

    // Check value
    if(const_i[i] > iq_max )
    {
			     alert('Maximum i value is iq_max')
				 //ODBSet(const_i_path[i], 511)
       const_i[i] =iq_max;
       paths[index]=const_i_path[i];
       values[index]=iq_max;
       index++;
     }
			  else if(const_i[i] < iq_min )
    {
			     alert('Minimum  i value is iq_min')
				 //ODBSet(const_i_path[i], 0)
       const_i[i] =0;
       paths[index]=const_i_path[i];
       values[index]=0;
       index++;
     }

           
    if(rstate==state_stopped)
    {       
       text +='<td class="'+channel_class[i]+'">'
    
        text +='<a href="#" onclick="myODBEdit(const_i_path['+i+'],  const_i['+i+'], update_psm )" >'
        text += const_i[i]
        text +='</a>';
        text +='' ;
        text +='</td>';    
     }
     else
       text +='<td class="'+channel_class[i]+'">'+const_i[i]+'</td>';  
     text +='</tr>'
         } // end of set const i
     
// ================================= ROW J ==================================================
         text +='<tr>'  // Set constant q in file  row # J

         text +='<td class="'+channel_class_iq[i]+'">&nbsp&nbsp Set constant q value in file?</td>'
      
         if(rstate==state_stopped)
         {        
           text +='<td class="'+channel_class_iq[i]+'">'
         // uses checkboxes  psmbox15-19
       ibox = i + 15 ;
       text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param(set_const_q_path['+i+'], this.checked?\'1\':\'0\')">'

        }
        else // running
          text +='<td class="'+channel_class_iq[i]+'">'+unbool(set_const_q[i]);  
        text +='</td></tr>'


// ================================= ROW K ==================================================

         if(set_const_q[i])
         {
            text +='<tr>'  // constant q  row # K

            text +='<td class="'+channel_class[i]+'">Constant q ('+iq_min+' <= 0 < '+iq_max+')</td>'
          
            // Check value
            if(const_q[i] > iq_max )
            {
			     alert('Maximum q value is '+iq_max)
				 // ODBSet(const_q_path[i], 511)
       const_q[i] =iq_max;
       paths[index]=const_q_path[i];
       values[index]=iq_max;
       index++;
  }
            else  if(const_q[i] < iq_min )
  {
			     alert('Minimum q value is '+iq_min)
				 //ODBSet(const_q_path[i], 0)
       const_q[i] =iq_min;
       paths[index]=const_q_path[i];
       values[index]=iq_min;
       index++;
  }
  if(rstate==state_stopped)
  {   
      text +='<td class="'+channel_class[i]+'">'
      text +='<a href="#" onclick="myODBEdit( const_q_path['+i+'],  const_q['+i+'], update_psm)" >'
      text += const_q[i]
      text +='</a>';
      text +='' ;
     }
     else
        text +='<td class="'+channel_class[i]+'">'+const_q[i];  
     text +='</td></tr>'
          } // end set const q

       } // end load_iq_file

     } // end iq_modulation mode enabled
  else
      { // SINGLE  TONE mode - show  idle i,q pair 
	// ==============Duplicates ================ ROW G ===========for Single Tone =======================================

      text +='<tr>';  // message
      // Check values of idle_i[i] and idle_q[i]
      var message=new Object();
     

      if(i>0 && pattern2s.test(ppg_mode))
	  message.myString ='<small>For <b>single tone mode</b> (unmodulated) the Idle i,q pair are set to a pair of constant values. <br>Except when a phase correction is applied, these are usually of the form  (0,'+iq_max+') or ('+iq_min+',0)</small>';
      else
	  message.myString ='<small>For <b>single tone mode</b> (unmodulated) the Idle i,q pair are set to a pair of constant values.<br>These are usually of the form  (0,'+iq_max+') or ('+iq_min+',0)</small>';

      var num = check_single_tone_idle_pair(i,index,paths,values,message); // returns number of values added to index
      index += num; // add

 

      text +='<td id="stc" class="'+channel_class[i]+'" colspan=2>'+message.myString+'</td>';

      // Phase Correction for i>0 only  mode 2s     uses checkboxes  psmbox 21,22..
      if(i>0  && pattern2s.test(ppg_mode))   
	  {
            text +='<tr>'; // phase correction row # G.1
            text +='<td  class="'+channel_class[i]+'">&nbsp&nbsp Apply phase correction? </td>'
            if(rstate==state_stopped)
            {       
	       text +='<td class="'+channel_class[i]+'">';
	       ibox = i + 20;
	       alert(' apply_phase_correction_path['+i+']='+  apply_phase_correction_path[i])
	       // text +='<a href="#" onclick="myODBEdit(apply_phase_correction_path['+i+'],  apply_phase_correction['+i+'], update_psm)" >'
               text +='<input  name="psmbox'+ibox+'"  type="checkbox"  onClick="set_psm_cb_param(apply_phase_correction_path['+i+'], this.checked?\'1\':\'0\')">';
	  
	    // text +='<td class="'+channel_class[i]+'">
	       //  text +=apply_phase_correction[i]
	       // text +='</a>';
	       // text +='' ;
            //   text +='</td>';
            }
            else // running
	        text +='<td class="'+channel_class[i]+'">'+ unbool(apply_phase_correction[i])
            text +='</td></tr>'
		} // end of i>0

       // Idle I
     text +='<tr>';  // Idle i row # G
     text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle i </td>'
        
     if(rstate==state_stopped)
     {       
        text +='<td class="'+channel_class[i]+'">'
           
        text +='<a href="#" onclick="myODBEdit( idle_i_path['+i+'],  idle_i['+i+'], update_psm)" >'
        text +=idle_i[i]
        text +='</a>';
        text +='' ;
        text +='</td>';
      }
      else // running
        text +='<td class="'+channel_class[i]+'">'+idle_i[i]+'</td>';  
      text +='</tr>'
	// ==============Duplicates ================ ROW H ===========for Single Tone =======================================

     text +='<tr>'  // Idle q row # H

      text +='<td class="'+channel_class[i]+'">&nbsp&nbsp Idle q </td>'

      // value checked above
      if(rstate==state_stopped)
      { 
         text +='<td class="'+channel_class[i]+'">'
         text +='<a href="#" onclick="myODBEdit( idle_q_path['+i+'],  idle_q['+i+'], update_psm)" >'
         text +=idle_q[i]
         text +='</a>';
         text +='' ;
       
        text +='</td>';
         
      }
      else // running
         text +='<td class="'+channel_class[i]+'">'+idle_q[i]+'</td>';  
      text +='</tr>'



	  } // end of single tone mode

  text +='</tr></tbody></table> ' // channel table ends
  text +='</th>'

      //  alert(' build_one_psm_channel: returning text')
return text;

}


function  initialize_psm_params(show_chanA,show_chanB)
{
    var i,idx;
    var index; 
    var lc_moduln_mode;
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> initialize_psm_params: starting ";
    //  alert('initialize_psm_params starting')
    
    //   alert('initialize_psm_params: show_chanA='+show_chanA+' and show_chanB= '+show_chanB);
    
    for(i=0; i<chan_max; i++)
	{
	    switch(i)
		{
		case 0:
		    if(typeof(document.form1.psmbox0) !='undefined')
			document.form1.psmbox0.checked= channel_enabled[i];
		    if(!channel_enabled[i])
			break;
		    
		case 1:
		    if(typeof(document.form1.psmbox1) !='undefined')
			document.form1.psmbox1.checked= channel_enabled[i];
		    if(!channel_enabled[i])
			break;
		    
		case 2:
		    if(typeof(document.form1.psmbox2) !='undefined')
			document.form1.psmbox2.checked= channel_enabled[i];
		    if(!channel_enabled[i])
			break;
		    
		case 3:
		    if(typeof(document.form1.psmbox3) !='undefined')
			document.form1.psmbox3.checked= channel_enabled[i];
		    if(!channel_enabled[i])
			break;
		    
		case 4:
		    if(typeof(document.form1.psmbox4) !='undefined')
			document.form1.psmbox4.checked= channel_enabled[i];
		    if(!channel_enabled[i])
			break;
		}
	} // for
    
    if(num_channels_enabled == 0)
	return;
    
    if(rstate==state_stopped)
	{
	    // only shown if freq_table_driven
	    if(typeof(document.form1.psmbox512) !='undefined')
		document.form1.psmbox512.checked= load_first;  // initialize to the correct value freq sweep load 1st val in idle
	    
	    
	    if(typeof(document.form1.jiradiogroup) !='undefined')
		{
		    
		    document.form1.jiradiogroup[0].checked=  pattern_y.test(jump_to_idle ) ;
		    document.form1.jiradiogroup[1].checked=  pattern_n.test(jump_to_idle);
		}
	  
       
	    for(i=0; i<chan_max; i++)
		{
		    if(!channel_enabled[i])
			continue;

		    if(i != show_chanA  && i != show_chanB)
			continue;
		    
		    if(enable_quad[i])   // quad mode
			index=0;
		    else                 // single tone
			index=1;

		    lc_moduln_mode = moduln_mode[i].toLowerCase();// initialize  modulation mode for each channel

		    switch(i)
			{       
			case 0:      
			    if(typeof(document.form1.quadradiogroup0) !='undefined')
				document.form1.quadradiogroup0[index].checked=1; // initialize  Enable Quad mode ch1
			    // ROW C
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup0) !='undefined')
					{   
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup0[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup0[1].checked= 1;
					}
				    
				    // ROW E psmbox 5
				    
				    if(typeof(document.form1.psmbox5) !='undefined') // ROW E  
					document.form1.psmbox5.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup0
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup0) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup0[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup0[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    
					    // ROW I   set const I   psmbox10
					    if(typeof(document.form1.psmbox10) !='undefined')
						document.form1.psmbox10.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox15
					    if(typeof(document.form1.psmbox15) !='undefined')
						document.form1.psmbox15.checked= set_const_q[i]; 
					} // end load_iq_file
				} // enable quad
			    break;
			    
			case 1:
			    
			    if(typeof(document.form1.quadradiogroup1) !='undefined')
				document.form1.quadradiogroup1[index].checked=1;  // initialize  Enable Quad mode ch2
			    
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup1) !='undefined')
					{                                                       // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup1[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup1[1].checked= 1;
					}
				    
				    // ROW E psmbox 6
				    
				    if(typeof(document.form1.psmbox6) !='undefined') // ROW E  
					document.form1.psmbox6.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup1
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup1) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup1[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup1[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox11
					    if(typeof(document.form1.psmbox11) !='undefined')
						document.form1.psmbox11.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox16
					    if(typeof(document.form1.psmbox16) !='undefined')
						document.form1.psmbox16.checked= set_const_q[i]; 
					} // end load_iq_file
				} // enable quad

			    else
				{
                                   if(typeof(document.form1.psmbox21) !='undefined')
				       document.form1.psmbox21.checked=apply_phase_correction[i];
                                   else
				       alert('typeof(document.form1.psmbox21) is undefined')
				}

			    break;
			    
			case 2:
			    if(typeof(document.form1.quadradiogroup2) !='undefined')
				document.form1.quadradiogroup2[index].checked=1;  // initialize  Enable Quad mode ch3
			    
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup2) !='undefined')
					{            
                                           // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup2[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup2[1].checked= 1;
					}
				    
				    // ROW E psmbox 7
				    
				    if(typeof(document.form1.psmbox7) !='undefined') // ROW E  
					document.form1.psmbox7.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup2
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup2) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup2[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup2[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox12
					    if(typeof(document.form1.psmbox12) !='undefined')
						document.form1.psmbox12.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox17
					    if(typeof(document.form1.psmbox17) !='undefined')
						document.form1.psmbox17.checked= set_const_q[i]; 
					} // end load_iq_file
				    
				    
				} // enable quad
			    break;
			    
			case 3:
			    if(typeof(document.form1.quadradiogroup3) !='undefined')
				document.form1.quadradiogroup3[index].checked=1;  // initialize  Enable Quad mode ch4
			    
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup3) !='undefined')
					{                                                       // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup3[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup3[1].checked= 1;
					}
				    
				    // ROW E psmbox 8
				    
				    if(typeof(document.form1.psmbox8) !='undefined') // ROW E  
					document.form1.psmbox8.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup3
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup3) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup3[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup3[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox13
					    if(typeof(document.form1.psmbox13) !='undefined')
						document.form1.psmbox13.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox18
					    if(typeof(document.form1.psmbox18) !='undefined')
						document.form1.psmbox18.checked= set_const_q[i]; 
					} // end load_iq_file
				    
				    
				} // enable quad
			    break;
			    
			case 4:
			    if(typeof(document.form1.quadradiogroup4) !='undefined')
				document.form1.quadradiogroup4[index].checked=1;  // initialize  Enable Quad mode f1
			    
			    
			    if(enable_quad[i] )
				{
				    if(typeof(document.form1.mmradiogroup4) !='undefined')
					{                                                       // initialize  modulation mode
					    if( pattern_ln_sech.test(lc_moduln_mode) )
						document.form1.mmradiogroup4[0].checked=  1;
					    else if (  pattern_hermite.test(lc_moduln_mode))
						document.form1.mmradiogroup4[1].checked= 1;
					} 
				    
				    // ROW E psmbox 9
				    
				    if(typeof(document.form1.psmbox9) !='undefined') // ROW E  
					document.form1.psmbox9.checked= load_iq_file[i]; 
				    
				    // ROW F jiqradiogroup4
				    if (load_iq_file[i])        
					{
					    if(typeof(document.form1.jiqradiogroup4) !='undefined')    // ROW F
						{  
						    if(jump_idle_iq[i])
							document.form1.jiqradiogroup4[0].checked= 1; // initialize jump to idle i,q pair  1f
						    else
							document.form1.jiqradiogroup4[1].checked= 1; // initialize stay at final i,q pair  1f
						}
					    // ROW I   set const I   psmbox14
					    if(typeof(document.form1.psmbox14) !='undefined')
						document.form1.psmbox14.checked= set_const_i[i];
					    // ROW J   set const Q   psmbox19
					    if(typeof(document.form1.psmbox19) !='undefined')
						document.form1.psmbox19.checked= set_const_q[i]; 
					} // end load_iq_file
				} // enable quad
			    break;
			} // switch
		    
		    
		} // for loop
	} // state stopped
    

      
    // PSM Footnotes
    if (num_channels_enabled > 0)
	{ // at least one channel is enabled
	    text ='<td class=footnote colspan=5>';
	    
	    if(document.getElementById("one_star") != undefined)   
		{
		    text +='<span class=allwhite><br>hi</span>'; // spacer
		    text +='<span class=asterisk>*</span> if the number of strobes or the gate exceeds the number of values in the table.';
		}
	    if( (show_chanA != chidx_none) || (show_chanB != chidx_none))
		{
		    text +='<span class=allwhite><br>hi</span>'; // spacer
		    text +='<span class=asterisk>**</span> &nbsp&nbsp';
		    spacer = '<br><span class=allwhite>hi**</span> &nbsp&nbsp'; // spacer
		    text +='Single tone mode is simulated by using Quadrature Mode with Idle i,q pair set to e.g. ('+iq_max+',0)';
		}	
	    text +='</td>'; // footnotes
	    document.getElementById("footnotes").innerHTML=text;
	    
	}
    // if no channels enabled, no footnotes needed; id="footnotes" not written
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="... and ending "
		     return;
}


function show_gate_control(param)
{
  var idx;
  var text;
  // Gate Control common to the module
  show_gate_params = get_bool(param);
  // alert(' show_gate_control : starting with  show_gate_params='+ show_gate_params)
  if(!have_psm)
     return;

  if(num_channels_enabled == 0)
      return; // no channels enabled, no Gates
   if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> show_gate_control: starting "

  // GATES 
       //  for(i=0; i<chan_max; i++)
		    //  {
		    //  if(!channel_enabled[i])
		    //      continue;

      if(show_gate_params)
      {
	    text="";
            idx =parseInt( gate_control[i]);
   
            if (rstate != state_stopped) // running
            {
               text +='<td colspan=1 class="'+channel_class_g[i]+'"><big><b>Gate Control:</b>  front panel gate input...</big></td>'
               text +='<td colspan=1 class="'+channel_class_g[i]+'"><big>'+gate_states[idx]+'</big></td></tr>'
            }
            else // stopped
            {
               text +='<td colspan=2 class="'+channel_class_g[i]+'"><big><b>Gate Control:</b>  front panel gate input...</big>'
               text +='<table style="width: 100%;" border="0" cellpadding="5" bgcolor="white">'  // GATE table
               text +='<tbody>'

               text +='<tr>'


               text +='<td  colspan=1 class="'+channel_class_g[i]+'">'
               text +='<input name="gateradiogroup'+i+'"  type="radio" value=0 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],0);">'
	       
               // gate_control = 0 (disabled);
               if(idx==0) // selected state
                  text +='<span class="error">'+gate_states[0]+'</span><br>'
               else
                  text +=gate_states[0]+'<br>'
               text +='<input name="gateradiogroup'+i+'"  type="radio" value=1 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],1);">'
	       
               // gate_control = 1 enabled (default)
               text +='<span style="color: green">'+gate_states[1]+'</span></td>'


               text +='<td  colspan=1 class="'+channel_class_g[i]+'">'
               text +='<input name="gateradiogroup'+i+'"  type="radio" value=2 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],2);">'
	      
               // gate_control = 2 (pulse inverted);
               if(idx==2) 
                 text +='<span class="error">'+gate_states[2]+'</span><br>'
               else
                 text +=gate_states[2]+'<br>'

	       text +='<input name="gateradiogroup'+i+'"  type="radio" value=3 onclick="gbl_code=2;cs_odbset(gate_control_path['+i+'],3);">'
	     
               // gate_control = 3 // ignored (int gate always on)
               if(idx==3)
                  text +='<span class="error">'+gate_states[3]+'</span>';
               else
                  text +=gate_states[3]

               text +='</td></tr>'
               text +='</table>';  // end of GATE table
     
               text +='</td>'
               text +='</tr>'
	    }  // end of stopped
      } // end of show_gate_params
  
     
 if(document.getElementById("psmGates") !=null)
	  {
             if(show_gate_params)
                 document.getElementById("psmGates").innerHTML=text;
             else     
             {
	        if(document.getElementById("psmGates") !=null)   
                    document.getElementById("psmGates").innerHTML="";
             }
	  }
      //  } // for loop

//  alert(' show_gate_control: calling initialize_gate_params')

  if(show_gate_params)
      initialize_gate_params();
 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> show_gate_control: ending "
  return;
}


function initialize_gate_params()
{
    var idx, i;

 if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b></b> initialize_gate_params : starting "
		  //   alert('initialize_gate_params: starting')
    if(rstate==state_stopped && show_gate_params)
    {
       for(i=0; i<chan_max; i++)
       {
          idx = gate_control[i];
          idx=parseInt(idx);
   
          idx = gate_control[i];
          idx=parseInt(idx);
        
	  switch(i)
	  {
	  case 0:
             if(typeof(document.form1.gateradiogroup0) !='undefined')
                     document.form1.gateradiogroup0[idx].checked = 1;
             break;
	  case 1:
             if(typeof(document.form1.gateradiogroup1) !='undefined')
                     document.form1.gateradiogroup1[idx].checked = 1;
             break;
	  case 2:
             if(typeof(document.form1.gateradiogroup2) !='undefined')
                     document.form1.gateradiogroup2[idx].checked = 1;
             break;
	  case 3:
             if(typeof(document.form1.gateradiogroup3) !='undefined')
                     document.form1.gateradiogroup3[idx].checked = 1;
             break;
	  case 4:
             if(typeof(document.form1.gateradiogroup4) !='undefined')
                     document.form1.gateradiogroup4[idx].checked = 1;
             break;

	  } // switch
       } // for 
    } // if rstate...
 if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending ";
}	  


function enable_channel(i,val)
{
    // PSMIII  automatically set amplitude to 0 if disabled, to default (181) if enabled

    var paths=new Array();  //  arrays for async_odbset
    var values=new Array();
    var amplitude_on = 181;
    var amplitude_off = 0;

    
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>enable_channel</b>: starting ";
    i=parseInt(i);
    val=parseInt(val);

    paths[0]=channel_enabled_path[i];
    paths[1]=amplitude_path[i];

    values[0]=val;
    if(val)
	values[1]=amplitude_on;
    else
	values[1]=amplitude_off;

    // alert(' enable_channel: channel_enabled_path='+channel_enabled_path[i])
 if(gbl_debug)document.getElementById('gdebug').innerHTML+='<br>i='+i+' and channel_enabled_path='+channel_enabled_path[i]
   
    if(i==0) // f0ch1
     document.form1.psmbox0.checked=val; // initialize to correct value
    else if (i==1) //  f0ch2
    document.form1.psmbox1.checked=val; // initialize to correct value
    else if (i==2) //  f0ch3
    document.form1.psmbox2.checked=val; // initialize to correct value
    else if(i==3)//  f0ch4
     document.form1.psmbox3.checked=val; // initialize to correct value
    else if(i==4)//  f1
     document.form1.psmbox4.checked=val; // initialize to correct value

     // ODBSet(channel_enabled_path[i], val);
    gbl_code=2; // for update
    // alert('Calling cs_odbset with i='+i+' and channel_enabled_path[i]='+ channel_enabled_path[i]+' and val= '+val)   
    //    cs_odbset(channel_enabled_path[i], val) 

    // alert('Calling aync_odbset with i='+i+' and paths='+paths+' and values='+values);
    async_odbset(paths,values); 
    if(gbl_debug)document.getElementById('gdebug').innerHTML+=".. and ending  (enable_channel) with a call to update  (2)"
    update();
 } 


function radio_quad(val,channel_index)
{
    // PSMIII only
    // val = 1 or 0   channel_index=PSM channel index
    var my_path;  
    var paths=new Array();
    var values=new Array();
    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>radio_quad</b>: starting ";
    //  alert(' radio_quad: val='+val+' psm_module='+psm_module)
    
    if(val==0)
	document.getElementById('smb').style.backgroundColor="navy"; // signifies clicked
    else
	document.getElementById('qmb').style.backgroundColor="azure"; // signifies clicked
    
    
    if(psm_module=="PSMIII") // PSM Module    psm quad val=1
	{
	    // alert('radio_quad  setting '+quadrature_path[channel_index]+' with val= '+val+'  i = '+i)

	    // Note - PSMIII has no register to set quad/single tone
	    // Assign common paths
	    paths[0]=quadrature_path[channel_index]; 
	    paths[1]=load_iq_file_path[channel_index];

	    // Load defaults for single tone mode
	    if(val==0)
		{ // single tone  modulate with fixed i,q pair or 0,512
		    values[0]=val; 
		    values[1]=0; // don't load i,q file

		    paths[2]=idle_i_path[channel_index]; values[2]=512; //idle i 
		    paths[3]=idle_q_path[channel_index]; values[3]=0; //  idle q  
		    paths[4]=jump_to_idle_iq_path[channel_index];  values[4]=1; // jump_to_idle_iq
		}
	    else
		{  // true quad mode - load an i,q file containing modulation
		    values[0]=val; 
		    values[1]=1; // load i,q file
		    
		    paths[2]=set_const_i_path[channel_index]; values[2]=0; // if using a file, don't set constant i value in file 
		    paths[3]=set_const_q_path[channel_index]; values[3]=0; // if using a file, don't set constant q value in file path
		    paths[4]=jump_to_idle_iq_path[channel_index];  values[4]=0; // jump_to_idle_iq (stay at last i,q, value)
		}

	} // end of PSMIII
    else
	{
	    alert(" radio_quad: unknown psm_module: "+psm_module);
	    return;
	}
    
    //   alert('radio_quad: val='+val+' channel_index= '+channel_index+'  paths= '+paths);
    progressFlag= progress_write_callback;
    mjsonrpc_db_paste(paths,values).then(function(rpc) {
	    var i,len;
	    document.getElementById('readStatus').innerHTML = 'radio_quad:  status= '+rpc.result.status;
	    len=rpc.result.status.length // get status array length
		// alert('radio_quad: length of rpc.result.status='+len)

		for (i=0; i<len; i++)
		    {
			if(rpc.result.status[i] != 1) 
			    alert('radio_quad: status error '+rpc.result.status[i]+' at channel_index '+i);
		    } 
	    document.getElementById('writeStatus').innerHTML='radio_quad: writing paths '+paths+' and values '+values;
	    progressFlag= progress_write_got_callback;
	    gbl_code=2; // for update
	    update(); // rebuild psm table
	}).catch(function(error)
		 { 
		     mjsonrpc_error_alert(error); });

    if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>radio_quad</b>: ending ";
    return;
}    
   
function check_single_tone_idle_pair(i,index,paths,values,message)
{
    // For single tone mode, i,q pair must be of the form 0, max e.g. 0,512 or -511,0  
    // unless using a phase correction
    // iq_max=512, iq_min=-511

    var flag = 0;
    var num=0;
    var idx=index;
   
    if(idle_i[i] == 0 && idle_q[i] == 0)
	message.myString='<span class="bolderror" style="font-size:110%">If i and q are both 0, there will be no RF</span>';
	
    else if(idle_i[i] != 0 && idle_q[i] != 0)
	message.myString='<span class="boldwarn" style="font-size:100%">One of i or q should be 0 for single tone mode</span>';




    else
	{
      if(idle_i[i] == 0)
        {
	    if(idle_q[i] > 0 && idle_q != iq_max)
		{
		    flag=1;
		    idle_q[i]=iq_max;
		}
            else if (idle_q[i] <= 0 && idle_q[i] != iq_min)
		{
		    idle_q[i]=iq_min;
		    flag=1;
		}
            if(flag)
		{ // write new q value
		    paths[idx]=idle_q_path[i]; values[idx]=idle_q[i];
		    idx++;
                    num++;
		}
        }

        flag=0;
        if(idle_q[i] == 0)
	{
	    if(idle_i[i] > 0 && idle_i != iq_max)
		{
		    idle_i[i]=512;
		    flag=1;
		}
            else if (idle_i[i] <= 0 && idle_i[i] != iq_min)
		{
		    flag=1;
		    idle_i[i]=iq_min;
		}
            if(flag)
		{ // write new i value
		    paths[idx]=idle_i_path[i]; values[idx]=idle_i[i];
		    idx++;
                    num++;
		}
	} 
    } // else
    return num;
}

function calculate_2s()
{
    //  alert('calculate_2s starting');
    var e2s_rf_cycle_time, e2s_nprebeam_cycles, e2s_nbeam_cycles, e2s_necho_cycles;
    var max_pi_cn =  3 * e2s_nfid_cycles  -4;

    if(e2s_nfid_cycles < 2 ) 
       alert('Please set number of FID cycles to be at least 2');
    if(e2s_pi_pulse_cycle_num < 3 ) 
       alert('Please set pi cycle number to at least 3');
    else if ( e2s_pi_pulse_cycle_num > max_pi_cn)
       alert('Maximum pi cycle number must be <= '+max_pi_cn+' for '+e2s_nfid_cycles+' FID cycles');
    else
    {
      if(!e2s_select_mode_se2)
         e2s_rf_cycle_time_ms = 2 * e2s_rf_half_pi_on_ms + e2s_xdwell_ms + e2s_zdwell_ms;  // SE1 these are floats
      else
         e2s_rf_cycle_time_ms = 8 * e2s_rf_half_pi_on_ms + 2 * (e2s_xdwell_ms + e2s_zdwell_ms);  // SE2 these are floats
      e2s_nprebeam_cycles =  parseInt(  0.5 + (prebeam_on_ms /  e2s_rf_cycle_time_ms)); // round up
      e2s_nbeam_cycles =     parseInt(  0.5 + (e2c_beam_on_ms /  e2s_rf_cycle_time_ms)); // round up
      e2s_necho_cycles = parseInt( 2 * e2s_nfid_cycles );
    

      e2s_tot_acq_ms  = 3 * e2s_nfid_cycles *  e2s_rf_cycle_time_ms;
  
      alert('RF cycle time (ms)='+ roundup( e2s_rf_cycle_time_ms,4)+' Num Prebeam Cycles= '+ e2s_nprebeam_cycles+ ' Num Beam cycles= '+ e2s_nbeam_cycles +' Num echo cycles= '+  e2s_necho_cycles + ' Total acq time (ms)='+ roundup(  e2s_tot_acq_ms,3));
     }
}


function add_display_channels()
{
    var i,text="";
   
    text='<tr><td class="param">Display Channel A:';
    text+='<select name="select_chanA" onChange="select_display_chan(1)" class="tune">';
    for(i=0; i<6; i++)  // extra value is "none"
	{
	    if (i == document.getElementById("last_selected_showchanA").value) // type=hidden
		sel="selected";
	    else
		sel="";
	    text+='<option value="0" '+sel+' >'+channels[i]+' </option>' ;
	}
    text+='</select></td>';
    
    text+='<td class="param">Display Channel B:';
    text+='<select name="select_chanB" onChange="select_display_chan(2)" class="tune">';
    for(i=0; i<6; i++)  // extra value is "none"
	{
	    if (i == document.getElementById("last_selected_showchanB").value) // type=hidden
		sel="selected";
	    else
		sel="";
	    text+='<option value="0" '+sel+' >'+channels[i]+' </option>' ;
	}
    text+='</select></td>';
    text +='</td></tr>';

    return text;
}

function check_consistency()
{
   var pattern_A = /2c/;
    var pattern_B = /1g|20/;  //  RFon_duration_dt 
    var pattern_C = /1[ab]|2[abde]/; // rf_on_time_ms 
    var pattern1_scan = /1[abfg]/;
    var pattern2_scan = /2[abce]/;
    var pattern2s = /2s/;
    var show_chanA,show_chanB;
    
    
    if(rstate != state_stopped)
	return; // do nothing 
    
    if(!have_psm)
	return; // no rf for this mode

    show_chanA = document.form1.last_selected_showchanA.value;
    show_chanB = document.form1.last_selected_showchanB.value;
    
    if(document.getElementById("psmerr") ==null) 
	{
	    alert(' check_consistency:  document.getElementById("psmerr") is not defined ' );
            return;
	}
    
    // clear error
    document.getElementById("psmerr").className="normal";
    document.getElementById("psmerr").innerHTML=""; // clear errors
    
    if(!channel_enabled[0])
	{ 
	    if( pattern1_scan.test(ppg_mode) || pattern2_scan.test(ppg_mode) || hole_burning_flag )  // a channel ought to be enabled
		{  // one of the PSM channels should be enabled for these modes - usually f0ch1
		    if(num_channels_enabled==0)
			{
			    document.getElementById("psmerr").className="bolderror";
			    document.getElementById("psmerr").innerHTML= "RF must be ENABLED when RFon duration > 0";
			}
		    else
			{
			    document.getElementById("psmerr").className="boldwarn";
			    document.getElementById("psmerr").innerHTML= "PSM f0 Channel 1 is usually used for this mode";
			}
		}
	    if(pattern_B.test(ppg_mode))  // 1g/20
		{
		    if (RFon_duration_dt <= 0) 
			{  // remove error in case it is set
			    if(document.getElementById("RFonDT").className == "error")
				document.getElementById("RFonDT").className="param";
			}
		}
	}
       else
	   { //  RF  is enabled (at least 1 channel) 
	       if (pattern_B.test(ppg_mode))
		   {  // 1g/20
		       // alert('check_consistency:  RFon_duration_dt '+RFon_duration_dt)
		       if (RFon_duration_dt <= 0)
			   {
			       if(document.getElementById("RFonDT") != undefined)
				   document.getElementById("RFonDT").className="error";
			       else 
				   alert('check_consistency: document.getElementById("RFonDT") = '+document.getElementById("RFonDT"));
			       
			       document.getElementById("psmerr").className="bolderror";
			       document.getElementById("psmerr").innerHTML = "RF should be disabled when RFon duration = 0";
			       
			       // if(typeof(document.getElementById("psm_on0")) != null)
			       // document.getElementById("psm_on0").className="error";
			   }
		       else
			   {  // no error
			       if(document.getElementById("RFonDT").className == "error")
				   document.getElementById("RFonDT").className="param";
			       
                               for(i=0; i<chan_max; i++)
				   { // check amplitude is > 0 for all enabled channels
				       if(channel_enabled[i] && amplitude[i]==0 )   // enabled with zero amplitude... no RF
					   {
					       document.getElementById("psmerr").className="bolderror";
					       document.getElementById("psmerr").innerHTML = "RF Amplitude should be set non-zero for enabled channel "+channels[i];
					   }
				   }
			       //      if(typeof(document.getElementById("psm_on0")) != null)
			       //  {	// put this in to check for error; will be null if channel 0 isn't displayed
			       //  if(document.getElementById("psm_on0").className=="error")
			       //   document.getElementById("psm_on0").className= profile_class[0]
			       //  }
			       
			   }
		       
		       
		   }
	       if (pattern_A.test(ppg_mode)  )
		   {   // e2c
		       if( e2c_rf_on_ms <= 0)
			   {
			       if(document.getElementById("e2cRFon") != undefined)
				   document.getElementById("e2cRFon").className="error";
			       //  document.getElementById("psm_on0").className="error";
			   }
		   }
	       if (pattern_C.test(ppg_mode)  )
		   { 
		       if(  rf_on_time_ms <= 0)
			   {
			       if(document.getElementById("RFonms") != undefined)
				   document.getElementById("RFonms").className="error";
			       //document.getElementById("psm_on0").className="error";
			   }
		   }
	       
	   }
}