/* setup_psmIII.h

Function prototypes

*/
#ifdef HAVE_PSMIII
#define MAX_CHANNELS 5 // f0Ch1 f0Ch2 f0Ch3 f0Ch4 f1 (ref)
INT init_freq_module(void);
INT init_freq_2 (void);
INT init_freq_20(void);
INT init_freq_1(void);
INT psm_setone(MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_Hz);
INT psm_loadFreqFile(MVME_INTERFACE *mvme, DWORD base_addr,  char* ppg_mode, BOOL random_flag);
INT psm_loadIQFile( MVME_INTERFACE *mvme, DWORD base_addr, char* ppg_mode, INT channel_index);
INT disable_psm(MVME_INTERFACE *mvme, DWORD base_addr);
INT init_psm( MVME_INTERFACE *mvme, DWORD base_addr);
INT setup_psmIII( MVME_INTERFACE *mvme, DWORD base_addr);
INT psm_load_iq(MVME_INTERFACE *mvme, DWORD base_addr);
INT psm_load_tuning_freq(MVME_INTERFACE *mvme, DWORD base_addr);
char *get_channel(INT n);
INT  psm_write_RFtripThreshold( MVME_INTERFACE *mvme, DWORD base_addr);
INT psm_LoadIdleFreqHz( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_hz);
INT psm_ReadIdleFreqHz( MVME_INTERFACE *mvme, DWORD base_addr);
INT psm_ReadIdleFreq( MVME_INTERFACE *mvme, DWORD base_addr);
INT psm_LoadIdleFreq ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq);
void psm_readsome(MVME_INTERFACE *mvme, DWORD base_addr);
INT psm_set_e1w_frequencies(MVME_INTERFACE *mvme, DWORD base_addr, INT fcw_index, INT *freq_hz_array);
#endif

