

/*-- individual scaler histo increment ------------------------------------*/
INT scaler_increment(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*
  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
*/
{
  DWORD *ps, i, h, ub, *pbuf, *pus, sdata;
  INT     channel_field;
  INT  index;
  INT ub_histo;
  INT first;
  pbuf = pfifo;
  /* user-bit histo number is hardcoded here */

  first=1;
  if (  strncmp(fs.input.experiment_name,"20",2) != 0)
    {
#ifdef TWO_SCALERS
      if(exp_mode == 1)
	ub_histo = N1_HISTO_MAXA -1;  // Type 1
      else
	ub_histo = N2_HISTO_MAXA -1;  // Type 2
#else
      if(exp_mode == 1)
	ub_histo = N1_HISTO_MAXB - 1; /* add user bit histo if one scaler only */
      else
	ub_histo = N2_HISTO_MAXB - 1;  // Type 2
#endif
      pus = histo[ub_histo].ph ;   /*  User bit histogram is hardcoded  */
      printf("ub_histo = %d\n",ub_histo);
    }
  else
    pus = NULL;
  
  
  if (dd[1]) printf("scaler_increment:  nwords:%d pfifo:%p, pus:%p\n",nwords, pbuf,pus);
  for (i=0 ; i < nwords ; i++)
    {
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
      ub = channel_field >>6;
      if (ub>4 || ub < 0)
	{
	  printf("\nub is not in limits : %i\n",ub);
	  return -1;
	}

      if (  strncmp(fs.input.experiment_name,"20",2) != 0)
	{ // All modes except type 20 - check each cycle userbits against the saved array	 
	  if (!fill_uarray && scaler_offset == 0)  // Scaler A (2 scalers) or Scaler B (1 scaler)
	    {
	      if(ub != *(pus + *gbl_bin))
		{
		  printf("Error current ub = %x; array ub = %x for i %d and h %d\n",
			 ub,*(pus + *gbl_bin),i,h);
		  // return -1;
		}
	    }
	} // end of except type 20  

      *userbit = ub; // fill return value  

      /* make sure channel is correct */
      if (h < maximum_channel)
	{
	  index = h + scaler_offset;
	  /* assign pointer */
	  ps = scaler[index].ps;
	  sdata = *pbuf & DATA_MASK; 
	  /* increment local cycle sum scaler */
	  scaler[index].sum_cycle += (double)sdata;
	  
	  /* temp measure for debugging*/
	  
	  if(dd[1])
	    {
	      if(i<=100 && h == 0){
		printf("i,pbuf,h,ub,sdata,gbl_bin: %d, %x, %d, %d, %x, %d\n",i,*pbuf,h,ub,sdata,*gbl_bin); 
	      }
	      if(i == nwords -1 )
		{
		  printf("Last i,pbuf,h,ub,sdata,gbl_bin: %d, %x, %d, %d, %x, %d\n",i,*pbuf,h,ub,sdata,*gbl_bin); 
		}
	    }
	  /* add current time bin content to cumulative scaler histo */
	  *(ps+*gbl_bin) += sdata;
	
#ifdef TWO_SCALERS
	  if(exp_mode == 1)
	    {
#ifdef THIRTYTWO
	      /* increment cycle sum according to User Bit */
	      if(index < 16)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index < 32)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#else  // TWO scaler inputs
	      /* increment cycle sum according to User Bit */
	      if(index ==0)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index ==1)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#endif // THIRTYTWO
	    }
#endif // TWO_SCALERS
	  
	  /* next incoming data */
	  pbuf++;
	  
	  /* Increment time bin every h = N_SCALER_REAL */
	  if (h == (maximum_channel-1) )
	    {
	      if (  strncmp(fs.input.experiment_name,"20",2) != 0)
		{ // All modes except type 20 - fill userbit histo once
		  if(fill_uarray && scaler_offset == 0)
		    {
		      if(first)printf("scaler_increment: filling ubit array; fill_uarray=%d \n",
				      fill_uarray);
		      first=0; // print message once
		      
		      *(pus + *gbl_bin) = ub;
		      if(dd[1]) 
			{
			  if(*gbl_bin < 5 ) 
			    printf ("update pus %p with ub %d\n",pus+*gbl_bin,ub);
			}
		    }
		} // end all modes except Mode 20
	      

	      // Type 2 modes :  check last bin for userbit 2
	      if(exp_mode == 2)
		{  // All type 2 modes have last bin marked with UB2
		  if(*gbl_bin == (n_bins-1) ) // last bin 
		    {
		      if (ub != 2) 
			printf("expect ub=2 on last bin for Type 20 (ub=%d *gbl_bin=%d n_bins=%d\n",
			       ub,*gbl_bin,n_bins);
		      else
			printf("last bin checked ub=2\n");
		    }
		} // end Type 2

	      (*gbl_bin)++;
	      if(dd[1])	printf("incremented gbl bin to %d; h=%d\n",*gbl_bin, h);
	    }
	}
      else
	printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x\n",i, nwords, h, *pbuf);    
    }
  return 0;
}
