/* setup_psmIII.c

This file included by febnmr.c if PSMIII is defined
$Log: setup_psmIII.c,v $
Revision 1.3  2019/05/29 22:51:05  suz
changes to mode 1w function

Revision 1.2  2019/05/02 23:08:42  suz
add mode 1w (CW) for bnmr

Revision 1.1  2018/08/22 23:57:09  suz
initial version



Initialize and setup the PSMIII


   ====================================================
   Routines for VME PSMIII frequency module  
   ====================================================
*/

/*---------------------------------------------------*/
INT init_freq_module(void)
/*---------------------------------------------------*/
{
  /* 
     Called from frontend_init
     Initialize and check the PSM
   */
#ifdef HAVE_PSMIII
  INT status,Ncmx;
  char str[128];
  DWORD data;
  

   dpsm=0; // debug
   // Reset is now done separately in bnmr_init

  /* See if we can read the temperature  ... is the module present */
  data=0;
  data = psmRegRead16(gVme, PSM_BASE,FPGA_TEMP);  // expect 0x94 for PSMII 0x84 for PSM   0 for PSMIII
  if(data == 0)
    {
      printf("init_freq_module: cannot read FPGA. PSMII module may not be present \n");
      return FE_ERR_HW ;
    }
  else if(data & 0x8000)
    {
      printf("init_freq_module: Illegal FPGA Temperature !!\n");
      return FE_ERR_HW ;
    }
  else
    {
      data = data >> 7;
      printf("init_freq_module: FPGA Temperature = %d degrees C (approx) \n",data);
    }
  printf("init_freq_module: detected a PSMIII module\n");

  Ncmx=4095; // PSMIII 
  /* Write Ncmx into the odb for rf_config to use */
  sprintf(str,"/Equipment/FIFO_acq/frontend/output/psm/max cycles iq pairs (Ncmx)");
  status = db_set_value(hDB,0,str,&Ncmx,sizeof(Ncmx),1,TID_INT);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR," init_freq_module","Error writing Ncmx=%d to ODB Key\"%s\"; cannot setup PSM (%d)",Ncmx,str,status);
      return FE_ERR_HW;
    }
  else
    printf("init_freq_module: wrote Ncmx=%d to ODB\n",Ncmx);

#ifdef BNMR
  data = psmRegWrite8(gVme, PSM_BASE ,  MODULE_OPERATING_MODE, 0 );
#else // BNQR (currently does not have PSMIII)
  data = psmRegWrite8(gVme, PSM_BASE ,  MODULE_OPERATING_MODE, 1 );
#endif

#endif
  return SUCCESS;
}


/*-------------------------------------------------------*/
INT init_freq_2 (void)

/*-------------------------------------------------------*/
{
  /* Called for all Type 2 freq table driven modes (i.e. NOT 20 2d,2s and 2w)
     
     If random_flag
       DOES NOT LOAD PSM frequency table
       Instead, frequencies will be randomized and loaded from a pointer each cycle 
         (in set_next_freq_2, called from cycle_start)
     If NOT random_flag
       Load the PSM frequency table (done once at begin-of-run)

     Writes end-of-sweep mode and Idle frequency 

     Loads iq table(s) 

*/

#ifdef HAVE_PSMIII
  DWORD status=0;
  DWORD nfreq,nf;
  char frqfile[64];
  DWORD first_frequency=-1;
  DWORD value=0;
  //  DWORD data,offset;

  if(!ftable_driven_mode)
    {
      printf("init_freq_2: error  - do not use this routine for non-frequency-table-driven modes. Use init_freq_20\n");
      return FE_ERR_HW;
    }


  printf("init_freq_2: starting, calling init_psm\n");
  status = init_psm(gVme, PSM_BASE); /* initialize psm */
  if(status != SUCCESS)
    {
      printf("init_freq_2: error from init_psm\n");
      return(FE_ERR_HW);
    }

  if(random_flag)
    { // randomized freq values will be loaded each cycle
      printf("init_freq_2: random_flag is true \n");
      /* Jump to idle at end of frequency sweep or stay at last value  */
      if(fs.hardware.psm.freq_end_sweep_jump_to_idle)
	{ 
	  status=SUCCESS; // default for later
	  /* jump to idle; if initial freq value is to be loaded into idle it will be done each cycle */
	  if(!fs.hardware.psm.freq_sweep_load_1st_val_in_idle)
	    {
	      /* load fs.hardware.psm.idle_freq__hz_  into Idle */
	      printf("init_freq_2: loading requested idle freq (%d Hz) into PSM Idle \n",value);
	      status = psm_LoadIdleFreqHz( gVme, PSM_BASE,fs.hardware.psm.idle_freq__hz_); /* Hz value to load */
	      if(status == SUCCESS)
		{
		  value = psm_ReadIdleFreqHz( gVme, PSM_BASE);
		  printf("init_freq_2: read back PSM Idle frequency as %d Hz\n",value);
		}
	    }
	}
    }
  else
    { // NOT random_flag
      sprintf(frqfile,"%s/%s.psm",fs.input.cfg_path,ppg_mode);
      if(dpsm)
	printf("init_freq_2: loading frequency file %s (random flag is %d)\n",frqfile,random_flag);
      status = psmLoadFreqFile(gVme, PSM_BASE, frqfile, &nfreq, &first_frequency); 
      
      if(status == -1)
	{
	  printf("init_freq_2: error loading PSM frequency file\n");
	  cm_msg(MERROR,"init_freq_2","Error: frequency file %s is not loaded", frqfile);
	  return(FE_ERR_HW);
	}
      
      printf("init_freq_2: successfully downloaded %d frequency values into PSM\n",nfreq);
      // if(dpsm)
	{
	  nf=nfreq;
	  // Reading back the first 20 words of frequency data
	  if(nf > 20) 
	    nf=20;
	  printf("init_freq_2: reading back the first %d frequency words:\n",nf);
	  psmReadFreqDM(gVme, PSM_BASE, nf, 0);  // read the data back starting at DM 0
	}
      
      // Load end-of-sweep registers
      /* Jump to idle at end of frequency sweep or stay at last value  */


      if(fs.hardware.psm.freq_end_sweep_jump_to_idle)
	{ 
	  /* jump to idle; load initial freq value into idle? */
	  if(fs.hardware.psm.freq_sweep_load_1st_val_in_idle)
	    status = psm_LoadIdleFreq( gVme, PSM_BASE,first_frequency); /* hex value to load */
	} 
      else
	{
	  /* load Idle value fs.hardware.psm.idle_freq__hz_   */
	  status = psm_LoadIdleFreqHz( gVme, PSM_BASE,fs.hardware.psm.idle_freq__hz_); /* Hz value to load */
	}
      
      if(status == SUCCESS)
	{
	  value = psm_ReadIdleFreqHz( gVme, PSM_BASE);
	  printf("init_freq_2: read back PSM Idle frequency as %d Hz\n",value);
	}
    } // end of non-random

  // All 
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"init_freq_2","error return from loading idle frequency");
      printf("init_freq_2: error return from loading idle frequency\n");
      return FAILURE;
    }

  // Now load I,Q for all enabled channels by calling psm_load_iq
  
  /*   psm_load_iq   for all enabled channels...
          If Quad Mode:  
             load the IQ pair file if required
             load idle IQ 	         
          If single tone:
             load the Idle IQ pair  512,0

          Set the IQ length register
   */

  printf("init_freq_2: calling psm_load_iq to load i,q idle pair and/or i,q table\n"); 
  status=psm_load_iq(gVme, PSM_BASE);
  if(status != SUCCESS)
    return status;

  if(random_flag)
    return SUCCESS;

 
  psm_readsome(gVme, PSM_BASE);
  
  printf("init_freq_2: done \n");
#endif
  return SUCCESS;
}

/*-------------------------------------------------------*/
INT init_freq_20(void)
/*-------------------------------------------------------*/
{
  /* Load the PSM frequency for type 20 (SLR) and 2d 2s 2w only
      (these are NOT frequency-table-driven i.e. use idle frequency.
       An iq table may still be loaded for modulation )
  */

#ifdef HAVE_PSMIII
  INT value,status;
  //  INT offset,data;

  printf(" init_freq_20: setting up PSM for non-frequency-table experiments i.e. SLR (20) 2d SpinEcho(2s) or Wurst(2w)\n");
  if(init_psm(gVme, PSM_BASE) == -1) return(FE_ERR_HW);
  
  /* The Idle frequency is in  fs.hardware.psm.idle_freq__hz_ 
  */

  printf("\n init_freq_20: Now loading PSM Idle frequency %d Hz \n",
	 fs.hardware.psm.idle_freq__hz_);
  status = psm_LoadIdleFreqHz (gVme, PSM_BASE,fs.hardware.psm.idle_freq__hz_);
  if(status == SUCCESS ) 
    {
      value = psm_ReadIdleFreqHz( gVme, PSM_BASE);
      printf(" init_freq_20: read back PSM Idle frequency as %d Hz\n",status);
    }
  else
    {
      cm_msg (MERROR," init_freq_20"," error writing PSM idle frequency %dHz",
	      fs.hardware.psm.idle_freq__hz_);
      return(FE_ERR_HW);
    }
  
  // For PSMIII make sure frequency sweep length register is set to 0 
  // or a frequency sweep reset will sent the address to 0x400 (bug in the board)

  // now  psmFreqSweepAddrReset sets the length to zero
  /* offset =  FREQ_SWEEP_LENGTH;
  value=0;
  data = psmRegWrite16(gVme, PSM_BASE, offset, value);  // 16 bit write 
    printf("init_freq_20: Clearing frequency Sweep Length register wrote 0x%x or %d, read back 0x%x or %d\n",
	 data,data,value,value);
  if(data != 0)
    {
      printf("init_freq_20: expect frequency sweep length register to be 0. Read back 0x%x %d\n",
	     data,data);
      cm_msg(MERROR,"init_freq_20","expect frequency sweep length register to be 0. Read back 0x%x %d",
	     data,data);
      return FAILURE;
    }
  */
 

 

  /* IQ pairs 
     psm_load_iq
     loads IQ pair and/or IQ file for all enabled profiles  
  */
  status = psm_load_iq(gVme, PSM_BASE);
  
  if(status != SUCCESS)
    {
      printf("init_freq_20: error from psm_load_iq\n");
      return status;
    }


  printf("init_freq_20: doing a psmFreqSweepAddrReset to set Freq Address Pointer to Idle\n");
  status = psmFreqSweepAddrReset(gVme, PSM_BASE); //function psmFreqSweepAddrReset sets the length to zero (bug)
  psm_readsome(gVme, PSM_BASE);


  printf(" init_freq_20: Sending an internal strobe command to the PSM  (type 20) \n");
  psmFreqSweepStrobe (gVme, PSM_BASE); 
#endif

  return SUCCESS;
}


void psm_readsome(MVME_INTERFACE *mvme, DWORD base_addr)
{
  INT nbits;
  INT offset;
  DWORD data1,data2,data3,data4,data5;

  offset = IDLE_FREQ_OFFSET; // Offset from freq sweep DM base
  nbits=32;
  data1 = psmRegReadFreqDM( mvme, base_addr , offset );
  printf("Idle Freq (offset 0x%4.4x) = %d Hz  (hex value= 0x%x) \n",offset, get_Hz(data1),data1);
  /* Frequency sweep address (11 bits) */ 
  offset =  FREQ_SWEEP_ADDRS;
  data1=(psmRegRead16(mvme, base_addr, offset )) &  ELEVEN_BIT_MASK;
  printf("Frequency sweep address  =     0x%3.3x (%d)\n",data1,data1);

  /* Frequency sweep length (11 bits) */
  offset =  FREQ_SWEEP_LENGTH;
  data1=(psmRegRead16(mvme, base_addr, offset )) &  ELEVEN_BIT_MASK;
  printf("Frequency sweep length  =     0x%3.3x (%d)\n",data1,data1);

  nbits=11; // 11 bits
  data1 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C1 + IQ_DM_LEN ) & 0x7FF; // 11 bits
  data2 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C2 + IQ_DM_LEN) & 0x7FF; 
  data3 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C3 + IQ_DM_LEN ) & 0x7FF; 
  data4 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C4 + IQ_DM_LEN ) & 0x7FF; 
  data5 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F1    + IQ_DM_LEN ) & 0x7FF; 
  printf("IQDM  Len  0x%2.2x    %2.2d   |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x  \n",
	 IQ_DM_LEN, nbits, data1,data2,data3,data4,data5);

  nbits=11; // 11 bits
  data1 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C1 + IQ_DM_ADDR ) & 0x7FF; // 11 bits
  data2 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C2 + IQ_DM_ADDR) & 0x7FF; 
  data3 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C3 + IQ_DM_ADDR ) & 0x7FF; 
  data4 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C4 + IQ_DM_ADDR ) & 0x7FF; 
  data5 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F1    + IQ_DM_ADDR ) & 0x7FF; 
  printf("IQDM Addr  0x%2.2x    %2.2d   |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x  \n",
	 IQ_DM_ADDR, nbits,data1,data2,data3,data4,data5);



  offset = IDLE_IQ_OFFSET;
  nbits=10;
  printf("I,Q Idle 0x%4.4x    %2.2d   ",
	 offset, nbits);

   char str[10];
   INT selected_channel,I,Q;
   DWORD base_iq_addr;
   for(selected_channel=1; selected_channel< 6; selected_channel++)   // selected_channel goes from 1,6
     {  // channels 1-5
       base_iq_addr=get_IQ_BaseAddr(selected_channel); 
       data1 = psmRegReadIQDM( mvme,  base_addr, base_iq_addr, offset, &I, &Q );
       sprintf(str,"%d,%d",I,Q);
       printf("|%9.9s",str);
     }
   printf("\n");
   return;
}



/*-------------------------------------------------------*/
INT init_freq_1(void)
/*-------------------------------------------------------*/
{

  /* Setup the PSM for Type 1 experiment modes except 1w (fcw mode)
     that use frequency
  */


#ifdef HAVE_PSMIII
  INT offset,value,status;
  DWORD data;
  printf(" init_freq_1: setting up PSM for Type 1 experiments with frequency scan\n");

  /* Initialize psm  */
  if(init_psm(gVme, PSM_BASE) == -1)  // this will call setup_PSMIII or  setup_PSMIII_cw for mode 1w
    return(FE_ERR_HW);

  if(mode1w_flag)
    return SUCCESS;
 
  /* loads IQ pair (or file) for all enabled profiles  */
  status = psm_load_iq(gVme, PSM_BASE);
  if(status != SUCCESS)
    return status;

  // frequency will be loaded into Idle by psm_setone
  printf("  init_freq_1: Loaded I,Q pair(s)  (Type 1 expt) \n");

  // For PSMIII make sure frequency sweep length register is set to 0 
  // or a frequency sweep reset will sent the address to 0x400 (bug in the board)
  offset =  FREQ_SWEEP_LENGTH;
  value=0;
  data = psmRegWrite16(gVme, PSM_BASE, offset, value);  /* 16 bit write */
  printf("init_freq_1: Clearing frequency Sweep Length register wrote 0x%x or %d, read back 0x%x or %d\n",
	 data,data,value,value);
  if(data != 0)
    {
      printf("init_freq_1: expect frequency sweep length register to be 0. Read back 0x%x %d\n",
	     data,data);
      cm_msg(MERROR,"init_freq_1","expect frequency sweep length register to be 0. Read back 0x%x %d",
	     data,data);
      return FAILURE;
    }

  psm_readsome(gVme, PSM_BASE);

#endif

  return SUCCESS;
}


INT setup_psmIII_cw( MVME_INTERFACE *mvme, DWORD base_addr)
{
#ifdef HAVE_PSMIII
  INT gate_control;
  BOOL enabled;
  INT channel_index;
  INT scale_factor;
  char str[8];
  INT j;
  DWORD data,offset;
  char channel_name[7];
  INT selected_channel=-1,status;
  INT ne; // counter for number of enabled PSM f0 channels (not ref f1)

  enabled=scale_factor=0; 
  gate_control=1;
  ne=0; // count number of enabled f0 channels

  printf("setup_psmIII_cw: starting\n");

  // Check operating mode parameter is set to "cw"
  strncpy(str,fs.hardware.psm.operating_mode,8);
  for(j=0; j<strlen(str);j++)
    str[j] = tolower (str[j]);

  if(strncmp(str,"cw",2)==0)
    { // set module into  CW_OPERATING_MODE
      data = psmRegWrite8(mvme, base_addr ,  MODULE_OPERATING_MODE, CW_OPERATING_MODE );
      printf("setup_psmIII_cw: set operating mode to CW, i.e. wrote %d, read back %d\n",CW_OPERATING_MODE,data);
      if( (data&3) != CW_OPERATING_MODE)
	{
	  cm_msg(MERROR,"setup_psmIII_cw","failure setting PSMIII into CW operating mode");
	  return FE_ERR_HW;
	}
    }
  else
    {
      cm_msg(MERROR,"init_freq_1w","PSM operating mode must be set to \"CW\" for mode 1w, not %s",
	     fs.hardware.psm.operating_mode);
      return DB_INVALID_PARAM;
    }



  for (channel_index=0; channel_index <MAX_CHANNELS; channel_index++)
    {
      switch (channel_index)
	{
	case 0: // f0Ch1
	  if(fs.hardware.psm.f0ch1.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII_cw: setting up channel=%s (index %d)\n",channel_name,channel_index);
	      scale_factor = fs.hardware.psm.f0ch1.scale_factor__def_181_max_255_;
	      gate_control = fs.hardware.psm.f0ch1.gate_control;
	      ne++; // count number of enabled f0 channels
	    }
	  break;

	case 1: // f0Ch2
	  if(fs.hardware.psm.f0ch2.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII_cw: setting up channel=%s (index %d)\n",channel_name,channel_index);
	      scale_factor = fs.hardware.psm.f0ch2.scale_factor__def_181_max_255_;
	      gate_control = fs.hardware.psm.f0ch2.gate_control;
	      ne++; // count number of enabled f0 channels
	    }
	  break;

	case 2: // f0Ch3
	  if(fs.hardware.psm.f0ch3.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII_cw: setting up channel=%s (index %d)\n",channel_name,channel_index);
	      scale_factor = fs.hardware.psm.f0ch3.scale_factor__def_181_max_255_;
	      gate_control = fs.hardware.psm.f0ch3.gate_control;
	      ne++; // count number of enabled f0 channels
	    }
	  break;


	case 3: // f0Ch4
	  if(fs.hardware.psm.f0ch4.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII_cw: setting up channel=%s (index %d)\n",channel_name,channel_index);
	      scale_factor = fs.hardware.psm.f0ch4.scale_factor__def_181_max_255_;
	      gate_control = fs.hardware.psm.f0ch4.gate_control;
	      ne++; // count number of enabled f0 channels
	    }
	  break;


	case 4:
	  if(fs.hardware.psm.f1.channel_enabled)
	    {
	      enabled=1;
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII_cw: setting up channel=%s (index %d)\n",channel_name,channel_index);
	      scale_factor = fs.hardware.psm.f1.scale_factor__def_181_max_255_;
	      gate_control = fs.hardware.psm.f1.gate_control;
	    }
	  break;

	default:
	  enabled=0;
	} // switch
 
      if(dpsm)
	{
	  printf("\nsetup_psmIII_cw:\n");
	  printf("Channel %s index %d selected_channel=%d :\n",channel_name,channel_index,selected_channel);
	  printf("Scale Factor = %d\n",scale_factor);
	  printf("Gate control =  %d\n\n",gate_control );
	}

      if(enabled)
	{
	  switch (gate_control)
	    {
	    case FP_GATE_DISABLED:
	      cm_msg(MERROR,"setup_psmIII_cw","WARNING: Front Panel Gate is Disabled. Use only for testing");
	      printf("setup_psmIII_cw: WARNING: Front panel gate is disabled\n");
	      break;
	    case  GATE_NORMAL_MODE:
	      printf("setup_psmIII_cw: Front panel gate is enabled (default mode)\n");
	      break;
	    case  GATE_PULSE_INVERTED:
	      cm_msg(MERROR,"setup_psmIII_cw","WARNING: Front Panel Gate Pulse Inverted. Use only for testing");
	      printf("setup_psmIII_cw: WARNING:Front panel gate pulse inverted\n");
	      break;
 	    case   INTERNAL_GATE:
	      cm_msg(MERROR,"setup_psmIII_cw","WARNING: Front Panel Gate Pulse Ignored and Gates are ALWAYS ON. Use only for testing");
	      printf("setup_psmIII_cw: WARNING Front panel gate pulse ignored. Gate are ALWAYS ON\n");
	      break;
            default:
	      cm_msg(MERROR,"setup_psmIII_cw","WARNING: Illegal gate parameter for channel %s. Using default mode",channel_name);
	      printf("setup_psmIII_cw:  Illegal gate parameter for channel %s. Using default mode\n",channel_name);
              gate_control=1;
	    } // switch
	  
	  /* write gate control for this channel */
	  
	  
	  if(dpsm)printf("\nNow writing gate control = %d for channel \"%s\"   \n",gate_control,channel_name);
	  status = psmWriteGateControl (mvme, base_addr, selected_channel, gate_control); 
	  if(status == -1)
	    {
	      cm_msg(MERROR,"setup_psmIII_cw","error return from psmWriteGateControl for channel \"%s\" ",channel_name);
	      return(status);
	    }
	  
	  
	  if(dpsm)printf("after writing gate control to channel \"%s\", read back  = 0x%x\n",channel_name,status);
	  
	  
	  /* Write the CW scale factor  */
	  if (scale_factor < 0 )
	    {
	      cm_msg(MERROR,"setup_psmIII_cw","Illegal scale factor (%d) for channel \"%s\" index=%d; it should be between 0 and 255",
		     scale_factor,channel_name,channel_index);
	      return -1;
	    }

	  else if (scale_factor > 255)
	    {
	      cm_msg(MINFO,"setup_psmIII_cw","illegal scale factor (%d) for channel \"%s\" index=%d; it will be set to default i.e. 80",
		     scale_factor,channel_name,channel_index);
	      scale_factor = 80;
	    }
	  if(channel_index < 4) // Channels f1ch1-4
	    {
	      offset =  FCW_SCALE_FACTOR + channel_index;
	      printf("setup_psmIII_cw: setting cw scale factor for channel \"%s\" index=%d to %d (0x%x) at offset 0x%x\n",
		     channel_name,channel_index,scale_factor,scale_factor,offset);
	      data = psmRegWrite8(mvme, base_addr, offset, scale_factor);
	      if(data != scale_factor)
		{
		  cm_msg(MERROR,"setup_psmIII_cw",
			 "wrote Fcw Scale Factor for channel  \"%s\" index=%d at offset 0x%x as 0x%x (%d); read back 0x%x",
			 channel_name,channel_index,offset,scale_factor,scale_factor,data);
		  return  FE_ERR_HW;
		}
	    }
	  else if(channel_index == 4)
	    { // f1 ref channel
	      status = psmSetScaleFactor(mvme, base_addr, selected_channel, scale_factor);
	      if(status == -1)
		{
		  cm_msg(MERROR,"setup_psmIII_cw","error return from psmSetScaleFactor for channel %s",channel_name);
		  return status;
		}

	      status = psm_load_tuning_freq(mvme, PSM_BASE); /* load tuning freq if channel f1 is enabled */
	      if (status != SUCCESS)
		return status;
	    }
	} /* end of this channel enabled */
    } // end of for loop

 // Check number of enabled PSM channels against number required by the function file
 if(ne != e1w_num_enabled_psmch)
    {
      cm_msg(MERROR,"read_freq_file_1w","number of enabled PSM channels is %d. Does not agree with value (%d) from reading file %s",
	     ne,e1w_num_enabled_psmch, e1w_freqfile);
      return DB_INVALID_PARAM;
    }
   if(e1w_num_enabled_psmch == 0)
    {
      cm_msg(MERROR,"setup_psmIII_cw","number of enabled PSMIII channels cannot be %d. At least one channel must be enabled",
	     e1w_num_enabled_psmch);
      return DB_INVALID_PARAM;
    }
   printf("setup_psmIII_cw: number of enabled f0 channels is %d\n",  e1w_num_enabled_psmch);
#endif
  return(SUCCESS);
}// end setup_psmIII_cw



INT psm_setone(MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_Hz)
{
#ifdef HAVE_PSMIII
  /* set one frequency for use in Type 1
     Load freq into idle and strobe

     Note: init_freq_* should have been called before calling this routine.
       init_freq_* call init_psm.

       freq_Hz is unsigned long (DWORD)
 */
  INT status;
  DWORD data;

  if(dpsm)
    printf("\npsm_setone: now loading frequency %uHz into IDLE\n",freq_Hz);
  status = psm_LoadIdleFreqHz (mvme, base_addr,freq_Hz);
  if(status == -1)

    {
      cm_msg(MERROR,"psm_setone","error writing idle frequency");
      return(status);
    }
  else
    {  // already checked values but can recheck

      data = psm_ReadIdleFreqHz( mvme, base_addr); // read back in Hz
      if(dpsm)
	printf("psm_setone: read back Idle frequency as %dHz\n",data);
      if(data != freq_Hz)
	{
	  cm_msg(MERROR,"psm_setone","read back %dHz for Idle, but set value was %dHz",
		 data,freq_Hz);
	  return FE_ERR_HW;
	}
    }
   
  if(dpsm)
    printf("\npsm_setone: Now resetting mem addr pointer & loading IDLE  (psmFreqSweepMemAddrReset)\n");
  /* load the IDLE freq */
  status = psmFreqSweepAddrReset (mvme, base_addr);//function psmFreqSweepAddrReset sets the length to zero (bug)

  if(dpsm)
    printf("\npsm_setone: Now writing internal strobe register\n");
  status = psmFreqSweepStrobe (mvme, base_addr);
#endif
  return status;
}

INT psm_set_e1w_frequencies(MVME_INTERFACE *mvme, DWORD base_addr, INT fcw_index, INT *freq_hz_array)
{
  /* Mode 1w (CW) only
     sets one or more frequencies from arrays fscanf1..fscanf4
  */
#ifdef HAVE_PSMIII

  INT i,channel_index;
  DWORD offset;
  INT data;

  if(!mode1w_flag)
    return DB_INVALID_PARAM;

  if(dpsm)
    {
      printf("psm_set_e1w_frequencies: starting with num_enabled_psmch=%d and fcw_index=%d\n",e1w_num_enabled_psmch,fcw_index);
      printf("psm_set_e1w_frequencies: writing frequency scan data at index %d to PSM:\n",fcw_index);
      printf(" enabled channels ch1,2,3,4 : %d %d %d %d ",
	     fs.hardware.psm.f0ch1.channel_enabled,
	     fs.hardware.psm.f0ch2.channel_enabled,
	     fs.hardware.psm.f0ch3.channel_enabled,
	     fs.hardware.psm.f0ch4.channel_enabled);
      if(fscanf1)printf("fscanf1[%d]=%d ",fcw_index,fscanf1[fcw_index]);
      if(fscanf2)printf("fscanf2[%d]=%d ",fcw_index,fscanf2[fcw_index]);
      if(fscanf3)printf("fscanf3[%d]=%d ",fcw_index,fscanf3[fcw_index]);
      if(fscanf4)printf("fscanf4[%d]=%d ",fcw_index,fscanf4[fcw_index]);
      printf("\n");
    }

    for(i=0;i<4;i++)
      freq_hz_array[i]=0;

  // Write the frequency in CW Mode
  for (channel_index=0; channel_index < (MAX_CHANNELS-1); channel_index++) // Reference f1 is included in MAX_CHANNELS
    {
      switch (channel_index)
	{
	case 0: // f0Ch1
	  if(fs.hardware.psm.f0ch1.channel_enabled)
	    {
	      if(fscanf1==NULL)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","fscanf1 is null; no frequency value supplied for f0ch1 ");
		  return DB_INVALID_PARAM;
		}
	      if(fscanf1[fcw_index]==0)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","illegal frequency value at fscanf1[%d]=%d for f0ch1; maybe empty function ",
			 fcw_index,fscanf1[fcw_index]);
		  return DB_INVALID_PARAM;
		}

	      offset = FCW_TUNING_FREQ + (4 * (channel_index));
	      if(dpsm)
		printf("psm_set_e1w_frequencies: writing frequency %d Hz to f0ch1 at offset 0x%x\n",fscanf1[fcw_index],offset);
	      data = psmWrite_reference_freq_Hz(mvme, base_addr, offset, fscanf1[fcw_index]);
	      if(data == FAILURE)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","failure writing frequency %d Hz at offset 0x%x  for channel f0Ch1 (channel_index=%d)",
			 fscanf1[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}

	      else if(data != fscanf1[fcw_index])
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","readback %d Hz disagrees with frequency %d Hz at offset 0x%x  for channel f0Ch1 (channel_index=%d)",
			 data,fscanf1[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}
	      freq_hz_array[0]= fscanf1[fcw_index];
	    }
	  break;

	case 1: // f0Ch2
	  if(fs.hardware.psm.f0ch2.channel_enabled)
	    {
	      if(fscanf2==NULL)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","fscanf2 is null; no frequency value supplied for f0ch2 ");
		  return DB_INVALID_PARAM;
		}
	      if(fscanf2[fcw_index]==0)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","illegal frequency value at fscanf2[%d]=%d for f0ch2 ",
			 fcw_index,fscanf2[fcw_index]);
		  return DB_INVALID_PARAM;
		}

	      offset = FCW_TUNING_FREQ + (4 * (channel_index));
	      if(dpsm)
		printf("psm_set_e1w_frequencies: writing frequency %d Hz to f0ch2 at offset 0x%x\n",fscanf2[fcw_index],offset);
	      data = psmWrite_reference_freq_Hz(mvme, base_addr, offset, fscanf2[fcw_index]);
	      if(data == FAILURE)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","failure writing frequency %d Hz at offset 0x%x  for channel f0Ch2 (channel_index=%d)",
			 fscanf2[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}

	      else if(data != fscanf2[fcw_index])
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","readback %d Hz disagrees with frequency %d Hz at offset 0x%x  for channel f0Ch2 (channel_index=%d)",
	
			 data,fscanf2[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}
	      freq_hz_array[1]= fscanf2[fcw_index];
	    }
	  break;

	case 2: // f0Ch3
	  if(fs.hardware.psm.f0ch3.channel_enabled)
	    {
	      if(fscanf3[fcw_index]==0)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","illegal frequency value at fscanf3[%d]=%d for f0ch3 ",fcw_index,fscanf3[fcw_index]);
		  return DB_INVALID_PARAM;
		}

	      offset = FCW_TUNING_FREQ + (4 * (channel_index));
	      if(dpsm)
		printf("psm_set_e1w_frequencies: writing frequency %d Hz to f0ch3 at offset 0x%x\n",fscanf3[fcw_index],offset);
	      data=psmWrite_reference_freq_Hz(mvme, base_addr, offset, fscanf3[fcw_index]);
	      if(data == FAILURE)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","failure writing frequency %d Hz at offset 0x%x  for channel f0Ch3 (channel_index=%d)",
			 fscanf3[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}

	      else if(data != fscanf3[fcw_index])
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","readback %d Hz disagrees with frequency %d Hz at offset 0x%x  for channel f0Ch3 (channel_index=%d)",
			 data,fscanf3[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}
	      freq_hz_array[2]= fscanf3[fcw_index];
	      break;
	    }

	case 3: // f0Ch4
	  if(fs.hardware.psm.f0ch4.channel_enabled)
	    {
	      if(fscanf4[fcw_index]==0)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","illegal frequency value at fscanf4[%d]=%d for f0ch4 ",fcw_index,fscanf4[fcw_index]);
		  return DB_INVALID_PARAM;
		}

	      offset = FCW_TUNING_FREQ + (4 * (channel_index));
	      if(dpsm)
		printf("psm_set_e1w_frequencies: writing frequency %d Hz to f0ch4 at offset 0x%x\n",fscanf4[fcw_index],offset);
	      data=psmWrite_reference_freq_Hz(mvme, base_addr, offset, fscanf4[fcw_index]);
	      if(data == FAILURE)
		{
		  cm_msg(MERROR,"psm_set_e1w_frequencies","failure writing frequency %d Hz at offset 0x%x  for channel f0Ch4 (channel_index=%d)",
			 fscanf4[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}

	      else if(data != fscanf4[fcw_index])
		{
 
		  cm_msg(MERROR,"psm_set_e1w_frequencies","readback %d Hz disagrees with frequency %d Hz at offset 0x%x  for channel f0Ch4 (channel_index=%d)",
			 data,fscanf4[fcw_index],offset,channel_index);
		  return DB_INVALID_PARAM;
		}
	        freq_hz_array[3]= fscanf4[fcw_index];
	    }
	  break;
	default:
	  cm_msg(MERROR,"psm_set_e1w_frequencies","illegal channel index for CW mode (%d)",channel_index);
	  return DB_INVALID_PARAM;
	}

    }
#endif
  return SUCCESS;
}
#ifdef GONE  // moved into init_freq_2 
INT psm_loadFreqFile(MVME_INTERFACE *mvme, DWORD base_addr,  char* ppg_mode, BOOL random_flag)
{
#ifdef HAVE_PSMIII
  DWORD status=0;
  DWORD nfreq;
  char frqfile[64];
  DWORD first_frequency=-1;
  DWORD value;
  
  value=0;
  /* If random_flag is FALSE
       loads frequency file into the PSM
     
     If random_flag is TRUE
         randomized frequency values will be loaded from an array by a call to psm_loadFreqDM_ptr
     
     Both routines also set the frequency length register


     called by init_freq_2  from begin_of_run for Type 2 runs
    */
  printf("psm_loadFreqFile: starting\n");
  sprintf(frqfile,"%s/%s.psm",fs.input.cfg_path,ppg_mode);
        

  if(dpsm)
    printf("\npsm_loadFreqFile:Now loading frequency file %s (random flag is %d)\n",frqfile,random_flag);

   if(random_flag)
    {
     if(dpsm || dd[13] )printf("psm_loadFreqFile:NOT calling LoadFreqDM_ptr with prandom_freq=%p \n",prandom_freq);
  //   status = psmLoadFreqPtr(mvme, base_addr, prandom_freq,  fs.output.num_frequency_steps, &first_frequency);
    }
  else
    status = psmLoadFreqFile(mvme, base_addr,frqfile, &nfreq, &first_frequency); 

  if(status == -1)
    {
      //  if(random_flag)
      //	cm_msg(MERROR,"psm_loadFreqFile","Error: frequency values at pointer %p are not loaded", prandom_freq);
      //  else
	cm_msg(MERROR,"psm_loadFreqFile","Error: frequency file %s is not loaded", frqfile);
      return(FE_ERR_HW);
    }
   
  if(!random_flag)
    {
      printf("psm_loadFreqFile: successfully downloaded %d frequency values into PSM\n",nfreq);
      if(dpsm)
	{
	  DWORD nf;
	  nf=nfreq;
	  // Reading back the first 20 words of frequency data
	  if(nf > 20) 
	    nf=20;
	  printf("psm_loadFreqFile: reading back the first %d frequency words:\n",nf);
	  psmReadFreqDM(mvme, base_addr, nf, 0);  // read the data back starting at DM 0
	}
    }



  /* Jump to idle at end of sweep or stay at last value (for all enabled profiles) */
  if(fs.hardware.psm.freq_end_sweep_jump_to_idle)
    { 
      /* jump to idle; load initial freq value into idle? */
      if(fs.hardware.psm.freq_sweep_load_1st_val_in_idle)
	{
	  /*  first value will be loaded into Idle */
	  status = psm_LoadIdleFreq(mvme,base_addr,first_frequency); /* hex value to load */
	} 
      else
	{
	  /* load fs.hardware.psm.idle_freq__hz_  into Idle */
	  status = psm_LoadIdleFreqHz(mvme,base_addr,fs.hardware.psm.idle_freq__hz_); /* Hz value to load */
	}
      if(status == SUCCESS)
	{
	  value = psm_ReadIdleFreqHz( gVme, PSM_BASE);
	  printf("psm_loadFreqFile: read back PSM Idle frequency as %d Hz\n",value);
	}
      else
	{
	  cm_msg(MERROR,"psm_loadFreqFile","error return from loading idle frequency");
	  printf("psm_loadFreqFile: error return from loading idle frequency\n");
	  return FAILURE;
	}
    }
#endif
  return SUCCESS;
}
#endif // GONE


INT psm_loadIQFile( MVME_INTERFACE *mvme, DWORD base_addr, char* ppg_mode, INT channel_index)
{
#ifdef HAVE_PSMIII
  // Called in Quad Mode. Channel_index 0-4
  //                      selected_channel for PSM routines 1-5  (i.e. channel_index+1)
  // 
  //    Loads a file into IQ memory 
  //    Loads idle i,q pair depending on ODB parameters
  
  INT status;
  char IQfile[64];
  INT npairs,I,Q;
  INT first_i,first_q;
  DWORD base_iq_addr;
  
  INT Ncic,Niq;
  BOOL jump_to_idle_iq,load_first_val_in_idle;
  INT idle_i,idle_q;
  char channel_name[7];
  INT selected_channel=0; // unknown ch
  
  sprintf(channel_name, "%s", get_channel(channel_index));
  
  /* Load an IQ pairs file into the PSM (file is 2's compliment data)    
     called if quadrature mode is enabled

     Note:  init_psm must be called before calling this routine 
   */

  printf(" psm_loadIQFile starting with channel \"%s\" index %d\n",channel_name,channel_index);

 
  switch (channel_index)
    {
    case 0:
      //      Ncic =fs.output.psm.cic_interpoln_rate__ncic_[channel_index];   // Ncic PSMIII?
      Niq = fs.output.psm.num_iq_pairs__niq_[channel_index];
      selected_channel = channel_index +1;
      jump_to_idle_iq =fs.hardware.psm.f0ch1.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.f0ch1.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.f0ch1.iq_modulation.idle_i__511__i__512_;
      idle_q =fs.hardware.psm.f0ch1.iq_modulation.idle_q__511__q__512_;

      base_iq_addr = BASE_DM_F0_CH1_IQ;
      break;

    case 1:
      Ncic =fs.output.psm.cic_interpoln_rate__ncic_[channel_index];
      Niq = fs.output.psm.num_iq_pairs__niq_[channel_index];
      selected_channel = channel_index +1;
      jump_to_idle_iq =fs.hardware.psm.f0ch2.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.f0ch2.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.f0ch2.iq_modulation.idle_i__511__i__512_;
      idle_q =fs.hardware.psm.f0ch2.iq_modulation.idle_q__511__q__512_;

      base_iq_addr = BASE_DM_F0_CH2_IQ;
      break;

    case 2:
      //   Ncic =fs.output.psm.cic_interpoln_rate__ncic_[channel_index];
      Niq = fs.output.psm.num_iq_pairs__niq_[channel_index];
      selected_channel = channel_index +1;
      jump_to_idle_iq =fs.hardware.psm.f0ch3.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.f0ch3.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.f0ch3.iq_modulation.idle_i__511__i__512_;
      idle_q =fs.hardware.psm.f0ch3.iq_modulation.idle_q__511__q__512_;

      base_iq_addr = BASE_DM_F0_CH3_IQ;
      break;


    case 3:
      //     Ncic =fs.output.psm.cic_interpoln_rate__ncic_[channel_index];
      Niq = fs.output.psm.num_iq_pairs__niq_[channel_index];
      selected_channel = channel_index +1;
      jump_to_idle_iq =fs.hardware.psm.f0ch4.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.f0ch4.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.f0ch4.iq_modulation.idle_i__511__i__512_;
      idle_q =fs.hardware.psm.f0ch4.iq_modulation.idle_q__511__q__512_;

      base_iq_addr = BASE_DM_F0_CH4_IQ;
      break;
      
      
      
    case 4:
      //     Ncic =fs.output.psm.cic_interpoln_rate__ncic_[channel_index];
      Niq = fs.output.psm.num_iq_pairs__niq_[channel_index];
      selected_channel = channel_index +1;      
      jump_to_idle_iq =fs.hardware.psm.f1.iq_modulation.jump_to_idle_iq;
      load_first_val_in_idle = fs.hardware.psm.f1.iq_modulation.load_first_val_in_idle;
      idle_i =fs.hardware.psm.f1.iq_modulation.idle_i__511__i__512_;
      idle_q =fs.hardware.psm.f1.iq_modulation.idle_q__511__q__512_;
      
      base_iq_addr = BASE_DM_F1_IQ;
      break;

    default:
      cm_msg(MERROR,"psm_loadIQFile","Error: channel index %d or name \"%s\" is not supported",channel_index,channel_name);
      return(FE_ERR_HW);
    }
 
  sprintf(IQfile,"%s/%s_iq_%s.psm", fs.input.cfg_path,ppg_mode,channel_name);
      
  //if(dpsm)
  printf("\npsm_loadIQFile: now calling psmLoadIQfile to load IQ file %s\n",IQfile);  // returns npairs
  status = psmLoadIQfile(mvme, base_addr, base_iq_addr, IQfile,  &npairs,  &first_i, &first_q); 
  if(status != SUCCESS)
    {
      cm_msg(MERROR," psm_loadIQFile","Error: IQ file %s for channel %s index %d is not loaded", 
	     IQfile,channel_name,channel_index);
      return(FE_ERR_HW);
    }
  
  if(dpsm)
    printf(" psm_loadIQFile: loaded IQ file for channel %s (index %d); first I,Q pair (2's comp) = %d,%d, no. pairs=%d\n",
	   channel_name,channel_index,first_i,first_q,npairs);
  
  if(npairs !=  Niq)
    {
      cm_msg(MINFO,"psm_loadIQFile",
	     "No. IQ pairs in file (%d) for channel %s (index %d) disagrees with odb value (%d); using file's value",
	     npairs, channel_name,channel_index,  Niq);      
    }
  /* psmLoadIQFile reads back & checks values against file */
  
  
  printf("psm_loadIQFile: successfully loaded I,Q pairs file %s for channel %s (index %d). Number of I,Q pairs in file=%d\n", 
	 IQfile,channel_name,channel_index, npairs);
  
  
  if(npairs > 0x7FF)
    {
      printf("Maximum number of IQ pairs is 0x7FF or 2047 (11 bit register). Setting npairs (%d) to 2047\n",npairs); 
      cm_msg(MINFO,"psm_loadIQFile","Maximum number of IQ pairs is 0x7FF or 2047 (11 bit register). Setting npairs (%d) to 2047",npairs); 
      npairs = 2047; // register is 11 bits
    }
  // Set the IQ length register 
  status = psmWriteIQLen( mvme, base_addr, selected_channel, npairs);
  if(status == SUCCESS)
    printf("psm_loadIQFile: Wrote IQ DM length 0x%x or %d for Channel %s (index %d) \n",
	   npairs,npairs,channel_name,channel_index);
  else
    {
      cm_msg(MERROR,"psm_loadIQFile","Error writing IQ DM length %d  for channel %s (index=%d)\n",
	     npairs,channel_name,channel_index);
      return FAILURE;
    }
  
  /*  NO equivalent for PSMIII
	  
      Write the CIC value to the PSM 
      status = psmSetCIC_Rate(mvme, base_addr, channel_index, Ncic);
      if(status == -1)
      {
      cm_msg(MERROR,"psm_loadIQFile","Error setting CIC Rate register to %d for channel %s",
      Ncic,channel_name);
      return(FE_ERR_HW);
      }
  */
  
  /* Check Jump to Idle for IQ pairs 
     Jump to idle at end of sweep or stay at last value ?
  */
  if(jump_to_idle_iq )
    { 
      /* jump to idle; load initial IQ pair into idle? */
      if(load_first_val_in_idle)
	{
	  /*  first value will be loaded into Idle */
	  I=first_i;
	  Q=first_q;
	  printf("psm_loadIQFile: first pair will be loaded into Idle (I,Q=%d,%d ) for channel %s (index %d)\n",
		 I,Q,channel_name,channel_index); 
	  status = psmLoadIdleIQ(mvme, base_addr, selected_channel ,I,Q); /* IQ pair to load */
	  if(status == -1)
	    {
	      cm_msg(MERROR,"psm_loadIQFile","error writing idle I,Q pair as %d,%d for channel %s (index %d)",
		     I,Q,channel_name,channel_index);
	      return(FE_ERR_HW);
	    }
	} /* end of loading first value into idle */
      else
	{
	  /* load fs.hardware.psm.iq_modulation  idle I,Q pair  into Idle */
	  I= idle_i;
	  Q= idle_q;
	  printf("psm_loadIQFile: supplied I,Q pair will be loaded into Idle (I,Q=%d,%d) for channel %s  (index %d)\n",
		 I,Q,channel_name,channel_index); 
	  status = psmLoadIdleIQ(mvme, base_addr, selected_channel,I,Q); /* IQ pair to load */
	  if(status == FAILURE)
	    {
	      cm_msg(MERROR,"psm_loadIQFile","error writing supplied idle I,Q pair as %d,%d for channel %s  (index %d)",
		     I,Q,channel_name,channel_index);
	      return(FAILURE);
	    }
	} 
    }
#endif
  return SUCCESS;
}


/* ==================================================*/
INT disable_psm(MVME_INTERFACE *mvme, DWORD base_addr)
{
#ifdef HAVE_PSMIII
  /* we will not be using the psm so disable it */
  INT status,size;
  char str[128];

  psmWriteGateControl(mvme, base_addr, ALLCH, FP_GATE_DISABLED);  // disable gates on all channels
  psmSetScaleFactor(mvme, base_addr, ALLCH, 0);  // 0 scale factor

  if(dpsm)printf("\ndisable_psm: setting device disabled flag in ODB\n"); 
  sprintf(str,"/Equipment/FIFO_acq/frontend/output/PSM/device disabled");
  fs.output.psm.device_disabled=1; /* set flag */
  size=sizeof(BOOL);
  status = db_set_value(hDB,0, str, &fs.output.psm.device_disabled, size,1,TID_BOOL);
  if(status != SUCCESS)
    printf("disable_psm: error writing to  %s (%d) \n",str,status);
#endif
  return status; 
}
 


/* ==================================================
 */
INT init_psm( MVME_INTERFACE *mvme, DWORD base_addr)
{
  // Called at begin_of_run
#ifdef HAVE_PSMIII
  INT status,size;
  char str[128];
  DWORD data;
  //dpsm = 1; // TEMP

  printf("\n init_psm: initializing PSMIII\n");

#ifdef BNMR
 status =psm_write_RFtripThreshold (mvme, base_addr);
  if(status == -1)
    { 
      cm_msg(MERROR,"init_psm","error return from psm_write_RFtripThreshold ");
      return FE_ERR_HW;
    } 

  // Now that cw mode exists on PSMIII set default operating mode once more

  data = psmRegWrite8(mvme, base_addr ,  MODULE_OPERATING_MODE, BNMR_OPERATING_MODE);
#else // BNQR (currently does not have PSMIII)
  printf("init_psm: setting module operating mode to default operating mode (bnqr)\n");
  data = psmRegWrite8(mvme, base_addr ,  MODULE_OPERATING_MODE, BNQR_OPERATING_MODE );
#endif

  printf("init_psm: clearing PSM RF Trip \n");
  data = psmClearRFpowerTrip(mvme, base_addr);
  printf("init_psm: After reset, register reads %d\n",data);
  if(data)  
    printf("init_psm: RFpower IS Tripped\n");
  else
    printf("init_psm: RFpower NOT Tripped\n");


  /* rf_config checked that at least one channel is enabled 
     initially, disable all channels by setting scale factor to 0 and disable gates
  */
  printf("init_psm: disabling gates on all channels:\n");  
  psmWriteGateControl(mvme, base_addr, ALLCH, FP_GATE_DISABLED ); // disable gates on all channels
  printf("init_psm: reading back gates on all channels (expect 0=disabled):\n"); 
  psmReadGateControl(mvme, PSM_BASE, ALLCH); // display data
  printf("init_psm: setting scale factor=0  on all channels:\n");  
  psmSetScaleFactor(mvme, base_addr, ALLCH, 0);  // 0 scale factor


  /* For each enabled channel, setup_psmIII sets up gate, scale factor etc. */ 

  if(strncmp(ppg_mode,"1w",2)==0)
    status = setup_psmIII_cw(mvme,base_addr);
  else
    setup_psmIII(mvme, base_addr);
  if(status == FAILURE) 
    return(FAILURE);

    
  printf("\ninit_psm: clearing psm device disabled flag in ODB\n"); 

  sprintf(str,"/Equipment/FIFO_acq/frontend/output/PSM/device disabled");
  fs.output.psm.device_disabled=0; /* clear flag */
  size=sizeof(BOOL);
  status = db_set_value(hDB,0, str, &fs.output.psm.device_disabled, size,1,TID_BOOL);

  if(status != SUCCESS)
    printf("init_psm: error writing to  %s (%d) \n",str,status);

#endif
  return SUCCESS;
}


INT   setup_psmIII( MVME_INTERFACE *mvme, DWORD base_addr)
{
  /*  Parameters: VMIC mvme structure, PSM base

       Loops round all channels
           For each enabled channel  (f0ch1 f0ch1 f0ch3 f0ch4 f1ref)
           set up any channel-dependent stuff read from ODB 
      
       channel_index goes from 0 to 4  (i.e. MAX_CHANNELS-1  where MAX_CHANNELS=5
       PSMIII routines use selected_channel where selected_channel goes from 1 to 6 (6=all channels)

       channel_index  channel    selected_channel
             0         F0 Ch1         1
	     1         F0 Ch2         2
	     2         F0 Ch3         3
	     3         F0 Ch4         4
	     4         F1 (ref)       5
                    
 
       Returns -1 for FAILURE
 */
#ifdef HAVE_PSMIII
  INT status,temp;
  INT quad_mode,jump_to_idle_iq, gate_control;
  INT bits, Nc, scale_factor;
  INT channel_index;
  INT selected_channel=0; // unknown channel
  char channel_name[5];
  BOOL enabled;
  INT code,nbits;
  DWORD offset;

  enabled=quad_mode=jump_to_idle_iq=bits=Nc=scale_factor=temp=0;
  gate_control=1;

  for (channel_index=0; channel_index <MAX_CHANNELS; channel_index++)
    {
      switch (channel_index)
	{
	case 0: // f0Ch1
	  if(fs.hardware.psm.f0ch1.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII: setting up channel=%s (index %d)\n",channel_name,channel_index);
	      
	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[channel_index];

	      scale_factor = fs.hardware.psm.f0ch1.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.f0ch1.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.f0ch1.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.f0ch1.gate_control;
	    }
	  break;

	case 1: // f0Ch2
	  if(fs.hardware.psm.f0ch2.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index)); 
	      selected_channel=channel_index +1;
	      printf("setup_psmIII: setting up channel=%s (channel_index %d selected_channel=%d)\n",
		     channel_name,channel_index,selected_channel);
	      
	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[channel_index];

	      scale_factor = fs.hardware.psm.f0ch2.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.f0ch2.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.f0ch2.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.f0ch2.gate_control;
	    }
	  break;


	case 2: // f0Ch3
	  if(fs.hardware.psm.f0ch3.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII: setting up channel=%s (channel_index %d selected_channel=%d)\n",
		     channel_name,channel_index,selected_channel);
	      
	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[channel_index];

	      scale_factor = fs.hardware.psm.f0ch3.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.f0ch3.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.f0ch3.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.f0ch3.gate_control;
	    }
	  break;


	case 3: // f0Ch4
	  if(fs.hardware.psm.f0ch4.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII: setting up channel=%s (channel_index %d selected_channel=%d)\n",
		     channel_name,channel_index,selected_channel);
	      
	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[channel_index];

	      scale_factor = fs.hardware.psm.f0ch4.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.f0ch4.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.f0ch4.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.f0ch4.gate_control;
	    }
	  break;

	case 4:
	  if(fs.hardware.psm.f1.channel_enabled)
	    {
	      enabled=1;
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      selected_channel=channel_index +1;
	      printf("setup_psmIII: setting up channel=%s (channel_index %d selected_channel=%d)\n",
		     channel_name,channel_index,selected_channel);

	      Nc = fs.output.psm.num_cycles_iq_pairs__nc_[channel_index];

	      scale_factor = fs.hardware.psm.f1.scale_factor__def_181_max_255_;
	      quad_mode = fs.hardware.psm.f1.quadrature_modulation_mode;
	      jump_to_idle_iq = fs.hardware.psm.f1.iq_modulation.jump_to_idle_iq;
              gate_control = fs.hardware.psm.f1.gate_control;
	    }
	  break;
	    
	default:
	  enabled=0;
	} // switch


      if(dpsm)
	{
	  printf("\nChannel %s index %d selected_channel=%d :\n",channel_name,channel_index,selected_channel);
	  printf("Nc = %d\n",Nc);
	  printf("Scale Factor = %d\n",scale_factor);
	  printf("Quad Mode = %d\n",quad_mode);
	  printf("Jump to Idle IQ = %d\n",jump_to_idle_iq);
	  printf("Gate control =  %d\n\n",gate_control );
	}


      if(enabled)
	{
	  switch (gate_control)
	    {
	    case FP_GATE_DISABLED:
	      cm_msg(MERROR,"setup_psmIII","WARNING: Front Panel Gate is Disabled. Use only for testing");
	      printf("setup_psmIII: WARNING: Front panel gate is disabled\n");
	      break;
	    case  GATE_NORMAL_MODE:
	      printf("setup_psmIII: Front panel gate is enabled (default mode)\n");
	      break;
	    case  GATE_PULSE_INVERTED:
	      cm_msg(MERROR,"setup_psmIII","WARNING: Front Panel Gate Pulse Inverted. Use only for testing");
	      printf("setup_psmIII: WARNING:Front panel gate pulse inverted\n");
	      break;
 	    case   INTERNAL_GATE:
	      cm_msg(MERROR,"setup_psmIII","WARNING: Front Panel Gate Pulse Ignored and Gates are ALWAYS ON. Use only for testing");
	      printf("setup_psmIII: WARNING Front panel gate pulse ignored. Gate are ALWAYS ON\n");
	      break;
            default:
	      cm_msg(MERROR,"setup_psmIII","WARNING: Illegal gate parameter for channel %s. Using default mode",channel_name);
	      printf("setup_psmIII:  Illegal gate parameter for channel %s. Using default mode\n",channel_name);
              gate_control=1;
	    } // switch
 
	  /* write gate control for this channel */
	
	  
	  if(dpsm)printf("\nNow writing gate control = %d for channel \"%s\"   \n",gate_control,channel_name);
	  status = psmWriteGateControl (mvme, base_addr, selected_channel, gate_control); 
	  if(status == -1)
	    {
	      cm_msg(MERROR,"setup_psmIII","error return from psmWriteGateControl for channel \"%s\" ",channel_name);
	      return(status);
	    }
	  
	  
	  if(dpsm)printf("after writing gate control to channel \"%s\", read back  = 0x%x\n",channel_name,status);
	  
	  
	  /* Write the scale factor  */
	  
	  if (scale_factor < 0 || scale_factor > 255)
	    {
	      cm_msg(MERROR,"setup_psmIII","Illegal scale factor (%d) for channel \"%s\" index=%d; it should be between 0 and 255",
		     scale_factor,channel_name,channel_index);
	      return -1;
	    }
	  
	  status = psmSetScaleFactor(mvme, base_addr, selected_channel, scale_factor);
	  if(status == -1)
	    {
	      cm_msg(MERROR,"setup_psmIII","error return from psmSetScaleFactor for channel %s",channel_name);
	      return status;
	    }

	  if(channel_index == 4)
	    {   
	      status = psm_load_tuning_freq(mvme, PSM_BASE); /* load tuning freq if channel f1 is enabled */
	      if (status != SUCCESS)
		return status;
	    }

	  
	  /* quadrature mode or "single tone mode" */
	      

	  code=3; // Buffer Factor
	  offset=(code-1) *2;
	  nbits=16; // 16-bit register
	  status = psmWriteBufFactor(mvme, base_addr, selected_channel, Nc);
	  if(status==SUCCESS)
	    printf("Wrote 0x%x to Channel Reg at offset 0x%x from base\n",Nc,offset);
	  else
	    {
	      cm_msg(MERROR,"setup_psmIII","error writing Nc=%d to Buffer Factor channel \"%s\" register at offset 0x%x\n ",
		     Nc,channel_name,offset);
	      return -1; 
	    }
	      
	  /* write IQ end sweep mode (into  End Sweep Control Reg bits 0-3) */

	  if(dpsm)printf("\nSetting IQ EndSweepMode to %d  \n",
			 jump_to_idle_iq);
	  status = psmWriteEndSweepControl(mvme, base_addr, FALSE, selected_channel,jump_to_idle_iq);  // FALSE= i,q  TRUE=freq
	      if(status == -1)
		{
		  cm_msg(MERROR,"setup_psmIII","error return from psmWriteEndSweepControl (IQ) for channel %s (index %d)",
			 channel_name,channel_index);
		  return(status);
		}
	      if(dpsm)printf("setup_psmIII:after writing IQ end sweep mode (%d) to end sweep reg, read back  = 0x%x for channel %s\n",

			     jump_to_idle_iq, status,channel_name);
	      

	} /* end of this channel enabled */
    } // end of for loop
#endif // HAVE_PSMIII
  return(SUCCESS);

} // end setup_psmIII


/*---------------------------------------------------------*/
INT psm_load_iq(MVME_INTERFACE *mvme, DWORD base_addr)
/*---------------------------------------------------------*/
  /* depending on ODB parameters load the IQ pair(s) for enabled channel(s)
       if loading a file, calls psm_load_iq_files

     quad mode : frequency modulated by IQ pairs, usually by loading an I,Q file
     single channel mode : frequency will be modulated by a single IQ pair loaded into Idle, i.e. I=512 Q=0
  */
{
#ifdef HAVE_PSMIII
  INT status,channel_index;
  INT selected_channel=0; // unknown ch 
  char channel_name[5];
  BOOL first,enabled;
  BOOL quad,load_iq_file;
  INT idle_i,idle_q;
  first=1;
  printf("psm_load_iq: starting\n");
  for (channel_index=0; channel_index< MAX_CHANNELS; channel_index++) /* cycle through channels */ 
    {
      enabled=quad=load_iq_file=0;

      // now allow user to vary these numbers in case of phase correction
      // later may not allow phase correction on f0ch1
      idle_i = 512;  // default for single tone mode
      idle_q = 0;    // default for single tone mode
      switch (channel_index)
	{
	case 0: // f0Ch1    
	  if(fs.hardware.psm.f0ch1.channel_enabled)
	    {
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      enabled=1;
              selected_channel = channel_index +1;
	      quad = fs.hardware.psm.f0ch1.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.f0ch1.iq_modulation.load_i_q_pairs_file;

	      // assign these even in single tone mode to allow phase correction
	      idle_i =fs.hardware.psm.f0ch1.iq_modulation.idle_i__511__i__512_;
	      idle_q =fs.hardware.psm.f0ch1.iq_modulation.idle_q__511__q__512_;
	    }
	    
	  break;

	case 1: // f0Ch2    
	  if(fs.hardware.psm.f0ch2.channel_enabled)
	    {
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      enabled=1;
              selected_channel = channel_index +1;
	      quad = fs.hardware.psm.f0ch2.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.f0ch2.iq_modulation.load_i_q_pairs_file;

	      // always assign these even in single tone mode to allow phase correction
	      idle_i =fs.hardware.psm.f0ch2.iq_modulation.idle_i__511__i__512_;
	      idle_q =fs.hardware.psm.f0ch2.iq_modulation.idle_q__511__q__512_;
		

	    }
	  break;

	case 2: // f0Ch3    
	  if(fs.hardware.psm.f0ch3.channel_enabled)
	    {
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      enabled=1;
              selected_channel = channel_index +1;
	      quad = fs.hardware.psm.f0ch3.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.f0ch3.iq_modulation.load_i_q_pairs_file;

	      // always assign these even in single tone mode to allow phase correction
	      idle_i =fs.hardware.psm.f0ch3.iq_modulation.idle_i__511__i__512_;
	      idle_q =fs.hardware.psm.f0ch3.iq_modulation.idle_q__511__q__512_;
		
	    }
	  break;


	case 3: // f0ch4    
	  if(fs.hardware.psm.f0ch4.channel_enabled)
	    {
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      enabled=1;
              selected_channel = channel_index +1;
	      quad = fs.hardware.psm.f0ch4.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.f0ch4.iq_modulation.load_i_q_pairs_file;
	      // always assign these even in single tone mode to allow phase correction
	      idle_i =fs.hardware.psm.f0ch4.iq_modulation.idle_i__511__i__512_;
	      idle_q =fs.hardware.psm.f0ch4.iq_modulation.idle_q__511__q__512_;

	    }
	  break;


	case 4:   //f1 ref 
	  if(fs.hardware.psm.f1.channel_enabled)
	    {
	      sprintf(channel_name, "%s", get_channel(channel_index));
	      enabled=1;
              selected_channel = channel_index +1;
	      quad = fs.hardware.psm.f1.quadrature_modulation_mode;
	      load_iq_file =  fs.hardware.psm.f1.iq_modulation.load_i_q_pairs_file;
	      // always assign these even in single tone mode to allow phase correction
		  idle_i =fs.hardware.psm.f1.iq_modulation.idle_i__511__i__512_;
		  idle_q =fs.hardware.psm.f1.iq_modulation.idle_q__511__q__512_;
	    }

	  break;
	default:
	  enabled=0;
	} // switch

      //    printf("psm_load_iq: channel_index=%d enabled=%d  quad=%d load_iq_file=%d\n",channel_index,enabled, quad,load_iq_file);

      if(enabled)
	{
	  if(quad && load_iq_file)
	    {
	      if(first) /* do this once only */
		{
		  if ( strncmp(fs.input.experiment_name,"20",2) == 0)
		    sprintf(ppg_mode,"00"); /* filename is 00 not 20; change ppg_mode (global) */
		  first=0;
		}
	      printf("psm_load_iq: Quadrature mode enabled for channel \"%s\" (index %d), loading I,Q pairs file\n",
		     channel_name,channel_index);

	      /* psm_loadIQFile loads the IQ pair file for quadrature mode (also loads Idle and sets length register) */
	      status = psm_loadIQFile(mvme, base_addr, ppg_mode, channel_index); // calculates selected_channel
	      if(status != SUCCESS)
		{
		  printf("psm_load_iq: error loading PSM IQ pairs file for channel \"%s\" (index %d) and ppg_mode %s\n",
			 channel_name,channel_index,ppg_mode);
		  return(status);
		}
	      printf("psm_load_iq: successfully loaded PSM IQ pairs file for channel \"%s\" (index %d) and ppg_mode %s\n",
		     channel_name,channel_index,ppg_mode);
	    }
	  else
	    { // single tone mode or quad modulated by a single value
	      status = psmLoadIdleIQ(mvme, base_addr, selected_channel ,idle_i, idle_q); /* IQ pair to load */
	      if(status == FAILURE)
		{
		  cm_msg(MERROR,"psm_load_iq","error writing idle I,Q pair as %d,%d for channel %s (channel_index %d)",
			 idle_i,idle_q,channel_name,channel_index);
		  return(FE_ERR_HW);
		}
	     
	      printf("psm_load_iq: wrote idle I,Q pair as %d,%d for channel %s (channel_index %d)",
		  idle_i,idle_q   ,channel_name,channel_index);
	      // Set the IQ length register to Zero 
	      status = psmWriteIQLen( mvme, PSM_BASE,  selected_channel, 0);
	      if(status != SUCCESS)
		{
		  cm_msg(MERROR,"psm_load_iq","Error writing IQ length = 0 for Idle I,Q pair for channel %s (index=%d)\n",
			 channel_name,channel_index);
		  return FAILURE;
		}
	      printf("psm_load_iq: Wrote IQ length = 0 for Idle pair for Channel %s (index %d) \n",
		     channel_name,channel_index);

	    } // end single tone mode or quad modulated by single value
	} /* end of this channel enabled */
    } /* end of for loop */
#endif
  return SUCCESS;
}



/*---------------------------------------------------------*/
INT psm_load_tuning_freq(MVME_INTERFACE *mvme, DWORD base_addr)
/*---------------------------------------------------------*/
{
#ifdef HAVE_PSMIII
  INT status;
  DWORD offset;

  /* If channel f1  is enabled ... */
  if(fs.hardware.psm.f1.channel_enabled)
    {
      offset =  F1_TUNING_FREQ;
      /* Load "reference tuning frequency (Hz)" into f1  */
      printf("\n psm_load_tuning_freq: loading PSM channel f1 tuning frequency %dHz at offset 0x%x  \n",
	     fs.hardware.psm.fref_tuning_freq__hz_ ,offset);

      status = psmWrite_reference_freq_Hz (mvme, base_addr, offset,  fs.hardware.psm.fref_tuning_freq__hz_);
      if(status != FAILURE) 
	printf("psm_load_tuning_freq:read back PSM f1 tuning frequency as %dHz\n",status);
      else
	{
	  cm_msg(MERROR,"psm_load_tuning_freq","error writing PSM f1 tuning frequency of %dHz",
		 fs.hardware.psm.fref_tuning_freq__hz_);
	  return(FE_ERR_HW);
	}
    }
  else
    printf("psm_load_tuning_freq: tuning frequency NOT loaded as reference channel f1 is disabled\n");
  printf("psm_load_tuning_freq returns success\n");

#endif // HAVE_PSMIII
  return SUCCESS;

}

char *get_channel(INT n)
{
#ifdef HAVE_PSMIII
  char *channel_name[]={"f0ch1","f0ch2","f0ch3","f0ch4","f1","all","unknown"};
 
  return (n<0 || n>MAX_CHANNELS) ? channel_name[4] : channel_name[n];
#endif // HAVE_PSMIII
  return "unknown";
}


#ifdef BNMR
INT  psm_write_RFtripThreshold( MVME_INTERFACE *mvme, DWORD base_addr)
{
#ifdef HAVE_PSMIII
  // actually same routine for PSM
  float ftemp;
  INT status;
  
  /* Write the RF Trip Threshold  */
  ftemp = fs.hardware.rf_trip_threshold__0_5v_;
  if(dpsm)printf("psm_write_RFtripThreshold:starting with ftemp=%f\n",ftemp);
  if (ftemp < 0 || ftemp > 5)
    {
      cm_msg(MERROR,"psm_write_RFtripThreshold","Illegal RF Trip threshold (%.2f); it should be between 0 and 5",ftemp);
      return -1;
    }
  
  
  status = psmWriteRFpowerTripThresh(mvme, base_addr,ftemp);
  if(dpsm)printf("psm_write_RFtripThreshold: read back 0x%2.2x (%d)\n",status,status); /* data */
#endif  
  return SUCCESS;
}
#endif /* BNMR */


INT psm_LoadIdleFreqHz( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq_hz)
{
#ifdef HAVE_PSMIII
  // Input value in Hz, return value in Hz
  DWORD freq;

  freq=get_hex(freq_hz);
  printf("psm_LoadIdleFreqHz: freq_hz=%d Hz ... calling psmLoadIdleFreq with hex freq = 0x%x\n",
	 freq_hz,freq);
  return  psm_LoadIdleFreq ( mvme, base_addr, freq); // SUCCESS or FAILURE
#else
  return SUCCESS;
#endif
}

INT psm_ReadIdleFreq( MVME_INTERFACE *mvme, DWORD base_addr)
{
#ifdef HAVE_PSMIII
  // returns idle frequency value in hex
  DWORD data;
  DWORD offset= IDLE_FREQ_OFFSET; // offset from Freq Sweep DM Base
  
  data = psmRegReadFreqDM( mvme,  base_addr, offset);
  return  data;
#else
  return 0;
#endif
}

INT psm_ReadIdleFreqHz( MVME_INTERFACE *mvme, DWORD base_addr)
{
#ifdef HAVE_PSMIII
  // returns idle frequency value in Hz
  DWORD data, freq_hz;
  DWORD offset = IDLE_FREQ_OFFSET; // offset from Freq Sweep DM Base

  data = psmRegReadFreqDM( mvme,  base_addr, offset);
  freq_hz=get_Hz(data);
  printf("psm_ReadIdleFreqHz: read hex freq = 0x%x or freq_hz=%d Hz\n",
	 data,freq_hz);
  return  freq_hz;
#else
  return 0;
#endif
}

INT psm_LoadIdleFreq ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD freq)
{
#ifdef HAVE_PSMIII
  // Load idle frequency
  // frequency value is in hex
  // Reads back to check value.
  // Returns SUCCESS or FAILURE

  DWORD offset = IDLE_FREQ_OFFSET; // offset from Freq Sweep DM Base
  DWORD data;
  char str[20];
  str[0]='\0';
  
  data = psmRegWriteFreqDM( mvme,  base_addr, offset, freq );
  if(data != freq)
    {
      printf("psm_LoadIdleFreq: Error -  values do not match at Offset 0x%x . Wrote 0x%x Read back 0x%x (hex values)\n",
	     offset,freq,data);
      return FAILURE;
    }

#endif
  return SUCCESS;
}






