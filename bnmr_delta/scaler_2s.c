#ifdef HAVE_SIS3801
/*-- individual scaler histo increment ------------------------------------*/
INT scaler_increment_2s(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*
  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
  - type 2: if random_flag && e2a_flag or e2e_flag or e2f_flag increment correct bin (see note below)
  -         if any other mode 2, histogram bins directly

FIFO data from scaler comes out in this order:
            
ch 0 bin 0 
ch 1 bin 0
........
ch N bin 0

ch 0 bin 1
ch 1 bin 1
.........
ch N bin 1

----------
ch 0 bin M
ch 1 bin M
.........
ch N bin M


NOTE
if discard_first_bin is true, n_bins is one more than n_his_bins. First bin discarded
 before histogramming etc.
*/

{
  DWORD *ps, i, h, ub, ub1, ub2, *pbuf, *pus, sdata;
  INT     channel_field;
  INT  index,status;
  INT ub_histo;
  INT hoffset;
  BOOL ub_flag;
  

  // This routine is used by modes 2s and 2w which send the userbit histogram
  if(!e2w_flag  && !e2s_flag)
    {
      cm_msg(MERROR,"scaler_increment_2s","programming error - this routine should not be called for ppg mode %s",ppg_mode);
      cm_msg(MERROR,"scaler_increment_2s: programming error - this routine should not be called for ppg mode %s\n",ppg_mode);
      return -1; // error
    }

  ub2=ub1=0; // zero these to get rid of warnings

  ub=0;
  ub_flag=1; /* userbins are being used for Modes 2s,2w */
  pbuf = pfifo;
  /* user-bit histo number is hardcoded here */
  
  ub_histo = 0; // default  not used
  
#ifdef TWO_SCALERS
  if(exp_mode == 1) // type 1
    ub_histo = N1_HISTO_MAXA -1;
  else if(e2s_flag || e2w_flag )
    {
    ub_histo = N2_HISTO_MAXA -1;  // Mode 2s supported for BNMR only (two scalers)
    //printf("scaler_increment_2s: ub_histo=%d\n",ub_histo);
    }
#else
  ub_histo = N1_HISTO_MAXB - 1; /* add user bit histo if one scaler only */
#endif
  pus = histo[ub_histo].ph ;   /*  User bit histogram is hardcoded  */

  // dd[1]=dsis=1;
  //  if (dd[1])
    { 
      printf("scaler_increment_2s: starting with  nwords:%d pfifo:%p, pus:%p maximum_channel=%d  *gbl_bin=%d\n",
	     nwords, pbuf,pus,maximum_channel,*gbl_bin);
      printf("               and  n_requested_bins=%d  n_bins=%d n_his_bins=%d gbl_nprebins=%d \n\n",
	     n_requested_bins, n_bins,n_his_bins,gbl_nprebins);
    } // debug
  
  for (i=0 ; i < nwords ; i++)
    { // main for loop on the number of words
      
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
      
        if(dsis)
	{
	  if(h<1 )
	    printf("scaler_increment_2s: i=%d  *gbl_bin=%d chan %d data 0x%x \n",i, *gbl_bin, h,(*pbuf & 0xFFFFF));
	}
      
      // this routine is only for modes e2s and e2w (checked above)
	if(exp_mode == 1 || e2s_flag || e2w_flag)
	  { 
	    ub = channel_field >>6;
	    if (ub>4 || ub < 0)
	      {
		printf("\nscaler_increment_2s: ERROR ub is not in limits : %i\n",ub);
		goto retminus1;
	      }

	    if(dsis)
	      {
		if(h<1 )
		  printf("scaler_increment_2s: i=%d  *gbl_bin=%d chan %d ub=%d data 0x%x \n",i, *gbl_bin, h,ub,(*pbuf & 0xFFFFF));
	      }

#ifdef XTWO_SCALERS	
  
	    if (!fill_uarray && scaler_offset == 0)
	      {  /* check that the user bit is the same for both modules */
	    
		if(ub != *(pus + *gbl_bin))
		  {
		    printf("scaler_increment_2s: ERROR current ub = %x; array ub = %x for i %d and h %d gbl_bin=%d\n",
			   ub,*(pus + *gbl_bin),i,h,*gbl_bin);
		    goto retminus1;
		  }
	      }
#endif
	  } // end of exp mode=1 or e2s/e2w_flag
	else  /* Type 2 */ 
	  {
	    *userbit = channel_field >>6;
	    ub = *userbit;
	    ub1 = ub & 1;
	    ub2 = ub & 2;
	  } //  end of exp mode=2
	
      
      
     
      if( h < maximum_channel)
	{
	  
	  index = h + scaler_offset;
	  /* assign pointer */
	  ps = scaler[index].ps;  /* pointer to start of histo  */
	  sdata = *pbuf & DATA_MASK; /* scaler data to be saved */
	  
	  	  
	  hoffset= *gbl_bin  ; /* histo offset within scaler histo for this bin */	
	  
	  /* 
	     Histogram the Data 
	  */
	  
	  /* Regular debugging */
	  if(dd[1])
	    {
	      if(i<=100 && h == 0)
		{
		  printf("scaler_increment_2s: h=%d,i=%d,pbuf=%p,*pbuf=%x,ub=%d,sdata=%x, *gbl_bin=%d,hoffset=%d\n",
			 h,i,pbuf,*pbuf,ub,sdata,*gbl_bin,hoffset); 
		  
		  if(i == nwords -1 )
		    {
		      printf("scaler_increment_2s: Last word h=%d,i=%d,pbuf=%p,*pbuf=%x,ub=%d,sdata=%x, *gbl_bin=%d,hoffset=%d\n",
			     h,i,pbuf,*pbuf,ub,sdata,*gbl_bin,hoffset); 
		    }
		}
	    } // if dd[1]
	  
	  
	  if(!ub_flag) /* check the User Bits are zero */
	    {
	      if(h==0 && ub != 0)
		{
		  printf("scaler_increment_2s: Unexpected user bits set  (ub=%d) at gbl_bin %d where i=%d ch=%d \n",ub,*gbl_bin,i,h);
		}
	    }
	  
	  /* check offset within histogram  */
	  if(hoffset > n_his_bins )
	    {
	      
	      printf("\nscaler_increment_2s: ERROR histo offset out of range: hoffset=%d  nhis=%d programming error \n",
		     hoffset,n_his_bins);
	      goto retminus1;
	    }
	  
	  
       	  /*  Now histogram the data
	      Add current time bin content to cumulative scaler histo */
	  *(ps+hoffset) += sdata;	  
	  /* increment local cycle sum scaler */
	  scaler[index].sum_cycle += (double)sdata;
	  
	  
#ifdef TWO_SCALERS
	  if(exp_mode == 1)
	    {
#ifdef THIRTYTWO
	      /* increment cycle sum according to User Bit */
	      if(index < 16)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;

	      else if (index < 32)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#else  // TWO scaler inputs
	      /* increment cycle sum according to User Bit */
	      if(index ==0)
		scaler[BACK_USB0 + ub].sum_cycle += sdata;
	      else if (index ==1)
		scaler[FRONT_USB0 + ub].sum_cycle += sdata;
#endif // THIRTYTWO
	    }
	
#endif	  //  TWO_SCALERS
	  
	  
	  /* next incoming data */
	  pbuf++;  
	  
	  /* Increment time bin every h = N_SCALER_REAL */
	  if (h == (maximum_channel-1) )
	    {
	      if((exp_mode == 1) || e2s_flag)  // Mode 2s also saves userbit histogram
		{
		  if(fill_uarray && scaler_offset == 0) /* true for Scaler A (2 scalers) or Scaler B (1 scaler)*/
		    {
		      *(pus + *gbl_bin) = ub;  // increment userbin histo
		        if(dd[1]) 
			{
			  if(*gbl_bin < 5 ) 
			    printf ("bin %d ub1=%d mod(bin#)=%d, updated pus  with ub %d\n",(INT)*gbl_bin,(INT)ub1, (INT)*gbl_bin%2 ,(INT)ub);
			}
		    }
		}
	  
	      (*gbl_bin)++;   // actual (true) bin number
	      //if(dd[1])
		 //	printf("h=%d, maximum_channel=%d incremented *gbl_bin to %d\n",h, maximum_channel, *gbl_bin);
	    } /* end of h == max channel-1 */
	}/* end of h< max_channel */
      else
	printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x  and *gbl_bin=%d\n",i, nwords, h, *pbuf, *gbl_bin);
      
      
    } // loop on the number of words
  status = 0;
  
 ret:
    dd[1]=0;
  if(dsis)
    printf("\nscaler_increment_2s returns with *gbl_bin %d\n",*gbl_bin);
  return status; /* success */
  

 retminus1:
  status =  -1;
  goto ret;
}
#endif
