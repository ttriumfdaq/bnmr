/*  Included by febnmr.c

CVS log information:
$Log: setup_scan_params.c,v $
Revision 1.2  2019/05/29 22:48:45  suz
mode 1w modifications

Revision 1.1  2019/05/02 23:14:12  suz
add mode 1w for bnmr

Revision 1.3  2018/08/22 22:59:15  suz
remove scan_jump used by POL expt

Revision 1.2  2015/06/22 22:40:03  suz
added mode 2f

Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version

Revision 1.11  2008/04/19 06:29:07  suz
revert to opening and closing epics scan iocs each run

Revision 1.10  2008/04/17 17:31:09  suz
do not reopen epics scan channels if already open

Revision 1.9  2005/08/13 01:35:34  suz
change a comment

Revision 1.8  2005/08/04 00:53:59  suz
call for 2a/2e non-random to fill pseqf array

Revision 1.7  2005/05/18 00:09:11  suz
frequency is unsigned int (needed for PSMII)

Revision 1.6  2005/03/16 19:04:11  suz
add a printf

Revision 1.5  2005/02/28 21:45:54  suz
change format for pol

Revision 1.4  2005/02/22 23:18:15  suz
fix bugs for POL version

Revision 1.3  2005/01/26 21:52:14  suz
add status

Revision 1.2  2005/01/25 23:27:40  suz
epics_reconnect now has a parameter for helicity CA

Revision 1.1  2005/01/24 17:57:02  suz
initial version: simplify begin_of_run by moving code to subroutines


*/
#include "setup_scan_params.h"
#include "midas.h"

INT mode10_set_scan_params(INT *ninc)
{
  /* called from begin of run to set up scan parameters */

  printf(" mode10_set_scan_params: setting up for Mode 10 (DUMMY Frequency scan) \n");
	  /* mode 10 -  frequency is not actually scanned,  so use some standard values  */
  freq_start = 335000; freq_stop  = 336000;  freq_inc   = 100; /* gives 20 frequency steps */
  freq_ninc = ( abs(freq_stop - freq_start)/ abs(freq_inc) ) +1;
  printf(" begin_of_run: Selected %d DUMMY frequency cycles \n",freq_ninc);
  gbl_scan_flag = 4;      /* set scan flag for dummy frequency scan  */
  *ninc = freq_ninc;
  return SUCCESS;
}	

#ifdef EPICS_ACCESS
INT epics_set_scan_params(INT *ninc)
{
  INT status;
  INT openflag;
  /* called from begin of run to set up scan parameters */

  printf("\n ***** epics_set_scan_params: starting \n");
  openflag=0;
  /*  
      Epics Scan : Scanning Laser ?  
  */
  if (strcmp (fs.output.e1n_epics_device, "Laser")==0)
    {
      printf("epics_set_scan_params: EPICS Laser scan detected\n");
      epics_params.Laser_flag = TRUE; /* LASER scan is selected */
      gbl_scan_flag = 2;   /* Laser (value = 2 ) */

    }
  /*
    Epics Scan : Scanning Field ?  
  */
  else if (strcmp (fs.output.e1n_epics_device, "Field")==0)
    {
      printf("epics_set_scan_params: EPICS Field scan detected\n");  /* 1e */
      epics_params.Field_flag = TRUE;  /* FIELD scan is selected */
      gbl_scan_flag = 3;     /* Field (value = 3) */
    }

  /*
    Epics Scan : Scanning NaCell ?  
  */    
  else if (strcmp (fs.output.e1n_epics_device, "NaCell")==0)
    {
      printf("epics_set_scan_params: EPICS Na/Rb scan detected\n");
      epics_params.NaCell_flag = TRUE; /* Na Cell scan is selected */
      gbl_scan_flag = 1;  /* NaCell (value = 1) */
    }
  /*
    Epics Scan : Scanning custom variable ?
  */
  else if (strcmp (fs.output.e1n_epics_device, "Custom")==0)
    {
      printf("epics_set_scan_params: EPICS custom scan detected\n");
      epics_params.Custom_flag = TRUE;
      strcpy(epics_params.Custom_read_var, fs.input.e1n_custom_epics_var_read);
      strcpy(epics_params.Custom_write_var, fs.input.e1n_custom_epics_var_write);
      gbl_scan_flag = 64;  /* Custom (value = 64) */
    }
  else
    {
      cm_msg(MERROR,"epics_set_scan_params","Unknown Epics device : %s", fs.output.e1n_epics_device);	  
      return(DB_NO_ACCESS);
    }
  /* GetIDs for Epics scan */
  status = epics_reconnect(FALSE); 
  if (status != SUCCESS)
    {
      /* Epics device not accessible  ....
	 mdarc should stop the run since client flag will not be set to success */
      return  (FE_ERR_HW);    
    }
  


#ifdef GONE   /* try keeping NaCell open  */
  /*
    Epics Scan : Scanning NaCell ?  
  */
  else if (strcmp (fs.output.e1n_epics_device, "NaCell")==0)
    { 
      printf("epics_set_scan_params: EPICS Na/RbCell scan detected\n");
      if (gbl_epics_scan == 1)
	{
	  printf("epics_set_scan_params: epics scan has been done previously; epics channels should be open\n");
	  if(epics_params.NaCell_flag != TRUE) /* Na Cell scan should be selected */
	    {
	      printf("epics_set_scan_params: error expect epics_params.NaCell_flag to be set TRUE\n");
	     
	    }

	  printf("epics_params.NaCell_flag=%d epics_params.XxWchid =%d  epics_params.XxRchid = %d\n",
	epics_params.NaCell_flag,	 epics_params.XxWchid, epics_params.XxRchid);
 
	  status = epics_watchdog ( &epics_params, FALSE);
	  if (status != SUCCESS)
	    {
	      printf("epics_set_scan_params: error from epics_watchdog \n");
	      /* Epics device not accessible  ....
		 mdarc should stop the run since client flag will not be set to success */
	      return  (FE_ERR_HW);    
	    }
	  openflag=1;
	}
      else if (gbl_epics_scan != 0)
	printf("epics_set_scan_params; changing epics scan\n");
      else
	printf("epics_set_scan_params: this is the first epics scan; epics channels should be closed\n");
	  
     
      epics_params.NaCell_flag = TRUE; /* Na Cell scan is selected */
      gbl_scan_flag = 1;  /* NaCell (value = 1) */
    }
  else
    {
      cm_msg(MERROR,"epics_set_scan_params","Unknown Epics device : %s", fs.output.e1n_epics_device);	  
      return(DB_NO_ACCESS);
    }
  
  /* should these Epics channels already be open ? */
  if(!openflag)
    {
      /* GetIDs for Epics scan */
      status = epics_reconnect(FALSE); 
      if (status != SUCCESS)
	{
	  /* Epics device not accessible  ....
	     mdarc should stop the run since client flag will not be set to success */
	  return  (FE_ERR_HW);    
	}
    }
#endif /* GONE */


  gbl_epics_scan = gbl_scan_flag;
  
  
  /* Time for watchdog on direct access to Epics channels */
  Epics_last_time = ss_time(); 
  
  status = EpicsInit( flip, &epics_params ); /* Initialize direct access to Epics device */
  if (status != SUCCESS) 
    {
      /* Epics device cannot be initialized  or the parameters were invalid ....
	 mdarc will stop the run since client flag will still show failure */
      return  (FE_ERR_HW);  
    }
  gbl_epics_live = TRUE;
  gbl_inc_cntr = epics_params.Epics_ninc; /* set to max to satisfy cycle_start routine */      
  *ninc = gbl_inc_cntr;
  return(SUCCESS);
}
#endif     /* EPICS ACCESS */

#ifdef CAMP_ACCESS
INT camp_set_scan_params(INT *ninc)
{
  float ftmp;
  /* called from begin_of_run */
  if(dc)printf("camp_set_scan_params: Camp scan detected\n");
  camp_start = fs.input.e1c_camp_start;   /* all these are defined in odb (even if #ifdef CAMP_ACCESS is false */
  camp_stop = fs.input.e1c_camp_stop;
  camp_inc = fs.input.e1c_camp_inc;
  
  if( camp_inc  == 0 )
    {
      cm_msg(MERROR,"camp_set_scan_params","Invalid camp scan increment value: %f",
	     camp_inc);
      return FE_ERR_HW;
    }
  
  
  if ( fabs(camp_inc) > fabs(camp_stop - camp_start)) 
    {
      cm_msg(MERROR,"camp_set_scan_params","Invalid camp scan increment value: %f",
	     camp_inc);
      return FE_ERR_HW;
    }
  
  if( ((camp_inc > 0) && (camp_stop < camp_start)) ||
      ((camp_inc < 0) && (camp_start < camp_stop)) )
    camp_inc *= -1;
  ftmp =  ( fabs(camp_stop - camp_start)/ fabs(camp_inc) ) +1;
  camp_ninc = (INT)ftmp;
  
  printf("ninc %f -> integer %d  diff %f camp_inc = %f\n",ftmp,camp_ninc,(camp_stop-camp_start),camp_inc);
  if( camp_ninc < 1)
    {
      cm_msg(MERROR,"camp_set_scan_params","Invalid number of sweep steps: %d",
	     camp_ninc);
      return FE_ERR_HW;
    }
  if(flip) camp_inc *= -1;
  gbl_FREQ_n = camp_ninc; /* set to max to satisfy cycle_start routine */
  set_camp_val = camp_start;
  printf("camp_set_scan_params: camp device will be set to =%f\n",set_camp_val);
  printf(" Selected %d CAMP scan increments starting with %f %s by steps of %f\n",
	 camp_ninc,camp_start, camp_params.units,camp_inc);


  /*  Note    gbl_scan_flag will be set by camp_update_params */
  *ninc = camp_ninc;
  return SUCCESS;
}
#endif /* CAMP */


INT random_set_scan_params(INT *ninc)
{
  INT i,status;

  /* called from Begin_of_run; randomized values for 1a/1b/2a/2c/2e/2f/1f 
     also called for non-random 2a,2e,2f to fill pseqf */

  /* frequency step values are read from the file generated by rf_config.
     returns the number of frequency increments.

  The frequency step values are read out from the file into an array. 
  For Type 1 the freq steps are then randomized, and the random frequency values are stored in an array
     pointed to by "prandom_freq"
  
  For Type 2, randomization is done every cycle, and the random values are then downloaded into the PPG.
  For Type 2e non-random pseq is used. This routine is called but pseq is then filled for non-random data 
  */
	  

  
  /* seed is defined in trandom.h */
  seed = fs.output.last_seed__for_random_freqs_; /* use last seed from odb to seed a new sequence of random nos. */
  if(random_flag)
    printf("random_set_scan_params:  frequency values will be randomized; initial seed=%d \n",seed);
  
  freq_ninc = fs.output.num_frequency_steps;
  
  status = read_freq_file(ppg_mode, freq_ninc); /* read the frequency file and allocate memory for arrays */
  if(status != SUCCESS) /* error message sent by read_freq_file */
    return status;

 *ninc = freq_ninc;


  /* For type 2, only 2a/2e/2f are currently supported */
  if( e2a_flag || e2e_flag || e2f_flag )
    {
    if(random_flag)/* no need to randomize freqs now, since it is done each cycle for Type 2 */
      return status;
    else
      {  /* for non-random data */
	if(pindex==NULL || prandom_freq==NULL  || pfreq == NULL || pseqf == NULL)
	  { 
	    /* read_freq_file must have been called prior to this routine to allocate space */
	    cm_msg(MERROR,"random_set_scan_params",
		   "array pointer(s) are null. Space has not been allocated");
	    return DB_INVALID_PARAM;
	  }
	/* fill pseqf, so we can point to non-random frequencies */
	for (i=0; i<freq_ninc; i++)
	  {
	    pindex[i]=i;  /* initialize index array */
	    prandom_freq[i]=pfreq[i]; /* random array = frequency array */
	    pseqf[i]=i; /* initialize pointer array for filling histo data */
	  }
	return status;
      }
    } /* end of Type 2 */


 /* Type 1 only */
  status = randomize_freq_values(ppg_mode, freq_ninc); /* fill prandom_freq with the random freq values */
  if(status != SUCCESS) /* error message sent by randomize_freq_values */
    return status;
 
  gbl_FREQ_n = freq_ninc; /* set to max to satisfy cycle_start routine */
  freq_val = (DWORD)prandom_freq[0];  /* the first value to set */
  
  printf(" Selected %d frequency cycles; frequency range is %u to %u, step size =%d\n",
	 freq_ninc, (DWORD)fs.input.frequency_start__hz_, 
	 (DWORD)fs.input.frequency_stop__hz_, fs.input.frequency_increment__hz_);
  printf(" Frequency steps are randomized; first value will be %u hz\n", freq_val);
  gbl_scan_flag = 0x800;
  return SUCCESS;
}




INT  RF_set_scan_params(INT *ninc)
{

#ifdef HAVE_PSM
  DWORD itmp;
#endif
  /* called from begin_of_run to set up frequency for Type 1 modes EXCEPT 1w
     (1f/1g/10) and 1a/1b (not randomized)  
  */
  if(debug)
 
   printf("RF_set_scan_params: Frequency scan detected \n");
  freq_start= (DWORD)fs.input.frequency_start__hz_;
  freq_stop = (DWORD)fs.input.frequency_stop__hz_;
  freq_inc = (INT)fs.input.frequency_increment__hz_;
  
  if( freq_inc == 0 )
    {
      cm_msg(MERROR,"RF_set_scan_params","Invalid frequency scan increment value: %d",
	     freq_inc);
      return FE_ERR_HW;
    }
  
  
  if ( abs(freq_inc) > abs(freq_stop - freq_start)) 
    {
      cm_msg(MERROR,"RF_set_scan_params","Invalid frequency increment value: %d",
	     freq_inc);
      return FE_ERR_HW;
    }
  
#ifdef HAVE_PSM
  /* check the values for psm */
  itmp = get_hex(freq_stop);
  if(itmp == 0)
    {
      cm_msg(MERROR,"RF_set_scan_params","Frequency stop %uHz is outside maximum for psm\n",freq_stop);
      return FE_ERR_HW;
    }
  itmp = get_hex(freq_start);
  if(itmp == 0)
    {
      cm_msg(MERROR,"RF_set_scan_params","Frequency start %uHz is outside maximum for psm\n",freq_start);
      return FE_ERR_HW;
    }
#endif	 // HAVE_PSM	
  
  if( ((freq_inc > 0) && (freq_stop < freq_start)) ||
      ((freq_inc < 0) && (freq_start < freq_stop)) )
    freq_inc *= -1;
  freq_ninc = ( abs(freq_stop - freq_start)/ abs(freq_inc) ) +1;
  printf("ninc %d diff %d freq_inc = %d\n",freq_ninc,(freq_stop-freq_start),freq_inc);
  if( freq_ninc < 1)
    {
      
      cm_msg(MERROR,"RF_set_scan_params","Invalid number of freq steps: %d",
	     freq_ninc);
      return FE_ERR_HW;
    }  
   
  if(flip) freq_inc *= -1; 
  freq_val = freq_start;
  gbl_FREQ_n = freq_ninc; /* set to max to satisfy cycle_start routine */
  if(mode1g_flag && flip)
    {
      printf(" Selected %d freq cycles starting with %u hz by steps of %d hz (cycle repeats at each helicity)\n",
	     freq_ninc,freq_start,freq_inc);
    }
  else
    printf(" Selected %d frequency cycles starting with %u hz by steps of %d hz\n",
	   freq_ninc,freq_start,freq_inc);

  if (mode1g_flag && flip)
    gbl_scan_flag = 0x1000; /* 1g with flip ... may not need a special value */
  else
    gbl_scan_flag = 0;  /* frequency etc (value = 0) 1f,1g (no flip), 1a/1b not random  */
  *ninc = freq_ninc;
  return SUCCESS;
}	  /* end of freq scan modes (1f,1g,1a,1b) */


INT set_scan_params_1w(INT *ninc)
{
  /* called from begin_of_run to set up frequency for Type 1w 
     Type 1w runs PSMIII in CW mode  (bnmr only)
  */
#ifdef HAVE_PSM
#ifdef HAVE_PSMIII

  INT j;
  INT status;
  const INT zero=0;
  //  xval, xstart,xstop,xincr,xninc are globals; uses gbl_FREQ_n

  if(!mode1w_flag)
    return SUCCESS; 
  
  if(debug)
    printf("set_scan_params_1w: Mode 1w frequency scan detected \n");
  
  xstart=  fs.input.e1w_x_start;
  xstop =  fs.input.e1w_x_stop;
  xincr =  fs.input.e1w_x_incr;

  // Checks on xstart, xstop and xincr done in type1_compute.c
  // This code is the same as in type1_compute.c so xincr is set to the same value as in the file
  if( ((xincr > 0) && (xstop < xstart)) ||
      ((xincr < 0) && (xstart < xstop)) )
    xincr *= -1;
  xninc = ( abs(xstop - xstart)/ abs(xincr) ) +1;
  printf("set_scan_params_1w: ninc %d (xstop - xstart)= %d xincr = %d\n",xninc,(xstop-xstart),xincr);
  if( xninc < 1)
    {
      
      cm_msg(MERROR,"set_scan_params_1w","Invalid number of steps: %d",
	     xninc);
      return FE_ERR_HW;
    }  

  // Helicity Flipping
  if(flip)xincr *= -1;

  xval = xstart;
  gbl_FREQ_n = xninc; /* set to max to satisfy cycle_start routine */

  printf("set_scan_params_1w: selected %d steps starting with parameter x=%d by increments of %d\n",
	   xninc,xstart,xincr);
  gbl_scan_flag = 0x20; // 1w scan flag value

  //free_fscan_pointers(); // freed at BOR

  status = read_freq_file_1w();
  if(status != SUCCESS)
    return status;

 // TEMP print out the scan function values
  printf("  j indx indvar       f1         f2         f3        f4 \n");
  for(j=0;j<xninc; j++)
    {
      printf("%3d %3d  %3d  ",j, fscanindex[j],fxscan[j]);
      
      if(fscanf1) 
	printf("%10d ",fscanf1[j]);
      else
	printf("%10d ",zero);
      
      if(fscanf2) 
	printf("%10d ",fscanf2[j]);
      else
	printf("%10d ",zero);
      
      if(fscanf3)
	printf("%10d ",fscanf3[j]);
      else
	printf("%10d ",zero);
      
      if(fscanf4)
	printf("%10d ",fscanf4[j]);
      else
	printf("%10d ",zero);
      
      printf("\n");
      
    }
  printf("\n"); // end of line

#endif
#endif
  return SUCCESS;
}

INT read_freq_file_1w(void)
{
  FILE *fin;
  INT ninc,status;
  char str[256];
  INT idx,j,n;
  char *p;
  char delim[]=" ";
  INT *data;
  BOOL gotinc;
  INT debug=0;
  INT first;
  INT nch_enabled;
  INT findex;
  INT *fscan;
  INT *fptrs[4];

  data=NULL;
  for(j=0;j<4;j++)
    fptrs[j]=NULL; // clear

  nch_enabled=ninc=gotinc=0;
  first=1;

 /* open the frequency file (produces by perlscript fcw.pl, called by rf_config) */
  printf("opening frequency file %s\n",e1w_freqfile);
  
  fin = fopen(e1w_freqfile,"r");
  ss_sleep(250);
  if(fin == NULL)
    {
      cm_msg(MERROR,"read_freq_file_1w","Mode 1w (CW) frequency file %s could not be opened", e1w_freqfile); 
      printf("read_freq_file_1w: Mode 1w (CW) frequency table file %s could not be opened\n", e1w_freqfile);
      return DB_INVALID_PARAM;
    }

  /* read the file  */
  j=0; // line/array counter
  while(fgets(str, 255, fin) !=NULL)
    {
      findex=0; // new line
      if(debug)
	printf("read_freq_file_1w: working on str=%s",str);  // contains \n
      if(strncmp(str,"#",1) != 0)
	{ // this line is not a comment
	  if(!gotinc)
	    {
	      cm_msg(MERROR,"read_freq_file_1w","did not find valid ninc (number of increments) in header of file %s",e1w_freqfile);
	      fclose(fin);
	      return DB_INVALID_PARAM;
	    }

	  p=strtok(str, delim);
	  if(isdigit(*p))
	    {
	      n = atoi(p); // new line, this is the index starting at 0
	      if(first && (n > 0)) 
		{
		  cm_msg(MERROR,"read_freq_file_1w","error reading file %s (expect index=0 when first is true, not %d)\n",
			 e1w_freqfile,n);
		  printf("read_freq_file_1w: error  - expect index=0 when first is true, not %d\n",n);
		  fclose(fin);
		  return DB_INVALID_PARAM;
		}
	      fscanindex[j]= n ; // store index
	    }

	  while (p != NULL)
	    {
	      //printf("p=%s\n",p);
	      p = strtok(NULL, delim);
	      if(p==NULL)
		break;
	      if(isdigit(*p))
		{
		  n=atoi(p);

		  if(findex == 0) // findex counts items on the line
		    // this is the scan value
		    fxscan[j]=n; // save x value
		  
		  else if (findex < 5)
		    {    // function values f1-f4
		      idx=findex-1;
		      fscan=fptrs[idx]; // get the pointer to data for f1/f2/f3/f4
		      if(fscan)
			fscan[j]=n;
		      else
			{
			  cm_msg(MERROR,"read_freq_file_1w","memory has not been allocated for this function i.e. fscan=fptrs[%d] is unexpectedly NULL;",idx);
			  printf("ERROR fscan=fptrs[%d] is unexpectedly NULL;\n",idx);
			}
		    }
		  else
		    {
		      cm_msg(MERROR,"read_freq_file_1w","too many values found (%d) on line \"%s\" when reading file %s",
			     findex,str,e1w_freqfile );
		      printf("read_freq_file_1w: illegal counter value findex=%d - too many values found\n",findex);
		      fclose(fin);
		      return DB_INVALID_PARAM;
		      break;
		    }
		
		  findex++; // next item on line
		  if(debug || first)printf("%d ",n);
		} // end isdigit

	    } // end while p

	  if(debug|| first) printf ("\n");
	  if(first)
	    first=0;
	  j++; // next index ready for next line
	}
      else
	{ // this line begins with "#"
	  status=0;
	  if(!gotinc)
	    { // look for number of increments; this line is the first line in the file
	      status = decode_ninc_1w(&gotinc,str);
	      if(status != SUCCESS)
		{
		  cm_msg(MERROR,"read_freq_file_1w","error return from decode_ninc_1w");
		  fclose(fin);
		  return  DB_INVALID_PARAM;
		}

	      if(gotinc)
		{
		  ninc = xninc;
		  printf("read_freq_file_1w: decode_ninc_1w returns SUCCESS; ninc=xninc=%d\n",ninc);
		}
	    }
	  else if(nch_enabled==0)
	    {  // gotinc is true; 
	      status = decode_fscans_1w(&nch_enabled, fptrs, str);
	      if(status != SUCCESS)
		{
		  cm_msg(MERROR,"read_freq_file_1w","error return from decode_fscans_1w");
		  fclose(fin);
		  return DB_INVALID_PARAM;
		}
	      printf("read_freq_file_1w: decode_fscans_1w returns success and nch_enabled=%d\n",nch_enabled);
	    }


	} // end line begins with #
    } // while

 
  // if(debug)
    printf("read_freq_file_1w: ninc = %d  j=%d \n",ninc,j);
  if(j != ninc) 
    {
      cm_msg(MERROR,"read_freq_file_1w","unexpected error - ninc=%d from file %s, but number of data lines is %d. These should be identical.\n",
	     ninc,e1w_freqfile,j);
      fclose(fin);
      return DB_INVALID_PARAM;
    }
  
  if(ninc == 0)
    {
      cm_msg(MERROR,"read_freq_file_1w","after reading file %s expect ninc (=%d) to be > 0",e1w_freqfile,ninc);
      fclose(fin);
      return DB_INVALID_PARAM;
    }
    

 
   // Save the number of enabled frequency channels f1..f4 from the file
  e1w_num_enabled_psmch=nch_enabled;
  // e1w_num_enabled_psmch is will be checked later against the number of PSM channels actually enabled by setup_psmIII_cw()
  fclose(fin);
  return SUCCESS;
}

INT decode_ninc_1w(BOOL *gotinc, char *str)
{
  // this line begins with "#" and ninc=0 
  // returns ninc 
  char *qq;
  INT ninc=0;

  // look for number of increments
  if(*gotinc) 
    return SUCCESS;

  *gotinc = 0; // false

  qq=strstr(str,"ninc=");
  //printf("*qq=\"%s\"\n",qq);
  if(!qq)
    {
      if(debug)printf("decode_ninc_1w: this line does not contain ninc\n");
      return SUCCESS; 
    }

  // This line does contain "ninc"
  if(debug)printf("decode_ninc_1w: working on str=%s\n",str);

  qq+=5; // skip over "ninc=" to point to number
  //printf("now *qq=\"%s\"\n",qq);
  if(isdigit(*qq))
    {
      ninc=atoi(qq);
      printf("decode_ninc_1w: read ninc=%d from file\n",ninc);
      if(ninc != xninc)
	{
	  cm_msg(MERROR,"decode_ninc_1w","number of increments from file (%d) should be equal to xninc (%d)",ninc,xninc);
	  return DB_INVALID_PARAM;
	}
    }
  *gotinc = 1; // true
  return SUCCESS; // success; found valid ninc
}

INT decode_fscans_1w(INT *nen,  int (**fptrs), char *str)
{
  //  this line begins with "#" 
  //  number of increments = xninc (global)
  char *qq,*pp;
  INT i,l;
  INT findex;
  *nen=l=0; // clear


  // looking for string beginning "# Index  "
  qq=strstr(str,"Index");
  if(!qq)
    { 
      if(debug)printf("decode_fscans_1w: line does not contain \"Index\"\n");
      return SUCCESS;
    }
  findex=0; // count number of channels assigned memory
  // this line contains the list of functions (f1...)

  if(debug)printf("decode_fscans_1w: working on str=%s\n",str);
  pp=strchr(qq,'f');
  while (pp)
    {
      pp++;
      if(isdigit(*pp))
	{
	  i=atoi(pp);			 
	  printf("decode function: found f%d\n",i);
	  
	  switch (i)
	    {
	    case 1:
	      /* allocate space for the f1 frequency scan array fscanf1 */
	      fscanf1 = (INT *) calloc(xninc, sizeof(INT));
	      if (fscanf1 == NULL)
		{
		  /*cm_msg(MERROR,"read_freq_file_1w","Could not allocate memory for freq array (%d * %d)bytes ",
		    num_frequency_steps, sizeof(INT) ); 
		  ;*/
		  cm_msg(MERROR,"decode_fscans_1w","could not allocate memory for f%d in  fscanf1 array (%d * %d)bytes ",
			 i, xninc, sizeof(INT) );
		  return  DB_INVALID_PARAM;
		}
	      else
		printf("decode_fscans: allocated memory for f%d in fscanf1 array  (%d * %d)bytes \n",
		       i, xninc, sizeof(INT) );
	      
	      fptrs[l]=fscanf1;
	      l++;
	      break;
	      
	    case 2:
	      fscanf2 = (INT *) calloc(xninc, sizeof(INT));
		  if (fscanf2 == NULL)
		    {
		      cm_msg(MERROR,"decode_fscans_1w","could not allocate memory for f%d in  fscanf2 array (%d * %d)bytes ",
			     i, xninc, sizeof(INT) );
		      return  DB_INVALID_PARAM;
		    }
		  else
		    printf("decode_fscans: allocated memory for f%d in fscanf2 array  (%d * %d)bytes \n",
		       i, xninc, sizeof(INT) );
	      
		  fptrs[l]=fscanf2;
		  l++;
		  break;
		  
	    case 3:
	      fscanf3 = (INT *) calloc(xninc, sizeof(INT));
	      if (fscanf3 == NULL)
		{
		  cm_msg(MERROR,"decode_fscans_1w","could not allocate memory for f%d in  fscanf3 array (%d * %d)bytes ",
			     i, xninc, sizeof(INT) );
		      return  DB_INVALID_PARAM;
		}
	      else
		printf("decode_fscans: allocated memory for f%d in fscanf3 array  (%d * %d)bytes \n",
		       i, xninc, sizeof(INT) );
	      
	      fptrs[l]=fscanf3;
	      l++;
	      break;
	      
	    case 4:
	      fscanf4 = (INT *) calloc(xninc, sizeof(INT));
	      if (fscanf4 == NULL)
		{
		  cm_msg(MERROR,"decode_fscans_1w","could not allocate memory for f%d in  fscanf4 array (%d * %d)bytes ",
			 i, xninc, sizeof(INT) );
		  return  DB_INVALID_PARAM;
		}
	      else
		printf("decode_fscans: allocated memory for f%d in fscanf4 array  (%d * %d)bytes \n",
		       i, xninc, sizeof(INT) );
	      
	      fptrs[l]=fscanf4;
	      l++;
	      break;
	      
	    default:
	      cm_msg(MERROR,"decode_fscans_1w","illegal function counter findex=%d (function f%d); only 4 functions are supported",findex,i);
	      return DB_INVALID_PARAM;
	      break;
	    } // switch  
	  findex++; // next item on line
	} // isdigit(pp)
      qq=pp;
      pp=strchr(qq,'f');
    } // end while pp
  printf("decode_fscans_1w: number of active channels according to file is  l=%d\n",l);

  if((fscanf1==NULL) && (fscanf2==NULL) && (fscanf3==NULL) && (fscanf4==NULL))
    {
      cm_msg(MERROR,"decode_fscans_1w","error -  no memory assigned for e1w function scans");
      printf("decode_fscans_1w: error - all fscanf1/B/C/D are NULL; no memory assigned for e1w function scans\n");
      return DB_INVALID_PARAM;
    }

   fscanindex = (INT *) calloc(xninc, sizeof(int));
   if (fscanindex == NULL)
     {
       cm_msg(MERROR,"decode_fscans_1w","could not allocate memory for fscanindex array (%d * %d)bytes",
	      xninc, sizeof(int) );
       return DB_INVALID_PARAM;
     }
   fxscan = (INT *) calloc(xninc, sizeof(int));
   if (fxscan == NULL)
     {
       cm_msg(MERROR,"decode_fscans_1w","could not allocate memory for fxscan array (%d * %d)bytes",
	      xninc, sizeof(int) );
       return DB_INVALID_PARAM;
     }
   *nen=l; // number of channels assigned memory
   return SUCCESS; // success
}


INT check_psm_freqval_1w(INT freq_hz)
{
  DWORD itmp;
#ifdef HAVE_PSM
  /* check the frequency values for psm are within range */
  itmp = get_hex(freq_stop);
  if(itmp == 0)
    {
      cm_msg(MERROR,"check_psm_freqval_1w","Frequency value %uHz is outside maximum for psm\n",freq_hz);
      return FE_ERR_HW;
    }
#endif 
  return SUCCESS;
}

