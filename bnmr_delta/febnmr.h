/*   febnmr.h
   Common defines for BNMR and POLB types 1 and 2


*/

#ifndef febnmr_header
#define febnmr_header

extern  int run_state;

/* Function prototypes */
BOOL wait_end_cycle(int transition, BOOL first);
INT  bnmr_init(char  *p_name_fifo, char *p_name_info, char *p_name_scalers);
INT  FIFO_acq(char *pevent, INT off);
INT  diag_read(char * pevent, INT off);
INT  info_odb(char *pevent, INT off);
INT  histo_read(char *pevent, INT off);
INT  histo_process(void);
INT  scalers_cycle_read(char *pevent, INT off);
INT  display_scalers_read(char *pevent, INT off);
INT  scaler_increment(const DWORD nwords,  DWORD * pfifo, INT scaler_offset, 
                      INT maximum_channel,INT *gbl_bin, DWORD *userbit); 
INT  scaler_increment_pulsepair(const DWORD nwords,  DWORD * pfifo, INT scaler_offset, 
                      INT maximum_channel,INT *gbl_bin, DWORD *userbit); 

INT  cycle_start(void);
INT  helicity_read(void);
INT helicity_history(char *pevent, INT off);
INT set_init_hel_state(BOOL flip, INT hel_state);
INT hel_send_edge(INT hel_state);
INT complement(INT hel);
void hot_reference_clear(void);
void histo_clear(INT h);
void scaler_clear(INT h);
void clear_all_histo(void);
INT check_file_time(void);
void show_scaler_pointers(void);
void die();
INT iwait(INT millisec, char* caller);
void ppgStop(void);
INT hardware_check(void);
INT print_hardware_flags(void);
void update_fix_counter(INT num_increments);
INT get_int_version(char *p, int len);
void setup_hotlinks(void);
INT prepare_to_stop(void);
INT post_end_run(INT run_number, char *error); /* post-stop transition */
INT set_client_flag(char *client_name, BOOL value);
INT thresh_check(double prev_hel_thr, double current_hel_thr, double ref_hel_thr);
BOOL flip_helicity(BOOL timer);
INT test_helicity(void);
void print_hel_time(INT state);
void print_time (void);
INT decode_hel_state(INT held, INT helu);
INT assign_2_params(void);
INT stop_run(void);
INT setup_debug_hotlinks(void);
void hot_threshold(HNDLE hDB, HNDLE hkey, void * info);
void call_back_debug(HNDLE hDB, HNDLE hkey, void * info);
void call_back_debug_level(HNDLE hDB, HNDLE hkey, void * info);
INT restore_watchdog(void);
INT set_alarm(INT alarm_num);
INT set_next_epics_value(void);
INT set_next_camp_value(void);
INT set_next_randomized_freq(void);
INT set_next_1g_flip(void);
INT set_next_randomized_freq(void); 
INT set_next_freq_1(void); 
INT set_next_freq_2(void);
INT set_next_1w_frequencies(void);
INT set_previous_freq_1f(void);
INT set_previous_1w_frequencies(void);
char *helicity_state(INT n);
INT set_threshold_alarm(INT alarm_num);
INT set_alarm_flag(char *flag_name, BOOL value);
INT write_client_alarm_message(char* message);
INT recover_helicity(void);
INT stop_scalers(void);
INT start_scalers(void);
void get_readout_time(void);
void free_fscan_pointers(void); // mode 1w
// these for mode 2g sample/reference
void sample_set(INT val);
void sample_read(void);
INT sample_flip(void);

INT scaler_increment_2s(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
			INT maximum_channel,INT *gbl_bin, DWORD *userbit );
#ifdef CAMP_ACCESS /* CAMP */
INT camp_create_rec(void);
INT camp_get_rec(void);
INT camp_update_params(void);
INT check_ramp_status(void);
INT set_camp_value(float set_camp_val);
INT camp_reconnect(void);
INT camp_watchdog(void);
INT set_long_watchdog(DWORD my_watchdog_timeout);
INT retry_rampStatus(float *rampStatus);

#endif // CAMP_ACCESS

#ifdef EPICS_ACCESS
INT epics_alive(char *pevent, INT off);
INT set_epics_val(void);
INT set_epics_incr(void);
INT read_epics_val(float *pval );
INT epics_reconnect(BOOL hel);
#endif /*  EPICS_ACCESS */

#ifdef HAVE_SIS3820
struct timeval eoct; // time at end-of-cycle
uint32_t sis3820_RegisterRead(MVME_INTERFACE *mvme, DWORD base, int offset);
#endif

INT ppg_load(char *ppgfile);
void check_ppg_running(void);
INT check_ppg_stopped(void);
INT wait_ppg_stop(void);

#ifdef HAVE_NIMIO32
INT hel_read_ioreg(BOOL write);
#endif

/* PSM subroutines */
#ifdef HAVE_PSM
INT write_psmregs(void);
INT init_freq_module(void);
INT init_freq_2(void);
INT init_freq_20(void);
INT init_freq_1(void);
INT set_frequency_value(void);
INT freq_reset_mem(void);
DWORD get_hex(DWORD freq_Hz); // trPSM.c
void debug_psm(void);
#endif



#ifdef BINCHECK
INT bincheck(INT h);
#endif
/* febnmr Equpiment indexes (constants) */
#define FIFO                     0
#define HISTO                    1
#define CYCLE                    2
#define DIAG                     3
#define INFO                     4
#ifdef HAVE_SIS3820
#define LNE                      5
#ifdef EPICS_ACCESS
#define EpicsAlive               6
#endif
#else
#ifdef EPICS_ACCESS
#define EpicsAlive               5
#endif
#endif




/********************************************
       SIS 38xx MultiChannel SCALERS 
 ********************************************

  Note : SIS38xxA : It is assumed ALL 32 inputs are ENABLED on Scaler A when
                       Scaler B is present.

         Important: if more than 24 channels are enabled on the SIS38xx all 32
         will be enabled (hardware feature). If MAX_CHAN_SIS38xxB is between 24
         and 32 the software (scaler_increment) will not work without modification. 

   Remove the following defines in Makefile for dasdevpc test station: 
	 TWO_SCALERS   is defined in the Makefile for TWO SIS38xx SCALER MODULES 
	 For Type 1 only :
	 EPICS_ACCESS  is defined in the Makefile if direct access to NaCell or LASER is available 
	 undefine if no access and no epics software running on ppc (e.g. if not using bnmrhmvw) 
	 CAMP_ACCESS   is defined in the Makefile for direct access to Camp; (VXWORKS must also be defined in Makefile) 
*/ 

#ifdef TWO_SCALERS

#ifdef THIRTYTWO //  Defined if  32 inputs are needed for Scaler A
#define MAX_CHAN_SIS38xxA  32  /* number of REAL scaler inputs used by SIS3801/20 A (FIXED at 32, the maximum for the module)  */
#else
#define MAX_CHAN_SIS38xxA 2   /* number of REAL scaler inputs used by SIS3801/20 A (FIXED at 2, 16 Front 16 Back summed in hardware)  */
#endif //  THIRTYTWO

#else /* 1 Scaler only -> Scaler B */
 #define MAX_CHAN_SIS38xxA  0
#endif //  TWO_SCALERS

/* this is BNMR (TWO_SCALERS) or BNQR (ONE_SCALER) */
#define MAX_CHAN_SIS38xxB   12   /* number of REAL scaler inputs used by SIS38xxB */
                       /*(presently limited on platform because of fibre-optic, later max=32) */


#ifdef HAVE_SIS3820  // BNQR has SIS3820
// SIS3820 Scaler B NIM inputs start at 16  (lower channels are ECL)
#define ECL_OFFSET 16
#else // SIS3801
#define ECL_OFFSET 0
#endif
/* define some channels in N_SCALER_B for ease of reference later */
/* 2 fluorescence (1st is actually SIS Ref now), 2 Pol, 4 neutral beam backwards, 4 neutral beam forwards */
#define FLUOR_CHAN1     ( MAX_CHAN_SIS38xxA + ECL_OFFSET ) /* fluorescence monitors (2) start here					
  	NOT USED for BNMR - instead REF CH 1 is enabled, ch 2 fluor 2 */
#define POL_CHAN1 (MAX_CHAN_SIS38xxA +2 + ECL_OFFSET )  /* polarimeter counter 1 (there are two)  Channel 2 */
#define FLUOR_CHAN2    ( MAX_CHAN_SIS38xxA + 1 + ECL_OFFSET ) /* fluor monitor 2  Channel 1 */ 

/* Input WAS broken for BNQR  on SIS3801 but is now fixed */
//#define FLUOR_CHAN2    ( MAX_CHAN_SIS38xxA + 3 + ECL_OFFSET ) /* dud channel for BNQR */ 
//#define POL_CHAN1 (MAX_CHAN_SIS38xxA + 1 + ECL_OFFSET) /* use fluor chan instead */



#define POL_CHAN2 (MAX_CHAN_SIS38xxA +3  + ECL_OFFSET)  /* polarimeter counters 2  Channel 3 */

#define NUM_NEUTRAL_BEAM   4  /* 4 neutral beam counters for front, 4 for back */
#define NEUTRAL_BEAM_B ( MAX_CHAN_SIS38xxA + 4  + ECL_OFFSET) /* first neutral beam backwards counter  */
#define NEUTRAL_BEAM_F ( NEUTRAL_BEAM_B + NUM_NEUTRAL_BEAM ) /* first neutral beam forwards  counter */



/********************************************
  Define histograms with time bin structure
 ********************************************/

/* Keep the histogram numbers the same for ONE_SCALER - they are not sent out */

#ifdef TWO_SCALERS     // BNMR has two scalers
 #define N1_HISTO_MAXA          3  /* Type 1 Front, back and user bits; 
                                      User bit histo is hardcoded in scaler_increment & set to N1_HISTO_MAXA-1 */ 
// #define N2_HISTO_MAXA          4  /* Type 2 Front, back for each helicity state */
 #define N2_HISTO_MAXA          5  /* Type 2 Front, back for each helicity state + userbit for mode 2s BNMR */
 #define N_HISTO_MAXA           5  /* must be the largest of the two above */

 #define N1_HISTO_MAXB             6   /* Type 1 histograms for  B channels 2 fluor,2 Pol, 2 neutral beam (sums backward and forward) */


#else   /* One scaler only (BNQR)  ... Scaler B */
 #define N1_HISTO_MAXA          0  /* Type 1 Front, back and user bits */ 
 #define N2_HISTO_MAXA          0  /* Type 2 Front, back for each helicity state */
 #define N_HISTO_MAXA           0  /* must be the largest of the two above */

 #define N1_HISTO_MAXB             7   /* Type 1 histograms for  B channels 2 fluor,2 Pol, 
					 2 neutral beam (sums backward and forward) and User Bits 
					 User bit histo is hardcoded in scaler_increment & set to N1_HISTO_MAXB-1 */
#endif //  TWO_SCALERS

/* Previously the following was true for ONE AND TWO Scalers */
//#define N2_HISTO_MAXB             10   /* Type 2 histograms for Scaler B  
//				       h1,2 fluor; h3,4 L,R pol-; h5.6 L,R pol+; h7,8 NBB+,NBF+,NBB-,NBF- ; */
//#define N_HISTO_MAXB              10   /* must be the largest of N1_HISTO_MAXB and N2_HISTO_MAXB  */ 
/* After adding Sarah's sample/ref mode (BNQR only at present) define more histograms for S/R */
#ifdef TWO_SCALERS
#define N2_HISTO_MAXB             10   /* Type 2 histograms for Scaler B  
				       h1,2 fluor; h3,4 L,R pol-; h5.6 L,R pol+; h7,8 NBB+,NBF+,NBB-,NBF- ; */
#define N_HISTO_MAXB              10   /* must be the largest of N1_HISTO_MAXB and N2_HISTO_MAXB  */ 
#else 
/* ONE_SCALER, bnqr only */
#define N2_HISTO_MAXB             18   /* Type 2 histograms for Scaler B  including sample/ref mode
				       h1,2 fluor;
                                       h3,4    L,R pol-; h5,6   L,R pol+; h7,8 NBB+,NBF+, h9,10 NBB-,NBF- ;   REFERENCE (microwave OFF) 
                                       the following filled in 2g mode ONLY
                                       h11,12  L,R pol-; h13,14 L,R pol+; h15,16 NBB+,NBF+, h17,18 NBB-,NBF- ; SAMPLE (microwave ON)  */  
#define N2_HISTO_SR                8   /*   8 extra Type 2 histograms for sample/ref mode */
#define N_HISTO_MAXB              18   /* must be the largest of N1_HISTO_MAXB and N2_HISTO_MAXB  */ 
#endif

/***********************************************************************
  Define scalers (totals for one cycle or cumulative since start of run
  ***********************************************************************/

#define N_SCALER_REAL ( MAX_CHAN_SIS38xxA +  MAX_CHAN_SIS38xxB)
                                    /* number of real (hardware) scalers used */
/* Note: If change # of scalers, for Type 2 only, make change in darc_odb.h (for mdarc)
         Analysis programs (both types) must also be modified
   Presently  no. of scalers = 44 = N_SCALER_TOTAL  */

/*          N_SCALER_REAL scalers listed above */

/*--------------------------------*/
/*    Calculated cycle scalers    */
/*--------------------------------*/
/*  Cycle Userbit scalers filled directly in scaler-increment */
#ifdef TWO_SCALERS
/* define some calculated scalers A only filled in type I (Imusr-like)*/
#define NA1_CYCLE_SCALER  8
#define BACK_USB0 (N_SCALER_REAL + 0)      /* sum of BACK scaler bins when USer_Bit is 0 */
#define BACK_USB1 (N_SCALER_REAL + 1)
#define BACK_USB2 (N_SCALER_REAL + 2)
#define BACK_USB3 (N_SCALER_REAL + 3)

#define FRONT_USB0 (N_SCALER_REAL + 4)
#define FRONT_USB1 (N_SCALER_REAL + 5)
#define FRONT_USB2 (N_SCALER_REAL + 6)
#define FRONT_USB3 (N_SCALER_REAL + 7)
/* define some calculated scalers A always - a suitable offset will be added*/
#define NA_CYCLE_SCALER      4   
#define NA_BACK_CYC          0  /* Sum of back counters  */
#define NA_FRONT_CYC         1  /* Sum of front counters */
#define NA_RATIO_CYC         2  /* ratio back/front   */
#define NA_ASYM_CYC          3 /* back/front assymetry  */
#else /* 1 scaler */
#define NA1_CYCLE_SCALER 0
#define NA_CYCLE_SCALER 0
#endif //  TWO_SCALERS

/*  SCALER B - calculated scalers - ALWAYS done - suitable offset to be added*/
#define NB_POL_CYC   0  /* sum of L & R polarimeter counters */ 
#define NB_POL_ASYM  1  /* Polarimeter assymetry */
#define NB_NB_CYC    2  /* sum of all neutral beam counters (Scaler B) */ 
#define NB_NB_ASYM   3  /* Neutral beam assymetry */
#define NB_CYCLE_SCALER  4  /* No. cycle scalers listed above */

#define N1_SCALER_CYC    NA1_CYCLE_SCALER + NA_CYCLE_SCALER + NB_CYCLE_SCALER  /* Type 1 Tot. Number of cycle scalers defined above */
#define N2_SCALER_CYC    NA_CYCLE_SCALER   + NB_CYCLE_SCALER   /* Type 2 Tot. Number of cycle scalers defined above */

  
/*--------------------------------*/
/*  Calculated cumulative scalers */
/*    clear only at BOR           */
/* Only scaler A values are used  */
/*--------------------------------*/
#ifdef TWO_SCALERS
#define BACK_CUM   0
#define FRONT_CUM  1
#define ASYM_CUM   2
#define RATIO_CUM  3
/* There will be 2 sets of the above scalers for HelDown and HelUp */
#define NA_SCALER_CUM             8  /* Cumulative scalers, LAST indices (CLEAR ONLY AT BOR) */
#else
#define NA_SCALER_CUM             0 
#endif //  TWO_SCALERS

/* Possibly cumulative scalers from ScalerB will be added later */
#define NB_SCALER_CUM             0

#define N1_SCALER_CUM               0  /*  TYPE 1 - no cumulative scalers */
#define N2_SCALER_CUM   NA_SCALER_CUM  + NB_SCALER_CUM /*  TYPE 2 - */

/*--------------------------------*/
/*  Overall number of scalers     */
/*--------------------------------*/

#define N1_SCALER_TOTAL (N_SCALER_REAL + N1_SCALER_CYC + N1_SCALER_CUM)    /* Maximum scalers for scaler bank,  real + (any) calculated */
#define N2_SCALER_TOTAL (N_SCALER_REAL + N2_SCALER_CYC + N2_SCALER_CUM)    /* Maximum scalers for scaler bank,  real + (any) calculated */
#define N_SCALER_MAX    N2_SCALER_TOTAL  /* Must be the larger of the two */

#define N_HISTO_MAX   (N_HISTO_MAXA + N_HISTO_MAXB)

/* Because N_BINS_MAX is needed for checking in rf_config and in Parameters web page, it will be written to 
   the ppg output area.

   (N_BINS_MAX * number of histos) must fit in the frontend event size  
*/  
#define N_BINS_MAX           128000  //  increased this from 32000 (VxWorks)
                                     

/*--------------------------------*/
/* Scalers passed to EPICS        */
/*--------------------------------*/

#ifdef TWO_SCALERS
#define N_EPICS_VAR_A  5
#else /* 1 scaler */
#define N_EPICS_VAR_A  0
#endif  //  TWO_SCALERS

/***************************************
  Other quantities and variables 
  **************************************/

#define HEL_DOWN                0         /* helicity down */
#define HEL_UP                  1         /* helicity up */

#define REFERENCE               0         /* ref */
#define SAMPLE                  1         /* sample */
#define FAILURE         0
/* PPG */
#ifdef HAVE_PPG
#ifdef HAVE_NEWPPG
#define PPG_BASE            0x00100000  // NEWPPG
#else
#define PPG_BASE            0x8000   // OLD PPG
#endif
#endif

#ifdef HAVE_PSM
#ifdef BNMR
/* set with Jumpers A17-23 on PSM board
   not now using same address as FSC since PSM,FSC may  be in the same crate 
 Note: "../ppcobj/reload" defines psm_base as this value when ppc booted
 (psm_base is used for debugging psm routines)  
*/
#ifdef HAVE_PSMIII
#define PSM_BASE          0x820000 /* PSMIII in BNMR crate  */
#else
#define PSM_BASE          0x820000 /* PSMII  in BNMR crate */
#endif
#else  // BNQR
#define PSM_BASE          0x800000 /* for now, PSM in BNQR crate  */
#endif // BNMR

#define RF_TRIPPED             0x1  
#endif // HAVE_PSM
/* later PSM in BNQR crate should also have base address as 0xC00000 */



/* SIS3801 */
#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS
// TEMP for testing swap A with B on BNMR (BNMR has 2 scalers)
#define SIS3801_BASE_B      0x001800  /* Module B Base Address was 0     - Interrupts ENabled - MAX_CHAN_SIS38xxB channels of real data  */
#define SIS3801_BASE_A      0x002800  /* Module A Base Address was 0x800 - Interrupts DISabled - MAX_CHAN_SIS38xxA channels of real data */

#else     
#ifdef BNMR  // TEMP  for testing swap A with B on BNMR         
#define SIS3801_BASE_B      0x001800  /* Module B Base Address 0  - Interrupts ENabled - MAX_CHAN_SIS38xxB channels of real data */
#else  // BNQR scaler remains at original address
#define SIS3801_BASE_B      0x002800  /* Module B Base Address 0  - Interrupts ENabled - MAX_CHAN_SIS38xxB channels of real data */
#endif // BNMR
#endif // TWO_SCALERS

#define IRQ_VECTOR_CIP        0x70
#define IRQ_LEVEL                5

#endif // HAVE_SIS3801

#define DATA_MASK         0xffffff  // 24bit data both SIS3801 and SIS3820


#ifdef HAVE_SIS3820 
#ifdef TWO_SCALERS
#define SIS3820_BASE_A   0x30000000  /* gVmeioBase Module A Base Address  Interrupts DISabled - MAX_CHAN_SIS38xxA channels of real data*/
#endif //  TWO_SCALERS
#define SIS3820_BASE_B   0x38000000  /* some suitable base address */


#define SIS3820_BASE_A    0x780000  /* gVmeioBase Module A Base Address 0x780000  Interrupts DISabled - MAX_CHAN_SIS38xxA channels of real data*/
#define SIS3820_FIFO_WORDCOUNTER   0x38  /*  FIFO word counter register RO */
#define SIS_FIFO_SIZE 64 * 1024  /* SIS3820 FIFO is very large. This is the array size for hfifo_A and hfifo_B */
#define SIS3820_FIFO_SIZE_CHECK  32*1024 /* pick a size ; readout if more than this number of words */
//static int gVmeIsIdle = 0;
static int gSisBufferOverflow = 0;
#define  LNE_PRESCALE_FACTOR  1  // fixed for BNM/QR with PPG (variable for SIS test mode 0)
#define NUM_DATA_BITS  24 // 24 bits data format (fixed for BNM/QR)  
// minimum dwell time - otherwise the SIS scaler does not see every LNE from PPG
//#define SIS32_MIN_DWELL_TIME_MS  0.000244  //   minimum dwell time for SIS in ms  (24/32 bits data)
INT data_bytes;  // will be set to 3 for 24 bit data by  get_SIS_dataformat

INT check_lne2(void);
#endif //  HAVE_SIS3820

#ifdef HAVE_SIS3801E
INT check_lne(void);
#endif

/* VME NIMIO32 */
#ifdef HAVE_NIMIO32
#define NIMIO32_BASE 0x100000
#define NUM_OUTPUTS           8        /*  number of output LEMOS fitted  */
#define NUM_INPUTS            4        /*  number of input LEMOS fitted  */

/* outputs */
#define OUTPUT0              0        /* VMEIO Output ch 0  Reserved for Latched Hel DOWN */
#define OUTPUT1              1        /* VMEIO Output ch 1  Reserved for Latched Hel UP   */
#define OUTPUT2              2        /* VMEIO Output ch 2  may be used for simulation test in nimio32 test pgm as beam */
#define OUTPUT3              3        /* VMEIO Output ch 3  40MHz clock usually; do not use as an output unless clock is disabled */

#define OUTPUT4              4        /* VMEIO Output ch 4   may be used for simulation test in nimio32 test pgm as hel up  */
#define OUTPUT5              5        /* VMEIO Output ch 5   may be used for simulation test in nimio32 test pgm as hel dwn */
#define OUTPUT6              6        /* VMEIO Output ch 6     gbl_IN_CYCLE */
#define OUTPUT7              7        /* VMEIO Output ch 7   computer busy (software) (1f mode) */


/* inputs */
#define INPUT0              0        /* VMEIO Input ch 0       Reserved for  Beam     In */
#define INPUT1              1        /* VMEIO Input ch 1       Reserved for  Hel Down In */
#define INPUT2              2        /* VMEIO Input ch 2       Reserved for  Hel Up   In */
#define INPUT3              3        /* VMEIO Input ch 3       Reserved for Latched Hel DOWN (connected to OUTPUT 0) */

#define INPUT4              4        /* VMEIO Input ch 4       Reserved for Latched Hel UP (connected to OUTPUT 1 */
#define INPUT5              5        /* VMEIO Input ch 5       Reserved for PPG DAQ Computer Busy */
#define INPUT6              6        /* VMEIO Input ch 6  */
#define INPUT7              7        /* VMEIO Input ch 7 */

#endif // HAVE_NIMIO32

/* VMEIO */
#ifdef HAVE_VMEIO
#define VMEIO_BASE        0x780000

/* outputs */
#define OUTPUT1              0x1         /* VMEIO Output ch 1 */
#define OUTPUT2              0x2         /* VMEIO Output ch 2 */
#define OUTPUT3              0x4         /* VMEIO Output ch 3 */
#define OUTPUT4              0x8         /* VMEIO Output ch 4 */
#define OUTPUT5              0x10        /* VMEIO Output ch 5 */
#define OUTPUT6              0x20        /* VMEIO Output ch 6 */
#define OUTPUT7              0x40        /* VMEIO Output ch 7 */
#define OUTPUT8              0x80        /* VMEIO Output ch 8 */
/* inputs */       
#define IN_BEAM               0x10         /* input */
#define IN_CYCLE              0x20         /* input */
/* pattern */
/*#define VMEIO_PULSE_BITS      BOB_PULSE | EOB_PULSE | BOC_PULSE | EOC_PULSE | DEBUG1_PULSE | DEBUG2_PULSE 
 */
#define  VMEIO_PULSE_BITS OUTPUT1 | OUTPUT2 | OUTPUT3 | OUTPUT4 | OUTPUT5  
/*   OUTPUT6 will be latched */
#endif //  HAVE_VMEIO
/* init ODB structures and strings */
/* pick up as much as possible from experiment.h */


FIFO_ACQ_FRONTEND fs;
CYCLE_SCALERS_SETTINGS cs;
INFO_ODB_EVENT cyinfo;

#ifdef CAMP_ACCESS
FIFO_ACQ_CAMP_SWEEP_DEVICE fcamp;
#endif //  CAMP_ACCESS

#ifdef EPICS_ACCESS
static EPICS_PARAMS epics_params ; /* structure defined in bnmr_epics.h */
#endif //  EPICS_ACCESS

/* these defined in bnmr_init.c */
BOOL  gbl_IN_CYCLE;
INT  gbl_BIN_A, gbl_BIN_B;  /* current time bin for each module */
INT     gbl_HEL;            /* current helicity state */

DWORD *pfifo_A, *pfifo_B; /* FIFO local buffers */
DWORD   csrdataA,csrdataB;
DWORD   gbl_bin_count;   /* internal cycle # */
INT     exp_mode;        /* =1 for all TYPE1 ; = 2 for others */
char    expt_name[32];
HNDLE hDB, hFS, hEPD, hEPM, hBsf, hInfo; /* these found in bnmr_init */

char beamline[40],BEAMLINE[5]; /* bnmr/bnqr/pol ("experiment_name" is used for ppg_mode) */
BOOL skip_cycle;
BOOL discard_first_bin;
/* end of bnmr_init defines */

/* must keep a NAMES structure for type 1 and type 2 of BNMR */

#ifdef THIRTYTWO

#define CYCLE_SCALERS_TYPE1_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[60] :",\
"[32] Back%BSeg00",\
"[32] Back%BSeg01",\
"[32] Back%BSeg02",\
"[32] Back%BSeg03",\
"[32] Back%BSeg04",\
"[32] Back%BSeg05",\
"[32] Back%BSeg06",\
"[32] Back%BSeg07",\
"[32] Back%BSeg08",\
"[32] Back%BSeg09",\
"[32] Back%BSeg10",\
"[32] Back%BSeg11",\
"[32] Back%BSeg12",\
"[32] Back%BSeg13",\
"[32] Back%BSeg14",\
"[32] Back%BSeg15",\
"[32] Front%FSeg00",\
"[32] Front%FSeg01",\
"[32] Front%FSeg02",\
"[32] Front%FSeg03",\
"[32] Front%FSeg04",\
"[32] Front%FSeg05",\
"[32] Front%FSeg06",\
"[32] Front%FSeg07",\
"[32] Front%FSeg08",\
"[32] Front%FSeg09",\
"[32] Front%FSeg10",\
"[32] Front%FSeg11",\
"[32] Front%FSeg12",\
"[32] Front%FSeg13",\
"[32] Front%FSeg14",\
"[32] Front%FSeg15",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Userbit=0",\
"[32] General%Back Userbit=1",\
"[32] General%Back Userbit=2",\
"[32] General%Back Userbit=3",\
"[32] General%Front Userbit=0",\
"[32] General%Front Userbit=1",\
"[32] General%Front Userbit=2",\
"[32] General%Front Userbit=3",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#define CYCLE_SCALERS_TYPE2_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[60] :",\
"[32] Back%BSeg00",\
"[32] Back%BSeg01",\
"[32] Back%BSeg02",\
"[32] Back%BSeg03",\
"[32] Back%BSeg04",\
"[32] Back%BSeg05",\
"[32] Back%BSeg06",\
"[32] Back%BSeg07",\
"[32] Back%BSeg08",\
"[32] Back%BSeg09",\
"[32] Back%BSeg10",\
"[32] Back%BSeg11",\
"[32] Back%BSeg12",\
"[32] Back%BSeg13",\
"[32] Back%BSeg14",\
"[32] Back%BSeg15",\
"[32] Front%FSeg00",\
"[32] Front%FSeg01",\
"[32] Front%FSeg02",\
"[32] Front%FSeg03",\
"[32] Front%FSeg04",\
"[32] Front%FSeg05",\
"[32] Front%FSeg06",\
"[32] Front%FSeg07",\
"[32] Front%FSeg08",\
"[32] Front%FSeg09",\
"[32] Front%FSeg10",\
"[32] Front%FSeg11",\
"[32] Front%FSeg12",\
"[32] Front%FSeg13",\
"[32] Front%FSeg14",\
"[32] Front%FSeg15",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"[32] General%Back Cumul +",\
"[32] General%Front Cumul +",\
"[32] General%B/F Cumul +",\
"[32] General%Asym Cumul +",\
"[32] General%Back Cumul -",\
"[32] General%Front Cumul -",\
"[32] General%B/F Cumul -",\
"[32] General%Asym Cumul -",\
"",\
NULL }

#else  // TWO channels on Scaler A

#define CYCLE_SCALERS_TYPE1_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[30] :",\
"[32] Back%BSegments",\
"[32] Front%BSegments",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Userbit=0",\
"[32] General%Back Userbit=1",\
"[32] General%Back Userbit=2",\
"[32] General%Back Userbit=3",\
"[32] General%Front Userbit=0",\
"[32] General%Front Userbit=1",\
"[32] General%Front Userbit=2",\
"[32] General%Front Userbit=3",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#define CYCLE_SCALERS_TYPE2_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[30] :",\
"[32] Back%BSegments",\
"[32] Front%FSegments",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"[32] General%Back Cumul +",\
"[32] General%Front Cumul +",\
"[32] General%B/F Cumul +",\
"[32] General%Asym Cumul +",\
"[32] General%Back Cumul -",\
"[32] General%Front Cumul -",\
"[32] General%B/F Cumul -",\
"[32] General%Asym Cumul -",\
"",\
NULL }

#endif // THIRTYTWO

// Debugging
HNDLE hdebug,hdbg,hdppg, hdcamp, hdpsm, hdmps, hdsis;
// see debug.h for dd[NUM_DEBUG_LEVELS
#include "debug.h"
BOOL debug; // turn on/off general debugging from ODB. Not hotlinked.
INT dppg; // debug ppg driver (hotlink)
INT dcamp; // debug camp (hotlink)
INT dpsm; // debug psm  (hotlink)
INT dsis; // debug sis  (hotlink)
INT manual_ppg_start; //  (hotlink)

#endif // febnmr_header
