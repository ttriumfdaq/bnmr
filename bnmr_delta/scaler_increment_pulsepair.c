/* 
 Scaler_increment without discard first bin, with UBIT histo
 for BNMR/BNQR with randomized frequency and pulse pair mode 
 i.e. Modes 2a,2b,etc
 */
/*-- individual scaler histo increment ------------------------------------*/
INT scaler_increment_pulsepair(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*
  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
  - type 2: if random_flag && e2a_flag or e2e_flag or e2f_flag increment correct bin (see note below)
  -         if any other mode 2, histogram bins directly

FIFO data from scaler comes out in this order:
            
ch 0 bin 0 
ch 1 bin 0
........
ch N bin 0

ch 0 bin 1
ch 1 bin 1
.........
ch N bin 1

----------
ch 0 bin M
ch 1 bin M
.........
ch N bin M

NOTE:  Type 2a : randomized
  *gbl_bin = true bin number
   findex =  RF data bin index; findex=0 at first bin with RF
   1st freq bin = number of bins with non-RF data
   actual freq value for this bin = prandom_freq[findex]
   pseqf[findex] = pointer in non-random freq array for this frequency value
  	
  Address to incr      *gbl_bin    Increment  Random   pseqf  Initial  
  data if not random                 at       freq(Hz)        freqency
  (Channel 0)
  ffc300                  0         ffc300        -      -       -  
  ffc304                  1         ffc30c      240000   2      200000   
  ffc308                  2         ffc304      200000   0      220000 
  ffc30c                  3         ffc310      260000   3      240000
  ffc310                  4         ffc308      220000   1      260000
  etc;

Similar tables could be made for the rest of the channels



Enable pulse pairs:
If pulse_pairs are enabled, we histogram based on the userbit1_action:
e2a pairs mode      userbit1_action:
0   pairs - histo all the data; n_bins = n_his_bins = fs.output.num_dwell_times as calculated by rf_config

                  Compaction Modes:
       n_bins =  fs.output.num_dwell_times;  n_his_bins =  n_bins -  output.num_freq_steps
1   first - histo only the first bin of the pair (odd) ; 
2   second - histo only the second bin of the pair (even); 
3   diff - odd-even; 

Action: 1=keep 2=discard 3=diff 

Note: Rf_config ensures number of bins with no RF MUST be set to 0 for pulse pair modes

Histogramming in pulse pair mode=1st (i.e. compaction) not random
 Address to incr      *gbl_bin pseqf UB1 Action   Increment   
  data                                                at         
  (Channel 0)                                                    
  ffc304                  0      0     1    1      ffc304     
  ffc308                  1            0    2        -    
  ffc30c                  2      1     1    1      ffc308      
  ffc310                  3            0    2        -
  ffc314                  4      2     1    1      ffc30C   
  ffc318                  5            0    2        -
  ffc31C                  6      3     1    1      ffc310
  ffc320                  7            0    2        -

  Derandomizing in pulse pair mode (pairs):

 Address to incr      *gbl_bin   UB1    Increment  Random   pseqf        Initial  
  data if not random                      at       freq(Hz)              frequency
  (Channel 0)                                                            Array
  ffc304                  0       0      ffc314      240000   2         200000   
  ffc308                  1       1      ffc318      240000   2         220000 
  ffc30c                  2       0      ffc31C      260000   3         240000
  ffc310                  3       1      ffc320      260000   3         260000
  ffc314                  4       0      ffc30C      220000   1
  ffc318                  5       1      ffc310      220000   1
  ffc31C                  6       0      ffc304      200000   0
  ffc320                  7       1      ffc308      200000   0
*/
{
  DWORD *ps, i, h, ub, ub1, ub2, *pbuf, sdata;
  INT     channel_field;
  INT  index;
  INT hoffset;
  // INT  ntuple_width_h ;
  INT ib,ll;
  INT nbin,im,imh;
  BOOL ntuple,last_ntuple,flag_twice,ub_flag;
  char region[20];
  BOOL discard,subtract;
  INT input_offset;
  INT beam_off_bin=0; // e2f only

  printf("\nscaler_increment_pulsepair: starting with ub_histo_flag=%d fill_uarray=%d fill_uarray_2=%d\n",ub_histo_flag,fill_uarray,fill_uarray_2);
  // Type 1 Modes and Type 20,2s,2w do not use this routine
  if(gbl_nprebins == n_bins)
    {
      printf("scaler_increment_pulsepair: ERROR this routine histograms Type 2 pulse-pair modes only; expect gbl_nprebins != n_bins\n");
      printf("scaler_increment_pulsepair: use scaler_increment for all other modes\n");

      cm_msg(MERROR,"scaler_increment_pulsepair",
	     "Programming ERROR this routine histograms Type 2 pulse-pair modes only; number of prebins should not equal num bins");
      goto retminus1;
    }

  input_offset=0; // inputs start at 0
#ifdef HAVE_SIS3820
  if(maximum_channel != 32)  // Scaler B  NIM inputs start at 16
      input_offset=16;
#endif //  HAVE_SIS3820

  ib=im=ub2=ub1=0; // zero these to get rid of warnings

  ub=discard=subtract=ntuple=flag_twice=0;
  ub_flag=1; /* default - if true, e2a/e2e userbit check is skipped */
  region[0]='\0'; // clear
  pbuf = pfifo;

  /* No user-bit histo for these modes */

  if (dd[1])
    { 
      printf("scaler_increment_pulsepair: starting with  nwords:%d pfifo:%p,  maximum_channel=%d  *gbl_bin=%d\n",
	     nwords, pbuf,maximum_channel,*gbl_bin);
      
#ifdef HAVE_SIS3820
      printf("scaler_increment_pulsepair: SIS3820 inputs are offset from 0 by %d i.e. connected inputs are %d to %d \n",
	     input_offset,input_offset, (input_offset+maximum_channel));
#endif
      
      if(dd[1])printf("scaler_increment_pulsepair n_requested_bins=%d  n_bins=%d n_his_bins=%d gbl_nprebins=%d \n",
		      n_requested_bins, n_bins,n_his_bins,gbl_nprebins);
      
      
      if(e2e_flag || e2a_flag || e2f_flag) printf("scaler_increment_pulsepair: Mode 2a, 2e or 2f random_flag=%d pseqf=%p\n",
						  random_flag,pseqf);
    } // debug
  
  if(e2f_flag)
    beam_off_bin = fs.output.num_dwell_times -  fs.input.e00_beam_off_dwelltimes;
  
  /* e2e/e2a/e2f : supports both random and not random */
  
  if(dd[17])
    {
      printf("\n nwords=%d  scaler_offset=%d",nwords,scaler_offset);
#ifdef TWO_SCALERS
      if(scaler_offset==0)
	printf("  Scaler A");
      else
	printf("  Scaler B");
#endif
      printf("\n");
    }
  
  for (i=0 ; i < nwords ; i++)
    { // main for loop on the number of words
      if(e2a_flag)
	discard=subtract=ntuple=ub_flag=0;
      else if(e2e_flag)
	flag_twice=ntuple=ub_flag=0;
      else if(e2f_flag)
        ub_flag=0;
      
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
      
    
      /* not histogramming userbits  */ 
      *userbit = channel_field >>6;
      ub = *userbit;
      ub1 = ub & 1;
      ub2 = ub & 2;
      
      if(dsis)
	{
	  if(h<1 )
	    printf("scaler_increment_pulsepair: i=%d  *gbl_bin=%d chan %d data 0x%x ub=%d \n",
		   i, *gbl_bin, h,(*pbuf & 0xFFFFF),ub);
	}
  
      /* make sure channel is correct */
#ifdef HAVE_SIS3820
      if (input_offset > 0)
	{
	  //if(dd[1]) printf("Channel h=%d from scaler will be adjusted to h=%d\n",h,(h-input_offset));
	  h=h - input_offset;
	}
#endif
      
      if( h < maximum_channel)
	{
	  
	  index = h + scaler_offset;
	  /* assign pointer */
	  ps = scaler[index].ps;  /* pointer to start of histo  */
	  sdata = *pbuf & DATA_MASK; /* scaler data to be saved */
	  
	  /* find the offset within the histogram for this bin  */
	  
	  if(*gbl_bin < gbl_nprebins)  // SIS3801  *gbl_bin never reached n_bins and starts at 0 (0-999 if 1000 bins)
	    // SIS3820  *gbl_bin reaches n_bins at eoc but should be zeroed before next cycle
	    {   /* PREBINS 
		   
		   PREBINS for 2a/2e/2f modes come here
		   prebins will be histogrammed directly
		*/
	      
	      sprintf(region,"PREBIN bin %d",*gbl_bin);
	      /* directly histo for each channel (pre bins) */
	      hoffset= *gbl_bin  ; /* histo offset within scaler histo for this bin */	
	    }
	  
	  else if (*gbl_bin >=  fs.output.num_dwell_times - gbl_npostbins)
	    {  /*  POSTBINS   */
	      nbin = (*gbl_bin + gbl_npostbins) -  fs.output.num_dwell_times; /* post bin no. */
	      sprintf(region,"POSTBIN bin %d (actual bin %d)",nbin,*gbl_bin );
	      hoffset= gbl_nprebins + gbl_nmidsection_h + nbin ; /* offset within histogram */
	      
	      /* check user bit for last bin */
	      if(*gbl_bin == fs.output.num_dwell_times -1)  // last bin
		{ 
		  ub_flag=1;
		  if(!ub2 && h==0)/* userbin same for all channels - check only 1 */
		    {
		      printf("*gbl_bin=%d, ub=%d; postbins - last bin expected to be flagged with ub2\n",
			     *gbl_bin,ub);
		      cm_msg(MERROR,"scaler_increment_pulsepair","*gbl_bin=%d, ub=%d; postbins - last bin expected to be flagged with ub2",
			     *gbl_bin,ub);
		    }
		}
	      // e2f has never been used
	      if(e2f_flag && h==0)
		{
		  if (*gbl_bin == beam_off_bin)
		    {
		      if ( ub1)
			ub_flag=1; // start of beam off flagged by ub1
		      else
			printf("Expect ub1 to mark start of beam off for e2f at *gbl_bin %d\n",*gbl_bin);
		    }
		  if (*gbl_bin >= beam_off_bin)
		    {
		      nbin = *gbl_bin - beam_off_bin;
		      sprintf(region, "BEAM OFF bin %d (actual bin %d)",nbin, *gbl_bin );
		    }
		}
	    } // end of POSTBINS
	  else 
	    {  /* now we have the NTUPLES	  
		  
		  e2e only.... look for the very first prebins which are part of the 1st ntuple. 
		  e2a and randomized e2f prebins are not part of the ntuple and gbl_ndepthbins =0
	       */
	      
	      ib = *gbl_bin-gbl_nprebins; /* ib = bin counter for ntuples */
	      if (ib < gbl_ndepthbins)
		{/* prebins e2e only */
		  sprintf(region,"Prebin word %d first Ntuple",ib);
		  ntuple=0; /* these bins are part of first ntuple */
		  hoffset= gbl_nprebins + pseqf[ntuple] * gbl_ntuple_width_h + ib ;   
		}
	      else
		{  /* ntuple bins */
		  ll = ib - gbl_ndepthbins;
		  /* calculate ntuple number */
		  ntuple = ll / gbl_ntuple_width_s;
		  if(ntuple > fs.output.num_frequency_steps )/* max ntuple */
		    {
		      printf("ERROR: calculated ntuple number (%d) > max (%d)\n",
			     ntuple, fs.output.num_frequency_steps);
		      cm_msg(MERROR,"scaler_increment_pulsepair","Programming Error: calculated ntuple number (%d) > max (%d)",
			     ntuple, fs.output.num_frequency_steps);
		      goto retminus1;
		    }
		  /* check for last ntuple & set a flag */
		  if( (ntuple+1) <  fs.output.num_frequency_steps )
		    last_ntuple=FALSE;
		  else
		    last_ntuple=TRUE; /* e2e data will be flagged by user bit 1 */
		  
		  im= ll %  gbl_ntuple_width_s  ; /* im is counter in scaler's ntuple */
		  imh= im+ gbl_ndepthbins; /* counter in histo's ntuple */
		  
		  /* RF region of ntuple... used for both e2e and e2a */
		  if(im < gbl_nRFbins)
		    { // RF region of ntuple
		      sprintf(region,"RFbin word %d",im);
		      /* if running 2a, deal with pulse-pair mode now */
		      if(e2a_flag)
			{  /* e2a pulse-pair mode : 
			      fs.output.e2a_pulse_pairs_mode = 9  not pulse pair mode
			      = 0  pairs mode
			      = 1  keep first bin of pair
			      = 2  keep second bin of pair
			      = 3  difference
			      
			      mode > 0 ... sort according to ub1 */
			  if( fs.output.e2a_pulse_pairs_mode < 4) /* pulse-pairs modes 0-3 */
			    {
			      ub_flag=1; /* special userbits; don't check ubins later */
			      
			      if(fs.output.e2a_pulse_pairs_mode==1)
				{       /* e2a mode 1 : save first bin of pulse-pairs (imh=0) */
				  if(!ub1)
				    discard=TRUE;
				}
			      else if(fs.output.e2a_pulse_pairs_mode == 2)
				{  /* mode 2 : save second bin of pulse-pairs (imh=1) */
				  if(ub1)
				    discard=TRUE;
				  else
				    imh=0; /* save data in the only RFbin in the Histo */
				}
			      else if ( fs.output.e2a_pulse_pairs_mode== 3)
				{ /* mode 3 : subtract second bin of pulse-pairs (imh=1) */
				  if(!ub1)
				    {
				      subtract=TRUE;
				      imh=0; /*  save data in the only RFbin in the Histo */
				    }
				}
			      else if ( fs.output.e2a_pulse_pairs_mode== 0)
				{
				  ub_flag = TRUE;
				  /* check for h==0 only */
				  if(h==0)
				    {
				      /* ub1 cycles on and off */
				      if (imh==0 && !ub1)
					printf("*gbl_bin=%d, 1st pulse of pair should have ub1 true ub=%d\n", *gbl_bin,ub); 
				      if (imh==1 && ub1)
					printf("*gbl_bin=%d, 2nd pulse of pair should have ub1 false ub=%d\n", *gbl_bin,ub); 
				    }
				}
			      if(dd[17])
				printf("imh %d ub1 %d e2a pulse pair mode %d discard %d subtract %d\n",
				       imh,ub1,fs.output.e2a_pulse_pairs_mode,discard,subtract);
			    } /* end of e2a pulse-pair mode enabled */
			  else 
			    {
			      ub_flag=TRUE;
			      /* e2a NOT pulse pair mode has ub1 set for all ntuple bins */
			      if (!ub1 && h==0) /* userbin same for all channels - check only 1 */
				printf("*gbl_bin=%d, ub=%d; RFbin e2a NOT pulsepair... expect ub1 to be true\n",
				       *gbl_bin,ub);
			      
			    } /* end of e2a NOT pulse pair */
			} /* end of e2a flag */
		      else if(e2e_flag)
			{ /*  2e mode */
			  if(last_ntuple)
			    {  /* check userbit for h=0 (all channels are the same)*/
			      ub_flag=TRUE;
			      if(!ub1 && h==0) /* only write the message for one channel */
				printf("*gbl_bin=%d, ub=%d; RFbin last ntuple... expect ub1 to be true\n",
				       *gbl_bin,ub);
			    }
			}
		      else if(e2f_flag)
			{ /* e2f flag
			     all frequency ntuple bins should have UB1 set TRUE */
			  
			  sprintf(region,"RFbin %d (actual bin %d) ",ntuple,*gbl_bin);
			  ub_flag = TRUE;
			  if (!ub1 && h==0) /* userbin same for all channels - check only 1 */
			    printf("*gbl_bin=%d, ub=%d; RFbin Mode e2f  ... expect ub1 to be true\n",
				   *gbl_bin,ub);
			  
			}
		    } /* end of RF region of ntuple */
		  
		  
		  else
		    { /* these are 2e POST RF depth bins in this Ntuple */
		      if( !last_ntuple )
			{  /* except for very last Ntuple.. they also form PRE RF depth bins for next Ntuple */
			  sprintf(region,"Postbin word %d (%d)",im,imh); 		    
			  flag_twice=1; /* flag these; they have to be histogrammed twice */
			  
			  /*  but are they from the last but one ntuple, i.e. prebins for last ntuple? */
			  if( ntuple+1 ==  fs.output.num_frequency_steps  - 1)
			    {
			      /* yes... they should be flagged with ub1 */
			      if(!ub1 && h==0) /* print message once only */
				{
				  printf("*gbl_bin=%d, ub=%d; expect postbins for penultimate ntuple to be flagged by ub1\n",
					 *gbl_bin,ub);
				  cm_msg(MERROR,"scaler_increment_pp",
					 "*gbl_bin=%d, ub=%d; expect postbins for penultimate ntuple to be flagged by ub1",
					 *gbl_bin,ub);
				}
			      ub_flag=TRUE; /* do not check ub */
			    }
			}  		    
		      else /* last ntuple, histogram bins only once as POST...check userbits */
			{
			  flag_twice=0;
			  ub_flag=TRUE;
			  
			  if(!ub1 && h==0) 
			    { /* print message once only; all channels are the same */
			      printf("*gbl_bin=%d, ub=%d; expect last ntuple postbins to be flagged by ub1\n",
				     *gbl_bin,ub);
			      cm_msg(MERROR,"scaler_increment_pp",
				     "*gbl_bin=%d, ub=%d; expect last ntuple postbins to be flagged by ub1",
				     *gbl_bin,ub);
			    }
			  if(imh ==  gbl_ndepthbins)
			    { /* last ntuple & last POST depth bin should also have ub2 true*/
			      if(!ub2 && h==0)/* userbin same for all channels - check only 1 */
				{
				  printf("*gbl_bin=%d, ub=%d; expect very last ntuple postbin to also be flagged by ub2\n",
					 *gbl_bin,ub);
				  cm_msg(MERROR,"scaler_increment_pp",
					 "*gbl_bin=%d, ub=%d; expect very last ntuple postbin to also be flagged by ub2",
					 *gbl_bin,ub);
				}
			      sprintf(region,"Postbin last word %d (%d) last Ntuple",im,imh);
			    }
			  else
			    sprintf(region,"Postbin word %d (%d) last Ntuple",im,imh); 
			} /* end of last Ntuple */
		    } /* end of 2e  POST-RF bins of Ntuple */	
		  
		  /* calculate offsets to histogram these RF or post-depth bins for this ntuple */
		  /*         start of ntuples + which ntuple +  offset in ntuple */	      
		  hoffset= gbl_nprebins + pseqf[ntuple]*gbl_ntuple_width_h + imh ; 
		} // end of ntuple bins
	    } // end of NTUPLES
	      
	    
	  /* 
	     Histogram the Data 
	  */
	  
	  /* Regular debugging */
	  if(dd[1])
	    {
	      if(i<=100 && h == 0)
		{
		  printf("i,pbuf,*pbuf,h,ub,sdata,*gbl_bin, hoffset: %d, %p %x, %d, %d, %x, %d, %d\n",
			 i,pbuf,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		  
		  if(i == nwords -1 )
		    {
		      printf("Last i,pbuf,h,ub,sdata,*gbl_bin,hoffset: %d, %x, %d, %d, %x, %d,  %d\n",
			     i,*pbuf,h,ub,sdata,*gbl_bin,hoffset); 
		    }
		}
	    } // if dd[1]
	  
	  
	   
	  /* 2e/2a/2f debugging.... will dump every single bin e2e/e2a/e2f only */
	  //   if(dd[17])printf("i=%d ub=%d *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s",
	  //		       i,ub,*gbl_bin,h,ntuple,ntuple,pseqf[ntuple],region);
	  
	  
	  
	  if(dd[17])
	    { /* 2e/2a/2f debugging channel 0 */
	      if(h==0)
		printf(" i=%d ub=%d  *gbl_bin=%d ch=%d hoffset=%d %s",
		       i,ub,*gbl_bin,h,hoffset,region);
	    }
	  
	  if(!ub_flag) /* check the User Bits are zero (unless ub_flag is set for a region of 2e/2a/2f) */
	    {
	      if(h==0 && ub != 0)
		{
		  
		  printf(" Unexpected user bits set  (ub=%d) at gbl_bin %d \n",ub,*gbl_bin);
		  printf("  where i=%d ub=%d  *gbl_bin=%d ch=%d  %s",
			 i,ub,*gbl_bin,h,region);
		}
	    }
	  
	  /* check offset within histogram  */
	  if(hoffset > n_his_bins )
	    {
	      if(e2e_flag || e2a_flag || e2f_flag)  // e2f has same number of scaler bins as histo bins
		{
		  if(!dd[17])printf("i=%d ub=%d *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s\n",
				    i,ub,*gbl_bin,h,ntuple,ntuple,pseqf[ntuple],region);
		}
	      printf("\n ERROR histo offset out of range (%s): hoffset=%d  nhis=%d programming error \n",
		     region,hoffset,n_his_bins);
	      goto retminus1;
	    }
		
	      
	      
	      
	      
	  /* Sum data, and histogram at offset hoffset */
	  if(e2a_flag)
	    {
	      if(subtract)
		{ /* e2a pulse pair mode Diff only */
		  
		  if(dd[17]) 
		    printf("subtracting data %d from stored data (%d)  at hoffset=%d\n",
			   sdata, *(ps+hoffset), hoffset);
		  *(ps+hoffset) -= sdata;   /* DIFF... subtract this data */
		  scaler[index].sum_cycle += (double)sdata; /* sum this data */
		  discard = TRUE; /* data alread dealt with */
		}
	      else if(discard)
		if(dd[17])printf("discarded data at *gbl_bin=%d\n", *gbl_bin);
	    }
	  else /* end of 2a_flag */
	    discard = FALSE;
	  
	  if (!discard)
	    {
	      if(dd[17])
		{
		  if(h==0)
		    printf(" moving scaler data =%d (actual bin=%d) at to hoffset %d\n",
			   sdata,*gbl_bin, hoffset);
		}
	      /*  Now histogram the data
		  
		  Add current time bin content to cumulative scaler histo */
	      *(ps+hoffset) += sdata;	  
	      /* increment local cycle sum scaler */
	      scaler[index].sum_cycle += (double)sdata;
	    }
	  
	  
	  /* e2e only.....
	     Now histogram data for post-depth bins also as 
	     pre-depth bins for the next ntuple (except for last one) */
	  if(e2e_flag && flag_twice)
	    {
	      flag_twice=0; 
	      if(ntuple +1 <=  fs.output.num_frequency_steps )
		{
		  sprintf(region, "is also Prebin word %d (%d)",im,im - gbl_nRFbins);
		  /*     start of ntuples + next ntuple +     offset in next ntuple */	      
		  hoffset= gbl_nprebins + pseqf[ntuple+1]*gbl_ntuple_width_h + im - gbl_nRFbins ; 
		  if(dd[17])printf("i=%d ub=%d *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s",
				   i,ub,*gbl_bin,h,ntuple+1,ntuple+1,pseqf[ntuple+1],region);
		  
		  
		  /* check the offsets */
		  if(hoffset > n_his_bins)
		    {
		      if(!dd[17])printf("i=%d  *gbl_bin=%d ch=%d ntuple=%d pseqf[%d]=%d %s",
					i,*gbl_bin,h,ntuple+1,ntuple+1,pseqf[ntuple+1],region);
		      printf("\nERROR histogram offset is out of range (%s): hoffset=%d  nhis=%d \n",
			     region,hoffset,n_his_bins);
		      
		      goto retminus1;
		    }
		  if(dd[17])printf(" moving data=%d to histo[%d]\n", sdata,hoffset);
		  
		  
		  /* add current time bin content to cumulative scaler histo */
		  *(ps+hoffset) += sdata;
		  
		  /* do not sum this; it has already been summed above */
		}
	    } /* end of e2e flag_twice */
	      
	      
	      
	      /* next incoming data */
	  pbuf++;  
	      
	  /* Increment time bin every h = N_SCALER_REAL */
	  if (h == (maximum_channel-1) )
	    {
	      (*gbl_bin)++;   // actual (true) bin number
	      if(dd[1])
		printf("h=%d, maximum_channel=%d incremented *gbl_bin to %d\n",h, maximum_channel, *gbl_bin);
	    } /* end of h == max channel-1 */
	}/* end of h< max_channel */
      else
	printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x  and *gbl_bin=%d\n",i, nwords, h, *pbuf, *gbl_bin);
	  
     
   
    } // loop on the number of words

  if(dsis)
    printf("\nscaler_increment_pulsepair: success, returning with *gbl_bin %d\n",*gbl_bin);
  return 0; /* success */
  
 retminus1:
  return -1;
}


