/*   setup_scan_params.h

*/
#ifndef _setup_scan_params_
#define _setup_scan_params_

/* Function prototypes */
INT mode10_set_scan_params(INT *ninc);
INT random_set_scan_params(INT *ninc);
INT  RF_set_scan_params(INT *ninc);
// mode 1w
INT set_scan_params_1w(INT *ninc);
INT read_freq_file_1w(void);
INT check_psm_freqval_1w(INT freq_hz);
INT decode_fscans_1w(INT *nen,  int (**fptrs), char *str);
INT decode_ninc_1w(BOOL *gotincr, char *str);
#ifdef CAMP_ACCESS
INT camp_set_scan_params(INT *ninc);
#endif

#ifdef EPICS_ACCESS
INT epics_set_scan_params(INT *ninc);
#endif

#endif //  _setup_scan_params_
