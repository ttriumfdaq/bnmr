// sis3801_code.h
#ifndef SISCODE1
#define SISCODE1

INT NumSisChannels_A, NumSisChannels_B;

// prototypes
INT get_SIS_dataformat(INT nbits, INT NumSisChannels );

void SIS_readback(unsigned int sis_base, unsigned int wrote_mode, unsigned int wrote_csr);
void opmode_bits(unsigned int data);
void csr_bits(unsigned int data);
int nchan_enabled(unsigned int bp, int en );
INT get_enabled_channels_bitpat(int num_ch_enabled);
#endif
