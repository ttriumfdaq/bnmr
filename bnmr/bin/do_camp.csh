#!/bin/csh 
#   (-x for debug)
# It checks the automatic header
# variables, making sure they are logging.  It ensures all logging variables
# are being polled.  It zeros the statistics on *ALL* camp variables.  
# 
# Further, for type-1 experiments, the list of logged variables is retrieved
# from Camp and put into the ODB.
#
# The camp_Cd commands setupLoggedVar and checkLoggedVars are helper procs 
# defined in camp.ini, so much of the work is performed within Camp itself.

# Add  input parameters :
#  0 default 
#  1 (for redo camp from custom page) to stop statistics being zeroed
#
#  $ppgmode 
#
#  $logfile_path
#
#
# NOTE: msg [type] [user] <msg> - compose user message
#
#  odb msg 1 -> error msg  (in black/red)  
#  odb msg 2 -> info msg   (in black/white)
#  odb -e  $MIDAS_EXPT_NAME -c 'msg script "this is yet another test" ' -> [script]this is another test
#  odb -e  $MIDAS_EXPT_NAME -c 'msg 1 script  "this is yet another test" ' ->  [ODBEdit1] this is yet another test
#  odb -e  $MIDAS_EXPT_NAME -c 'msg 2 script  "this is yet another test" ' ->  [ODBEdit1] [odbedit.c:2424:scr] this is yet another test
#
set MAX_LOGGED = 16


echo "argv: $argv ; number of args: $#argv" 
#echo "do_camp.sh starting with argv: $argv ; number of args: $#argv" > $fout
set param = 0

echo "number of arguments: $#argv";
if ($#argv < 3) then
  echo "not enough arguments" 
  exit
endif

 set param = $argv[1];
 set ppgmode = $argv[2];
 set logpath = $argv[3];
 
 
 set fout = "$logpath/do_camp.tmp"
 
 echo "fout=$fout"


echo "param: $param ; ppgmode $ppgmode;  $MIDAS_EXPT_NAME" >> $fout


if ($param == 0) then
  # zero statistics at begin of run only (param = 0)
  # Zero all Camp statistics
  echo "zeroing Camp statistics"
  echo "zeroing Camp statistics" >> $fout
  /home/musrdaq/musr/camp/linux/camp_cmd 'foreach v [sysGetInsNames] {varDoSet /$v -z}'

  # Zero run statistics (work-around bug)
  odb -e $MIDAS_EXPT_NAME -c 'set "/Equipment/FIFO_acq/mdarc/histograms/output/histogram totals"[*] 0'
  odb -e $MIDAS_EXPT_NAME -c 'set "/Equipment/FIFO_acq/mdarc/histograms/output/total saved" 0'
  odb -e $MIDAS_EXPT_NAME -c "msg   'at_start_run' 'Zeroed run statistics' "
else
   echo "NOT zeroing Camp statistics"
   echo "NOT zeroing Camp statistics" >> $fout
   odb -e $MIDAS_EXPT_NAME -c "msg   'at_start_run' 'Redoing camp log: run stats have NOT been zeroed' "
endif

switch ( "${ppgmode}x" )
  case 1*:
    set logppg = "log_mdarc log_mdarc_ti log_i_musr"
    breaksw
  case 2*:
    set logppg = "log_mdarc log_mdarc_td log_td_musr"
    breaksw
endsw
echo "Collect logging actions $logppg for ppg mode $ppgmode " 
echo "Collect logging actions $logppg for ppg mode $ppgmode " >> $fout

# Get list of logged variables, ensuring online and polling (using checkLoggedVars in Camp)
set resp = `/home/musrdaq/musr/camp/linux/camp_cmd "checkLoggedVars $logppg"`
if ( $status != 0 ) then
   set message="Variable logging problem: $resp"
   echo $message
   echo $message >> $fout

#  split this message and send it in pieces if it is long ( > 79 characters); 
#                      ( limit is about 200 characters or msg fails )
#  split.pl sends the message to odb
   echo $message | $my_path/split.pl
 
#  Limit length of Alarm Message in odb  or will get record size mismatch error
   set nchar = $%message
   if($nchar > 79 ) then    # limit length of Alarm Message in odb or get record size mismatch error
      set message = "Non-fatal error from log CAMP variable. Click on Messages button for details"
      echo "Alarm Message has had to be truncated. Number of characters now is $%message"
   endif

   odb -e $MIDAS_EXPT_NAME -c "set '/Alarms/Alarms/elog/Alarm Message' '$message'" 

   # trigger odb alarm /equipment/fifo_acq/client flags/elog alarm
   odb -e $MIDAS_EXPT_NAME -c 'set "/equipment/fifo_acq/client flags/elog alarm" 1'
   goto err
else
   set loggedvars = "$resp"
   echo "Initial list: $resp "
   echo "Initial list: $resp " >> $fout
endif

# For type-1 experiments, copy list of logged variables (and their poll intervals?)
# to the odb at /Equipment/Camp/Settings
switch ( "${ppgmode}x" )
  case 1*:
    set fin = "/home/$MIDAS_EXPT_NAME/tmp/odbcampvars"
    echo "fin=$fin"
    echo "cd /Equipment/Camp/Settings" > $fin
    set idx = ( 0 )
    foreach var ( $loggedvars )
       echo "Var $var"
       if ( $idx < $MAX_LOGGED ) then
          echo "set var_path[$idx] '${var}'" >> $fin
       endif
       @ idx ++
    end
    if ( $idx <= $MAX_LOGGED ) then
       echo "set n_var_logged $idx " >> $fin
    else 
       echo "set n_var_logged $MAX_LOGGED " >> $fin
       set message = "Too many Camp variables to log (${idx}); using $MAX_LOGGED "
       odb -e $MIDAS_EXPT_NAME -c "msg '1'  'at_start_run' '$message'"
       odb -e $MIDAS_EXPT_NAME -c "set '/Alarms/Alarms/elog/Alarm Message' '$message'"

       # trigger odb alarm /equipment/fifo_acq/client flags/elog alarm
       odb -e $MIDAS_EXPT_NAME -c 'set "/equipment/fifo_acq/client flags/elog alarm" 1'
    endif
    odb -e $MIDAS_EXPT_NAME -c @$fin
    breaksw
endsw

echo "success"
echo "success" >> $fout
odb -e $MIDAS_EXPT_NAME -c "msg 'at_start_run' '(exit) sets camp ok to 1'"
odb -e $MIDAS_EXPT_NAME -c 'set "/equipment/camp/settings/camp ok" 1' # success
exit 

err:
    echo "failure"
    echo "failure" >> $fout
    odb -e $MIDAS_EXPT_NAME -c "msg '1'  'at_start_run' '(err) sets camp ok to 0'"
    odb -e $MIDAS_EXPT_NAME -c 'set "/equipment/camp/settings/camp ok" 0' # failure
    exit
end
