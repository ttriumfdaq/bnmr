set "/custom/hidden/poll on perl" 0
set "/custom/hidden/clicked ppgmode button" n
set "/custom/hidden/clicked runmode button" n
set "/custom/hidden/clicked toggle button" n
set "/runinfo/transition in progress" 0
set "/equipment/FIFO_acq/client flags/mdarc" y
set "/equipment/FIFO_acq/client flags/rf_config" y
set "/equipment/FIFO_acq/client flags/mheader" y
set "/equipment/FIFO_acq/client flags/frontend" y
set "/equipment/FIFO_acq/client flags/fe_epics" y
set "/equipment/FIFO_acq/client flags/client alarm" 0
set "/equipment/FIFO_acq/client flags/epics access alarm" 0
set "/equipment/FIFO_acq/client flags/prestart_failure" n
set "/equipment/FIFO_acq/client flags/start_abort" n
set "/equipment/FIFO_acq/client flags/hel_mismatch" n
set "/equipment/FIFO_acq/mdarc/toggle" n
set "/equipment/FIFO_acq/mdarc/enable prestart rn check" n
msg 2 "kill-all: init_odb.com has run"
