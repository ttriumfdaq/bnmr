#!/bin/tcsh
# Zaher Salman, 7 October 2005.

# Donald Arseneau  7 May 2007
#   $Log: elog_every_run.csh,v $
#   Revision 1.5  2018/06/13 00:39:05  suz
#   add check so elog runs on DAQ_HOST
#
#   Revision 1.4  2014/09/03 21:34:48  suz
#   add single/dual mode
#
#   Revision 1.2  2013/05/10 21:21:32  suz
#   change melog to elog for external elog
#
#   Revision 1.1  2013/04/25 20:05:26  suz
#   this version supports both bnmr and bnqr
#
#   Revision 1.4  2008/12/10 22:33:45  suz
#   two elogs now; do not clear camp_ok here
#
#   Revision 1.3  2008/05/01 22:29:01  suz
#   fix some bugs with MIDAS_EXPT_NAME
#
#   Revision 1.2  2008/05/01 22:21:06  suz
#   make common between bnmr and bnqr
#


# This script is started at the end of each run. It takes the values
# for the statistics of the logged devices in camp and produces a 
# nice looking entry in the appropriate elog

# Match to at_start_run.csh script
#
# Check for input files
if ($#argv == 1) then
  if (-e $1) then
    echo "Processing from file"
    set cmd = `echo 'load '$1`
    odb -e $MIDAS_EXPT_NAME -c "$cmd"
  endif
endif
echo "exp:   $MIDAS_EXPT_NAME"
#                This is you T           This is you H        This is the power
#set Log_Vars = ('/Sample/read_A' '/Magnet/mag_field' '/rf_power/max_pow_2')
# changed /Magnet/mag_field to match camp variable /magnet/mag_field 14Jul06

# Retry getting logged vars from Camp if there is a time signature or brackets that
# would confuse a foreach command (in tcsh)
set Log_Vars = `camp_cmd "sysGetLoggedVars"`
if ("$Log_Vars" =~ *{:[0-9][0-9]:[0-9][0-9],[\[\]\n]}*) set Log_Vars = (`camp_cmd "sysGetLoggedVars"`)

echo "Logging: $Log_Vars"


# Collect Camp paths for automatic headers from odb.  Note that bad or 
# unmatching values, or even odb errors, won't match one of the logged
# vars, so will not be used.  Retry the ODB call if it seems to contain 
# a timestamp.
set Tvar = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/alias/Auto Headers&/temperature variable"'`
if ("$Tvar" =~ *:[0-9][0-9]:[0-9][0-9]*) set Tvar = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/alias/Auto Headers&/temperature variable"'`
set Hvar = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/alias/Auto Headers&/field variable"'`
if ("$Hvar" =~ *:[0-9][0-9]:[0-9][0-9]*) set Hvar = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/alias/Auto Headers&/field variable"'`
set RFvar = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/alias/Auto Headers&/RF power variable"'`
if ("$RFvar" =~ *:[0-9][0-9]:[0-9][0-9]*) set RFvar = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/alias/Auto Headers&/RF power variable"'`

# This is the file where the elog entry is saved temporarily
set fin = "/home/$MIDAS_EXPT_NAME/tmp/info_for_elog.txt"
if (-e $fin) then
  rm -f $fin
endif
touch $fin

# This is the file where the camp variables log is saved temporarily
# They are saved separately because we want to list them underneath some
# numbers that may depend on them.
set fcam = "/home/$MIDAS_EXPT_NAME/tmp/campstats_elog.txt"
echo "Logged Camp variables:" > $fcam

# Start collecting information from ODB first. Retry (once) any time the result 
# seems to contain a timestamp, likely from an unrelated error message.

set number = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Runinfo/Run number"'`
if ("$number" =~ *:[0-9][0-9]:[0-9][0-9]*) set number = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Runinfo/Run number"'`

set sample = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/sample"'`
if ("$sample" =~ *:[0-9][0-9]:[0-9][0-9]*) set sample = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/sample"'`

set T = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/temperature"'`
if ("$T" =~ *:[0-9][0-9]:[0-9][0-9]*) set T = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/temperature"'`

set H = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/field"'`
if ("$H" =~ *:[0-9][0-9]:[0-9][0-9]*) set H = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/field"'`

# There is no RF entry in the run headers, but there is an RF variable (maybe) in the ODB
set RF = '??'

set author = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/experimenter"'`
if ("$author" =~ *:[0-9][0-9]:[0-9][0-9]*) set author = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/experimenter"'`

set title = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/run_title"'`
if ("$title" =~ *:[0-9][0-9]:[0-9][0-9]*) set title = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/run_title"'`

set exp_num = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/experiment number"'`
if ("$exp_num" =~ *:[0-9][0-9]:[0-9][0-9]*) set exp_num = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Experiment/Edit on Start/experiment number"'`

# path for VMIC has been changed
#set exp_type = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Equipment/FIFO_acq/sis mcs/Input/Experiment name"'`
set exp_type = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Equipment/FIFO_acq/frontend/Input/Experiment name"'`
if ("$exp_type" =~ *:[0-9][0-9]:[0-9][0-9]*) set exp_type = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Equipment/FIFO_acq/frontend/Input/Experiment name"'`
set type_dir = "/PPG/PPG$exp_type"
#set type = `odb -e $MIDAS_EXPT_NAME -c "ls  $type_dir" | sed 's/ ->.*//'  `
#set type = `odb -e $MIDAS_EXPT_NAME -c "ls  $type_dir" |  sed 's/ ->.*//'| sed '/^ /s/.*/&%/' | tr -d '\n' | tr '%' '\n' | tr -s ' '`
#echo $type

set dual_chan_mode = `odb -e $MIDAS_EXPT_NAME -c 'ls -v "/Equipment/FIFO_acq/frontend/Hardware/Enable dual channel mode"'`
if ($dual_chan_mode == 'y') then
   set mode = "dual channel"
else
   set mode = "single channel"
endif

# Finished with the ODB

# Now start collecting information from camp.
# For the specified variables get statistics

set widest = 1

# Determine widest Camp path in list (just for pretty printing)
foreach i ($Log_Vars)
     if ( $%i > $widest ) then
        set widest = $%i
     endif
end

foreach i ($Log_Vars)

     echo "Camp Variable $i"

# Get number of averaged readings
     set stats = `camp_cmd "list [varNumGetNum $i] [varNumGetSum $i] [varNumGetSumSquares $i] [varNumGetSumOffset $i]"`
     if ( $status == 0 ) then
        set NUM = "$stats[1]"
	set SUM = "$stats[2]"
        set SUMSQ = "$stats[3]"
        set OFSET = "$stats[4]"
        set units = `camp_cmd "varNumGetUnits $i"`
     else
        set NUM = 0
        set units = ""
        echo "Non-numeric Variable $i"
     endif

     if ( $NUM > 0 ) then
	set MEAN = `echo $SUM $NUM $OFSET | awk '{print ($1/$2+$3)""}'`
        if ($NUM == 1) then
# No error for one measurement
  	   set ERR = "0"
        else
	   set ERR = `echo $SUM $NUM $SUMSQ | awk '{print sqrt(($3-($1*$1/$2))/($2-1.0)) ""}'`
        endif
     else
# If NUM=0 just use current value
        set MEAN = `camp_cmd "varGetVal $i"`
	set ERR = "0"
     endif


# Now depending on what type of variable you are reading make a nice
# looking number out of it...
     if ("x$MEAN" != "x") then
       echo "Raw Mean: $MEAN"
       switch ( "un$units" )
             case unK:
# it is a temperature: round it to 3 fractional digits
                set MEAN = `echo $MEAN | awk '{print int($1*1000)/1000}'`
                set ERR = `echo $ERR | awk '{print int($1*1000)/1000}'`
                breaksw
             case unT:
# it is a field, in Tesla: round it to 4 fractional digits 
                set MEAN = `echo $MEAN | awk '{print int($1*10000)/10000}'`
                set ERR = `echo $ERR | awk '{print int($1*10000)/10000}'`
                breaksw
             case unW:
# it is the *attenuated* RF power, in W: convert to unattenuated mW
	        set MEAN = `echo $MEAN | awk '{print $1*10000000}'`
	        set ERR = `echo $ERR | awk '{print $1*10000000}'`
                breaksw
       endsw
       if ($ERR != 0) then
	    set MEAN = "$MEAN+-$ERR"
       endif

       echo $widest $i $MEAN | awk '{printf "    %-*s  %s \n", $1, $2, $3}' >> $fcam

       if ( "x$Tvar" == "x$i" ) then
           set T = "$MEAN"
       endif

       if ( "x$Hvar" == "x$i" ) then
           set H = "$MEAN"
       endif

       if ( "x$RFvar" == "x$i" ) then
           set RF = "$MEAN"
       endif

    endif
end

# Finished calculating average and error for all camp logged variables

# Now create the temporary file to be sent to the elog
echo "Run # $number  PPG Mode $exp_type $mode" >> $fin
odb -e $MIDAS_EXPT_NAME -c 'ls "/Runinfo/Start time"' >> $fin
odb -e $MIDAS_EXPT_NAME -c 'ls "/Runinfo/Stop time"' >> $fin
echo "$sample at T = $T K, H = $H T and RF = $RF mW">> $fin
echo "Run Title   : $title" >> $fin
echo "Experimenter: $author" >> $fin
echo "Experiment #: $exp_num" >> $fin
echo "-------------------------------------------------------------" >>$fin
# latest midas shows links as well - remove them
odb -e $MIDAS_EXPT_NAME -c "ls -r $type_dir"  |  sed 's/ ->.*//'| sed '/^ /s/.*/&%/' | tr -d '\n' | tr '%' '\n' | tr -s ' ' >> $fin
echo "-------------------------------------------------------------" >>$fin
cat $fcam >> $fin
echo "-------------------------------------------------------------" >>$fin
# This last one needed to ensure previous gets into elog:
echo " " >>$fin

# You left the sample empty, I'll just put none.
if ("x$sample" == "x") then 
   set sample = 'none'
endif
# You left the author empty, I'll just put my name then.
if ("x$author" == "x") then 
   set author = 'Auto'
endif



# Send information to the elog for any run (test or real)
###if ($number >= 40000) then 
# Nov 08 We were previously using only one elog (BNMR).
# Now people seem to want two elogs (bnmr,bnqr). 
# (If mhttpd for BNMR was not running, BNQR was unable to elog as melog did not work)
# So change to send to the elog for this experiment 

set logbook = ${MIDAS_EXPT_NAME}online
set pwd = ${MIDAS_EXPT_NAME}2k14
#echo "pwd $pwd logbook $logbook"

# port for external elog
set port = '8089'


echo "about to send elog (expt $MIDAS_EXPT_NAME, port $port , host $HOST, logbook $logbook)"

# For unknown reasons, bnqr has decided to run at_end_run.csh as lxbnqr rather
# than isdaq01 as expected. elog is built to run on isdaq01.
# so added a test for hostname, if not DAQ_HOST then use ssh 
echo  $HOST > ~/temp
grep --silent $DAQ_HOST ~/temp
if ( "$?" != "0" )  then 
   echo "elog will be run with ssh to $DAQ_HOST  (NOT on $HOST)"
   cat $fin

   echo cmd is  ssh $DAQ_HOST  /home/midas/packages/elog/elog -h $DAQ_HOST  -p $port -l $logbook -u $MIDAS_EXPT_NAME $pwd  -a author=$author -a Type="Automatic Elog" -a System="PPG$exp_type" -a Subject="$sample"  -m $fin

   ssh $DAQ_HOST  /home/midas/packages/elog/elog -h $DAQ_HOST  -p $port -l $logbook -u $MIDAS_EXPT_NAME $pwd  -a author=$author -a Type="Automatic Elog" -a System="PPG$exp_type" -a Subject="$sample"  -m $fin
else
   cat $fin
# melog for internal elog
#melog -h $HOST -p $port -l $MIDAS_EXPT_NAME -a author=$author -a Type="Automatic Elog" -a System="Elog" -a Subject="$sample"  -m $fin

   echo cmd is /home/midas/packages/elog/elog -h $HOST  -p $port -l $logbook -u $MIDAS_EXPT_NAME $pwd  -a author=$author -a Type="Automatic Elog" -a System="PPG$exp_type" -a Subject="$sample"  -m $fin

# elog for external elog
   /home/midas/packages/elog/elog -h $HOST  -p $port -l $logbook -u $MIDAS_EXPT_NAME $pwd  -a author=$author -a Type="Automatic Elog" -a System="PPG$exp_type" -a Subject="$sample"  -m $fin
endif
