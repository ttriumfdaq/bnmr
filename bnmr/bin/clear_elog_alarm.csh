#!/bin/csh 
#
# clear elog and epics alarms
#
echo "$MIDAS_EXPT_NAME"
odb -e $MIDAS_EXPT_NAME -c "set '/equipment/FIFO_acq/client flags/elog alarm' 0"
odb -e $MIDAS_EXPT_NAME -c "set '/equipment/FIFO_acq/client flags/epicslog alarm' 0"
odb -e $MIDAS_EXPT_NAME -c "set '/equipment/fifo_acq/client flags/epics access alarm' 0"
odb -e $MIDAS_EXPT_NAME -c "alarm"
exit 
