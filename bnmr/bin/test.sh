#!/bin/csh

# Start the PPG control program rf_config
odb -c scl | grep --silent  rx_config
if ( "$?" != "0" )  then
    echo "Starting rf_config"
    if ( 0 && $?DISPLAY )  then 
    xterm -geometry 74x37+920+330 -fg $FGCOL -bg $BGCOL \
	  -e /usr/bin/screen -t "${MIDAS_EXPT_NAME} rf_config" -S ${MIDAS_EXPT_NAME}_rf_config \
              ~/$ONLINE/$MIDAS_EXPT_NAME/rx_config  -e $MIDAS_EXPT_NAME &
    else
       #echo "Starting rf_config as a daemon"
       /usr/bin/screen -d -m -t "${MIDAS_EXPT_NAME} rf_config" -S ${MIDAS_EXPT_NAME}_rf_config \
          ~/$ONLINE/$MIDAS_EXPT_NAME/rx_config  -e $MIDAS_EXPT_NAME
    endif
endif
