// Contains sis3801 functions

INT init_sis3801(DWORD base, BOOL enable_ref1)
{ 
#ifdef  HAVE_SIS3801
  // Modes defined in sis3801_Setup
  const int real_mode=5; // real mode
  //const int test_mode=1; // default test mode
  int mode;

  if(fs.sis_test_mode.sis3801.test_mode)
    {
      mode = fs.sis_test_mode.sis3801.test_setup_mode_number; // test mode number
    }	  
  else
    mode=real_mode;  // 5

  sis3801_Setup(gVme,base,mode);  // sets up test or real mode (in sis3801.c)
 
  if(enable_ref1)
    {
      printf("Enabling Ref ch1 \n");
      sis3801_ref1(gVme, base, SIS3801_ENABLE_REF_CH1_WO);
    }
  else
  {
      printf("Disabling Ref ch1 \n");
      sis3801_ref1(gVme, base, SIS3801_DISABLE_REF_CH1_WO);
    }

  SIS3801_Status(gVme, base);
#endif
  return SUCCESS;
}


INT check_sis3801(MVME_INTERFACE *mvme, DWORD base, DWORD *id)
{
#ifdef HAVE_SIS3801
  DWORD module_id;
  DWORD modid;
  DWORD firmware;

  modid = sis3801_module_ID(mvme, base);
  *id=modid;
  printf("check_sis3801: Base Address 0x%x  ModuleID and Firmware: 0x%x\n", base, modid);
  firmware = (modid & 0xFFFF)>>12;
  module_id = modid >> 16;
  printf("check_sis3801:  Base Address 0x%x  Module ID: 0x%x and Firmware: 0x%x\n", base, module_id,firmware);
  if (module_id != 0x3801)
    {
      printf("check_sis3801:  Wrong VME module (0x%x) is installed at Base Address 0x%x . Expecting a SIS3801 \n",modid,base);
      cm_msg(MERROR, "check_sis3801","Wrong VME module (0x%x) is installed at Base Address 0x%x . Expecting a SIS3801",modid, base);
      return FE_ERR_HW;
    }
 


#ifdef HAVE_SIS3801E
  if(firmware == 0xE) 
    printf("check_sis3801: a SIS3801 with Firmware \"E\" has been detected at base address 0x%x\n",base);
  else
    {
      printf("check_sis3801: SIS3801 at base address 0x%x DOES NOT HAVE Firmware \"E\" \n",base);
#ifdef TWO_SCALERS
      if (base ==  SIS3801_BASE_A )
	printf("  temporary - allowing Module A to have older firmware (Module B will count LNE\n");
      OLD_SISA_FLAG=1;
#else
        printf("        Cannot run with a VMIC\n");
        return  FE_ERR_HW;
#endif
    }
#endif // HAVE_SIS3801E
  return SUCCESS;
#endif // HAVE_SIS3801
  return FAILURE; // HAVE_SIS3801 is not defined
}





/*-- Setup SIS ---------------------------------------------------------*/
INT sis3801_setup_bor(DWORD nchan, float dwell)
/*------------------------------------------------------------------------*/
/*
  - Initialize the SIS3801 module(s) based on the rm struct and /settings
  - Enable channels 
  - Check and set dwelling time
  Inputs:  nchan        no. of channels (= # of hardware scalers)
	   dwell        dwell time (FULL TEST mode ONLY)

  NOTE - SIS3801 module can enable up to 24 channels individually 
         SIS3801E module  can enable up to 16 channels individually

         Anything larger enable all 32
        
*/
{
#ifdef HAVE_SIS3801

  DWORD   nchan_A,nchan_B; /* number of enabled REAL channels for SIS modules */
  
  nchan_A=MAX_CHAN_SIS38xxA;

  if (dsis) printf ("sis3801_setup_bor: starting with nchan = %d\n",nchan);
  /* check & set number of active channels */
#ifdef TWO_SCALERS
  if (nchan < MAX_CHAN_SIS38xxA)
  {
    printf("sis3801_setup_bor: Invalid number of channels supplied (%d)\n",nchan);
    printf("   Expect %d for SIS3801 module A  plus at least 1 from module B\n",MAX_CHAN_SIS38xxA);
    return FE_ERR_HW;
  }
  nchan_B = nchan -  MAX_CHAN_SIS38xxA;  /* find how many channels in 2nd module */
#else
  nchan_B = nchan;
#endif
  if (nchan_B >  MAX_CHAN_SIS38xxB)
  {
    printf("sis3801_setup_bor: Number of channels for SIS3801 module B (%d) must be less than %d\n",nchan_B,MAX_CHAN_SIS38xxB);
    return FE_ERR_HW;
  }

  sis3801_channel_enable(gVme, SIS3801_BASE_B, nchan_B); 
#ifdef TWO_SCALERS
  sis3801_channel_enable(gVme, SIS3801_BASE_A, nchan_A);
  if(dsis) printf("sis3801_setup_bor: SIS enabled channels : Module A %d Module B %d\n", nchan_A,
         nchan_B);
#else
  if(dsis) printf("sis3801_setup_bor: SIS enabled channels : Module B %d\n", nchan_B);
#endif 

  if(fs.sis_test_mode.sis3801.test_mode)
    {  // write the dwell_time parameter  10MHz internal LNE 
      DWORD factor = (DWORD) 10000 *  fs.sis_test_mode.sis3801.dwell_time__ms_;
      printf("test_mode: Writing %d to prescale factor to hopefully get the correct dwell time \n",(int)factor);
        sis3801_dwell_time(gVme,  SIS3801_BASE_B, factor);      /* Dwell time  */
#ifdef TWO_SCALERS
  sis3801_dwell_time(gVme,  SIS3801_BASE_A, factor);      /* Dwell time  */
#endif
    }
  else
    /* Using an external pulse (i.e. from PPG) for dwell time */
    //if(dsis) 
    printf("sis3801_setup_bor: Using external next pulse\n");

#ifdef HAVE_SIS3801E
  // write the preset acquisition count i.e. number of bins
  printf("sis3801_setup_bor: Firmware E, writing number of bins = LNE preset = %u to preset register \n",n_bins); // global 
#ifdef TWO_SCALERS
  if(OLD_SISA_FLAG)
    printf("OLD FIRMWARE for Scaler A... NOT writing number of acquisitions\n");
  else
    {
      sis3801_write_preset(gVme,  SIS3801_BASE_A, (n_bins-1)); // set number of acquisitions (LNE or bins)
      printf("sis3801_setup_bor: wrote preset %u (n_bins-1) to base address 0x%x\n",(n_bins-1), SIS3801_BASE_A);  
    }
#endif
  sis3801_write_preset(gVme,  SIS3801_BASE_B, (n_bins-1)); // set number of acquisitions (LNE or bins)   
  printf("sis3801_setup_bor: wrote preset %u (n_bins-1) to base address 0x%x\n",(n_bins-1), SIS3801_BASE_B);  


#endif //  HAVE_SIS3801E
#endif // HAVE_SIS3801
  return FE_SUCCESS ;
}




// VMIC must be used with SIS3801 Firmware Version E 
// This code was for VxWorks
#ifdef VXWORKS  //  interrupts for VxWorks not VMIC
#ifdef HAVE_SIS3801
/* -------------------------------------------------------------------*/

void isr_CIP(void)

/* -------------------------------------------------------------------*/

/*
  Interrupt service routine for CIP (Copy In Progress)
  Takes about 4us +2us jitter.

  Note: CIP signal is used to count the bins (decrements)

*/
{
  sysIntDisable(IRQ_LEVEL);

#ifdef TWO_SCALERS
  csrdataA = sis3801_CSR_read(SIS3801_BASE_A, CSR_FULL);
#endif
  csrdataB = sis3801_CSR_read(SIS3801_BASE_B, CSR_FULL);

#ifdef TWO_SCALERS
  sis3801_int_source_disable(SIS3801_BASE_A, SOURCE_CIP);
#else
  sis3801_int_source_disable(SIS3801_BASE_B, SOURCE_CIP);
#endif

 
  if (gbl_bin_count <= 0)
  {
    /* end of cycle */  
#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS  
    sis3801_next_logic(SIS3801_BASE_A, DISABLE_NEXT_CLK); /* disable SIS */ 
#endif
    sis3801_next_logic(SIS3801_BASE_B, DISABLE_NEXT_CLK); /* disable SIS */ 
#endif // HAVE_SIS3801

    /*    vmeio_pulse_write(VMEIO_BASE, EOC_PULSE); *//* send end-of-cycle pulse */
    /* vmeio_latch_write(VMEIO_BASE, 0); */
    /* indirectly by disabling the PPG */
#ifdef HAVE_PPG
    /*  previously here we did a test between type1 or type2 
	for single/dual channel mode, PPG is no longer free-running for Type 2 
	if(exp_mode == 1) */
    ppgStopSequencer(PPG_BASE); 
#endif // HAVE_PPG
    gbl_IN_CYCLE = FALSE;
  }
  else
  {
    gbl_bin_count--; /* decrement bin count */
#ifdef HAVE_SIS3801
#ifdef TWO_SCALERS
    sis3801_int_source_enable(SIS3801_BASE_A, SOURCE_CIP);
#else
    sis3801_int_source_enable(SIS3801_BASE_B, SOURCE_CIP);
#endif // TWO_SCALERS 
#endif //   HAVE_SIS3801
   sysIntEnable(IRQ_LEVEL);
  }
}
#endif // SIS3801
#endif // VMIC
