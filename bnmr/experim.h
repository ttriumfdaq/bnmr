/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Wed Sep 25 14:13:42 2019

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      color[32];
  char      sample[32];
  char      run_description[256];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"color = STRING : [32] yellow",\
"sample = STRING : [32] gold",\
"Run Description = STRING : [256] Test run",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  char      run_title[128];
  DWORD     experiment_number;
  char      experimenter[34];
  char      sample[61];
  char      orientation[15];
  char      temperature[80];
  char      field[80];
  INT       number_of_scans;
  BOOL      write_data;
  BOOL      edit_run_number;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"run_title = STRING : [128] Imprinted aPS, 6.55 T, HV=23 kV, T=300K, SLR (restart based on helicity mismatch)",\
"experiment number = DWORD : 1760",\
"experimenter = STRING : [34] wam, mhd, df, vlk",\
"sample = STRING : [61] Imprinted aPS",\
"orientation = STRING : [15] ",\
"temperature = STRING : [80] 300.000(0.002)K",\
"field = STRING : [80] 65500.8(0.0)G",\
"Number of scans = LINK : [48] /Equipment/FIFO_acq/frontend/hardware/num scans",\
"write data = LINK : [35] /Logger/Channels/0/Settings/Active",\
"Edit run number = BOOL : n",\
"",\
NULL }

#ifndef EXCL_EPICS

#define EPICS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} EPICS_COMMON;

#define EPICS_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 9",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] feEpics",\
"Frontend file name = STRING : [256] frontend.c",\
"Status = STRING : [256] Ok",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define EPICS_SETTINGS_DEFINED

typedef struct {
  struct {
    INT       epics;
  } channels;
  struct {
    struct {
      char      channel_name[20][32];
      BOOL      enabled;
    } epics;
  } devices;
  char      names[20][32];
  float     update_threshold_measured[20];
} EPICS_SETTINGS;

#define EPICS_SETTINGS_STR(_name) const char *_name[] = {\
"[Channels]",\
"Epics = INT : 24",\
"",\
"[Devices/Epics]",\
"Channel name = STRING[20] :",\
"[32] BNMR:VAR1",\
"[32] BNMR:VAR2",\
"[32] BNMR:VAR3",\
"[32] BNMR:VAR4",\
"[32] BNMR:VAR5",\
"[32] BNMR:VAR6",\
"[32] BNMR:VAR7",\
"[32] BNMR:VAR8",\
"[32] BNMR:VAR9",\
"[32] BNMR:VAR10",\
"[32] BNMR:VAR11",\
"[32] BNMR:VAR12",\
"[32] BNMR:VAR13",\
"[32] BNMR:VAR14",\
"[32] BNMR:VAR15",\
"[32] BNMR:VAR16",\
"[32] BNMR:VAR17",\
"[32] BNMR:VAR18",\
"[32] BNMR:VAR19",\
"[32] BNMR:VAR20",\
"Enabled = BOOL : y",\
"",\
"[.]",\
"Names = STRING[20] :",\
"[32] Front+Back in Cycle",\
"[32] Asymmetry for Cycle",\
"[32] Back for Cycle",\
"[32] Front for Cycle",\
"[32] B/F for cycle",\
"[32] Pol Left",\
"[32] Pol Right",\
"[32] Pol. Cycle Sum",\
"[32] Pol. Cycle Asym",\
"[32] NeutBeam Back Sum",\
"[32] NeutBeam Front Sum",\
"[32] NeutBeam B+F Sum",\
"[32] NeutBeam Asym",\
"[32] not used",\
"[32] Cycle #",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"[32] Watchdog",\
"Update Threshold Measured = FLOAT[20] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 2",\
"[8] 2",\
"[9] 2",\
"[10] 2",\
"[11] 2",\
"[12] 2",\
"[13] 2",\
"[14] 2",\
"[15] 2",\
"[16] 2",\
"[17] 2",\
"[18] 2",\
"[19] 2",\
"",\
NULL }

#endif

#ifndef EXCL_CAMP

#define CAMP_SETTINGS_DEFINED

typedef struct {
  INT       camp_ok;
  char      perl_script[80];
  INT       cvar_event_period__s_;
  INT       n_var_logged;
  char      var_path[16][132];
  INT       polling_interval[16];
} CAMP_SETTINGS;

#define CAMP_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"camp OK = INT : 0",\
"perl_script = STRING : [80] camp.pl",\
"cvar event period (s) = INT : 5",\
"n_var_logged = INT : 13",\
"var_path = STRING[16] :",\
"[132] /mass_flow/read_flow",\
"[132] /rf_level_cont/dac_set",\
"[132] /Needle/read_position",\
"[132] /CryoEx_MassFlow/read_flow",\
"[132] /CryoEx_MassFlow/set_flow",\
"[132] /Cryo_level/He_level",\
"[132] /Cryo_level/N2_level",\
"[132] /Sample/read_A",\
"[132] /Sample/read_B",\
"[132] /Sample/read_C",\
"[132] /Sample/setpoint_1",\
"[132] /Sample/current_read_1",\
"[132] /PVac/adc_read",\
"[132] /PVac/adc_read",\
"[132] ",\
"[132] ",\
"polling_interval = INT[16] :",\
"[0] 10",\
"[1] 10",\
"[2] 10",\
"[3] 10",\
"[4] 10",\
"[5] 10",\
"[6] 10",\
"[7] 10",\
"[8] 10",\
"[9] 10",\
"[10] 10",\
"[11] 10",\
"[12] 10",\
"[13] 10",\
"[14] 10",\
"[15] 10",\
"",\
NULL }

#define CAMP_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} CAMP_COMMON;

#define CAMP_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 13",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 5000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] mheader",\
"Frontend file name = STRING : [256] src/mheaderBnmr.c",\
"Status = STRING : [256] mheader@isdaq01.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_HEADER

#define HEADER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HEADER_COMMON;

#define HEADER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 14",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 17",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] mheader",\
"Frontend file name = STRING : [256] src/mheaderBnmr.c",\
"Status = STRING : [256] mheader@isdaq01.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_HDIAGNOSIS

#define HDIAGNOSIS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HDIAGNOSIS_COMMON;

#define HDIAGNOSIS_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 4",\
"Trigger mask = WORD : 16",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] /home/bnmr/online/bnmr_delta/febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_FIFO_ACQ

#define FIFO_ACQ_FRONTEND_DEFINED

typedef struct {
  struct {
    char      ppg_loadfile[128];
    char      ppg_template[64];
    float     beam_on_time__ms_;
    DWORD     max_histo_bins;
    float     frequency_stop__hz_;
    DWORD     num_frequency_steps;
    DWORD     e1_number_of_incr__ninc_;
    DWORD     e1_number_of_cycles_per_scan;
    float     dwell_time__ms_;
    DWORD     num_dwell_times;
    float     ppg_cycle_time__ms_;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    DWORD     compiled_file_time__binary_;
    char      compiled_file_time[32];
    char      e1c_camp_path[32];
    char      e1c_camp_ifmod[10];
    char      e1c_camp_instr_type[32];
    char      e1n_epics_device[32];
    INT       scan_jump_inc1;
    INT       scan_jump_inc2;
    float     scan_jump_value1;
    float     scan_jump_value2;
    DWORD     last_seed__for_random_freqs_;
    BOOL      epics_dual_channel_switch;
    DWORD     e2a_pulse_pairs_mode;
    DWORD     e1b_bg_delay__dwelltimes_;
    DWORD     e1b_rf_delay__dwelltimes_;
    DWORD     e1b_rf_on__dwelltimes_;
    DWORD     e1b_rf_off__dwelltimes_;
    BOOL      vme_beam_control;
    DWORD     e2e_num_histo_bins_per_ch;
    DWORD     e2e_histo_ntuple_width__bins_;
    DWORD     e2e_num_post_ntuple_bins;
    DWORD     e2e_ntuple_depth__bins_;
    DWORD     e2e_num_beam_off_dwelltimes;
    DWORD     e2e_scaler_ntuple_width__bins_;
    DWORD     e2f_num_beam_on_dwelltimes;
    float     e2s_total_daq_time__ms_;
    float     epics_bnmr_beam_period;
    float     epics_bnmr_beam_delay;
    float     epics_bnqr_beam_period;
    float     epics_bnqr_beam_delay;
    struct {
      INT       lne_frequency__hz_;
      DWORD     lne_preset;
      DWORD     modid_a;
      DWORD     modid_b;
    } sis3820;
    struct {
      DWORD     modid_a;
      DWORD     modid_b;
      DWORD     lne_preset;
    } sis3801;
    struct {
      BOOL      device_disabled;
      INT       max_cycles_iq_pairs__ncmx_;
      INT       num_iq_pairs__niq_[5];
      INT       cic_interpoln_rate__ncic_[5];
      float     bandwidth__rad_per_sec_[5];
      float     pulse_width__msec_[5];
      INT       num_cycles_iq_pairs__nc_[5];
      float     linewidth_mod_param__a_[5];
      float     amplitude_mod_param__alpha_[5];
    } psm;
  } output;
  struct {
    DWORD     num_cycles;
    DWORD     fluor_monitor_thr;
    float     cycle_thr1;
    float     cycle_thr2;
    float     cycle_thr3;
    INT       diagnostic_channel_num;
    BOOL      re_reference;
    BOOL      out_of_tol_repeat_scan;
    DWORD     skip_ncycles_out_of_tol;
    DWORD     num_polarization_cycles;
    DWORD     polarization_switch_delay;
    BOOL      enable_sis_ref_ch1_scaler_a;
    BOOL      enable_sis_ref_ch1_scaler_b;
    BOOL      enable_helicity_flipping;
    INT       helicity_flip_sleep__ms_;
    BOOL      enable_all_hel_checks;
    BOOL      enable_epics_switch_checks;
    BOOL      enable_dual_channel_mode;
    BOOL      check_rf_trip;
    INT       num_scans;
    float     rf_trip_threshold__0_5v_;
    INT       skip_ncycles_laser_tol;
    BOOL      disable_laser_thr_check;
    BOOL      enable_sampleref_mode;
    BOOL      daq_drives_sampleref;
    INT       dummy;
    struct {
      INT       sis_mode;
      DWORD     input_mode;
      DWORD     output_mode;
      struct {
        DWORD     enable_channels_mask;
      } module_a;
      struct {
        DWORD     enable_channels_mask;
      } module_b;
      BOOL      send_mcs_bank;
      BOOL      discard_first_bin;
      BOOL      hot_debug;
    } sis3820;
    struct {
      char      operating_mode[8];
      char      module[8];
      DWORD     idle_freq__hz_;
      BOOL      freq_end_sweep_jump_to_idle;
      BOOL      freq_sweep_load_1st_val_in_idle;
      DWORD     fref_tuning_freq__hz_;
      struct {
        BOOL      profile_enabled;
        BOOL      simulate_single_tone_mode;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__max_plus_or_minus_511_;
          INT       idle_q__max_plus_or_minus_511_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__max_plus_or_minus_511_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__max_plus_or_minus_511_;
        } iq_modulation;
        INT       gate_control;
      } one_f;
      struct {
        BOOL      profile_enabled;
        BOOL      simulate_single_tone_mode;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__max_plus_or_minus_511_;
          INT       idle_q__max_plus_or_minus_511_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__max_plus_or_minus_511_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__max_plus_or_minus_511_;
        } iq_modulation;
        INT       gate_control;
      } fref;
      struct {
        BOOL      profile_enabled;
        BOOL      simulate_single_tone_mode;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__max_plus_or_minus_511_;
          INT       idle_q__max_plus_or_minus_511_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__max_plus_or_minus_511_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__max_plus_or_minus_511_;
        } iq_modulation;
        INT       gate_control;
      } three_f;
      struct {
        BOOL      profile_enabled;
        BOOL      simulate_single_tone_mode;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__max_plus_or_minus_511_;
          INT       idle_q__max_plus_or_minus_511_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__max_plus_or_minus_511_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__max_plus_or_minus_511_;
        } iq_modulation;
        INT       gate_control;
      } five_f;
      struct {
        BOOL      channel_enabled;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__511__i__512_;
          INT       idle_q__511__q__512_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__511__i__512_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__511__q__512_;
        } iq_modulation;
        INT       gate_control;
      } f0ch1;
      struct {
        BOOL      channel_enabled;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__511__i__512_;
          INT       idle_q__511__q__512_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__511__i__512_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__511__q__512_;
          BOOL      apply_phase_correction;
          float     phase_correction__degrees_;
          INT       q_phase_correction__calc_;
          INT       i_phase_correction__calc_;
        } iq_modulation;
        INT       gate_control;
      } f0ch2;
      struct {
        BOOL      channel_enabled;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__511__i__512_;
          INT       idle_q__511__q__512_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__511__i__512_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__511__q__512_;
          BOOL      apply_phase_correction;
          float     phase_correction__degrees_;
          INT       q_phase_correction__calc_;
          INT       i_phase_correction__calc_;
        } iq_modulation;
        INT       gate_control;
        float     phase_correction__degrees_;
      } f0ch3;
      struct {
        BOOL      channel_enabled;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__511__i__512_;
          INT       idle_q__511__q__512_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__511__i__512_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__511__q__512_;
          BOOL      apply_phase_correction;
          float     phase_correction__degrees_;
          INT       q_phase_correction__calc_;
          INT       i_phase_correction__calc_;
        } iq_modulation;
        INT       gate_control;
      } f0ch4;
      struct {
        BOOL      channel_enabled;
        BOOL      quadrature_modulation_mode;
        INT       scale_factor__def_181_max_255_;
        struct {
          char      moduln_mode__ln_sech_hermite_[32];
          INT       requested_bandwidth__hz_;
          BOOL      jump_to_idle_iq;
          BOOL      load_first_val_in_idle;
          INT       idle_i__511__i__512_;
          INT       idle_q__511__q__512_;
          BOOL      load_i_q_pairs_file;
          BOOL      set_constant_i_value_in_file;
          INT       const_i__511__i__512_;
          BOOL      set_constant_q_value_in_file;
          INT       const_q__511__q__512_;
          BOOL      apply_phase_correction;
          float     phase_correction__degrees_;
          INT       q_phase_correction__calc_;
          INT       i_phase_correction__calc_;
        } iq_modulation;
        INT       gate_control;
      } f1;
      struct {
        float     e2s_3f_phase_correction_qpc3;
        float     e2s_5f_phase_correction_qpc5;
        float     e2s_fref_phase_correction_qpcf;
      } phase;
    } psm;
  } hardware;
  struct {
    struct {
      BOOL      test_mode;
      DWORD     num_bins;
      float     dwell_time__ms_;
      INT       test_setup_mode_number;
    } sis3801;
    struct {
      INT       lne_prescale_factor;
      INT       num_bins_requested;
      struct {
        DWORD     test_pulse_mask;
      } module_a;
      struct {
        DWORD     test_pulse_mask;
      } module_b;
    } sis3820;
  } sis_test_mode;
  struct {
    BOOL      hold;
  } flags;
  struct {
    char      experiment_name[32];
    char      cfg_path[128];
    char      ppg_path[128];
    float     beam_off_time__ms_;
    DWORD     num_beam_precycles;
    DWORD     num_beam_acq_cycles;
    DWORD     frequency_start__hz_;
    DWORD     frequency_stop__hz_;
    DWORD     frequency_increment__hz_;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    DWORD     num_rf_on_delays__dwell_times_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    float     mcs_max_delay__ms_;
    float     daq_service_time__ms_;
    float     minimal_delay__ms_;
    float     time_slice__ms_;
    float     e1b_dwell_time__ms_;
    float     rf_delay__ms_;
    float     bg_delay__ms_;
    DWORD     e1f_num_dwell_times;
    DWORD     num_rf_cycles;
    INT       e2b_num_beam_on_dwell_times;
    BOOL      check_recent_compiled_file;
    DWORD     freq_single_slice_width__hz_;
    DWORD     num_freq_slices;
    float     prebeam_on_time__ms_;
    float     flip_360_delay__ms_;
    float     flip_180_delay__ms_;
    float     f_slice_internal_delay__ms_;
    char      beam_mode;
    char      counting_mode;
    float     f_select_pulselength__ms_;
    DWORD     e00_prebeam_dwelltimes;
    DWORD     rfon_dwelltime;
    DWORD     e00_beam_on_dwelltimes;
    DWORD     e00_beam_off_dwelltimes;
    INT       rfon_duration__dwelltimes_;
    INT       num_type1_frontend_histograms;
    INT       num_type2_frontend_histograms;
    INT       num_type2_fe_histos_sr_mode;
    INT       num_type2_fe_histos_mode_2s;
    char      e1a_and_e1b_pulse_pairs[4];
    char      e1a_and_e1b_freq_mode[3];
    BOOL      randomize_freq_values;
    float     e2c_beam_on_time__ms_;
    INT       num_cycles_per_supercycle;
    float     navolt_start;
    float     navolt_stop;
    float     navolt_inc;
    float     laser_start;
    float     laser_stop;
    float     laser_inc;
    float     e1c_camp_start;
    float     e1c_camp_stop;
    float     e1c_camp_inc;
    float     field_start;
    float     field_stop;
    float     field_inc;
    float     scan_jump_start;
    float     scan_jump_stop;
    BOOL      enable_scan_jump;
    BOOL      e2a_pulse_pairs;
    BOOL      e2a_180;
    char      e2a_ubit1_action[10];
    DWORD     e2e_num_dwelltimes_per_freq;
    DWORD     e2e_num_rf_dwelltimes_per_freq;
    DWORD     e2e_num_postrfbeamon_dwelltimes;
    BOOL      e1f_const_time_between_cycles;
    float     e1f_min_daq_service__ms_;
    float     e2s_rf_pi_on__ms_;
    float     e2s_rf_half_pi_on__ms_;
    float     e2s_z_dwell_time__ms_;
    float     e2s_x_dwell_time__ms_;
    INT       e2s_num_fid_cycles;
    INT       e2s_select_mode;
    BOOL      e2s_enable_pi_pulse;
    INT       e2s_pi_pulse_cycle_num;
    INT       e2w_central_freq_f0__khz_;
    INT       e2w_frequency_sweep__khz_;
    float     e2w_li_precession_f1__khz_;
    INT       e2w_n__40_or_80_;
    INT       e2w_q0__default_5_;
    INT       e2w_freq_resolution_nf;
    INT       e2w_total_num_freq_scans;
    float     e2w_tp__ms_;
    float     e2s_xy_flip_half_pulse__ms_;
    char      e1w_f1_function[64];
    char      e1w_f2_function[64];
    char      e1w_f3_function[64];
    char      e1w_f4_function[32];
    INT       e1w_x_start;
    INT       e1w_x_stop;
    INT       e1w_x_incr;
    INT       e1w_y_constant;
    BOOL      e1n_use_custom_var;
    float     e1n_custom_start;
    float     e1n_custom_stop;
    float     e1n_custom_inc;
    char      e1n_custom_epics_var_read[32];
    char      e1n_custom_epics_var_write[32];
    float     e1n_custom_tolerance;
  } input;
  struct {
    BOOL      debug;
    BOOL      dppg;
    BOOL      dcamp;
    BOOL      dpsm;
    BOOL      manual_ppg_start;
    BOOL      dsis;
    INT       hot_debug_level___1_clr_;
    INT       hot_threshold_trip;
  } debug;
} FIFO_ACQ_FRONTEND;

#define FIFO_ACQ_FRONTEND_STR(_name) const char *_name[] = {\
"[Output]",\
"PPG_loadfile = STRING : [128] 00.ppg",\
"PPG Template = STRING : [64] 00_0P.ppg",\
"beam on time (ms) = FLOAT : 4000",\
"Max histo bins = DWORD : 128000",\
"frequency stop (Hz) = FLOAT : 4.1275e+07",\
"num frequency steps = DWORD : 101",\
"e1 number of incr (ninc) = DWORD : 51",\
"e1 number of cycles per scan = DWORD : 102",\
"dwell time (ms) = FLOAT : 10",\
"num dwell times = DWORD : 1650",\
"ppg cycle time (ms) = FLOAT : 16500",\
"RF on time (ms) = FLOAT : 0",\
"RF off time (ms) = FLOAT : 0",\
"compiled file time (binary) = DWORD : 1564500443",\
"compiled file time = STRING : [32] Tue Jul 30 08:27:23 2019",\
"e1c Camp Path = STRING : [32] /biasV",\
"e1c Camp IfMod = STRING : [10] 0",\
"e1c Camp Instr Type = STRING : [32] galil_rio47xxx",\
"e1n Epics device = STRING : [32] none",\
"scan jump inc1 = INT : 0",\
"scan jump inc2 = INT : 0",\
"scan jump value1 = FLOAT : 0",\
"scan jump value2 = FLOAT : 0",\
"last seed (for random freqs) = DWORD : 943613758",\
"epics dual channel switch = BOOL : n",\
"e2a pulse pairs mode = DWORD : 9",\
"e1b Bg delay (dwelltimes) = DWORD : 0",\
"e1b RF delay (dwelltimes) = DWORD : 20",\
"e1b RF on (dwelltimes) = DWORD : 20",\
"e1b RF off (dwelltimes) = DWORD : 4",\
"VME beam control = BOOL : n",\
"e2e num histo bins per ch = DWORD : 509",\
"e2e histo ntuple width (bins) = DWORD : 5",\
"e2e num post ntuple bins = DWORD : 2",\
"e2e ntuple depth (bins) = DWORD : 2",\
"e2e num beam off dwelltimes = DWORD : 0",\
"e2e scaler ntuple width (bins) = DWORD : 3",\
"e2f num beam on dwelltimes = DWORD : 35",\
"e2s total daq time (ms) = FLOAT : 345",\
"epics bnmr beam period = FLOAT : -1",\
"epics bnmr beam delay = FLOAT : -1",\
"epics bnqr beam period = FLOAT : -1",\
"epics bnqr beam delay = FLOAT : -1",\
"",\
"[Output/sis3820]",\
"LNE frequency (Hz) = INT : 0",\
"LNE preset = DWORD : 21",\
"Modid_A = DWORD : 0",\
"Modid_B = DWORD : 941621516",\
"",\
"[Output/sis3801]",\
"Modid_A = DWORD : 939646976",\
"Modid_B = DWORD : 0",\
"LNE preset = DWORD : 0",\
"",\
"[Output/PSM]",\
"device disabled = BOOL : y",\
"max cycles iq pairs (Ncmx) = INT : 4095",\
"num IQ pairs (Niq) = INT[5] :",\
"[0] 2048",\
"[1] 2039",\
"[2] 2039",\
"[3] 2039",\
"[4] 2039",\
"CIC interpoln rate (Ncic) = INT[5] :",\
"[0] 63",\
"[1] 63",\
"[2] 63",\
"[3] 63",\
"[4] 63",\
"bandwidth (rad per sec) = FLOAT[5] :",\
"[0] 628.2064",\
"[1] 2512.825",\
"[2] 2512.825",\
"[3] 314.0367",\
"[4] 314.0367",\
"pulse width (msec) = FLOAT[5] :",\
"[0] 159.1834",\
"[1] 39.79584",\
"[2] 39.79584",\
"[3] 35.28",\
"[4] 35",\
"num cycles iq pairs (Nc) = INT[5] :",\
"[0] 1647",\
"[1] 157",\
"[2] 157",\
"[3] 157",\
"[4] 157",\
"linewidth mod param (A) = FLOAT[5] :",\
"[0] 0.1",\
"[1] 0.1",\
"[2] 0.1",\
"[3] 0.39714",\
"[4] 0.2",\
"amplitude mod param (alpha) = FLOAT[5] :",\
"[0] 5",\
"[1] 5",\
"[2] 5",\
"[3] 2.2",\
"[4] 1.1",\
"",\
"[Hardware]",\
"num cycles = DWORD : 0",\
"Fluor monitor thr = DWORD : 0",\
"Cycle thr1 = FLOAT : 0",\
"Cycle thr2 = FLOAT : 0",\
"Cycle thr3 = FLOAT : 50",\
"Diagnostic channel num = INT : 10",\
"Re-reference = BOOL : n",\
"out-of-tol repeat scan = BOOL : n",\
"skip ncycles out-of-tol = DWORD : 1",\
"num polarization cycles = DWORD : 0",\
"polarization switch delay = DWORD : 0",\
"Enable SIS ref ch1 scaler A = BOOL : n",\
"Enable SIS ref ch1 scaler B = BOOL : y",\
"Enable helicity flipping = BOOL : y",\
"helicity flip sleep (ms) = INT : 3000",\
"enable all hel checks = BOOL : y",\
"enable epics switch checks = BOOL : y",\
"Enable dual channel mode = BOOL : n",\
"Check RF trip = BOOL : y",\
"Num scans = INT : 0",\
"RF trip threshold (0-5V) = FLOAT : 0.75",\
"skip ncycles laser-tol = INT : 1",\
"disable Laser thr check = BOOL : y",\
"enable SampleRef mode = BOOL : n",\
"DAQ drives SampleRef = BOOL : n",\
"dummy = INT : 0",\
"",\
"[Hardware/sis3820]",\
"SIS mode = INT : 1",\
"Input Mode = DWORD : 3",\
"Output Mode = DWORD : 2",\
"",\
"[Hardware/sis3820/module_A]",\
"Enable Channels mask = DWORD : 0",\
"",\
"[Hardware/sis3820/module_B]",\
"Enable Channels mask = DWORD : 4026597375",\
"",\
"[Hardware/sis3820]",\
"send mcs bank = BOOL : n",\
"discard first bin = BOOL : y",\
"hot debug = BOOL : y",\
"",\
"[Hardware/PSM]",\
"Operating Mode = STRING : [8] bnmr",\
"Module = STRING : [8] PSMIII",\
"Idle freq (Hz) = DWORD : 500000",\
"freq end sweep jump to idle = BOOL : y",\
"freq sweep load 1st val in idle = BOOL : n",\
"fREF Tuning freq (Hz) = DWORD : 75000",\
"",\
"[Hardware/PSM/one_f]",\
"profile enabled = BOOL : n",\
"simulate single tone mode = BOOL : y",\
"quadrature modulation mode = BOOL : y",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/one_f/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 400",\
"jump to idle IQ = BOOL : n",\
"load first val in idle = BOOL : n",\
"idle i (max plus or minus 511) = INT : 511",\
"idle q (max plus or minus 511) = INT : 0",\
"load i,q pairs file = BOOL : y",\
"set constant i value in file = BOOL : y",\
"const i (max plus or minus 511) = INT : 511",\
"set constant q value in file = BOOL : y",\
"const q (max plus or minus 511) = INT : 0",\
"",\
"[Hardware/PSM/one_f]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/fref]",\
"profile enabled = BOOL : n",\
"simulate single tone mode = BOOL : y",\
"quadrature modulation mode = BOOL : y",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/fref/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] Hermite",\
"requested bandwidth (Hz) = INT : 50",\
"jump to idle IQ = BOOL : n",\
"load first val in idle = BOOL : n",\
"idle i (max plus or minus 511) = INT : 511",\
"idle q (max plus or minus 511) = INT : 0",\
"load i,q pairs file = BOOL : y",\
"set constant i value in file = BOOL : y",\
"const i (max plus or minus 511) = INT : 511",\
"set constant q value in file = BOOL : y",\
"const q (max plus or minus 511) = INT : 0",\
"",\
"[Hardware/PSM/fref]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/three_f]",\
"profile enabled = BOOL : n",\
"simulate single tone mode = BOOL : y",\
"quadrature modulation mode = BOOL : y",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/three_f/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 400",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i (max plus or minus 511) = INT : 0",\
"idle q (max plus or minus 511) = INT : 0",\
"load i,q pairs file = BOOL : y",\
"set constant i value in file = BOOL : n",\
"const i (max plus or minus 511) = INT : 511",\
"set constant q value in file = BOOL : n",\
"const q (max plus or minus 511) = INT : 0",\
"",\
"[Hardware/PSM/three_f]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/five_f]",\
"profile enabled = BOOL : n",\
"simulate single tone mode = BOOL : y",\
"quadrature modulation mode = BOOL : y",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/five_f/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 400",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i (max plus or minus 511) = INT : 0",\
"idle q (max plus or minus 511) = INT : 0",\
"load i,q pairs file = BOOL : n",\
"set constant i value in file = BOOL : n",\
"const i (max plus or minus 511) = INT : 511",\
"set constant q value in file = BOOL : n",\
"const q (max plus or minus 511) = INT : 0",\
"",\
"[Hardware/PSM/five_f]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/f0ch1]",\
"channel enabled = BOOL : n",\
"quadrature modulation mode = BOOL : n",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/f0ch1/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 100",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i(-511<=i<=512) = INT : 512",\
"idle q(-511<=q<=512) = INT : 0",\
"load i,q pairs file = BOOL : n",\
"set constant i value in file = BOOL : n",\
"const i(-511<=i<=512) = INT : 512",\
"set constant q value in file = BOOL : n",\
"const q(-511<=q<=512) = INT : 0",\
"",\
"[Hardware/PSM/f0ch1]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/f0ch2]",\
"channel enabled = BOOL : n",\
"quadrature modulation mode = BOOL : n",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/f0ch2/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] Hermite",\
"requested bandwidth (Hz) = INT : 100",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i(-511<=i<=512) = INT : 512",\
"idle q(-511<=q<=512) = INT : 0",\
"load i,q pairs file = BOOL : n",\
"set constant i value in file = BOOL : n",\
"const i(-511<=i<=512) = INT : 512",\
"set constant q value in file = BOOL : n",\
"const q(-511<=q<=512) = INT : 0",\
"apply phase correction = BOOL : n",\
"phase correction (degrees) = FLOAT : 5",\
"q phase correction (calc) = INT : 0",\
"i phase correction (calc) = INT : -507",\
"",\
"[Hardware/PSM/f0ch2]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/f0ch3]",\
"channel enabled = BOOL : n",\
"quadrature modulation mode = BOOL : n",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/f0ch3/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 100",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i(-511<=i<=512) = INT : 512",\
"idle q(-511<=q<=512) = INT : 0",\
"load i,q pairs file = BOOL : n",\
"set constant i value in file = BOOL : n",\
"const i(-511<=i<=512) = INT : 512",\
"set constant q value in file = BOOL : n",\
"const q(-511<=q<=512) = INT : 0",\
"apply phase correction = BOOL : n",\
"phase correction (degrees) = FLOAT : 7",\
"q phase correction (calc) = INT : 0",\
"i phase correction (calc) = INT : -507",\
"",\
"[Hardware/PSM/f0ch3]",\
"gate control = INT : 1",\
"Phase correction (degrees) = FLOAT : 0",\
"",\
"[Hardware/PSM/f0ch4]",\
"channel enabled = BOOL : n",\
"quadrature modulation mode = BOOL : n",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/f0ch4/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] ln-sech",\
"requested bandwidth (Hz) = INT : 100",\
"jump to idle IQ = BOOL : y",\
"load first val in idle = BOOL : n",\
"idle i(-511<=i<=512) = INT : 512",\
"idle q(-511<=q<=512) = INT : 0",\
"load i,q pairs file = BOOL : y",\
"set constant i value in file = BOOL : n",\
"const i(-511<=i<=512) = INT : 512",\
"set constant q value in file = BOOL : n",\
"const q(-511<=q<=512) = INT : 0",\
"apply phase correction = BOOL : n",\
"phase correction (degrees) = FLOAT : 10",\
"q phase correction (calc) = INT : 0",\
"i phase correction (calc) = INT : 89",\
"",\
"[Hardware/PSM/f0ch4]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/f1]",\
"channel enabled = BOOL : n",\
"quadrature modulation mode = BOOL : y",\
"scale factor (def 181 max 255) = INT : 0",\
"",\
"[Hardware/PSM/f1/IQ modulation]",\
"Moduln mode (ln-sech,Hermite) = STRING : [32] Hermite",\
"requested bandwidth (Hz) = INT : 100",\
"jump to idle IQ = BOOL : n",\
"load first val in idle = BOOL : n",\
"idle i(-511<=i<=512) = INT : 512",\
"idle q(-511<=q<=512) = INT : 0",\
"load i,q pairs file = BOOL : y",\
"set constant i value in file = BOOL : n",\
"const i(-511<=i<=512) = INT : 512",\
"set constant q value in file = BOOL : n",\
"const q(-511<=q<=512) = INT : 0",\
"apply phase correction = BOOL : n",\
"phase correction (degrees) = FLOAT : 0",\
"q phase correction (calc) = INT : 0",\
"i phase correction (calc) = INT : 0",\
"",\
"[Hardware/PSM/f1]",\
"gate control = INT : 1",\
"",\
"[Hardware/PSM/phase]",\
"e2s 3f phase correction QPC3 = FLOAT : 5",\
"e2s 5f phase correction QPC5 = FLOAT : 4",\
"e2s fREF phase correction QPCF = FLOAT : 0",\
"",\
"[sis test mode/sis3801]",\
"test mode = BOOL : n",\
"num bins = DWORD : 500",\
"dwell time (ms) = FLOAT : 0.1",\
"Test Setup Mode Number = INT : 2",\
"",\
"[sis test mode/sis3820]",\
"LNE prescale factor = INT : 1000",\
"num bins requested = INT : 500",\
"",\
"[sis test mode/sis3820/module_a]",\
"test pulse mask = DWORD : 0",\
"",\
"[sis test mode/sis3820/module_b]",\
"test pulse mask = DWORD : 4161798143",\
"",\
"[flags]",\
"hold = BOOL : n",\
"",\
"[Input]",\
"Experiment name = STRING : [32] 1n",\
"CFG path = STRING : [128] /home/bnmr/online/bnmr/ppgload",\
"PPG path = STRING : [128] /home/bnmr/online/ppg-templates",\
"beam off time (ms) = FLOAT : 0",\
"num beam PreCycles = DWORD : 0",\
"num_beam_acq_cycles = DWORD : 0",\
"frequency start (Hz) = DWORD : 41230000",\
"frequency stop (Hz) = DWORD : 41330000",\
"frequency increment (Hz) = DWORD : 2000",\
"RF on time (ms) = FLOAT : 319",\
"RF off time (ms) = FLOAT : 20",\
"num RF on delays (dwell times) = DWORD : 2",\
"MCS enable delay (ms) = FLOAT : 0.1",\
"MCS enable gate (ms) = FLOAT : 10",\
"MCS max delay (ms) = FLOAT : 1",\
"DAQ service time (ms) = FLOAT : 160",\
"Minimal delay (ms) = FLOAT : 0.0005",\
"Time slice (ms) = FLOAT : 0.0001",\
"E1B Dwell time (ms) = FLOAT : 0.5",\
"RF delay (ms) = FLOAT : 10",\
"Bg delay (ms) = FLOAT : 0",\
"e1f num dwell times = DWORD : 100",\
"Num RF cycles = DWORD : 100",\
"E2B Num beam on dwell times = INT : 0",\
"Check recent compiled file = BOOL : y",\
"freq single slice width (Hz) = DWORD : 0",\
"Num freq slices = DWORD : 1",\
"prebeam on time (ms) = FLOAT : 15",\
"flip 360 delay (ms) = FLOAT : 10",\
"flip 180 delay (ms) = FLOAT : 40",\
"f slice internal delay (ms) = FLOAT : 0",\
"beam_mode = CHAR : C",\
"counting_mode = CHAR : 1",\
"f select pulselength (ms) = FLOAT : 64",\
"e00 prebeam dwelltimes = DWORD : 50",\
"RFon dwelltime = DWORD : 50",\
"e00 beam on dwelltimes = DWORD : 400",\
"e00 beam off dwelltimes = DWORD : 1200",\
"rfon duration (dwelltimes) = INT : 0",\
"num type1 frontend histograms = INT : 9",\
"num type2 frontend histograms = INT : 14",\
"num type2 fe histos SR mode = INT : 14",\
"num type2 fe histos mode 2s = INT : 15",\
"e1a and e1b pulse pairs = STRING : [4] 000",\
"e1a and e1b freq mode = STRING : [3] FR",\
"randomize freq values = BOOL : n",\
"e2c beam on time (ms) = FLOAT : 10",\
"num cycles per supercycle = INT : 1",\
"NaVolt start = FLOAT : 10",\
"NaVolt stop = FLOAT : 110",\
"NaVolt inc = FLOAT : 1",\
"Laser start = FLOAT : -2",\
"Laser stop = FLOAT : 2",\
"Laser inc = FLOAT : -0.005",\
"e1c Camp Start = FLOAT : -1.205",\
"e1c Camp Stop = FLOAT : -1.165",\
"e1c Camp Inc = FLOAT : 0.0005",\
"Field start = FLOAT : 0",\
"Field stop = FLOAT : 0",\
"Field inc = FLOAT : 0",\
"scan jump start = FLOAT : 0",\
"scan jump stop = FLOAT : 0",\
"enable scan jump = BOOL : n",\
"e2a pulse pairs = BOOL : n",\
"e2a 180 = BOOL : n",\
"e2a ubit1 action = STRING : [10] 1st",\
"e2e num dwelltimes per freq = DWORD : 3",\
"e2e num RF dwelltimes per freq = DWORD : 1",\
"e2e num postRFbeamOn dwelltimes = DWORD : 2",\
"e1f const time between cycles = BOOL : n",\
"e1f min daq service (ms) = FLOAT : 90",\
"e2s RF pi on (ms) = FLOAT : 1",\
"e2s RF half-pi on (ms) = FLOAT : 0.5",\
"e2s Z dwell time (ms) = FLOAT : 5",\
"e2s X dwell time (ms) = FLOAT : 2",\
"e2s num FID cycles = INT : 5",\
"e2s select mode = INT : 2",\
"e2s enable pi pulse = BOOL : y",\
"e2s pi pulse cycle num = INT : 3",\
"e2w central freq f0 (kHz) = INT : 100000",\
"e2w frequency sweep (kHz) = INT : 1000",\
"e2w Li precession f1 (kHz) = FLOAT : 0.45",\
"e2w N (40 or 80) = INT : 80",\
"e2w Q0 (default 5) = INT : 5",\
"e2w freq resolution Nf = INT : 75",\
"e2w total num freq scans = INT : 40",\
"e2w Tp (ms) = FLOAT : 32",\
"e2s xy flip half pulse (ms) = FLOAT : 0.75",\
"e1w f1 function = STRING : [64] y-3*x",\
"e1w f2 function = STRING : [64] y-1*x",\
"e1w f3 function = STRING : [64] y+1*x",\
"e1w f4 function = STRING : [32] y+3*x",\
"e1w x start = INT : 0",\
"e1w x stop = INT : 20000",\
"e1w x incr = INT : 200",\
"e1w y constant = INT : 41272715",\
"e1n use custom var = BOOL : n",\
"e1n custom start = FLOAT : 0",\
"e1n custom stop = FLOAT : 0",\
"e1n custom inc = FLOAT : 0",\
"e1n custom epics var read = STRING : [32] ",\
"e1n custom epics var write = STRING : [32] ",\
"e1n custom tolerance = FLOAT : 0",\
"",\
"[debug]",\
"debug = BOOL : n",\
"dppg = BOOL : y",\
"dcamp = BOOL : n",\
"dpsm = BOOL : y",\
"manual_ppg_start = BOOL : n",\
"dsis = BOOL : n",\
"hot_debug_level (-1=clr) = INT : 20",\
"hot threshold trip = INT : 3",\
"",\
NULL }

#define FIFO_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} FIFO_ACQ_COMMON;

#define FIFO_ACQ_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 10",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] /home/bnmr/online/bnmr_delta/febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define FIFO_ACQ_MDARC_DEFINED

typedef struct {
  struct {
    struct {
      BOOL      use_defaults;
      INT       number_of_regions;
      INT       first_time_bin[5];
      char      range_label[5][12];
      INT       last_time_bin[5];
      INT       start_freq__hz_;
      INT       end_freq__hz_;
    } midbnmr;
    struct {
      double    histogram_totals[25];
      double    total_saved;
    } output;
    INT       number_defined;
    INT       num_bins;
    float     dwell_time__ms_;
    INT       resolution_code;
    char      titles[25][32];
    INT       bin_zero[25];
    INT       first_good_bin[25];
    INT       last_good_bin[25];
    INT       first_background_bin[25];
    INT       last_background_bin[25];
    char      type1_titles[25][32];
  } histograms;
  char      time_of_last_save[32];
  char      last_saved_filename[128];
  char      saved_data_directory[128];
  DWORD     num_versions_before_purge;
  BOOL      endrun_purge_and_archive;
  BOOL      suppress_save_temporarily;
  DWORD     save_interval_sec_;
  char      archiver_task[80];
  char      archived_data_directory[128];
  char      last_archived_filename[128];
  BOOL      toggle;
  BOOL      disable_run_number_check;
  char      run_type[5];
  char      perlscript_path[50];
  BOOL      enable_mdarc_logging;
  BOOL      enable_prestart_rn_check;
  struct {
    char      camp_hostname[128];
    char      temperature_variable[128];
    char      field_variable[128];
    char      rf_power_variable[128];
    BOOL      enable_weighted_averaging;
    BOOL      record_temperature_error;
    BOOL      record_field_error;
  } camp;
} FIFO_ACQ_MDARC;

#define FIFO_ACQ_MDARC_STR(_name) const char *_name[] = {\
"[histograms/midbnmr]",\
"use defaults = BOOL : y",\
"number of regions = INT : 1",\
"first time bin = INT[5] :",\
"[0] 1",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"range label = STRING[5] :",\
"[12] ",\
"[12] ",\
"[12] ",\
"[12] ",\
"[12] ",\
"last time bin = INT[5] :",\
"[0] 100",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"start freq (Hz) = INT : 41230000",\
"end freq (Hz) = INT : 41330000",\
"",\
"[histograms/output]",\
"histogram totals = DOUBLE[25] :",\
"[0] 7218813",\
"[1] 8229489",\
"[2] 6770393",\
"[3] 9124616",\
"[4] 3299900416",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 67178848",\
"[11] 158142368",\
"[12] 67518696",\
"[13] 158663824",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"total saved = DOUBLE : 3782747568",\
"",\
"[histograms]",\
"number defined = INT : 14",\
"num bins = INT : 1650",\
"dwell time (ms) = FLOAT : 10",\
"resolution code = INT : -1",\
"titles = STRING[25] :",\
"[32] B+",\
"[32] F+",\
"[32] B-",\
"[32] F-",\
"[32] Const",\
"[32] FluM2",\
"[32] L+",\
"[32] R+",\
"[32] L-",\
"[32] R-",\
"[32] NBMB+",\
"[32] NBMF+",\
"[32] NBMB-",\
"[32] NBMF-",\
"[32] Ubits",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"bin zero = INT[25] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"first good bin = INT[25] :",\
"[0] 50",\
"[1] 50",\
"[2] 50",\
"[3] 50",\
"[4] 50",\
"[5] 50",\
"[6] 50",\
"[7] 50",\
"[8] 50",\
"[9] 50",\
"[10] 50",\
"[11] 50",\
"[12] 50",\
"[13] 50",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"last good bin = INT[25] :",\
"[0] 1650",\
"[1] 1650",\
"[2] 1650",\
"[3] 1650",\
"[4] 1650",\
"[5] 1650",\
"[6] 1650",\
"[7] 1650",\
"[8] 1650",\
"[9] 1650",\
"[10] 1650",\
"[11] 1650",\
"[12] 1650",\
"[13] 1650",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"first background bin = INT[25] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"last background bin = INT[25] :",\
"[0] 50",\
"[1] 50",\
"[2] 50",\
"[3] 50",\
"[4] 50",\
"[5] 50",\
"[6] 50",\
"[7] 50",\
"[8] 50",\
"[9] 50",\
"[10] 50",\
"[11] 50",\
"[12] 50",\
"[13] 50",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"type1_titles = STRING[25] :",\
"[32] B+",\
"[32] F+",\
"[32] B-",\
"[32] F-",\
"[32] Const",\
"[32] FluM2",\
"[32] NBMB+",\
"[32] NBMF+",\
"[32] NBMB-",\
"[32] NBMF-",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"",\
"[.]",\
"time_of_last_save = STRING : [32] Tue Jul 30 08:31:03 2019",\
"last_saved_filename = STRING : [128] /isdaq/data1/bnmr/dlog/current/040641.msr",\
"saved_data_directory = STRING : [128] /isdaq/data1/bnmr/dlog/current",\
"num_versions_before_purge = DWORD : 40",\
"endrun_purge_and_archive = BOOL : y",\
"suppress_save_temporarily = BOOL : n",\
"save_interval(sec) = DWORD : 60",\
"archiver task = STRING : [80] scp -B -i /home/bnmr/bnmr_archiver_id",\
"archived_data_directory = STRING : [128] musrarc@cmms:dlog",\
"last_archived_filename = STRING : [128] musrarc@cmms:dlog/040641.msr",\
"toggle = BOOL : n",\
"disable run number check = BOOL : n",\
"run type = STRING : [5] test",\
"perlscript path = STRING : [50] /home/bnmr/online/perl",\
"enable mdarc logging = BOOL : n",\
"enable prestart rn check = BOOL : n",\
"",\
"[camp]",\
"camp hostname = STRING : [128] bnmrvw.triumf.ca",\
"temperature variable = STRING : [128] /Sample/read_A",\
"field variable = STRING : [128] none",\
"RF power variable = STRING : [128] /rf_level_cont/dac_set",\
"enable weighted averaging = BOOL : y",\
"record temperature error = BOOL : y",\
"record field error = BOOL : y",\
"",\
NULL }

#define FIFO_ACQ_CLIENT_FLAGS_DEFINED

typedef struct {
  BOOL      mdarc;
  BOOL      rf_config;
  BOOL      mheader;
  BOOL      frontend;
  BOOL      fe_epics;
  BOOL      enable_client_check;
  INT       client_alarm;
  INT       epics_access_alarm;
  DWORD     error_code_bitpat;
  char      error_code_from[30];
  BOOL      logging_camp;
  BOOL      logging_epics;
  BOOL      got_event;
  INT       elog_alarm;
  INT       epicslog_alarm;
  INT       mh_on_hold;
  BOOL      prestart_failure;
  BOOL      start_abort;
  BOOL      hel_mismatch;
} FIFO_ACQ_CLIENT_FLAGS;

#define FIFO_ACQ_CLIENT_FLAGS_STR(_name) const char *_name[] = {\
"[.]",\
"mdarc = BOOL : y",\
"rf_config = BOOL : y",\
"mheader = BOOL : y",\
"frontend = BOOL : y",\
"fe_epics = BOOL : y",\
"enable client check = BOOL : y",\
"client alarm = INT : 0",\
"epics access alarm = INT : 0",\
"error code bitpat = DWORD : 0",\
"error code from = STRING : [30] mdarc",\
"logging camp = BOOL : y",\
"logging epics = BOOL : y",\
"got_event = BOOL : y",\
"elog alarm = INT : 0",\
"epicslog alarm = INT : 0",\
"mh_on_hold = INT : 0",\
"prestart_failure = BOOL : n",\
"start_abort = BOOL : n",\
"hel_mismatch = BOOL : n",\
"",\
NULL }

#define FIFO_ACQ_MODE_PARAMETERS_DEFINED

typedef struct {
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_10;
  struct {
    char      beam_mode;
    char      pulse_pairs[4];
    char      frequency_mode[3];
    float     background_delay__ms_;
    float     rf_delay__ms_;
    DWORD     number_of_rf_cycles;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1a;
  struct {
    char      beam_mode;
    char      pulse_pairs[4];
    char      frequency_mode[3];
    float     background_delay__ms_;
    float     rf_delay__ms_;
    DWORD     number_of_rf_cycles;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    float     dwell_time__ms_;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1b;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    float     camp_start_scan;
    float     camp_stop_scan;
    float     camp_increment;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    INT       integer_conversion_factor;
  } mode_1c;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      const__time_between_cycles;
    float     daq_service_time_ms_;
  } mode_1f;
  struct {
    float     mcs_enable_gate__ms_;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     rfon_delay__dwelltimes_;
    INT       rfon_duration__dwelltimes_;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_1g;
  struct {
    float     mcs_enable_gate__ms_;
    float     camp_start_scan;
    float     camp_stop_scan;
    float     camp_increment;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    INT       integer_conversion_factor;
  } mode_1j;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    float     start_nacell_scan__volts_;
    float     stop_nacell_scan__volts_;
    float     nacell_increment__volts_;
    INT       num_cycles_per_scan_incr;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      custom_var_enabled;
    float     start_custom_scan;
    float     stop_custom_scan;
    float     custom_increment;
    char      custom_var_read_name[32];
    char      custom_var_write_name[32];
    float     custom_readback_tolerance;
  } mode_1n;
  struct {
    float     mcs_enable_gate__ms_;
    DWORD     number_of_prebeam_dwelltimes;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     rfon_delay__dwelltimes_;
    INT       rfon_duration__dwelltimes_;
    BOOL      enable_sample_reference_mode;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_20;
  struct {
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     number_of_beam_precycles;
    BOOL      enable_180;
    BOOL      enable_pulse_pairs;
    char      bin_parameter[10];
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2a;
  struct {
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     number_of_beam_precycles;
    DWORD     number_of_beam_on_dwelltimes;
    DWORD     num_postrf_dwelltimes;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2b;
  struct {
    char      beam_mode;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    float     prebeam_on_time__ms_;
    float     beam_on_time__ms_;
    float     rf_on_time__ms_;
    DWORD     number_of_frequency_slices;
    DWORD     freq_single_slice_width__hz_;
    float     freq_single_slice_int_delay_ms_;
    float     flip_180_delay__ms_;
    float     flip_360_delay__ms_;
    char      counting_mode;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2c;
  struct {
    char      beam_mode;
    char      pulse_pairs[4];
    char      frequency_mode[3];
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     background_delay__ms_;
    float     rf_delay__ms_;
    DWORD     number_of_rf_cycles;
    float     dwell_time__ms_;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2d;
  struct {
    float     rf_on_time__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     num_dwelltimes_per_freq;
    DWORD     num_post_rfbeamon_dwelltimes;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      dual_channel_mode;
  } mode_2e;
  struct {
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    DWORD     num_rf_on_delays__dwelltimes_;
    float     beam_off_time__ms_;
    DWORD     number_of_beam_off_dwelltimes;
    DWORD     num_post_rfbeamon_dwelltimes;
    DWORD     frequency_scan_start__hz_;
    DWORD     frequency_scan_stop__hz_;
    DWORD     frequency_scan_increment__hz_;
    BOOL      randomize_freq_scan_increments;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2f;
  struct {
    float     z_dwell_time__ms_;
    float     x_dwell_time__ms_;
    float     prebeam_on_time__ms_;
    float     beam_on_time__ms_;
    float     pi_pulse_length__ms_;
    float     half_pi_pulse_length__ms_;
    INT       num_fid_cycles;
    BOOL      mode_se2_selected;
    BOOL      enable_pi_pulse;
    INT       pi_pulse_cycle_num;
    float     xy_flip_half_pulse__ms_;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
  } mode_2s;
  struct {
    INT       number_of_prebeam_dwelltimes;
    INT       number_of_beam_on_dwelltimes;
    INT       central_freq_f0__khz_;
    INT       frequency_sweep__khz_;
    float     li_precession_f1__khz_;
    INT       n__40_or_80_;
    INT       q0__default_5_;
    INT       freq_resolution_nf;
    INT       total_number_of_frequency_scans;
    float     tp__ms_;
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      dual_channel_mode;
  } mode_2w;
  struct {
    float     mcs_enable_gate__ms_;
    INT       number_of_bins;
    INT       num_cycles_per_scan_incr;
    float     parameter_x_start;
    float     parameter_x_stop;
    float     parameter_x_incr;
    INT       parameter_y__constant_;
    char      f1_frequency_function[64];
    char      f2_frequency_function[64];
    char      f3_frequency_function[64];
    char      f4_frequency_function[64];
    BOOL      flip_helicity;
    INT       helicity_sleep_time__ms_;
    BOOL      const__time_between_cycles;
    float     daq_service_time_ms_;
  } mode_1w;
} FIFO_ACQ_MODE_PARAMETERS;

#define FIFO_ACQ_MODE_PARAMETERS_STR(_name) const char *_name[] = {\
"[Mode 10]",\
"MCS enable gate (ms) = FLOAT : 8",\
"Number of bins = INT : 108",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3998",\
"",\
"[Mode 1a]",\
"Beam mode = CHAR : P",\
"Pulse pairs = STRING : [4] 000",\
"Frequency mode = STRING : [3] F0",\
"Background delay (ms) = FLOAT : 0",\
"RF delay (ms) = FLOAT : 10",\
"Number of RF cycles = DWORD : 1",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 20",\
"MCS Enable Delay (ms) = FLOAT : 0.01",\
"MCS Enable Gate (ms) = FLOAT : 10",\
"Frequency Scan Start (Hz) = DWORD : 41220000",\
"Frequency Scan Stop (Hz) = DWORD : 41340000",\
"Frequency Scan Increment (Hz) = DWORD : 2000",\
"Randomize Freq Scan Increments = BOOL : y",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1b]",\
"Beam mode = CHAR : C",\
"Pulse pairs = STRING : [4] 000",\
"Frequency mode = STRING : [3] FR",\
"Background delay (ms) = FLOAT : 0",\
"RF delay (ms) = FLOAT : 10",\
"Number of RF cycles = DWORD : 100",\
"RF On Time (ms) = FLOAT : 10",\
"RF Off Time (ms) = FLOAT : 2",\
"MCS Enable Delay (ms) = FLOAT : 0.2",\
"MCS enable gate (ms) = FLOAT : 0.1",\
"Dwell Time (ms) = FLOAT : 0.5",\
"Frequency Scan Start (Hz) = DWORD : 41220000",\
"Frequency Scan Stop (Hz) = DWORD : 41340000",\
"Frequency Scan Increment (Hz) = DWORD : 2000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1c]",\
"MCS enable gate (ms) = FLOAT : 30",\
"Number of bins = INT : 100",\
"Camp Start scan = FLOAT : -1.205",\
"Camp Stop scan = FLOAT : -1.165",\
"Camp Increment = FLOAT : 0.0005",\
"Num cycles per scan incr = INT : 0",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"integer conversion factor = INT : 10000",\
"",\
"[Mode 1f]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 100",\
"Frequency Scan Start (Hz) = DWORD : 41230000",\
"Frequency Scan Stop (Hz) = DWORD : 41330000",\
"Frequency Scan Increment (Hz) = DWORD : 2000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 0",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 8000",\
"Const. time between cycles = BOOL : y",\
"DAQ service time(ms) = FLOAT : 160",\
"",\
"[Mode 1g]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of Prebeam dwelltimes = DWORD : 5",\
"Number of Beam On dwelltimes = DWORD : 400",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"RFon Delay (dwelltimes) = DWORD : 50",\
"RFon Duration (dwelltimes) = INT : 50",\
"Frequency Scan Start (Hz) = DWORD : 41220000",\
"Frequency Scan Stop (Hz) = DWORD : 41340000",\
"Frequency Scan Increment (Hz) = DWORD : 2000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 1j]",\
"MCS enable gate (ms) = FLOAT : 6",\
"Camp Start scan = FLOAT : 1.5",\
"Camp Stop scan = FLOAT : 8.5",\
"Camp Increment = FLOAT : 1",\
"Number of Prebeam dwelltimes = DWORD : 5",\
"Number of Beam On dwelltimes = DWORD : 400",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"Num cycles per scan incr = INT : 0",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 3000",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"integer conversion factor = INT : 10000",\
"",\
"[Mode 1n]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 100",\
"Start NaCell scan (Volts) = FLOAT : 10",\
"Stop NaCell scan (Volts) = FLOAT : 110",\
"NaCell Increment (Volts) = FLOAT : 1",\
"Num cycles per scan incr = INT : 1",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"Custom var enabled = BOOL : y",\
"Start custom scan = FLOAT : 1",\
"Stop custom scan = FLOAT : 10",\
"Custom Increment = FLOAT : 1",\
"Custom var read name = STRING : [32] ILE2:DPPLR:CH0:RDVOL",\
"Custom var write name = STRING : [32] ILE2:DPPLR:CH0:VOL",\
"Custom readback tolerance = FLOAT : 0.1",\
"",\
"[Mode 20]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of Prebeam dwelltimes = DWORD : 50",\
"Number of Beam On dwelltimes = DWORD : 400",\
"Number of Beam Off dwelltimes = DWORD : 1200",\
"RFon Delay (dwelltimes) = DWORD : 50",\
"RFon Duration (dwelltimes) = INT : 0",\
"Enable Sample Reference mode = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 2a]",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 20",\
"MCS Enable Delay (ms) = FLOAT : 0.1",\
"MCS enable gate (ms) = FLOAT : 10",\
"Num RF On Delays (Dwelltimes) = DWORD : 0",\
"Beam Off time (ms) = FLOAT : 0",\
"Number of Beam precycles = DWORD : 0",\
"Enable 180 = BOOL : n",\
"Enable Pulse Pairs = BOOL : n",\
"Bin parameter = STRING : [10] 1st",\
"Frequency Scan Start (Hz) = DWORD : 40000000",\
"Frequency Scan Stop (Hz) = DWORD : 41000000",\
"Frequency Scan Increment (Hz) = DWORD : 5000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 2b]",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 20",\
"MCS Enable Delay (ms) = FLOAT : 0.1",\
"MCS enable gate (ms) = FLOAT : 10",\
"Num RF on Delays (dwelltimes) = DWORD : 0",\
"Beam Off time (ms) = FLOAT : 0",\
"Number of Beam precycles = DWORD : 0",\
"Number of Beam On dwelltimes = DWORD : 2",\
"Num postRF dwelltimes = DWORD : 0",\
"Frequency Scan Start (Hz) = DWORD : 40000000",\
"Frequency Scan Stop (Hz) = DWORD : 41000000",\
"Frequency Scan Increment (Hz) = DWORD : 5000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 5000",\
"",\
"[Mode 2c]",\
"Beam mode = CHAR : C",\
"MCS Enable Delay (ms) = FLOAT : 0.1",\
"MCS enable gate (ms) = FLOAT : 10",\
"Num RF On Delays (Dwelltimes) = DWORD : 0",\
"Beam Off time (ms) = FLOAT : 0",\
"Prebeam On time (ms) = FLOAT : 1",\
"Beam On time (ms) = FLOAT : 0",\
"RF On time (ms) = FLOAT : 64",\
"Number of frequency slices = DWORD : 1",\
"Freq single slice width (Hz) = DWORD : 0",\
"Freq single slice int delay(ms) = FLOAT : 0",\
"Flip 180 delay (ms) = FLOAT : 40",\
"Flip 360 delay (ms) = FLOAT : 10",\
"Counting mode = CHAR : 1",\
"Frequency Scan Start (Hz) = DWORD : 40000000",\
"Frequency Scan Stop (Hz) = DWORD : 41000000",\
"Frequency Scan Increment (Hz) = DWORD : 5000",\
"Randomize Freq Scan Increments = BOOL : n",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 3000",\
"",\
"[Mode 2d]",\
"Beam mode = CHAR : C",\
"Pulse pairs = STRING : [4] 000",\
"Frequency mode = STRING : [3] F0",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 20",\
"Background delay (ms) = FLOAT : 0",\
"RF delay (ms) = FLOAT : 10",\
"Number of RF cycles = DWORD : 1",\
"Dwell Time (ms) = FLOAT : 4",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 0",\
"",\
"[Mode 2e]",\
"RF On Time (ms) = FLOAT : 319",\
"Num RF on Delays (dwelltimes) = DWORD : 2",\
"Beam Off time (ms) = FLOAT : 0",\
"Num dwelltimes per freq = DWORD : 3",\
"Num post RFbeamOn dwelltimes = DWORD : 2",\
"Frequency Scan Start (Hz) = DWORD : 41270000",\
"Frequency Scan Stop (Hz) = DWORD : 41275000",\
"Frequency Scan Increment (Hz) = DWORD : 50",\
"Randomize Freq Scan Increments = BOOL : y",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 8000",\
"Dual channel mode = BOOL : n",\
"",\
"[Mode 2f]",\
"RF On Time (ms) = FLOAT : 40",\
"RF Off Time (ms) = FLOAT : 0.0005",\
"MCS Enable Delay (ms) = FLOAT : 0.001",\
"MCS enable gate (ms) = FLOAT : 39.009",\
"Num RF on Delays (dwelltimes) = DWORD : 5",\
"Beam Off time (ms) = FLOAT : 1000",\
"Number of Beam Off dwelltimes = DWORD : 20",\
"Num post RFbeamOn dwelltimes = DWORD : 10",\
"Frequency Scan Start (Hz) = DWORD : 40000000",\
"Frequency Scan Stop (Hz) = DWORD : 41000000",\
"Frequency Scan Increment (Hz) = DWORD : 50000",\
"Randomize Freq Scan Increments = BOOL : y",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 5000",\
"",\
"[Mode 2s]",\
"Z dwell time (ms) = FLOAT : 5",\
"X dwell time (ms) = FLOAT : 2",\
"Prebeam on time (ms) = FLOAT : 15",\
"Beam On time (ms) = FLOAT : 10",\
"Pi pulse length (ms) = FLOAT : 1",\
"Half-Pi pulse length (ms) = FLOAT : 0.5",\
"Num FID cycles = INT : 5",\
"Mode SE2 selected = BOOL : y",\
"Enable Pi pulse = BOOL : y",\
"Pi pulse cycle num = INT : 3",\
"xy flip half pulse (ms) = FLOAT : 0.75",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 4000",\
"",\
"[Mode 2w]",\
"Number of prebeam dwelltimes = INT : 50",\
"Number of beam on dwelltimes = INT : 300",\
"Central freq f0 (kHz) = INT : 100000",\
"Frequency sweep (kHz) = INT : 1000",\
"Li precession f1 (kHz) = FLOAT : 0.45",\
"N (40 or 80) = INT : 80",\
"Q0 (default 5) = INT : 5",\
"freq resolution Nf = INT : 75",\
"Total number of frequency scans = INT : 40",\
"Tp (ms) = FLOAT : 32",\
"Flip helicity = BOOL : n",\
"Helicity sleep time (ms) = INT : 4000",\
"Dual channel mode = BOOL : n",\
"",\
"[Mode 1w]",\
"MCS enable gate (ms) = FLOAT : 10",\
"Number of bins = INT : 100",\
"Num cycles per scan incr = INT : 0",\
"Parameter X start = FLOAT : 0",\
"Parameter X stop = FLOAT : 20000",\
"Parameter X incr = FLOAT : 200",\
"Parameter Y (constant) = INT : 41272715",\
"f1 frequency function = STRING : [64] y-3*x",\
"f2 frequency function = STRING : [64] y-1*x",\
"f3 frequency function = STRING : [64] y+1*x",\
"f4 frequency function = STRING : [64] y+3*x",\
"Flip helicity = BOOL : y",\
"Helicity sleep time (ms) = INT : 8000",\
"Const. time between cycles = BOOL : y",\
"DAQ service time(ms) = FLOAT : 160",\
"",\
NULL }

#define FIFO_ACQ_CAMP_SWEEP_DEVICE_DEFINED

typedef struct {
  char      camp_path[32];
  char      gpib_port_or_rs232_portname[10];
  char      instrument_type[32];
  char      camp_scan_path[80];
  char      scan_units[10];
  float     maximum;
  float     minimum;
  char      camp_device_dependent_path[80];
  INT       integer_conversion_factor;
} FIFO_ACQ_CAMP_SWEEP_DEVICE;

#define FIFO_ACQ_CAMP_SWEEP_DEVICE_STR(_name) const char *_name[] = {\
"[.]",\
"Camp path = STRING : [32] /biasV",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] galil_rio47xxx",\
"Camp scan path = STRING : [80] /biasV/output1",\
"Scan units = STRING : [10] volts",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"Camp device dependent path = STRING : [80] ",\
"integer conversion factor = INT : 10000",\
"",\
NULL }

#endif

#ifndef EXCL_CYCLE_SCALERS

#define CYCLE_SCALERS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} CYCLE_SCALERS_COMMON;

#define CYCLE_SCALERS_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 8",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] /home/bnmr/online/bnmr_delta/febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define CYCLE_SCALERS_SETTINGS_DEFINED

typedef struct {
  char      names[30][32];
} CYCLE_SCALERS_SETTINGS;

#define CYCLE_SCALERS_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[30] :",\
"[32] Back%BSegments",\
"[32] Front%FSegments",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"[32] General%Back Cumul +",\
"[32] General%Front Cumul +",\
"[32] General%B/F Cumul +",\
"[32] General%Asym Cumul +",\
"[32] General%Back Cumul -",\
"[32] General%Front Cumul -",\
"[32] General%B/F Cumul -",\
"[32] General%Asym Cumul -",\
"",\
NULL }

#endif

#ifndef EXCL_HISTO

#define HISTO_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HISTO_COMMON;

#define HISTO_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 4",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 17",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] /home/bnmr/online/bnmr_delta/febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define HISTO_SETTINGS_DEFINED

typedef struct {
  char      names[60][32];
} HISTO_SETTINGS;

#define HISTO_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[60] :",\
"[32] Back%BSeg00",\
"[32] Back%BSeg01",\
"[32] Back%BSeg02",\
"[32] Back%BSeg03",\
"[32] Back%BSeg04",\
"[32] Back%BSeg05",\
"[32] Back%BSeg06",\
"[32] Back%BSeg07",\
"[32] Back%BSeg08",\
"[32] Back%BSeg09",\
"[32] Back%BSeg10",\
"[32] Back%BSeg11",\
"[32] Back%BSeg12",\
"[32] Back%BSeg13",\
"[32] Back%BSeg14",\
"[32] Back%BSeg15",\
"[32] Front%FSeg00",\
"[32] Front%FSeg01",\
"[32] Front%FSeg02",\
"[32] Front%FSeg03",\
"[32] Front%FSeg04",\
"[32] Front%FSeg05",\
"[32] Front%FSeg06",\
"[32] Front%FSeg07",\
"[32] Front%FSeg08",\
"[32] Front%FSeg09",\
"[32] Front%FSeg10",\
"[32] Front%FSeg11",\
"[32] Front%FSeg12",\
"[32] Front%FSeg13",\
"[32] Front%FSeg14",\
"[32] Front%FSeg15",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"[32] General%Back Cumul +",\
"[32] General%Front Cumul +",\
"[32] General%B/F Cumul +",\
"[32] General%Asym Cumul +",\
"[32] General%Back Cumul -",\
"[32] General%Front Cumul -",\
"[32] General%B/F Cumul -",\
"[32] General%Asym Cumul -",\
"",\
NULL }

#endif

#ifndef EXCL_INFO_ODB

#define INFO_ODB_EVENT_DEFINED

typedef struct {
  DWORD     helicity_set;
  DWORD     current_cycle;
  DWORD     cancelled_cycle;
  DWORD     current_scan;
  double    ref_helup_thr;
  double    ref_heldown_thr;
  double    current_helup_thr;
  double    current_heldown_thr;
  double    prev_helup_thr;
  double    prev_heldown_thr;
  DWORD     rf_state;
  DWORD     fluor_monitor_counts;
  float     epicsdev_set_v_;
  float     epicsdev_read_v_;
  float     campdev_set;
  float     campdev_read;
  float     laser_power_v_;
  DWORD     last_failed_thr_test;
  DWORD     cycle_when_last_failed_thr;
  DWORD     last_good_hel;
  DWORD     ncycle_sk_tol;
  DWORD     helicity_read;
  INT       const1f_ppg_cb_cntr;
  INT       const1f_cb_cntr;
  float     const1f_percent;
  float     dummy;
  double    const1f_max_cb__ms_;
  double    const1f_av_cb__ms_;
  double    const1f_min_cb__ms_;
  double    dummy3;
  INT       wrong_hel_cntr;
  INT       microwave_set;
  INT       microwave_read;
  INT       rf_set_1f;
} INFO_ODB_EVENT;

#define INFO_ODB_EVENT_STR(_name) const char *_name[] = {\
"[.]",\
"helicity set = DWORD : 1",\
"current cycle = DWORD : 10",\
"cancelled cycle = DWORD : 1",\
"current scan = DWORD : 0",\
"Ref HelUp thr = DOUBLE : 3334965",\
"Ref HelDown thr = DOUBLE : 3334965",\
"Current HelUp thr = DOUBLE : 3873874",\
"Current HelDown thr = DOUBLE : 3979215",\
"Prev HelUp thr = DOUBLE : 3864315",\
"Prev HelDown thr = DOUBLE : 3938709",\
"RF state = DWORD : 0",\
"Fluor monitor counts = DWORD : 0",\
"EpicsDev Set(V) = FLOAT : 0",\
"EpicsDev Read(V) = FLOAT : 0",\
"Campdev set = FLOAT : 0",\
"Campdev read = FLOAT : 0",\
"Laser Power(V) = FLOAT : 0",\
"last failed thr test = DWORD : 0",\
"cycle when last failed thr = DWORD : 0",\
"last good hel = DWORD : 0",\
"ncycle sk tol = DWORD : 0",\
"helicity read = DWORD : 0",\
"Const1f ppg_cb_cntr = INT : 0",\
"Const1f cb_cntr = INT : 0",\
"Const1f perCent = FLOAT : 0",\
"dummy = FLOAT : 0",\
"Const1f max cb (ms) = DOUBLE : 0",\
"Const1f av cb (ms) = DOUBLE : 0",\
"Const1f min cb (ms) = DOUBLE : 0",\
"dummy3 = DOUBLE : 0",\
"Wrong hel cntr = INT : 0",\
"microwave set = INT : 0",\
"microwave read = INT : 0",\
"rf_set_1f = INT : 0",\
"",\
NULL }

#define INFO_ODB_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} INFO_ODB_COMMON;

#define INFO_ODB_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 1024",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 273",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] /home/bnmr/online/bnmr_delta/febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_EPICSLOG

#define EPICSLOG_SETTINGS_DEFINED

typedef struct {
  INT       epics_ok;
  INT       n_epics_logged;
  INT       evar_event_period__s_;
  char      epics_path[8][32];
  struct {
    INT       num_log_ioc;
    char      epics_log_ioc[8][32];
  } output;
} EPICSLOG_SETTINGS;

#define EPICSLOG_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"epics OK = INT : 1",\
"n_epics_logged = INT : 6",\
"evar event period (s) = INT : 5",\
"epics_path = STRING[8] :",\
"[32] BNMR:HVBIAS:POS:RDVOL",\
"[32] BNMR:HVBIAS:NEG:RDVOL",\
"[32] ITE:BIAS:RDVOL",\
"[32] ILE2:BIAS15:RDVOL",\
"[32] ILE2:LAS:RDPOWER",\
"[32] ILE2:BIASTUBE:VOL",\
"[32] ",\
"[32] ",\
"",\
"[output]",\
"num_log_ioc = INT : 6",\
"epics_log_ioc = STRING[8] :",\
"[32] BNMR:HVBIAS:POS:RDVOL",\
"[32] BNMR:HVBIAS:NEG:RDVOL",\
"[32] ITE:BIAS:RDVOL",\
"[32] ILE2:BIAS15:RDVOL",\
"[32] ILE2:LAS:RDPOWER",\
"[32] ILE2:BIASTUBE:VOL",\
"[32] ",\
"[32] ",\
"",\
NULL }

#define EPICSLOG_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} EPICSLOG_COMMON;

#define EPICSLOG_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 19",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 5000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq01.triumf.ca",\
"Frontend name = STRING : [32] mheader",\
"Frontend file name = STRING : [256] src/mheaderBnmr.c",\
"Status = STRING : [256] mheader@isdaq01.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_EPICSALIVE

#define EPICSALIVE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} EPICSALIVE_COMMON;

#define EPICSALIVE_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 12",\
"Trigger mask = WORD : 4096",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 30000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] /home/bnmr/online/bnmr_delta/febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_LNE

#define LNE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} LNE_COMMON;

#define LNE_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 5",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_TEST

#define TEST_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} TEST_COMMON;

#define TEST_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 15",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_HELICITY

#define HELICITY_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HELICITY_COMMON;

#define HELICITY_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 4096",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] lxbnmr.triumf.ca",\
"Frontend name = STRING : [32] feBNMR_VMIC",\
"Frontend file name = STRING : [256] febnmr_vmic.c",\
"Status = STRING : [256] feBNMR_VMIC@lxbnmr.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#define HELICITY_SETTINGS_DEFINED

typedef struct {
  char      names[6][32];
} HELICITY_SETTINGS;

#define HELICITY_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[6] :",\
"[32] PPG Set Value",\
"[32] Readback (Latched if DCM)",\
"[32] Enable Dual Channel Mode",\
"[32] Current Hel (DCM only)",\
"[32] max",\
"[32] min",\
"",\
NULL }

#endif

