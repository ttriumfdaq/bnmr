/*  SwitchTest.c


Tests dual channel switch epics routines used in febnmr.c

SwitchTest()   tests getting ID, reading

Used with routines in conn.c (use callbacks)


CVS log information:
$Log: SwitchTest.c,v $
Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version



*/

WARNING - this doesn't seem to work!!!
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "EpicsStep.h"

#ifndef LOCAL
#define LOCAL static
#endif
char * ca_name (int chan);

void SwitchTest(void);

int test_cycle(int Rchan_id, int Wchan_id);

/* globals */
int ddd=0;
int Rchid_BnmrPeriod;
int Rchid_BnqrPeriod;
int Rchid_BnmrDelay;
int Rchid_BnqrDelay;
int Rchid_EnableSwitching;

/* Epics names of Dual Ch variables */ 

 char RbnmrPeriod[]="BNMR:BNQRSW:BNMRPERIOD";
char RbnqrPeriod[]="BNMR:BNQRSW:BNQRPERIOD";
char RbnmrDelay[]="BNMR:BNQRSW:BNMRDELAY";
char RbnqrDelay[]="BNMR:BNQRSW:BNQRDELAY";
char REnableSwitching[]="BNMR:BNQRSW:ENBSWITCHING";

void SwitchTest(void)
{
  int status;
  float value;

  /* BNMR */
  status=caGetSingleId ( RbnmrPeriod, &Rchid_BnmrPeriod );
  if(status==-1)
    {
      printf("SwitchTest: Bad status after caGetID for %s\n", RbnmrPeriod);
      caExit(); /* clear any channels that are open */
      return;
    }

  status=caGetSingleId ( RbnmrDelay ,&Rchid_BnmrDelay );
  if(status==-1)
    {
      printf("SwitchTest: Bad status after caGetID for %s\n", RbnmrDelay);
      caExit(); /* clear any channels that are open */
      return;
    }
 

  printf("calls to caGetSingleId return Rchid_BnmrPeriod = %d and Rchid_BnmrDelay = %d\n",
	Rchid_BnmrPeriod, Rchid_BnmrDelay);

  
  if (caCheck(Rchid_BnmrPeriod))
    printf("caCheck says channel %s is connected\n",RbnmrPeriod);
  else
    printf("caCheck says channel %s is NOT connected\n",RbnmrPeriod);
  if (caCheck(Rchid_BnmrDelay))
    printf("caCheck says channel %s is connected\n",RbnmrDelay);
  else
   printf("caCheck says channel %s is NOT connected\n",RbnmrDelay);

 
  /* BNQR */
  status=caGetSingleId ( RbnqrPeriod, &Rchid_BnqrPeriod );
  if(status==-1)
    {
      printf("SwitchTest: Bad status after caGetID for %s\n",RbnqrPeriod);
      caExit(); /* clear any channels that are open */
      return;
    }

  status=caGetSingleId ( RbnqrDelay, &Rchid_BnqrDelay );
  if(status==-1)
    {
      printf("SwitchTest: Bad status after caGetID for %s\n",RbnqrDelay);
      caExit(); /* clear any channels that are open */
      return;
    }
 

  printf("calls to caGetSingleId return Rchid_BnqrPeriod = %d and Rchid_BnqrDelay = %d\n",
	Rchid_BnqrPeriod, Rchid_BnqrDelay);

  
  if (caCheck(Rchid_BnqrPeriod))
    printf("caCheck says channel %s is connected\n",RbnqrPeriod);
  else
    printf("caCheck says channel %s is NOT connected\n",RbnqrPeriod);
  if (caCheck(Rchid_BnqrDelay))
    printf("caCheck says channel %s is connected\n",RbnqrDelay);
  else
   printf("caCheck says channel %s is NOT connected\n",RbnqrDelay);
 

  /* switch */
  status=caGetSingleId ( REnableSwitching, &Rchid_EnableSwitching );
  if(status==-1)
    {
      printf("SwitchTest: Bad status after caGetSingleID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("caGetSingleId returns Rchid_EnableSwitching = %d \n",Rchid_EnableSwitching);
  
  if (caCheck(Rchid_EnableSwitching))
    printf("caCheck says channel %s is connected\n",REnableSwitching);
  else
    printf("caCheck says channel %s is NOT connected\n",REnableSwitching);
  



  printf("\nHelTest: Now calling caRead for %s  with chan_id=%d\n",REnableSwitching,Rchid_EnableSwitching);
  status=caRead(Rchid_EnableSwitching,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_EnableSwitching));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_EnableSwitching),value);



  /* BNMR */

  printf("\nHelTest: Now calling caRead for %s  with chan_id=%d\n",RbnmrPeriod,Rchid_BnmrPeriod);
  status=caRead(Rchid_BnmrPeriod,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_BnmrPeriod));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_BnmrPeriod),value);



  printf("HelTest: Now calling caRead for %s  with chan_id=%d\n",RbnmrDelay,Rchid_BnmrDelay);
  status=caRead(Rchid_BnmrDelay,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_BnmrDelay));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_BnmrDelay),value);


  /* BNQR */



  printf("\nHelTest: Now calling caRead for %s  with chan_id=%d\n",RbnqrPeriod,Rchid_BnqrPeriod);
  status=caRead(Rchid_BnqrPeriod,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_BnqrPeriod));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_BnqrPeriod),value);



  printf("HelTest: Now calling caRead for %s  with chan_id=%d\n",RbnqrDelay,Rchid_BnqrDelay);
  status=caRead(Rchid_BnqrDelay,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_BnqrDelay));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_BnqrDelay),value);

  caExit();

  return;
}


