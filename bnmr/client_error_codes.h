/* client error codes

 These codes are supposed to indicate to the custom_status page why the run stopped
 */
#define EPICS_ERR 1   /* EPICS scan */
#define CAMP_ERR 2    /* CAMP scan */
#define RF_ERR 3      /* RF_CONFIG (parameters or bytecode.dat) */
#define CAMP_LOG_ERR 4 /* CAMP logged variables */
#define EPICS_LOG_ERR 5 /* EPICS logged variables */
#define LIMIT_ERR  6   /* frontend reached number of cycles/scans */
#define MHEADER1 7      /* mheader was started while a Type 1 run was in progress */
/* If new codes are added, change max_ccode in Custom_Status.html and add message for
        new code


Codes are  written to 
       e.g.  /equipment/fifo_acq/client flags/error code 
 */
