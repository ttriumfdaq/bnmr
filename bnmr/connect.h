/* connect.h

Prototypes for routines in connect.c 

CVS log information:
$Log: connect.h,v $
Revision 1.1  2013/01/21 20:58:10  suz
initial VMIC version

Revision 1.3  2007/09/26 18:34:51  suz
add prototype for caInit

Revision 1.2  2005/02/04 22:19:17  suz
add prototype (needed by HelTest.c)

Revision 1.1  2003/05/01 21:29:31  suz
initial bnmr version; identical to bnmr1 Rev1.1

Revision 1.1  2002/06/07 18:21:12  suz
replaces conn.h


*/

/* caRead, caWrite, caGetId, caExit */
int caGetId(char *pRname, char *pWname, int *pchan_Rid, int *pchan_Wid);
int caRead(int chan_id, float *pval);
int caWrite(int chan_id, float *pval);
void caExit(void);
int caCheck(int chan_id);
int caGetSingleId(char* pSname, int *pSchan_ID);
int caInit( void );
