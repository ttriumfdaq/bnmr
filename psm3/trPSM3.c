/*------------------------------------------------------------------*
 * trPSM3.c
 * Procedures for handling the VME Freq Synthesizer Module
 * This is a VME board specified by Syd Kreitzman - TRIUMF
 * designed and produced by Triumf electronics group - Hubert Hui
 * for use on BNQR experiment
 *
 * This code based on code for PSMI and II
 *
 * CVS log information:
 *$Log: trPSM3.c,v $
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>     /* gmtime(),strftime() */
#include <ctype.h>
#include "unistd.h" // for sleep
#include "math.h"
#include "trPSM3.h"
#include "vmicvme.h"
#define FAILURE -1  // SUCCESS 1 defined in trPSM3.h
#define FALSE 0
#define TRUE 1

int pdd=0; /* debug */
int *pdata;
int status;

double finc;  // conversion factor
double FmaxHz; // maximum frequency in Hz
static BOOL init_flag=0; // used to call calc_freq_convertion to evaluate constants

INT bits_array[32]; // used by show_bits
INT data_array[5]; // used by psmReadChannelReg
//static char *gate_control[]={"disable gate input","enable gate input (default)","invert gate pulse","internal gate always on"};


void calc_freq_conversion_factors(void)
{
  // Calculate the conversion factor for the frequency

  // For PSM3, formula is (see f1 tuning frequency word in manual)

  //    fHz = FTW *200*10**6/2**32
  // or fHz = FTW * finc
  // where fHz is frequency in Hz, FTW is Frequency Tuning Word (Freq in hex)
  finc = 200 * pow(10,6) /pow(2,32);
  FmaxHz =  200 * pow(10,6) - finc;  // max freq in Hz
  init_flag=1; // set flag
  return;
}

// there are four channels in this module, f0 channels 1-4
//       and three ref channels, f1,fC0 fC1

/*****************************************************************/
/*   VMIC
Read register value  (8 bits)
*/
uint32_t regRead8(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  if(pdd)printf("regRead8: base=0x%x offset=0x%x\n",base,offset);
  return (mvme_read_value(mvme, base + offset) & 0xFF);
}
/*****************************************************************/
//Read register value  (16 bits)

uint32_t regRead16(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  if(pdd)printf("regRead: base=0x%x offset=0x%x\n",base,offset);
  return ( mvme_read_value(mvme, base +offset) & 0xFFFF);
}
/*****************************************************************/
//Read register value  (32 bits)

uint32_t regRead32(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  if(pdd)
    printf("regRead32: reading from base=0x%x + offset=0x%x\n",base,offset);
  return ( mvme_read_value(mvme, base +offset));
}

/*****************************************************************/
/*
Write register value    (8 bits)
*/
void regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  if(pdd)
      printf("regWrite8 writing value=0x%x or %d to base (0x%x) + offset (0x%x) =0x%x\n",
	     value,value,base,offset,(base+offset));
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/
/*
Write register value  (16 bits)
*/
void regWrite16(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  if(pdd)printf("regWrite16: base=0x%x offset=0x%x value=0x%x or %d\n",base,offset,value,value);
  mvme_write_value(mvme, base + offset, value);
}
/*****************************************************************/
/*
Write register value  (32 bits)
*/
void regWrite32(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A24);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  if(pdd)
    printf("regWrite32: base=0x%x offset=0x%x writing to base+offset=0x%x value=0x%x or %d\n",base,offset,(base+offset),value,value);
  mvme_write_value(mvme, base + offset, value);
}

/*------------------------------------------------------------------*/

// Read a 32-bit control register
INT psmRegRead32(MVME_INTERFACE *mvme, const DWORD base_addr, DWORD reg_offset)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)
    printf("psmRegRead: base=0x%x offset=0x%x\n",base_addr,offset);

  return regRead32(mvme, base_addr, offset);
}

// Read a 16-bit control register
INT psmRegRead16(MVME_INTERFACE *mvme, const DWORD base_addr, DWORD reg_offset)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)printf("psmRegRead: base=0x%x offset=0x%x\n",base_addr,offset);

  return regRead16(mvme, base_addr, offset);
}

// Read an 8-bit control register
/*------------------------------------------------------------------*/
INT psmRegRead8(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)printf("psmRegRead8: base=0x%x offset=0x%x\n",base_addr,reg_offset);
  return ( regRead8 (mvme, base_addr, offset)  & 0xff);
}

// Write to a 32-bit control register
INT psmRegWrite32(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,
                           const DWORD value)
{
  // Control Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset; //  0xC000
  // if(pdd)printf("psmRegWrite32: base=0x%x offset=0x%x value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite32(mvme,base_addr,offset,(uint32_t) value);
  return  regRead32( mvme,base_addr,offset);
}

// Write to a 16-bit control register
INT psmRegWrite16(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,
                           const WORD value)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset; //  0x09000
  if(pdd)printf("psmRegWrite16: base=0x%x offset=0x%x value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite16(mvme,base_addr,offset,(uint32_t) value);
  return  regRead16( mvme,base_addr,offset);
}

// Write to an 8-bit control register
INT psmRegWrite8(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,
                           const BYTE value)
{
  // Registers are offset from BASE_CONTROL_REGS
  int offset;
  offset = BASE_CONTROL_REGS + (int)reg_offset;
  if(pdd)printf("psmregWrite8: base=0x%x offset=0x%x value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite8(mvme,base_addr,offset,(uint32_t) value);
  return regRead8(mvme,base_addr,offset);
}


/** psmRegReadFreqDM
    Read 32bit from Frequency Sweep DM
    @memo 32bit read.
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @return read back value
*/
// Read 32-bit from Frequency Sweep Data Memory
 INT psmRegReadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset)
{
  // DM  reg_offset is offset from  BASE_DM_FREQ_SWEEP
  int offset;
  offset =  BASE_DM_FREQ_SWEEP + (int)reg_offset;  //  BASE_DM_FREQ_SWEEP=0xA000
  if(pdd)printf("psmRegReadDM: base=0x%x offset=0x%x\n",base_addr,offset);
  return regRead32(mvme, base_addr, offset);
}

/*------------------------------------------------------------------*/
/** psmRegWriteFreqDM
    @memo 32bit write to Frequency Sweep DM
    @param base\_adr PSM VME base address
    @param reg\_offset register offset (starting at 00)
    @param value to be written
    @return read back value
*/
 INT psmRegWriteFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD reg_offset,
                           const DWORD value)
{
  // DM  reg_offset is offset from   BASE_DM_FREQ_SWEEP
  int offset;
  offset =  BASE_DM_FREQ_SWEEP + (int)reg_offset;  //  BASE_DM_FREQ_SWEEP=0xA000
  if(pdd)printf("psmRegWriteDM: base=0x%x offset=0x%x  value=0x%x or %d\n",base_addr,offset,value,value);
  regWrite32(mvme,base_addr,offset,(uint32_t) value);

  return regRead32 (mvme,base_addr,offset);
}



//  32-bit read IQ memory (read one I,Q pair)

/** psmRegReadIQDM
    Read 32bit from DM
    @memo 32bit read.
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @return read back value
*/
INT psmRegReadIQDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD base_iq_addr,  DWORD reg_offset, INT *I, INT *Q)
{
  // base_iq_addr must be one of  BASE_DM_F0_CH1_IQ, BASE_DM_F0_CH2_IQ,
  //                              BASE_DM_F0_CH3_IQ, BASE_DM_F0_CH4_IQ, BASE_DM_F1_IQ
  //
  //
  // reg_offset is offset from base_iq_addr
  //
  // I,Q data returned as integers (not 2s comp)
  int offset;
  DWORD data;
  offset = base_iq_addr + (int)reg_offset;
  if(pdd)printf("psmRegReadIQDM: base=0x%x offset=0x%x\n",base_addr,offset);
  data = regRead32(mvme, base_addr, offset);

  getIQpair(data, I,Q); // get I,Q pair out of the 32-bit data
  if(pdd)printf("after getIQpair I,Q pair is %d,%d  or 0x%x,0x%x \n",*I,*Q,*I,*Q);
  return data; // return 32-bits
}


// Read one of the "channel" registers repeated for f0 ch1-4 or f1
INT psmReadChannelReg( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset, INT nbits, INT selected_channel, INT len, INT *data_array)
{
  // Data is returned in data_array. Length of data_array = len
  // reg_offset is offset from channel base, e.g. 2 for scale factor reg
  // nbits is 8 or 16 depending on length of register
  DWORD base,offset;
  INT data;
  INT *p;
  INT channel;
  p=data_array;

  if(!check_selected_channel(selected_channel))
    {
      printf("psmReadChannelReg: selected_channel (%d) is illegal\n",selected_channel);
      return 0; // Failure
    }

  if(selected_channel == ALLCH)
    {
      //  all channels
      if(len<5)
	{
	  printf("psmReadChannelReg: array length (%d) is too small for all channels\n",len);
	  return 0;
	}
    }

  if(pdd)
    printf("psmReadChannelReg: starting with reg_offset=0x%x array_length=%d selected_channel=%d\n",
	   reg_offset,len,selected_channel);

  if(selected_channel == ALLCH)
    channel=1; // first channel
  else
    channel=selected_channel;

  while(channel < 6)
    {
      switch (channel)
	{
	case 1:
	  base = BASE_F0_C1;
	  break;
	case 2:
	  base = BASE_F0_C2;
	  break;
	case 3:
	  base = BASE_F0_C3;
	  break;
	case 4:
	  base = BASE_F0_C4;
	  break;
	case 5:
	  base = BASE_F1;
	  break;
	default:
	  printf("psmReadChannelReg: illegal channel %d\n",channel);
	  return FAILURE;
	  break;
	}
      offset = base + reg_offset;
      printf("psmReadChannelReg: ch %d cntrl base = 0x%2.2x offset= 0x%x ",
	     channel,base,reg_offset);

      if(nbits < 9)
	data = psmRegRead8(mvme, base_addr, offset);
      else
	// no control regs are 32-bit
	data = psmRegRead16(mvme, base_addr, offset);

      printf("data= %d or 0x%x \n",data,data);
      *p=data;

      if(selected_channel != ALLCH)
	break;
      channel++;
      p++;
    } // while
  return SUCCESS;
}


// Write one of the "channel" registers repeated for f0 ch1-4 or f1
INT psmWriteChannelReg( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset, INT nbits, INT selected_channel, INT data)
{
  // write data to one or all channel registers
  // reg_offset is offset from channel base, e.g. 2 for scale factor reg
  // nbits is 8 or 16 depending on length of register
  DWORD base,offset;
  INT channel;


  if(! check_selected_channel(selected_channel))
    {
      printf("psmWriteChannelReg: selected_channel (%d) is illegal\n",selected_channel);
      return 0; // Failure
    }


  if(pdd)
    printf("psmWriteChannelReg: starting with reg_offset=0x%x selected_channel=%d data=%d or 0x%x\n",
	   reg_offset,selected_channel,data,data);

  if(selected_channel == ALLCH)
    channel=1; // first channel
  else
    channel=selected_channel;

  while(channel < 6)
    {
      switch (channel)
	{
	case 1:
	  base = BASE_F0_C1;
	  break;
	case 2:
	  base = BASE_F0_C2;
	  break;
	case 3:
	  base = BASE_F0_C3;
	  break;
	case 4:
	  base = BASE_F0_C4;
	  break;
	case 5:
	  base = BASE_F1;
	  break;
	default:
	  printf("psmWriteChannelReg: illegal channel %d\n",channel);
	  return FAILURE;
	  break;
	}
      offset = base + reg_offset;
      if(pdd)printf("psmWriteChannelReg: channel %d control base = 0x%x  reg_offset= 0x%x \n",
	     channel,base,reg_offset);

      if(nbits < 9)
	data = psmRegWrite8(mvme, base_addr, offset, data);
      else
	// no control regs are 32-bit
	data = psmRegWrite16(mvme, base_addr, offset, data);

      if(pdd)printf("psmWriteChannelReg: channel %d read back data as %d or 0x%x \n",channel,data,data);

      if(selected_channel != ALLCH)
	break;
      channel++;
    } // while
  return SUCCESS;
}




INT psmWriteIQLen( MVME_INTERFACE *mvme, DWORD base_addr,  INT selected_channel, INT data)
{
  INT code=4; // IQ DM Length
  INT nbits = 16; // 16-bit write
  INT offset;

  if(!check_selected_channel(selected_channel))
    {
      printf("psmWriteIQLen: illegal channel %d\n",selected_channel);
      return FAILURE;
    }

  offset=(code-1) *2;
  if(data > 0x7FF)
    {
      printf("Maximum number of IQ pairs is 0x7FF or 2047 (11 bit register). Adjusting number from %d to 2047\n",data);
      data = 2047; // register is 11 bits
    }

  printf("psmWriteIQLen: Writing IQ length 0x%x or %d to Channel Reg at offset 0x%x for selected_channel %d\n",
	 data,data,offset,selected_channel);
  if(psmWriteChannelReg(mvme,base_addr, offset, nbits, selected_channel, data) == SUCCESS)
    printf("psmWriteIQLen: Wrote 0x%x to Channel Reg at offset 0x%x from base for channel %d\n",data,offset,selected_channel);

  else
    {
      printf("psmWriteIQLen: Failure attempting to write 0x%x to Channel Reg at offset 0x%x from base for channel %d\n",
	     data,offset,selected_channel);
      return FAILURE;
    }
  return SUCCESS;
}

// Read one of the "channel" registers repeated for f0 ch1-4 or f1
// using channel_index instead of selected_channel; handles one channel at a time (no ALL)
INT psmRead1ChanReg( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset, INT nbits, INT selected_channel, DWORD *data)
{
  // Returns FAILURE or SUCCESS and  data in parameter
  // reg_offset is offset from channel base, e.g. 2 for scale factor reg
  // nbits is 8 or 16 depending on length of register
  DWORD base,offset;
  INT channel;

  if(!check_selected_channel(selected_channel))
    {
      printf("psmRead1ChanReg: illegal channel %d\n",selected_channel);
      return FAILURE;
    }

  if(selected_channel == ALLCH)
    {
      //  all channels not supported
	{
	  printf("psmRead1ChanReg: use routine psmReadChannelReg for ALL channels\n");
	  return 0;
	}
    }


  if(pdd)
    printf("psmRead1ChanReg: starting with reg_offset=0x%x  selected_channel=%d\n",
	   reg_offset,selected_channel);

  channel=selected_channel;

  switch (channel)
    {
    case 1:
      base = BASE_F0_C1;
      break;
    case 2:
      base = BASE_F0_C2;
      break;
    case 3:
      base = BASE_F0_C3;
      break;
    case 4:
      base = BASE_F0_C4;
      break;
    case 5:
      base = BASE_F1;
      break;
    default:
      printf("psmRead1ChanReg: error: illegal channel %d\n",channel);
      return FAILURE;
      break;
    }
  offset = base + reg_offset;
  printf("psmRead1ChanReg: ch %d cntrl base = 0x%2.2x offset= 0x%x ",
	 channel,base,reg_offset);

  if(nbits < 9)
    *data = psmRegRead8(mvme, base_addr, offset);
  else
    // no control regs are 32-bit
    *data = psmRegRead16(mvme, base_addr, offset);

  printf("data= %d or 0x%x \n",*data,*data);
  return SUCCESS;
}




/*------------------------------------------------------------------*/
/** psmReadFreqDM
    Read back sweep data memory
    @memo Read back frequency sweep data memory.
    @param base\_adr PSM VME base address
    @param file frequency file
    @param returns last loaded frequency
    @return 1=SUCCESS, FAILURE=failure
*/
INT psmReadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD nfreq, DWORD offset)
{
  /* read back nfreq frequency values starting offset (offset=0 for start of DM) */
  DWORD  data;
  INT    i;
  INT nbits=32; // 32 bits
  // if(pdd)
    printf("psmReadFreqDM: reading %d freq values from Freq DM starting at offset 0x%x\n",
		nfreq, offset);

  for(i=0;i<nfreq;i++)
    {
      data = psmRegReadFreqDM( mvme,  base_addr, offset );
      printf("psmReadFreqDM: i=%d  Offset 0x%x Read back  0x%x or %d -> %d Hz  (%d bits)\n",
	     i,offset,data,data, get_Hz(data), nbits);
      offset+=4;
    }
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/** psmLoadFreqFile
    Load frequency sweep data memory from a file. File data is in hex
    @memo Load frequency sweep data memory.
    @param base\_adr PSM VME base address
    @param file frequency file
    @param returns no. of frequency increments
    @param returns first frequency value (hex value)
    @return SUCCESS, FAILURE=File not found
*/
INT psmLoadFreqFile( MVME_INTERFACE *mvme, DWORD base_addr, char * file , DWORD *nfreq, DWORD *first_freq)
{
  FILE  *psminput;
  DWORD  freq,reg_offset;
  INT    counter,data;
  //char string[128];
  BOOL first;
  DWORD offset=0; // start at   BASE_DM_FREQ_SWEEP

  // loads frequency file and writes the number of frequencies to the frequency length register

  psminput = fopen(file,"r");
  sleep(0.5); // sleep for 0.5s
  if(psminput == NULL){
    printf("psmLoadFreqFile: PSM freq sweep data file \"%s\" could not be opened.\n", file);
    return FAILURE;
  }


  first=TRUE;
  counter=0;
  while (fscanf(psminput,"%x", &freq) != EOF)
    {
      if(pdd)
      printf("counter=%d loading  freq= 0x%x at offset 0x%x\n",
	     counter,freq,offset);
      if(first)
	{
	  *first_freq = freq;  /* remember this value */
	  first=FALSE;
	}


      psmRegWriteFreqDM(mvme, base_addr,  offset, freq);  // 32-bit write
      // offset++;
      offset+=4;
      counter++;
    }
  fclose(psminput);

  printf("psmLoadFreqFile: frequency range is 0x%x to 0x%x \n",*first_freq,freq);

  /* PSM needs length+1 */
  if(pdd) printf("Num freq.incr.=%d; PSM needs one extra for data length reg.\n",counter);
  counter++;

  /* write this value to the freq sweep length register */
  reg_offset =  FREQ_SWEEP_LENGTH;
  printf("Now writing data length %d to freq sweep length reg (at offset=0x%x)\n",
	 counter,reg_offset);
  data = psmRegWrite16(mvme, base_addr, reg_offset, counter);  /* 16 bit write */
  *nfreq=counter;


  return SUCCESS;
}


/*------------------------------------------------------------------*/
/** psmLoadFreqPtr
    Load frequency sweep data memory from a pointer
    @memo Load frequency sweep data memory.
    @param input base\_adr PSM VME base address
    @param input pointer to list of frequency values (hex)
    @param input number of frequency increments
    @param returns first frequency value (hex value)
    @return SUCCESS, FAILURE=File not found
*/
 INT psmLoadFreqPtr( MVME_INTERFACE *mvme, DWORD base_addr, INT *pfreq , INT nfreq, DWORD *first_freq)
{
  DWORD  freq,reg_offset;
  INT    counter,data;
  BOOL first;
  DWORD offset=0; // start at   BASE_DM_FREQ_SWEEP

  // loads frequencies from a pointer, and writes the number of frequencies to the frequency length register

  if(pdd)printf("psmLoadFreqPtr: starting with pfreq=%p\n",pfreq);
  if(pfreq == NULL){
    printf("psmLoadFreqPtr: PSM freq sweep data could not be loaded as pointer is NULL\n");
    return FAILURE;
  }
  if(nfreq <= 0){
    printf("psmLoadFreqPtr: PSM freq sweep data could not be loaded as number of values is invalid (%d)\n",nfreq);
    return FAILURE;
  }

  first=TRUE;
  counter=0;

  while (counter < nfreq)
    {
      freq = (DWORD)pfreq[counter];
      if(pdd)
	printf("psmLoadFreqPtr: loading  freq[%d]= 0x%x \n",counter, freq);
      if(first)
	{
	  *first_freq = freq;  /* remember this value */
	  first=FALSE;
	}
      psmRegWriteFreqDM(mvme, base_addr,  offset, freq);  // 32-bit write

      offset+=4;
      counter++;
    }

  if(pdd)printf("Frequency range is 0x%x to 0x%x \n",*first_freq,freq);

 /* PSM needs length+1 */
  if(pdd)
    printf("psmLoadFreqPtr: Num freq.incr.=%d; PSM needs one extra for data length reg.\n",counter);
  counter++;

 /* write this value to the freq sweep length register */
  reg_offset =  FREQ_SWEEP_LENGTH;
  if(pdd)printf("psmLoadFreqPtr: Now writing data length %d to freq sweep length reg (at offset=0x%x)\n",
	 counter,reg_offset);
  data = psmRegWrite16(mvme, base_addr, reg_offset, counter);  /* 16 bit write */

  return SUCCESS;
}





INT  getIQpair(DWORD data, INT *I, INT *Q)
{ // get I,Q pair out of the 32-bit data.
  // convert to decimal if negative
  // return decimal values (and twos comp values, needed for checking against a file in 2s comp)
  INT i,q;
  q=data & 0x3FF;  // lsb  10 bits
  i=(data  >> 16) & 0x03FF ; // shift away lower 16 bits and mask

  if(pdd)
    printf("getIQpair: data=0x%x 2s comp data  i=0x%x q=0x%x\n",data,i,q);

  if(q > 512) // 10 bits  negative number - convert
    *Q=TwosComp_convert(q,1); // convert from 2s comp
  else
    *Q=q;

  if(i > 512) //  10 bits  negative number - convert
    *I=TwosComp_convert(i,1); // convert from 2s comp
  else
    *I=i;
  if(pdd)
    printf("getIQpair: returning I,Q pair as (%d,%d) and i,q pair \n",*I,*Q);
  return SUCCESS;
}







//  32-bit write IQ memory (write I,Q pair)
/*------------------------------------------------------------------*/
/** psmRegWriteIQDM
    @memo 32bit write.
    @param base\_adr PSM VME base address
    @param reg\_offset register offset
    @param value to be written
    @return read back value
*/
 INT psmRegWriteIQDM( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD base_iq_addr, const DWORD reg_offset,
		      INT I, INT Q )
{
  // base_iq_addr is one of BASE_DM_F0_CH1_IQ, BASE_DM_F0_CH2_IQ,
  //                        BASE_DM_F0_CH3_IQ, BASE_DM_F0_CH4_IQ, BASE_DM_F1_IQ
  //
  // reg_offset is the offset from base_iq_addr
  // I,Q will be converted to 2s comp if negative (same values if positive or 0)
  DWORD value;
  int offset;

  offset = base_iq_addr + (int)reg_offset;

  if(I < 0)
    I=TwosComp_convert(I,0); // convert into 2s comp
  if(Q < 0)
    Q=TwosComp_convert(Q,0);

  value = I<<16 | Q;
  //printf("after shift, value=0x%x\n",value);
 if(pdd)printf("psmRegWriteDM: base=0x%x  offset=0x%x I=%d or 0x%x  Q=%d or 0x%x ; writing value=0x%x\n",
	       base_addr,offset,I,I,Q,Q,value);
  regWrite32(mvme,base_addr,offset,(uint32_t) value);

  return regRead32 (mvme,base_addr,offset);
}


INT psmLoadIQfile ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD base_iq_addr, char *file,  INT *nvalues,  INT *I1, INT *Q1)
{
  // Load a file starting at base_iq_addr
  // base_iq_addr is one of BASE_DM_F0_CH1_IQ, BASE_DM_F0_CH2_IQ,
  //                        BASE_DM_F0_CH3_IQ, BASE_DM_F0_CH4_IQ, BASE_DM_F1_IQ
  //
  // File contains I,Q pairs. Negative values will be converted to 2s comp by psmRegWriteIQDM

 /* The first i & q pair might be loaded in the last memory
     address (called IDLE) as well as in the 1st memory address

     - these values will be returned, also the number of iq pairs in the file is returned
 */

  FILE  *psminput;
  INT counter=0; /* count how many pairs of values are loaded */
  DWORD offset=0;
  INT I,Q,i,q;
  INT data;


  psminput = fopen(file,"r");
  sleep(1);// sleep for 1s
  if(psminput == NULL)
    {
      printf("psmLoadIQfile: PSM I,Q  data file %s could not be opened.\n", file);
      return FAILURE;
    }
  else
    printf("psmLoadIQfile: PSM I,Q data file %s successfully opened\n", file);


  while (fscanf(psminput,"%d %d", &I,&Q) != EOF)
    {
      counter++;
      if(counter > 4096)
	{
	  printf("psmLoadIQfile: Too many I,Q pairs in file. Maximum is 4096\n");
	  return FAILURE;
	}
      if(pdd)
	printf("psmLoadIQfile: Index=%d: value from file: I=%d Q=%d\n",counter,I,Q);
      if(counter==1)
	{ /* return the first values */
	  *I1 = I;
	  *Q1 = Q;
	}
      data = psmRegWriteIQDM( mvme,  base_addr, base_iq_addr,  offset, I,Q );


      // File may contain 2s complement data, so not using getIQpair
      q=data & 0x3FF;  // lsb  10 bits
      i=(data  >> 16) & 0x03FF ; // shift away lower 16 bits and mask

      // Check readback is correct

      if((check_IQ_readback(I,i) == FAILURE) || (check_IQ_readback(Q,q) == FAILURE))
	{
	  printf("psmLoadIQfile: Readback check on IQ memory at address 0x%x failed, counter=%d. Wrote (%d,%d) Read back (%d,%d) in twos complement\n",
		 (base_iq_addr+offset), counter,I,Q,i,q);
	  return FAILURE;
	}

      offset+=4; // increment offset
    }
  *nvalues=counter;

  return SUCCESS;
}

INT psmWrite_reference_freq_hex( MVME_INTERFACE *mvme, DWORD base_addr, DWORD offset, const DWORD freq)
{
  /* tuning freq is in HEX
     offset is one of F1_TUNING_REG, FC1_TUNING_FREQ, FC0_TUNING_FREQ
  */

  DWORD data;
  if(pdd)printf("psmWrite_reference_freq_hex: Input freq in hex: 0x%x \n",(unsigned int)freq);
  data= psmRegWrite32(mvme,base_addr, offset, freq);
  if(pdd)printf("psmWrite_reference_freq_hex: Wrote 0x%x ; Read back 0x%x \n",(unsigned int)freq,(unsigned int)data);
  return(data);

}

/*------------------------------------------------------------------*/
INT psmWrite_reference_freq_Hz( MVME_INTERFACE *mvme, DWORD base_addr, DWORD offset, const DWORD freq_Hz)
{
  /* Freq is in Hz;  returns freq in Hz
   offset is one of F1_TUNING_REG, FC1_TUNING_FREQ, FC0_TUNING_FREQ
 */
  DWORD data, data_Hz;
  //  volatile WORD * spec_Adr;
  DWORD freq;

  freq=get_hex(freq_Hz); /* convert to hex */
  if (freq == 0)
    {
      if(freq_Hz > 0)
	{
	  printf("psmWrite_reference_freq_Hz: input freq %dHz out of range\n",freq_Hz);
	  return FAILURE;
	}
    }
  // printf("Frequency in Hz (%d) converted to hex frequency 0x%x\n",freq_Hz, freq);

  data = psmWrite_reference_freq_hex( mvme, base_addr, offset, freq);
  if(data != freq)
    {
      printf("psmWrite_reference_freq_Hz: Wrote 0x%x ; Read back 0x%x; Error - values should match\n",freq,data);
      return FAILURE;
    }
  data_Hz=get_Hz(data);
  if(pdd)printf("psmWrite_reference_freq_Hz: Wrote f1 freq=%d Hz ; Read back f1 freq=%d Hz\n",freq_Hz,data_Hz);
  return (INT)data_Hz;
}

INT psmRead_reference_freq( MVME_INTERFACE *mvme, DWORD base_addr, DWORD offset, DWORD *freq_Hz)
{
  /*  returns freq in Hz.  Freq in Hz returned via parameter.
   offset is one of F1_TUNING_REG, FC1_TUNING_FREQ, FC0_TUNING_FREQ or one of CW TUNING FREQ registers
 */
  DWORD data;

  data= psmRegRead32(mvme,base_addr, offset);
  *freq_Hz = get_Hz(data);
  return data;
}


INT check_offset(INT offset)
{
  // check offset is divisible by 4 else return adjusted value
  INT remainder;

  remainder = (offset%4);
  if(remainder)
    {
      offset -= remainder;
      printf("Offset must be divisible by 4. Offset adjusted to 0x%x \n",offset);
    }
  return offset;
}

INT psmLoadIdleIQ ( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel,  INT I, INT Q)
{
  /* Load the IDLE IQ pair data at offset IDLE_IQ_OFFSET */
  DWORD offset, base_iq_addr, data;
  INT II,QQ;
  INT nbits=32;
  offset = IDLE_IQ_OFFSET; // offset from base I,Q mem  0x1FFC
  base_iq_addr=get_IQ_BaseAddr(selected_channel); // offset

  data = psmRegWriteIQDM( mvme,  base_addr, base_iq_addr,  offset, I,Q );

  if(pdd)printf("psmLoadIdleIQ: Channel %d Base_iq_addr=0x%x, Offset 0x%x Wrote I=%d Q=%d ; Read back  %d  (%d bits) \n",
		 selected_channel, base_iq_addr,  offset,  I,Q,data, nbits);
  getIQpair(data, &II, &QQ); // get I,Q pair out of the 32-bit data


  printf(" psmLoadIdleIQ: Channel %s : wrote i,q pair as %d,%d; read back %d,%d\n",
	 channel_name(selected_channel), I,Q,II,QQ);
  // Check values match
  if(check_IQ_match(I,II) && check_IQ_match(Q,QQ))  // writes a message if they don't match
    printf(" psmLoadIdleIQ: Values match\n");
  else
    return FAILURE;

  return SUCCESS;
}

void dump(MVME_INTERFACE *mvme,  DWORD base_addr)
{
 /* dump the device registers */

  //  DWORD offset;
  DWORD data1,data2,data3,data4,data5;
  INT nbits;
  INT offset;

  printf("\n               Frequency  \n");
  // Idle Freq
  offset = IDLE_FREQ_OFFSET; // Offset from freq sweep DM base
  nbits=32;
  data1 = psmRegReadFreqDM( mvme, base_addr , offset );
  printf("Idle Freq (offset 0x%4.4x) = %d Hz  (hex value= 0x%x) \n",offset, get_Hz(data1),data1);

  /* Frequency sweep address (11 bits) */
  offset =  FREQ_SWEEP_ADDRS;
  data1=(psmRegRead16(mvme, base_addr, offset )) &  ELEVEN_BIT_MASK;
  printf("Frequency sweep address  =     0x%3.3x (%d)\n",data1,data1);

  /* Frequency sweep length (11 bits) */
  offset =  FREQ_SWEEP_LENGTH;
  data1=(psmRegRead16(mvme, base_addr, offset )) &  ELEVEN_BIT_MASK;
  printf("Frequency sweep length  =     0x%3.3x (%d)\n",data1,data1);




  // Read reference tuning registers
  nbits=32;
  offset =  F1_TUNING_FREQ;
  DWORD freq_Hz;
  data1 = psmRead_reference_freq(mvme, base_addr,  F1_TUNING_FREQ , &freq_Hz);
  printf("f1 tuning frequency = %d Hz (hex value = 0x%x)\n",freq_Hz,data1);
  data1 = psmRead_reference_freq(mvme, base_addr, FC0_TUNING_FREQ , &freq_Hz);
  printf("fC0 tuning frequency = %d Hz (hex value = 0x%x)\n",freq_Hz,data1);
  data1 = psmRead_reference_freq(mvme, base_addr, FC1_TUNING_FREQ , &freq_Hz);


  printf("\n      Control Registers   \n");
  printf("Register Register #Valid|                 F0                        F1\n");
  printf("Name     Offset   bits  |   Chan1 |   Chan2 |   Chan3 |   Chan4 |       \n");

  nbits=16; // 16 bits
  data1 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C1 + PHASE_MOD );
  data2 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C2 + PHASE_MOD );
  data3 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C3 + PHASE_MOD );
  data4 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C4 + PHASE_MOD );
  data5 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F1    + PHASE_MOD );


  printf("Phase Mod  0x%2.2x    %2.2d   |  0x%4.4x |  0x%4.4x |  0x%4.4x |  0x%4.4x |  0x%4.4x  \n",
	 PHASE_MOD, nbits,data1,data2,data3,data4,data5);

  nbits=8; // 8 bits
  data1 = (DWORD)psmRegRead8(mvme, base_addr,  BASE_F0_C1 + OP_SCALE_FAC);
  data2 = (DWORD)psmRegRead8(mvme, base_addr,  BASE_F0_C2 + OP_SCALE_FAC);
  data3 = (DWORD)psmRegRead8(mvme, base_addr,  BASE_F0_C3 + OP_SCALE_FAC);
  data4 = (DWORD)psmRegRead8(mvme, base_addr,  BASE_F0_C4 + OP_SCALE_FAC);
  data5 = (DWORD)psmRegRead8(mvme, base_addr,  BASE_F1    + OP_SCALE_FAC);
  printf("Scale Fac  0x%2.2x    %2.2d   |    0x%2.2x |    0x%2.2x |    0x%2.2x |    0x%2.2x |    0x%2.2x  \n",
	 OP_SCALE_FAC,nbits,data1,data2,data3,data4,data5);

  nbits=10; // 10 bits
  data1 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C1 + NC_BUF_FAC ) & 0x3FF; // 10 bits
  data2 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C2 + NC_BUF_FAC ) & 0x3FF;
  data3 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C3 + NC_BUF_FAC ) & 0x3FF;
  data4 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C4 + NC_BUF_FAC ) & 0x3FF;
  data5 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F1    + NC_BUF_FAC ) & 0x3FF;
  printf("Buff  Fac  0x%2.2x    %2.2d   |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x  \n",
	 NC_BUF_FAC, nbits,data1,data2,data3,data4,data5);

  nbits=11; // 11 bits
  data1 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C1 + IQ_DM_LEN ) & 0x7FF; // 11 bits
  data2 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C2 + IQ_DM_LEN) & 0x7FF;
  data3 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C3 + IQ_DM_LEN ) & 0x7FF;
  data4 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C4 + IQ_DM_LEN ) & 0x7FF;
  data5 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F1    + IQ_DM_LEN ) & 0x7FF;
  printf("IQDM  Len  0x%2.2x    %2.2d   |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x  \n",
	 IQ_DM_LEN, nbits, data1,data2,data3,data4,data5);

  nbits=11; // 11 bits
  data1 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C1 + IQ_DM_ADDR ) & 0x7FF; // 11 bits
  data2 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C2 + IQ_DM_ADDR) & 0x7FF;
  data3 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C3 + IQ_DM_ADDR ) & 0x7FF;
  data4 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F0_C4 + IQ_DM_ADDR ) & 0x7FF;
  data5 = (DWORD)psmRegRead16(mvme, base_addr, BASE_F1    + IQ_DM_ADDR ) & 0x7FF;
  printf("IQDM Addr  0x%2.2x    %2.2d   |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x |   0x%3.3x  \n",
	 IQ_DM_ADDR, nbits,data1,data2,data3,data4,data5);



  offset = IDLE_IQ_OFFSET;
  nbits=10;
  printf("I,Q Idle 0x%4.4x    %2.2d   ",
	 offset, nbits);

   char str[10];
   INT ichan,I,Q;
   DWORD base_iq_addr;
   for(ichan=1; ichan< 6; ichan++)
     {  // channels 1-5
       base_iq_addr=get_IQ_BaseAddr(ichan); // use channel
       data1 = psmRegReadIQDM( mvme,  base_addr, base_iq_addr, offset, &I, &Q );
       sprintf(str,"%d,%d",I,Q);
       printf("|%9.9s",str);
     }
   printf("\n");




   printf("\nBit Pattern  registers\n");
   printf("Reg      Reg    Reg\n");
   printf("Name     Offset Value  Ch1  Ch2  Ch3  Ch4   F1  Freq \n");


  INT size=sizeof(bits_array)/sizeof(int);
  INT i,k,nvb,nb;
  // Read end sweep control register
  offset = END_SWEEP_CONTROL; // Offset from Control base
  nbits=6;
  data1 = psmRegRead8(mvme, base_addr, offset)  & SIX_BIT_MASK;  // 6 bits
  printf("EndSweep 0x%4.4x  0x%2.2x ",
	 offset, data1);
  nvb=6; nb=1; // num valid bits, num bits per ch
  if(show_bits(data1,nvb,nb,size,bits_array))
    {
      for (i=0;  i< 6; i++)
	{
	  k=bits_array[i];
	  printf("   %d ",k);
	}
      printf("  0=%s 1=%s",endsweep_info(0),endsweep_info(1));
    }
  printf("\n");

  // Read Gate control register
  offset = GATE_CONTROL_REG; // Offset from Control base
  nbits=10;
  data1 = psmRegRead16(mvme, base_addr, offset)  & TEN_BIT_MASK;
  printf("Gate     0x%4.4x 0x%3.3x ",
	 offset, data1);
  nvb=10; nb=2; // num valid bits, num bits per ch
  if(show_bits(data1,nvb,nb,size,bits_array))
    {
      for (i=0;  i< 5; i++)
	{
	  k=bits_array[i];
	  printf("   %d ",k);
	}
      printf("        ");
      printf("0=disable 1=enable 2=invert 3=always on");
    }
  printf("\n");

  // Read Ancillary Control Reg
  offset=ANCILLARY_IO_CONTROL;
  data1 = psmRegRead8(mvme, base_addr, offset) &  0xF;  // 4 bits
  nvb=4; nb=1;
  printf("AncCntrl 0x%4.4x 0x%3.3x ",
	 offset, data1);
 if(show_bits(data1,nvb,nb,size,bits_array))
    {
      for (i=0;  i< 4; i++)
	{
	  k=bits_array[i];
	  printf("   %d ",k);
	}
       printf("             ");
      printf("0=NIM input 1=NIM output");
    }
  printf("\n");

  // Read Ancillary Input Reg
  offset=ANCILLARY_INPUT;
  data1 = psmRegRead8(mvme, base_addr, offset) &  0xF;  // 4 bits
  nvb=4; nb=1;
  printf("AncIn    0x%4.4x 0x%3.3x ",
	 offset, data1);
 if(show_bits(data1,nvb,nb,size,bits_array))
    {
      for (i=0;  i< 4; i++)
	{
	  k=bits_array[i];
	  printf("   %d ",k);
	}
       printf("             ");
      printf("0=Off 1=On");
    }
  printf("\n");

  // Read Ancillary Output Reg
  offset=ANCILLARY_OUTPUT;
  data1 = psmRegRead8(mvme, base_addr, offset) &  0xF;  // 4 bits
  nvb=4; nb=1;
  printf("AncOut   0x%4.4x 0x%3.3x ",
	 offset, data1);
 if(show_bits(data1,nvb,nb,size,bits_array))
    {
      for (i=0;  i< 4; i++)
	{
	  k=bits_array[i];
	  printf("   %d ",k);
	}
       printf("             ");
      printf("0=Off 1=On");
    }
  printf("\n");


   printf("\nOther registers\n");
   printf("Reg         Reg     Reg\n");
   printf("Name        Offset  Value\n");

  // RF Pregated Output Select view port
  offset=RF_PRE_GATED_OUTPUT_SELECT;
  sprintf(str,"PreGated");
  data1 = psmRegRead8(mvme, base_addr, offset) &  0x7;  // 3 bits
  printf("PreGatedOut 0x%4.4x 0x%3.3x ",
	 offset, data1);
  if(data1 == 6)
    printf("RF %9.9s port: Sum selected\n",str);
  else
    printf("RF %9.9s port: %s selected\n",str,channel_name(data1+1));

  // RF Gated Output Select view port
  offset=RF_GATED_OUTPUT_SELECT;
  sprintf(str,"Gated");
  data1 = psmRegRead8(mvme, base_addr, offset) &  0x7;  // 3 bits
  printf("GatedOut    0x%4.4x 0x%3.3x ",
	 offset, data1);
  if(data1 == 6)
    printf("RF %9.9s port: Sum selected\n",str);
  else
    printf("RF %9.9s port: %s selected\n",str,channel_name(data1+1));

  // Voltage Trip
  offset= RF_TRIP_THRESHOLD;
  data1 = psmRegRead8(mvme, base_addr, offset);   // 8 bits
  printf("RFtripThr   0x%4.4x 0x%4.4x ",
	 offset, data1);
  float vtrip = (float)(5 * data1/255);
  printf("Voltage trip at %f Volts\n",vtrip);

  // Voltage Trip Status
  offset= RF_TRIP_STATUS_RESET;
  data1 = psmRegRead8(mvme, base_addr, offset) & 1;   // 1 bit
  printf("RFtrip      0x%4.4x 0x%4.4x ",
	 offset, data1);
  printf("1=tripped\n");

  // Operating Mode
  offset=  MODULE_OPERATING_MODE;
  data1 = psmRegRead8(mvme, base_addr, offset) & 3;   // 2 bits
  printf("OpMode      0x%4.4x 0x%4.4x ",
	 offset, data1);
  printf("0=BNMR 1=BNQR 2=CW \n");

  // FPGA Temp
  offset=  FPGA_TEMP;
  data1 = psmRegRead16(mvme, base_addr, offset) ;   // 16 bit
  printf("FPGA Temp   0x%4.4x 0x%4.4x ",
	 offset, data1);
  if(data1 & 0x8000)printf("Temperature is negative!!\n");
  else
    {
      data1 = data1 >> 7;
      printf("Temperature %d degrees C (approx) \n",data1);
    }
  printf("\n         CW registers\n");
  printf("Register                  Offset         Freq Hex    Freq Hz\n");
  for(i=0; i<4; i++)
    {
      offset = FCW_TUNING_FREQ + (i * 4);
      data1 = psmRead_reference_freq(mvme, base_addr, offset , &freq_Hz);
      printf("Fcw%d tuning frequency     0x%4.4x       0x%8.8x      %d \n",i,offset,data1, freq_Hz);
    }

  printf("\nRegister        Offset      Value  \n");
  for(i=0; i<4; i++)
    {
      offset = FCW_SCALE_FACTOR + i ;
      data1 =  (DWORD)psmRegRead8(mvme, base_addr, offset) ;   // 8 bit
      printf("Fcw%d Scale Fac  0x%2.2x     0x%2.2x or %d\n",
	     i,offset, data1,data1);
    }
}

INT check_selected_channel(selected_channel)
{
  // used non-interactively to make sure a channel is selected
  if(selected_channel < 0 || selected_channel > ALLCH)
    return FAILURE;
  else
    return SUCCESS;
}

void show_selected_channel(INT *my_selected_channel)
{
  INT selected_channel=*my_selected_channel;

  if(selected_channel > 0 && selected_channel <= ALLCH)  // valid channel, display selected channel
    {
      printf("Selected channel is \"%s\" (%d)\n ", channel_name(selected_channel), selected_channel);
      return;  // valid selected channel
    }

  printf("No valid channel is selected.\n");
  *my_selected_channel = select_channel();
  return;  // invalid channel - called select_channel
}



INT select_channel(void)
{ // select regular channel or FC0 channel
  char my_code[10];
  INT selected_channel;

  int i;

 top:
  // Ask user to select a channel
  printf("Enter Channel (\"ch1\", \"ch2\", \"ch3\", \"ch4\", reference \"f1\" or \"all\") ");

  scanf("%s",my_code);
  my_code[4]='\0';

  selected_channel=ALLCH; // default

  for (i=0; i<strlen(my_code);i++)
    my_code[i]=toupper (my_code[i]);

  if (strncmp(my_code,"CH1",3)==0)
    selected_channel=CH1;
  else if (strncmp(my_code,"CH2",3)==0)
    selected_channel=CH2;
  else if (strncmp(my_code,"CH3",3)==0)
    selected_channel=CH3;
  else if (strncmp(my_code,"CH4",3)==0)
    selected_channel=CH4;
  else if (strncmp(my_code,"F1",2)==0)
    selected_channel=F1;
  else if (strncmp(my_code,"ALL",3)==0)
    selected_channel=ALLCH; // all channels Ch1-4 and F1
  else
    {
      printf("**  Illegal input  ** \n");
      goto top;
    }

  show_selected_channel(&selected_channel);
  return selected_channel;
}


INT bitset(INT data, INT bit, INT val)
{
  /* Set or clear a bit in a word while preserving original value of other bits
       - for  8 bits only

       bit = bit (0-15) to be set/cleared ( = the shift value)
       val = 0 to clear, 1 to set
       data = word whose bit(s) is/are to be set/cleared

       e.g.  data = 0x10  bit =1  val=1    data-> 0x12
           data = 0x12    bit =4  val=0    data-> 0x02

  */
  INT mask;

  mask=1<<bit;
  if(pdd)printf("bitset: data=0x%x, mask = 0x%x, bit=%d val=0x%x\n",data,mask,bit,val);
  if(val)
    data= data | mask;
  else
    {
      mask = ~mask & 0xFF;
      data=data & mask;
    }
  data=data & 0xFF; /* mask to 8 bits only */

  return data;
}

INT psmReadGateControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel)
{
  DWORD reg_offset;
  INT data;
  INT i,ich,k,size;
  const INT nvb=10; // number valid bits for this register is 10
  const INT nb=2; // two bits per channel for gate_code

  if(!check_selected_channel(selected_channel))
    {
      printf("psmReadGateControl: illegal selected channel %d\n",selected_channel);
      return FAILURE;
    }

  reg_offset = GATE_CONTROL_REG;
  /* read the register */
  data = psmRegRead16(mvme, base_addr, reg_offset) &  TEN_BIT_MASK;  // 10 bits
  printf("Read gate control register as 0x%3.3x\n",data);
  size=sizeof(bits_array)/sizeof(int);
  if(!show_bits(data,nvb,nb,size,bits_array))
    return data;

  if(selected_channel == ALLCH)
    {
      for (i=0;  i< 5; i++)
	{
	  k=bits_array[i];
	  printf("%s : %d %s \n",
		 channel_name(i+1), k, gate_info(k));
	}
    }
  else
    {
      ich=selected_channel;
      k=bits_array[ich-1];
      printf("%s : %d %s \n",
	     channel_name(ich), k, gate_info(k));
    }
  return data;
}


INT psmWriteAncilIOControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT code)
{
  DWORD reg_offset;
  INT data0,data;
 const INT nvb=4; // number valid bits for this register is 4
  const INT nb=1; // one bit per channel for ancillary control reg

  // Uses selected_channel for the channel
  if(pdd)printf("psmWriteAncilIOControl: starting with selected_channel=%d, code = %d\n",
	 selected_channel,code);
  if(!check_selected_channel(selected_channel))
    {
      printf("psmWriteAncilIOControl: error no channel has been selected\n"); // global selected_channel
      return FAILURE;
    }
  reg_offset =  ANCILLARY_IO_CONTROL;
  /* read the register */
  data0 = psmRegRead8(mvme, base_addr, reg_offset) &  0xF;  // 4 bits
  if(pdd)printf("Read ancilliary IO control register as 0x%1.1x\n",data0);

  /* check to see if we are writing all channels or just one */
  show_selected_channel(&selected_channel);
  if(selected_channel == ALLCH)  // data in reg will be overwritten
      data=getbitpat( 0, nvb, nb, SETALL, code);  // e.g. for gate reg
  else
      data=getbitpat( data0, nvb, nb, selected_channel, code);
  data0 = psmRegWrite8(mvme, base_addr, reg_offset, data) ;  // Write the data to the register
  return data0;
}

INT psmReadAncilIOControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel)
{
  DWORD reg_offset;
  INT data;
  INT i,ich,k,size;
  const INT nvb=4; // number valid bits for this register is 4
  const INT nb=1; // one bits per channel for  ancillary control reg
  const char *NIM_info[]={"NIM input","NIM output"};

  if(!check_selected_channel(selected_channel))
    {
      printf("psmReadAncilIOControl: error no channel has been selected\n"); // global selected_channel
      return FAILURE;
    }
  reg_offset =  ANCILLARY_IO_CONTROL;
  /* read the register */
  data = psmRegRead8(mvme, base_addr, reg_offset) &  0xF;  // 4 bits
  printf("Read ancillary control register as 0x%1.1x\n",data);
  size=sizeof(bits_array)/sizeof(int);
  if(!show_bits(data,nvb,nb,size,bits_array))
    return data;

  if(selected_channel == ALLCH)
    {
      for (i=0;  i< 4; i++)   // there are only 4 channels for ancillary control
	{
	  k=bits_array[i];
	  printf("Chan %d : %d %s \n",
		 (i+1), k, NIM_info[k]);
	}
    }
  else
    {
      ich=selected_channel;
      k=bits_array[ich-1];
      printf("Chan %d : %d %s \n",
	     ich, k, NIM_info[k]);
    }
  return data;
}


INT psmWriteAncilOutput( MVME_INTERFACE *mvme, DWORD base_addr,  INT selected_channel, INT code)
{
  DWORD reg_offset;
  INT data0,data;
 const INT nvb=4; // number valid bits for this register is 4
  const INT nb=1; // one bits per channel for  ancillary output reg

  // Uses selected_channel for the channel
  if(pdd)printf("psmWriteAncilOutput: starting with selected_channel=%d, code = %d\n",
	 selected_channel,code);
  if(!check_selected_channel(selected_channel))
    {
      printf("psmWriteAncilOutput: error no channel has been selected\n"); // global selected_channel
      return FAILURE;
    }
  reg_offset =  ANCILLARY_OUTPUT;
  /* read the register */
  data0 = psmRegRead8(mvme, base_addr, reg_offset) &  0xF;  // 4 bits
  if(pdd)printf("Read ancillary output  register as 0x%1.1x\n",data0);

  /* check to see if we are writing all channels or just one */
  show_selected_channel(&selected_channel);
  if(selected_channel == ALLCH)  // data in reg will be overwritten
      data=getbitpat( 0, nvb, nb, SETALL, code);  // e.g. for gate reg
  else
      data=getbitpat( data0, nvb, nb, selected_channel, code);
  data0 = psmRegWrite8(mvme, base_addr, reg_offset, data) ;  // Write the data to the register
  return data0;
}

INT psmReadAncilIO( MVME_INTERFACE *mvme, DWORD base_addr, int offset, INT selected_channel)
{
  // read Ancillary Input or Output register
  INT data;
  INT i,ich,k,size;
  const INT nvb=4; // number valid bits for this register is 4
  const INT nb=1; // one bits per channel for ancillary output
  const char *NIM_state[]={"0ff","On"};

  if(!check_selected_channel(selected_channel))
    {
      printf("psmReadAncilIO: error no channel has been selected\n"); // global selected_channel
      return FAILURE;
    }


  if(offset ==   ANCILLARY_INPUT)
    printf("Reading Ancillary Input Reg:\n");
  else if(offset ==   ANCILLARY_OUTPUT)
    printf("Reading Ancillary Output Reg:\n");
  else
    {
      printf("Illegal offset 0x%x. Must be one of Ancillary IO registers, i.e. 0x%x or 0x%x\n",
	     offset,  ANCILLARY_INPUT,  ANCILLARY_OUTPUT );
      return 0;
    }

  /* read the register */
  data = psmRegRead8(mvme, base_addr, offset) &  0xF;  // 4 bits
  printf("Read ancillary register as 0x%1.1x\n",data);
  size=sizeof(bits_array)/sizeof(int);
  if(!show_bits(data,nvb,nb,size,bits_array))
    return data;

  if(selected_channel == ALLCH)
    {
      for (i=0;  i< 4; i++)  // only 4 ch for ancillary IO
	{
	  k=bits_array[i];
	  printf("Chan %d : %d %s \n",
		 (i+1), k, NIM_state[k]);
	}
    }
  else
    {
      ich=selected_channel;
      k=bits_array[ich-1];
      printf("Chan %d : %d %s \n",
	    ich, k, NIM_state[k]);
    }
  return data;
}



INT psmWriteGateControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT gate_code)
{
  DWORD reg_offset;
  INT data0,data;
  const INT nvb=10; // number valid bits for this register is 10
  const INT nb=2; // two bits per channel for gate_code

  if(pdd)printf("psmWriteGateControl: starting with channel_index=%d, gate code = %d(0x%2.2x)\n",
	 selected_channel,gate_code,gate_code);

  if(!check_selected_channel(selected_channel))
    {
      printf("psmReadAncilIO: error no channel has been selected\n"); // global selected_channel
      return FAILURE;
    }

  reg_offset = GATE_CONTROL_REG;
  /* read the register */
  data0 = psmRegRead16(mvme, base_addr, reg_offset) &  TEN_BIT_MASK;  // 10 bits
  if(pdd)printf("Read gate control register as 0x%3.3x\n",data0);

  if(selected_channel == ALLCH)  // data in reg will be overwritten
      data=getbitpat( 0, nvb, nb, SETALL, gate_code);  // e.g. for gate reg
  else
      data=getbitpat( data0, nvb, nb, selected_channel, gate_code);  // e.g. for gate reg
  data0 = psmRegWrite16(mvme, base_addr, reg_offset, data) ;  // Write the data to the register
  return data0;
}



INT psmWriteEndSweepControl( MVME_INTERFACE *mvme, DWORD base_addr, BOOL freq_sweep, INT selected_channel, INT code)
{
  // freq_sweep is TRUE if writing frequency sweep bit
  //                      FALSE if writing I,Q bit for selected_channel
  // code 0 = "stop at Nth"
  //      1 = "jump to idle"
  DWORD reg_offset;
  INT data0,data;
  const INT nvb=6; // number valid bits for this register is 6
  const INT nb=1; //  1 bit per channel for end sweep code

  //if(pdd)
    printf("psmWriteEndSweepControl: starting with freq_sweep=%d selected_channel=%d, code = %d\n",
		freq_sweep, selected_channel,code);
  reg_offset = END_SWEEP_CONTROL ;
  /* read the register */
  data0 = psmRegRead8(mvme, base_addr, reg_offset) &  SIX_BIT_MASK;  // 6 bits
  //  if(pdd)
  printf(" psmWriteEndSweepControl: Read end sweep control register as 0x%2.2x\n",data0);
  if(freq_sweep)
    {
      printf("Writing freq sweep bit as code=%d\n",code);
      data=getbitpat( data0, nvb, nb, 6, code); // ch 6 for freq sweep
      printf("after getbitpat, data to write=0x%x\n",data);
      data0 = psmRegWrite8(mvme, base_addr, reg_offset, data) ;  // Write the data to the register
      printf("after psmregwrite8, read data=0x%x\n",data0);
      return data0;
    }
  else
    {
      if(!check_selected_channel(selected_channel))
	{
	  printf("psmWriteEndSweepControl: error no channel has been selected\n"); // global selected_channel
	  return FAILURE;
	}
    }

  /* check to see if we are writing all profiles or just one */
  show_selected_channel(&selected_channel);
  if(selected_channel == ALLCH)
    {
      int fsdata = data0 & 0x20; // remember frequency sweep data
      data=getbitpat( 0, nvb, nb, SETALL, code);
      data = data | fsdata; // restore the frequency sweep data
    }
  else
    data=getbitpat( data0, nvb, nb, selected_channel, code);

  data0 = psmRegWrite8(mvme, base_addr, reg_offset, data) ;  // Write the data to the register
  return data0;
}

INT psmReadEndSweepControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT display)
{
  // display = 1 read selected_channel 1-5 or ALLCH 1,2,3,4,5
  // display = 2 read freq sweep only
  // display = 3 read all channels including freq sweep
  DWORD reg_offset;
  INT data;
  INT i,ich,k,size;

  const INT nvb=6; // number valid bits for this register is 6
  const INT nb=1; //  1 bit per channel for end sweep code

  reg_offset = END_SWEEP_CONTROL ;

  if(!check_selected_channel(selected_channel))
    {
      printf("psmReadEndSweepControl: error no channel has been selected\n"); // global selected_channel
      return FAILURE;
    }


  /* read the register */
  data = psmRegRead8(mvme, base_addr, reg_offset) ;  // 6 bits
  printf("psmReadEndSweepControl Read end sweep control register as 0x%2.2x\n",data);


  size=sizeof(bits_array)/sizeof(int);
  if(!show_bits(data,nvb,nb,size,bits_array))
    return data;

  if(display == 1 || display == 3 )
    {
      if(selected_channel == ALLCH || display == 3)
	{
	  for (i=0;  i< 5; i++)
	    {
	      k=bits_array[i];
	      printf("%s : %d %s \n",
		     channel_name(i+1), k, endsweep_info(k));
	    }
	}
      if(display == 3)
	{
	  // also show the frequency sweep
	  k=bits_array[5];
	  printf("%s : %d %s \n",
		 "Freq sweep", k, endsweep_info(k));

	}
    }
  else
    { // individual channels
      if(display == 2)
	{
	  k=bits_array[5];
	  printf("%s : %d %s \n",
		 "Freq sweep", k, endsweep_info(k));
	}
      else
	{
	  ich=selected_channel;
	  k=bits_array[ich-1];
	  printf("%s : %d %s \n",
		 channel_name(ich), k, endsweep_info(k));
	}
    }
  return data;
}
//============================================

/*------------------------------------------------------------------*/
INT   psmWriteRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr, float Vtrip)
{
  DWORD reg_offset;
  BYTE ival;
  INT jval;
  INT data;
  if(pdd)
    printf("psmWriteRFpowerTripThresh: starting with base_addr=0x%x, Vtrip=%f Volts\n",
	   base_addr, Vtrip);

  jval = ( (INT) (0.5 + Vtrip*255/5));  // round up
  ival = jval & 0xFF; /* mask to 8 bits */
  if(pdd)
    printf("psmWriteRFpowerTripThresh:Vtrip = %.2f Volts, data=0x%2.2x (%d)\n",Vtrip,ival,ival);
  reg_offset =  RF_TRIP_THRESHOLD  ;
  data = psmRegWrite8(mvme, base_addr,reg_offset, ival);
  return(data);
}

/*------------------------------------------------------------------*/
float psmReadRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;
  float Vtrip;

  if(pdd)
    printf("psmReadRFpowerTripThresh: starting with base_addr=0x%x\n",
	   base_addr);

  reg_offset =  RF_TRIP_THRESHOLD  ;
  data = psmRegRead8(mvme, base_addr,reg_offset);
  data = data &0xFF;
  Vtrip = 5 * data/255;

  if(pdd)
    printf("psmReadRFpowerTripThresh: data= 0x%2.2x (%d)  Vtrip = %.2f Volts\n",data,data, Vtrip);

  return(Vtrip);
}

/*------------------------------------------------------------------*/

 INT psmReadRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr)
{
 DWORD reg_offset;
 INT data;

  reg_offset =  RF_TRIP_STATUS_RESET ;
  data=psmRegRead8(mvme, base_addr, reg_offset );  /* word */
  data = data & 0x1;
  if(pdd)printf("RF power trip reg =       0x%2.2x (%d)\n",data,data);
  return data;
}

/*------------------------------------------------------------------*/
 INT psmClearRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =  RF_TRIP_STATUS_RESET  ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,0);
  return(data);
}
//========================================




INT getbitpat(INT data, INT nvb, INT nb, INT chan, INT code)
{
  INT a,i,j;
  INT shift,mask;

  // Register is expected to have a bitpattern for each channel,
  // e.g. gate control register. It contains the gate bits,
  // two for each channel. Number of valid bits = 10, nb=2 bits per channel
  // index=selected_channel-1  unless set to 99 (set all)


   a=0;
   for (i=0; i<nb; i++)
    a += 1<<i ;  // nb=0 a=1; nb=2 a=3
    code=code & a; // to mask away unwanted bits

  //  printf("getbitpat: nb=%d nitems=%d a=%d masked code=%d\n",nb,nitems,a,code); // for 1 bit, mask a=1; for two bits, mask a=3

  int nitems = nvb/nb ;

  if(chan == 99)
    {  /* all channels */
      j=nb*nitems;
      //  printf("show bits: nb=%d nvb=%d a=%d\n",nb,nvb,a); // for 1 bit, mask a=1; for two bits, mask a=3

      data = 0 ; /* all the bits will be overwritten */

      /* calculate bit pattern */

      for (j=0; j<nitems; j++)
	data=(data | code<<(j*nb)); /* value to write to the register */

    }
  else
    { /* one channel */
      //    printf("selected channel=%d  %s\n",
      //    selected_channel, channel_name(selected_channel));

      j = chan-1;  /* start at zero */
      shift = j*nb;
      mask = a << shift;
      code = code << shift;  // shifted code

      //  printf("data=0x%x   shift=%d mask=%d 0x%x  shifted code=0x%x\n",data, shift, mask,mask, code);


      // mask away old data for this channel
      int datamask=0xFFFF & ~mask;
      //  printf("data= 0x%x  datamask=0x%x\n",data, datamask);

     data = data & datamask;
     //  printf("data & datamask = 0x%x\n",data);


     data = data | code;
     // printf("data ored with shifted code = 0x%x\n",data);


    }

  return(data);
}



char *gate_info(INT n)
{
  static char *gate_control[]={"disable gate input","enable gate input (default)","invert gate pulse","internal gate always on","illegal"};
  return (n < 0 || n > 4) ? gate_control[0] : gate_control[n];
}
char *channel_name(INT n)
{
  static char *channel_names[]={"unknown","F0 ch1","F0 ch2","F0 ch3","F0 ch4","F1    ",
				"all","end"};
  return (n < 0 || n > ALLCH) ? channel_names[0] : channel_names[n];
}
char *endsweep_info(INT n)
{
  static char *endsweep[]={"stop at Nth","jump to idle","illegal"};
  return (n < 0 || n > 1) ? endsweep[0] : endsweep[n];
}

INT show_bits(INT data, INT nvb, INT nb, INT size, INT *array)
{
  // separate the bit pattern and put each value into an array
  // this routine is general

  // Inputs
  // data= register data contains bit pattern e.g. gate control
  // nb = number of bits used for each item ( nb=2 for gate control)
  // nvb = number of valid bits in this reg
  //  note that array length (size) must be at least  nvb/nb

  // Outputs
  // Array containing the value of each item, e.g. 0-3 for gate control
  //    array[0]=value for f1 array[1]=value for f2 etc.

  INT i,j,k,a,mask,nitems;
  INT *p;
  p=array;
  a=0;
  for (i=0; i<nb; i++)
    a += 1<<i ;

  nitems=nvb/nb;
  if(nitems > size)
    {
      printf("array length (%d) is not big enough to contain all the channels (%d)\n",size,nitems);
      return 0;
    }
  //printf("show bits: nb=%d nitems=%d a=%d\n",nb,nitems,a); // for 1 bit, mask a=1; for two bits, mask a=3

  for (i=0; i< nitems; i++)
    {
      j=i*nb;  // number of bits to slide each time; 0,2,4 for 2 bits
      mask=a<<j; // sliding mask
      k= (data & mask) >>j;
      //  printf("show_bits: item %d : bits = %d\n",i,k);
      *p=k;  // store into array
      p++;   // increment array index
    }
  return 1;
}


DWORD get_Hz(DWORD Fhex)
{
  double Freq;

  if(!init_flag) calc_freq_conversion_factors();
  Freq = (double)Fhex * finc;
  if(pdd)printf("get_Hz: Input freq=0x%x hex; -> Freq=%.1f, Freq(Hz)=%d \n",Fhex,Freq, (DWORD)Freq);

  return (DWORD)(Freq+0.5);
}

DWORD get_hex(DWORD freq_Hz)
{
  double Freq;
  DWORD Fhex;
  DWORD tmax;

  if(!init_flag) calc_freq_conversion_factors();
  tmax=(DWORD) (FmaxHz+0.5); /* round up */
  if(pdd)printf("get_hex:starting with freq_Hz=%d Hz, max frequency is =%d\n",freq_Hz,tmax);
  /* conversion for Hz */
  Freq = (double)freq_Hz;
  if (Freq > FmaxHz)
    {
      printf("get_hex: Requested frequency %dHz is larger than maximum  (%dHz)\n",freq_Hz,tmax);
      return 0; // use 0 not FAILURE
    }
  Freq = Freq/finc;
  Fhex=  (DWORD)(Freq+ 0.5);
  if(pdd)
    printf("get_hex: Input freq=%d Hz; -> hex equivalent is Freqx=0x%x \n",freq_Hz,Fhex);
  return Fhex;
}


INT TwosComp_convert(INT value, BOOL twos_comp)
{
 /*  Convert data into twos complement and back (10 bit data)
     for the PSM (not true twos compliment)

     twos_comp = FALSE -> converts data into 2s Compliment
     twos_comp = TRUE  -> converts data out of 2s Compliment

   Register is 10 bits.
     Valid input data can be from -511 to +512 to convert to 2s compliment.
     2's compliment data is from 0 to 1023

            Example:
     Input        2s compliment
     512             512
     511             511
     480             480
      30              30
       1               1
       0               0
      -1            1023
     -30             994
    -254             770
    -511             513

  */

  INT IQ,iq;
  if(pdd)printf("TwosComp_convert: starting with value=%d twos_comp=%d\n",value,twos_comp);

  if(! twos_comp)
    {
      if(pdd)printf("TwosComp_convert: convert data to twos_comp \n");
      IQ=value;

      if(IQ > 512 || IQ < -511)
	{
	  printf("TwosComp_convert: Data is out of range for 10 bits (-512<IQ<512) \n");
	  return -1;    ;
	}
      if(IQ > 0)
	iq=IQ;
      else
	{
          IQ*=-1;
	  iq=(~IQ +1) & I_Q_DATA_MASK ;
	}
      if(pdd)
	printf("*** TwosComp_convert: Data (%d 0x%x) converted to 2's complement and masked = %d 0x%x\n",IQ,IQ,iq,iq);
      return iq;
    }

  else
    {
      if(pdd)printf("TwosComp_convert: converting data back from twos_comp\n");
      iq=value;
      if(iq>1023 || iq < 0)
	{
	  printf("TwosComp_convert: 2s complement data (%d) is out of range for 10 bits (-1<iq<1024) \n",iq);
	  return -1024;
	}

       if(iq<=512)
      //	IQ=~(iq)+1;
	 IQ=iq;
      else
	{
	  IQ=~(iq-1);
	  IQ = IQ & I_Q_DATA_MASK ;
	  IQ*=-1;
	}
      if(pdd)
	printf("TwosComp_convert: Data (%d 0x%x) converted back from 2's complement = %d 0x%x\n",iq,iq,IQ,IQ);
      return IQ;
    }
}

INT check_IQ_readback(INT IQ, INT iq)
{
    // Check readback is correct
  // IQ I or Q value as integer  (wrote; may be in twos complement)
  // iq i or q value IS in twos complement  (readback)

  INT IQtmp;

  if(IQ == iq) // positive numbers and 2s complement numbers match
    return SUCCESS;


  if(iq > 512) //  10 bits  negative number - convert
    {
      IQtmp=TwosComp_convert(iq,1); // convert from 2s comp
      if (IQ != IQtmp)
	{
	  printf("check_IQreadback: I or Q value %d does not match twos complement value %d \n",
		 IQ,iq);
	return FAILURE;
	}
    }
  return SUCCESS;
}

INT check_IQ_match(INT IQ, INT iq)
{
  // check two I or Q values match. The first value is NOT in twos complement. The second may be.
  if(IQ == iq)
    return SUCCESS;

  if(IQ < 0 )
    return (check_IQ_readback ( IQ, iq) ); // writes a message if they don't match
  else
    return FAILURE; // values should match if positive
}

INT get_IQ_BaseAddr(INT chan)
{
  // use chan to return the base address for IQ DM
  if(chan < 1 || chan > 5)
    return -1; // illegal channel
  else
    return (0x2000 * (chan-1));

}


/*------------------------------------------------------------------*/
 INT psmVMEReset( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =  PSM_VME_RESET  ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}

/*------------------------------------------------------------------*/
 INT psmFreqSweepStrobe( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =   FREQ_SWEEP_INT_STROBE  ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}
/*------------------------------------------------------------------*/





 INT psmFreqSweepAddrReset( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data,value;

  /* Reset Frequency Sweep Address pointer to the Idle frequency

   NOTE:
   This register resets the Frequency Sweep Address pointer to the Idle
   frequency ONLY IF the Frequency Sweep Length register is set to 0.

   Strangely, if the Frequency Sweep Length register is non-zero,
   a write cycle to this register resets the Frequency Sweep Address to the
   unexpected value of 0x400.

   This is a bug.

   Therefore, to avoid problems, this routine will set the Frequency Sweep Length
   before the reset

  */

  // Set sweep length to zero (see above)
  if(pdd)printf("psmFreqSweepAddrReset: setting FreqSweepLength register to zero\n");
  value=0;
  reg_offset =  FREQ_SWEEP_LENGTH;
  data = psmRegWrite16(mvme, base_addr, reg_offset, value);  /* 16 bit write */
  if(pdd)printf("psmFreqSweepAddrReset: clearing Frequency Sweep Length register; wrote 0x%x or %d, read back 0x%x or %d\n",
	data,data,value,value);
  if(data != 0)
    {
      printf("psmFreqSweepAddrReset: error - FreqSweepLength register should now be zero not %d\n",data);
      return -1;
    }

  reg_offset =     FREQ_SWEEP_ADDR_RESET ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}
/*------------------------------------------------------------------*/
 INT psmFreqSweepAddrPreset( MVME_INTERFACE *mvme, DWORD base_addr)
{
  DWORD reg_offset;
  INT data;

  reg_offset =     FREQ_SWEEP_ADDR_PRESET ;
  data = psmRegWrite8(mvme, base_addr, reg_offset,1);
  return(data);
}
/*------------------------------------------------------------------*/

INT psmSetScaleFactor ( MVME_INTERFACE *mvme,  DWORD base_addr, INT selected_channel,  DWORD value)
{
  /* Load the Output Scale Factor register
       of one or all channels depending on selected_channel
  */
  INT nbits=8; // 8-bit register
  INT status;
  INT offset=0x2; // Scale Factor

  if(!check_selected_channel(selected_channel))
    {
      printf("psmSetScaleFactor: error invalid channel has been selected\n"); // global selected_channel
      return FAILURE;
    }

  // WriteChannelReg adds channel offset and loops is selected_channel=ALLCH
  status = psmWriteChannelReg(mvme, base_addr, offset, nbits, selected_channel, value);
  if(status != SUCCESS)
    return status;
  if(pdd)
    printf("psmSetScaleFactor: Wrote 0x%x to Channel Reg at offset 0x%x from base for selected_channel=%d\n",
	   value,offset,selected_channel);

  printf("psmSetScaleFactor: Wrote scale factor = 0x%x or %d  for channel %s\n",value,value,channel_name(selected_channel));
  return SUCCESS;
}


INT psmWriteBufFactor( MVME_INTERFACE *mvme, DWORD base_addr,  INT selected_channel, INT data)
{
  INT code=3; // Buffer Factor
  INT nbits = 16;
  INT offset;

  if(!check_selected_channel(selected_channel))
    {
      printf("psmWriteBufFactor: error invalid channel has been selected\n"); // global selected_channel
      return FAILURE;
    }


  offset=(code-1) *2;
  if(psmWriteChannelReg(mvme, base_addr, offset, nbits, selected_channel, data) == SUCCESS)
    {
      if(pdd)
	printf("psmWriteBufFactor: Wrote 0x%x to Channel Reg at offset 0x%x from base\n",data,offset);
      printf("psmWriteBufFactor: Wrote buffer factor = 0x%x or %d  for channel %s\n",data,data,channel_name(selected_channel));
    }
  else
    {
      printf("psmWriteBufFactor: Failure attempting to write 0x%x to Channel Reg at offset 0x%x from base\n",data,offset);
      return FAILURE;
    }
  return SUCCESS;
}





void show_data_array(INT selected_channel)
{
  // called after  psmReadChannelReg
  // print out data_array
  INT i,istart,istop;
  INT size = sizeof(data_array)/sizeof(INT);

  if(selected_channel == ALLCH)
    {
      istart=0;
      istop=4;  // 4  for F0 ch1-4 and F1
      if(size < istop)
	istop = size;
    }
  else
    {
      istart=selected_channel;
      istop=istart+1;
    }
  for(i=istart;i<istop; i++)
    printf("data_array[%d]=%d or 0x%x\n",i,data_array[i],data_array[i]);
}


INT psmInit(MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel)
{
  /* initialize PSM to single tone mode

  */
  DWORD data, data0,freq_Hz, offset,base_iq_addr;

  printf("psmInit will \n");
  printf("  reset PSM\n");
  printf("  enable all channels\n");
  //printf("  all other channels will be disabled\n");
  printf("  enable gates always\n");
  printf("  set amplitude (scale factor) to 181 \n");
  printf("  set Idle Frequency to 1MHz\n");
  printf("  set Reference Frequency to 500MHz\n");
  printf("  set fC0 Frequency to 100kHz\n");
  printf("  set fC1 Frequency to 200kHz\n");
  printf("  set I,Q Idle pair to 0,511\n");
  printf("  issue a Frequency Strobe\n\n");
  printf(" This should result in single tone mode and a frequency of 1MHz on F0 Ch1-4 \n\n");



  // disable all channels
  //  psmWriteGateControl(mvme, base_addr, selected_channel, 0);
  // psmSetScaleFactor(mvme, base_addr,  selected_channel,0);

  // selected_channel=CH1;   // enable selected_channel
  show_selected_channel(&selected_channel);

  // the following two routines handle selected_channel=ALLCH
  psmWriteGateControl(mvme, base_addr,  selected_channel, INTERNAL_GATE);  // enable gates always
  psmSetScaleFactor(mvme, base_addr, selected_channel, 181);  // default scale factor

  offset = IDLE_FREQ_OFFSET; // offset from Freq Sweep DM Base
  freq_Hz=1000000; // 1MHz Idle freq
  data0=get_hex((unsigned int)freq_Hz); // convert Hz to hex
  data = psmRegWriteFreqDM( mvme, base_addr, offset, data0 );
  printf("Offset 0x%x Wrote 0x%x  Read back  0x%x or %d Hz\n",
	 offset, data0, data, get_Hz(data) );

   freq_Hz=500000; // 500000Hz Tuning freq
   offset =  F1_TUNING_FREQ;
   psmWrite_reference_freq_Hz(mvme, base_addr, offset, freq_Hz);

  freq_Hz=100000; // 100kHz FC0 freq
  offset =  FC0_TUNING_FREQ;
  psmWrite_reference_freq_Hz(mvme, base_addr, offset, freq_Hz);

  freq_Hz=200000; // 200kHz FC0 freq
  offset =  FC1_TUNING_FREQ;
  psmWrite_reference_freq_Hz(mvme, base_addr, offset, freq_Hz);


  // Load I,Q pair
  offset = IDLE_IQ_OFFSET;
  INT ichan,istart,istop;
  INT I,Q,II,QQ;
  I=0;Q=511;
  if(selected_channel == ALLCH)
    {  //
      istart=1;
      istop=6;
    }
  else
    {
      istart=selected_channel;
      istop=istart+1;
    }

  for(ichan=istart; ichan< istop; ichan++)
    {
      base_iq_addr=get_IQ_BaseAddr(ichan); // use channel
      data = psmRegWriteIQDM( mvme,  base_addr, base_iq_addr,  offset, I,Q );
      if(pdd)printf("Channel %d Base_iq_addr=0x%x, Offset 0x%x Wrote I=%d Q=%d ; Read back  %d  \n",
		    ichan, base_iq_addr,  offset,  I,Q,data);
      getIQpair(data, &II, &QQ); // get I,Q pair out of the 32-bit data


      printf("Channel %s : wrote i,q pair as %d,%d; read back %d,%d\n",
	     channel_name(ichan), I,Q,II,QQ);
      // Check values match
      if(check_IQ_match(I,II) && check_IQ_match(Q,QQ))  // writes a message if they don't match
	printf("Values match\n");
    }
  printf("Now sending a frequency address reset\n");
  psmFreqSweepAddrReset( mvme, base_addr); // sets data length to 0 before performing reset (bug)
  printf("Now sending a frequency strobe\n");
  psmFreqSweepStrobe(mvme, base_addr);

  return SUCCESS;

}

void psmTest1( MVME_INTERFACE *mvme, DWORD base_addr, INT max_chan, INT *times_ms)
{
  INT i,ichan;
  INT max;
  max=max_chan;
  INT my_selected_channel;
  if(max_chan == 5) // reference is enabled; on always
    {
      my_selected_channel=5;
      psmWriteGateControl(mvme, base_addr ,  my_selected_channel, INTERNAL_GATE); // gates always
      psmSetScaleFactor(mvme, base_addr , my_selected_channel, 181); // default amplitude
      max=4;
    }

  for (i=0; i<max; i++)
    printf("times_ms[%d] is %d ms\n",i,times_ms[i]);


  while (1)
    {
      for(ichan=1; ichan<max+1; ichan++)
	{
	  // Enable channel
	  my_selected_channel=ichan;
	  psmWriteGateControl(mvme, base_addr , my_selected_channel, INTERNAL_GATE); // gates always
	  //  psmSetScaleFactor(mvme, base_addr , my_selected_channel, 181); // default amplitude

	  printf("Enabled channel %d, sleeping %d ms\n",ichan, times_ms[ichan-1]);
	  usleep(times_ms[ichan-1]*1000); // usleep in microseconds

	  psmWriteGateControl(mvme, base_addr ,my_selected_channel, FP_GATE_DISABLED); // no gates
	  //	  psmSetScaleFactor(mvme, base_addr ,  my_selected_channel, 0); // no amplitude
	  printf("Disabled channel %d\n", ichan);
	}
    }
  return;
}



void psm(const DWORD psm_base)
{

   printf("\nPol Synthesizer Module: base address  psm_base=0x%x\n",psm_base);
   printf("       TRIUMF PSM function support\n");

   printf("A Read Control Reg         a Write Control Reg   \n");
   printf("B Select BNMR/BNQR/CW Mode b Set BNQR PhaseShift \n");
   printf("C Select Channel(s)        c End Sweep Control\n");
   printf("D Dump Registers           d Read/Write Freq Sweep Length\n");
   printf("E Read Freq Memory         e Read IQ Memory \n");
   printf("F Write Freq Memory(Hz)    f Write IQ Memory\n");
   printf("G Gate Control             g Pre/Gated Output Select\n");
   printf("H help                     I Init module  \n");
   printf("J Write Tuning Freq        j Read Tuning Freq\n");
   printf("K Ancillary Control        k Ancillary I/O \n");
   printf("L Read/Write Channel Reg   l Read/Write Scale Factor   \n");
   printf("M Load frequency file(hex) m Read/Write Buffer Factor \n");
   printf("N Write FCW freq (Hz)      n Write FCW Scale Factor \n");
   printf("P Print this list          p Freq Sweep Addr Preset  \n");
   printf("R VME Reset                r Freq Sweep Addr Reset\n");
   printf("S status                   s Freq Sweep Strobe\n");
   printf("V RF Power Trip Threshold  v RF Power Trip Status/Reset \n");
   printf("d debug (toggles)          X Exit\n");
   printf("Note: use L or l to set scale factor\n");
   printf("\nTest procedures:\n");
   printf("1 Set freq, phase & turn channels on for a certain time\n");
   printf("2 Load freq and iq memory from files, load idle, set length regs, preset addr, strobe\n");
}


/*  For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {

  int selected_channel=10; // illegal channel selected
  int s;
  //  DWORD PSM_BASE  = 0xC00000;
  DWORD PSM_BASE  = 0x820000;
  MVME_INTERFACE *myvme;
  int status,size;
  int nbits;
  char cmd[]="hallo";
  char expt[20];
  INT i,j,num;
  const DWORD mask8=0xFF;
  const DWORD mask16=0xFFFF;
  DWORD offset;
  DWORD data0,data,value;
  DWORD freq_Hz;
  INT I,Q,II,QQ;
  INT ichan,istart,istop;
  INT flag;
  DWORD base_iq_addr;
  char filename[80];
  char filepath[128];
  INT code;
  char string[80];
  INT active_time_ms[5];
  FILE *fin;
  const char iqdefault_file[]={"iq_defaults.txt"};
  BOOL idle_flag;
  float volts;
  const float psfactor0 = 65536.0/360.0;
  const float psfactor1 = 360.0/65536.0; // inverse

  const char loadfiledir[]={"/home/bnmr/online/bnmr/ppgload"}; // default
  const char defaultIQfilename[]={"2e_iq_f0ch1.psm"};
  const char defaultFreqfilename[]={"2e.psm"};

  calc_freq_conversion_factors(); // calculate frequency conversion factor for this module
  printf("calc_freq_conversion sets max freq = %f Hz and  finc=%f \n",FmaxHz,finc);
  strncpy(expt, getenv("MIDAS_EXPT_NAME"), sizeof(expt));
  printf("Current experiment is %s\n",expt);


  if (argc>1) {
    sscanf(argv[1],"%x", &PSM_BASE);
  }
  // Test under vmic
  status = mvme_open(&myvme, 0);
  if(status != SUCCESS)
    {
      printf("failure after mvme_open, status = %d\n",status);
      return status;
    }

  psm( PSM_BASE);


  while ( isalpha(cmd[0]) ||  isdigit(cmd[0]) )
    {
      printf("\nEnter command (A-Z a-z) X to exit?  ");
      scanf("%s",cmd);
      s=cmd[0];

      // data=offset=nbits=value=num=freq=jump_to_idle=0;
      offset=num=data=data0=0;
      switch(s)
	{
	case ('A'):   //  psmRegRead16 or  psmRegRead8

	  printf("Number of bits to read (8 or 16) :");
	  scanf("%d",&nbits);
	  printf("Enter Register Offset from base 0xC000 :0x");
	  scanf("%x",&offset);

	  if(nbits == 8)
	    data = psmRegRead8(myvme, PSM_BASE, offset);
	  else
	    {
	      nbits=16;
	      data = psmRegRead16(myvme, PSM_BASE, offset);
	    }
	  printf("Read back  0x%x or %d  (%d bits)\n",data,data, nbits);
	  break;


	case ('a'):  //  psmRegWrite16 or  psmRegWrite8

	  printf("Number of bits to write (8 or 16) :");
	  scanf("%d",&nbits);
	  printf("Enter Register Offset from base 0xC000 :0x");


	  scanf("%x",&offset);
	  printf("Enter Value to write :0x");
	  scanf("%x",&value);

	  if(nbits == 8)
	    {
	      data = psmRegWrite8(myvme, PSM_BASE, offset, (value & mask8));
	      printf("Wrote  0x%2.2x or %d (8 bits) \n", (value & mask8), (value & mask8) );
	      printf("Read back  0x%x or %d  \n", data,data );
	    }
	  else
	    {
	      data = psmRegWrite16(myvme, PSM_BASE, offset, (value & mask16));
	      printf("Wrote  0x%4.4x or %d (16 bits) \n", (value & mask16), (value & mask16));
	      printf("Read back  0x%x or %d  \n", data,data );
	    }
	  break;

	case ('b'):
	  // set BNQR phase shift
	  // Read bnm/qr control reg
	  if(psmRegRead8(myvme, PSM_BASE, MODULE_OPERATING_MODE) == 0)
	    {
	      printf("BNQR Operating Mode is NOT selected. This register has no effect \n");
	      break;
	    }
	  show_selected_channel(&selected_channel);
	  offset=0; nbits=16; // phase shift register
	  size = sizeof(data_array)/sizeof(INT);
	  psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;

	  if(selected_channel == ALLCH)
	    {
	      istart=0;
	      istop=4;  // 4  for F0 ch1-4 and F1
	      if(size < istop)
		istop = size;
	    }
	  else
	    {
	      istart=selected_channel;
	      istop=istart+1;
	    }

	  //ps=360* data /65536
	  float phase_shift;
	  for(i=istart;i<istop; i++)
	    {
	      phase_shift= psfactor1 * (float)data_array[i];
	      printf("data_array[%d]=%d or 0x%x  phase_shift = %f deg\n",i,data_array[i],data_array[i], phase_shift);
	    }

	  printf("Change phase shift? y/n ");
	  scanf("%s",cmd);
	  cmd[0]=toupper(cmd[0]);
	  if(strncmp(cmd,"N",1)==0)
	    break;

	  printf("Enter phase shift (0-360) ?");
	  scanf("%f",&phase_shift);
	  if(phase_shift < 0 || phase_shift > 360)
	    {
	      printf("Illegal value %f\n",phase_shift);
	      break;
	    }

	  data = (INT) ( 0.5 + phase_shift * psfactor0 ) ;
	  printf("Writing data = %d for phase shift = %f deg \n",
		 data,phase_shift);
	  psmWriteChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, data) ;
	  printf("Reading back data from phase shift register(s)\n");
	  psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;
	  for(i=istart;i<istop; i++)
	    {
	       phase_shift= psfactor1 * (float)data_array[i];
	      printf("data_array[%d]=%d or 0x%x  phase_shift = %f deg\n",i,data_array[i],data_array[i], phase_shift);
	    }

	  break;

	case ('C'):  //  Select channel

	  show_selected_channel(&selected_channel);
	    {
	      printf("Change selected channel? y/n ");
	      scanf("%s",cmd);
	      cmd[0]=toupper(cmd[0]);
	      if(strncmp(cmd,"Y",1)==0)
		  selected_channel = select_channel();
	    }
	  break;

	case ('D'):  // Dump registers
	  dump(myvme, PSM_BASE);
	  break;

	case ('d'): // Read/Write Freq Sweep length reg
	  nbits=16;
	  offset =  FREQ_SWEEP_LENGTH;
	  data = psmRegRead16(myvme, PSM_BASE, offset);  /* 16 bit read */
	  printf("Read Frequency Sweep Length register as 0x%x or %d\n",data,data);
	  printf("Write to Freq Sweep Length register? y/n ");
	  scanf("%s",cmd);
          cmd[0]=toupper(cmd[0]);
          if(strncmp(cmd,"Y",1)==0)
	    {
	      printf("Enter length to write (dec) :");
	      scanf("%d",&value);
	      data = psmRegWrite16(myvme, PSM_BASE, offset, value);  /* 16 bit write */
 printf("Frequency Sweep Length register wrote 0x%x or %d, read back 0x%x or %d\n",
	data,data,value,value);
	    }
	  break;
	case ('E'):  //  Read DM
          nbits=32;
          printf("Read Idle Freq? y/n ");
          scanf("%s",cmd);
          cmd[0]=toupper(cmd[0]);
          if(strncmp(cmd,"Y",1)==0)
	    {
              idle_flag=1;
	      offset = IDLE_FREQ_OFFSET; // offset from Freq Sweep DM Base
	      num=1;
	    }
	  else
	    {
	      idle_flag=0;
	      printf("Enter Offset from frequency memory base :0x");
	      scanf("%x",&offset);
	      offset=check_offset(offset);

	      printf("Enter number of 32-bit words to read :");
	      scanf("%d",&num);
	    }

	  psmReadFreqDM( myvme,  PSM_BASE, num, offset );

	  break;

	case ('F'):  //  Write DM
          nbits=32;
	  printf("Write Idle Freq ? y/n ");
          scanf("%s",cmd);
          cmd[0]=toupper(cmd[0]);
          if(strncmp(cmd,"Y",1)==0)
	    {
	      offset = IDLE_FREQ_OFFSET; // offset from Freq Sweep DM Base
              idle_flag=1;
	      num=1;
	    }
	  else
	    {
              idle_flag=0;
	      printf("Enter Offset from DM base :0x");
	      scanf("%x",&offset);
	      offset=check_offset(offset);
	      printf("Enter number of 32-bit data words to write : ");
	      scanf("%d",&num);
	    }

	  for(i=0;i<num;i++)
	    {
	      printf("Enter data to write (Hz) :");
	      scanf("%d",&value);
              data0=get_hex(value);
	      data = psmRegWriteFreqDM( myvme,  PSM_BASE, offset, data0 );
	      printf("Offset 0x%x Wrote 0x%x  Read back  0x%x or %d  (%d bits)\n",
		     offset, data0, data,data, nbits);
	      offset+=4;
	    }

          if(idle_flag)
	    {
	      printf("Now sending a frequency address reset\n");
	      psmFreqSweepAddrReset( myvme, PSM_BASE);  // sets data length to 0 before performing reset (bug)
	      printf("Now sending a frequency strobe\n");
	      psmFreqSweepStrobe(myvme, PSM_BASE);
              break;
	    }


	  /* Load the frequency sweep length register
	     PSM needs length+1 (check this) */
	  if(pdd) printf("Num freq.incr.=%d; PSM needs one extra for data length reg.\n",num);
	  num++;


	  DWORD reg_offset;
	  /* write this value to the freq sweep length register */
	  reg_offset =  FREQ_SWEEP_LENGTH;
	  printf("Now writing data length %d to freq sweep length reg (at offset=0x%x)\n",
		 num,reg_offset);
	  data = psmRegWrite16(myvme, PSM_BASE, reg_offset, num);  /* 16 bit write */



	  // This will reset freq sweep regs to first frequency location
	  printf("Now sending a Preset to load the first freq\n");
	  psmFreqSweepAddrPreset( myvme, PSM_BASE);

	  printf("Now sending a frequency strobe to load the first freq\n");
	  psmFreqSweepStrobe(myvme, PSM_BASE);

	  printf("Sending a more strobes should step through the frequencies\n");

	  break;

	case ('G'):
	  // Read or Write Gate Control Reg
	  show_selected_channel(&selected_channel);
	  psmReadGateControl(myvme, PSM_BASE, selected_channel); // display data

	  printf("Gate selection:\n");
	  printf("0=%s   1 = %s\n",gate_info(0),gate_info(1));
	  printf("2=%s   3 = %s; gate input ignored\n",gate_info(2),gate_info(3));

	  printf("Select gate code (0-3) or 4 to exit :");
	  scanf("%d",&code);
	  if(code < 0 || code > 3)
	    {
	      printf("Gate code must be in range 0-3 (not %d)\n",code);
	      break;
	    }
	  data0 = psmWriteGateControl( myvme,  PSM_BASE, selected_channel,code);
	  printf("Read back data0=0x%x\n",data0);
	  psmReadGateControl(myvme, PSM_BASE, selected_channel); // display data
	  break;


	case ('c'):

	  // Read or Write End Sweep Control Reg
	  printf("Reading Sweep Control register (all channels and freq sweep)\n");
	  psmReadEndSweepControl(myvme, PSM_BASE, selected_channel, 3); // display=3 all data
	  printf("\nWrite Register? (Y/N)\n");
	  scanf("%s",cmd);
	    cmd[0]=tolower(cmd[0]);
	    if(strncmp(cmd,"n",1)==0)
	      break;
	  INT freq_sweep;
	  printf("\nSelect Frequency Sweep (F) or  I&Q End Sweep Mode (IQ) :");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	    if(strncmp(cmd,"F",1)==0)
	      freq_sweep=1;
	    else
	      freq_sweep=0;

	  if(!freq_sweep)
	    show_selected_channel(&selected_channel);



	  printf("End Sweep Control selection:\n");
	  printf("0=%s   1 = %s\n",endsweep_info(0),endsweep_info(1));

	  printf("Enter (0-1) or 2 to exit :");
	  scanf("%d",&code);
	  if(code < 0 || code > 1)
	    {
	      printf("Sweep code must be in range 0-1 (not %d)\n",code);
	      break;
	    }
	  data0 = psmWriteEndSweepControl( myvme,  PSM_BASE, freq_sweep, selected_channel, code);
	  printf("Read back data0=0x%x\n",data0);

	  printf("Now calling psmReadEndSweepControl  freq_sweep=%d\n",freq_sweep);
	  if(freq_sweep)
	    psmReadEndSweepControl(myvme, PSM_BASE, selected_channel, 2); // display freq sweep data only
	  else
	    psmReadEndSweepControl(myvme, PSM_BASE, selected_channel, 1); // display IQ data only
	  break;

	case ('I'):  //   init all channels
	  selected_channel = ALLCH;
	  psmInit(myvme, PSM_BASE, selected_channel );
	  break;
	case ('J'):  //  Write Tuning frequency

          nbits=32;
	  printf("Enter tuning frequency (Hz) : ");
	  scanf("%d",&freq_Hz);

	  printf("Enter Tuning frequency channel (f1, fc0,fc1) ? :");
          scanf("%s",cmd);
	  for(j=0;j<4;j++)
	    cmd[j]=tolower(cmd[j]);

          if(strncmp(cmd,"f1",2)==0)
	    offset =  F1_TUNING_FREQ;
	  else if (strncmp(cmd,"fc0",3)==0)
	    offset =  FC0_TUNING_FREQ;
	  else if (strncmp(cmd,"fc1",3)==0)
	    offset =  FC1_TUNING_FREQ;
	  else
	    {
	      printf("Unknown tuning register %s. Enter one of \"f1\", \"fc0\" or \"fc1\"\n",cmd);
	      break;
	    }

	  psmWrite_reference_freq_Hz(myvme, PSM_BASE, offset, freq_Hz);
	  break;

	case ('j'):  //  Read Tuning frequency registers f1, fc0, fc1

          nbits=32;
	  offset =  F1_TUNING_FREQ;
	  data = psmRead_reference_freq(myvme, PSM_BASE,  F1_TUNING_FREQ , &freq_Hz);
	  printf("f1 tuning frequency = %d Hz (hex value = 0x%x)\n",freq_Hz,data);
	  data = psmRead_reference_freq(myvme, PSM_BASE, FC0_TUNING_FREQ , &freq_Hz);
	  printf("fC0 tuning frequency = %d Hz (hex value = 0x%x)\n",freq_Hz,data);
	  data = psmRead_reference_freq(myvme, PSM_BASE, FC1_TUNING_FREQ , &freq_Hz);
	  printf("fC1 tuning frequency = %d Hz (hex value = 0x%x)\n",freq_Hz,data);
	  break;


	case 'K' :
	  // ancillary control
	  show_selected_channel(&selected_channel);
	  psmReadAncilIOControl(myvme, PSM_BASE, selected_channel);
	  printf("\nWrite Ancillary Control Register? (Y/N)\n");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;

	  printf("Set NIM Input (0) or NIM Output (1) ? ");
	  scanf("%d",&code);
	  psmWriteAncilIOControl(myvme, PSM_BASE, selected_channel, code);
	  psmReadAncilIOControl(myvme, PSM_BASE, selected_channel);

	  break;

	case 'k' :
	  // ancillary input or output registers
	  // read both registers
	  show_selected_channel(&selected_channel);
	  psmReadAncilIO(myvme, PSM_BASE,  ANCILLARY_INPUT, selected_channel );
	  psmReadAncilIO(myvme, PSM_BASE,  ANCILLARY_OUTPUT, selected_channel);

	  printf("\nWrite Ancillary Output Register? (Y/N)\n");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;

	  printf("Enter value (1/0) ? ");
	  scanf("%d",&code);
	  psmWriteAncilOutput(myvme, PSM_BASE, selected_channel, code);
	  psmReadAncilIO(myvme, PSM_BASE,  ANCILLARY_OUTPUT, selected_channel);
	  break;

	case 'L' :
	  // read/write channel reg
	  show_selected_channel(&selected_channel);

	  size = sizeof(data_array)/sizeof(INT);

	  printf("Select register \n");
	  printf("1=Phase Modln  2=Scale Factor  3=Buffer Factor\n");
	  printf("4=IQ DM Length                 5=IQ DM Address\n");
	  printf("Enter selection (1-5) or 6 to exit? ");
	  scanf("%d",&code);
	  if(code < 1 || code > 5)
	    break;
	  offset=(code-1) *2;
	  if(code == 2)
	    nbits=8;
	  else
	    nbits=16;

	  printf("Reading channel register..\n");

	  psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;
	  //   show_data_array(selected_channel); not needed

	  printf("\nWrite to this channel Register? (Y/N) ");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;
	  printf("\nEnter value to write in hex 0x");
	  scanf("%x",&data);
	  if(psmWriteChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, data) == SUCCESS)
	      printf("Wrote 0x%x to Channel Reg at offset 0x%x from base\n",data,offset);

	    break;

	case 'l' :
	  // test SetScaleFactor routine
	  show_selected_channel(&selected_channel);
	  code=2;nbits=8;  offset=(code-1) *2; // Scale Factor
	  if(selected_channel == ALLCH)
	    {
	      size = sizeof(data_array)/sizeof(INT);
	      psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;
	    }
	  else
	    psmRead1ChanReg(myvme, PSM_BASE, offset, nbits, selected_channel, &data);

	  printf("\nWrite to Scale Factor Register ? (Y/N) ");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;
	  printf("Enter scale factor to write 0-255 or -1 to exit? ");
	  scanf("%d",&data);

	  psmSetScaleFactor(myvme, PSM_BASE, selected_channel, data);
	  // Read back
	  if(selected_channel == ALLCH)
	    psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;
	  else
	    psmRead1ChanReg(myvme, PSM_BASE, offset, nbits, selected_channel, &data);


	  break;

	case 'm' :
	  // test WriteBufferFactor routine
	  show_selected_channel(&selected_channel);
	  code=3; nbits=16;  offset=(code-1) *2; // Buffer Factor
	  if(selected_channel == ALLCH)
	    {
	      size = sizeof(data_array)/sizeof(INT);
	      psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;
	    }
	  else
	    psmRead1ChanReg(myvme, PSM_BASE, offset, nbits, selected_channel, &data);

	  printf("\nWrite to Buffer Factor Register ? (Y/N) ");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;
	  printf("\nEnter value Nc to write ");
	  scanf("%d",&data);

	  psmWriteBufFactor(myvme, PSM_BASE, selected_channel, data);
	  // Read back
	  if(selected_channel == ALLCH)
	    psmReadChannelReg(myvme, PSM_BASE, offset, nbits, selected_channel, size, data_array) ;
	  else
	    psmRead1ChanReg(myvme, PSM_BASE, offset, nbits, selected_channel, &data);

	  break;



	case 'r' :
	  // freq sweep addr reset
	  psmFreqSweepAddrReset( myvme, PSM_BASE);
          break;

	case 'M' :
	  // Load freq file (hex)
	  printf("Default directory for loadfiles is \"%s\"\n",loadfiledir);
	  printf("Enter frequency file name e.g. %s ? ",defaultFreqfilename);
	  scanf("%s",filename);
	  sprintf(filepath,"%s/%s",loadfiledir,filename);
	  printf("Frequencies will be loaded starting at DM Base (0xA000)\n");
	  DWORD nfreq;
	  DWORD first_freq;
	  psmLoadFreqFile( myvme, PSM_BASE, filepath , &nfreq, &first_freq); // does a strobe
	  printf("Loaded frequency file %s . Number of frequencies=%d first frequency = 0x%x\n",
		 filename, nfreq,first_freq);



	  /* preset Frequency Address register  */
	  printf("\nSending psmFreqSweepAddrPreset to set Freq Sweep Address to 0\n");
	  psmFreqSweepAddrPreset(myvme,PSM_BASE  );

	  printf("\nReading Freq Sweep Address reg\n");
	  offset =  FREQ_SWEEP_ADDRS;
	  data=(psmRegRead16(myvme,PSM_BASE  , offset )) &  ELEVEN_BIT_MASK;
	  printf("Frequency sweep address  =     0x%3.3x (%d)\n",data,data);

	  printf("\nSending a strobe to load the first frequency\n");
	  psmFreqSweepStrobe(myvme, PSM_BASE );

	  break;

	case 'N' :
	  // write Fcw frequency value
	  printf("Module should be set into CW Mode (command 'B')\n");
          printf("Enter channel to write (1-4) or 0 for all? :");
	  scanf("%d", &ichan);
	  if(ichan<0 || ichan>4)
	    break; // illegal
	  nbits=32;
	  j=1; // default - write to 1 channel
	  if(ichan==0)
	    {
	      j=4;
	      ichan=1; // write to 4 channels starting at 1
	    }
	  while(j>0)
	    {
	      printf("\nEnter Fcw channel %d tuning frequency (Hz) : ",ichan);
	      scanf("%d",&freq_Hz);
	      offset = FCW_TUNING_FREQ + (4 * (ichan-1));
	      psmWrite_reference_freq_Hz(myvme, PSM_BASE, offset, freq_Hz);
	      j--;
	      ichan++; // next channel
	    }
	  break;

	case 'n' :
	  // write Fcw scale factor
	  printf("Module should be set into CW Mode (command 'B')\n");
          printf("Enter channel to write (1-4) or 0 for all? :");
	  scanf("%d", &ichan);
	  if(ichan<0 || ichan>4)
	    break; // illegal
	  nbits=8;
	  j=1; // default - write to 1 channel
          flag=0;
	  if(ichan==0)
	    {
	      j=4;  // write to 4 channels starting at 1
	      ichan=1;
	      printf("Write the same scale factor to all channels ? y/n :");
	      scanf("%s",cmd);
	      cmd[0]=toupper(cmd[0]);
	      if(strncmp(cmd,"Y",1)==0)
		{
		  printf("Enter scale factor : ");
		  scanf("%d",&value);
		  flag=1;
		}
	    }
	  while(j>0)
	    {
	      if(!flag)
		{
		  printf("Enter Fcw channel %d scale factor : ",ichan);
		  scanf("%d",&value);
		}
	      offset= FCW_SCALE_FACTOR +  (ichan-1);
	      data = psmRegWrite8(myvme, PSM_BASE, offset, (value & mask8));
	      printf("Wrote  0x%2.2x or %d (8 bits) to register at offset 0x%x \n", (value & mask8), (value & mask8),offset );
	      printf("Read back  0x%x or %d  \n", data,data );
	      j--;
	      ichan++;
	    }
	  break;

	case 's' :
	  // Freq sweep Strobe
	  psmFreqSweepStrobe( myvme, PSM_BASE );
	break;


	case 'R' :
	  // VME Reset
	  psmVMEReset(myvme,  PSM_BASE);
          break;


	case 'V' :
	  // Voltage trip Threshold Register
	  volts = psmReadRFpowerTripThresh(myvme, PSM_BASE);  // 8 bits
	  printf("Voltage Trip Threshold Register set to trip at %f volts\n",volts);
	  printf("\nWrite to Voltage Trip Threshold Register? (Y/N)\n");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;
	  printf("\nEnter RF Trip value in volts (0-5)? \n");
	  scanf("%f",&volts);
	  if(volts < 0 || volts > 5)
	    {
	      printf("Illegal value %f\n",volts);
	      break;
	    }
	  data = psmWriteRFpowerTripThresh(myvme, PSM_BASE,volts);
	  printf("Wrote %f Volts ; read back %d (0x%x) ",volts,data,data);
	  volts = 5 * data/255;
	  printf(" or %f Volts\n",volts);
	  break;

	case 'v' :
	  // Voltage Trip/Reset Reg
          data =  psmReadRFpowerTrip(myvme, PSM_BASE);  // 8 bits
	  data = psmRegRead8(myvme, PSM_BASE , RF_TRIP_STATUS_RESET ) & 1;  // 1 bits
	  printf("RF Power Trip Status/Reset Register = 0x%x (%d)\n",data,data);
	  if(data)printf("\nRF has tripped. Reset ? (Y/N)\n");
	  scanf("%s",cmd);
	  cmd[0]=tolower(cmd[0]);
	  if(strncmp(cmd,"n",1)==0)
	    break;
	  data=psmClearRFpowerTrip(myvme, PSM_BASE);
	  printf("After reset, register reads %d\n",data);
	  if(data)
	    printf("RFpower IS still Tripped\n");
	  else
	    printf("RFpower NOT Tripped\n");
	  break;


 	case 'B' :
	  // BNM/QR or CW Operating mode
	  data = psmRegRead8(myvme, PSM_BASE ,  MODULE_OPERATING_MODE ) & 1;  // 1 bits
	  printf("BNM/QR or CW Operating Mode Register = 0x%x (%d)\n",data,data);

	  printf("\nSelect BNMR (0) or BNQR (1) or CW (2) Mode or exit (9) ? \n");
	  scanf("%d",&num);
	  if(num < 0 || num > 3) // CW is 2 or 3
	    break;
	  data = psmRegWrite8(myvme, PSM_BASE ,  MODULE_OPERATING_MODE, num );
	  printf("Wrote %d, read back %d\n",num,data);
	  break;


	case ('e'):  //  Read I,Q  DM pair
	  show_selected_channel(&selected_channel);
	  nbits=32;
          printf("Read Idle I,Q pair? y/n ");
          scanf("%s",cmd);
          cmd[0]=toupper(cmd[0]);
          if(strncmp(cmd,"Y",1)==0)
	    {
	    offset = IDLE_IQ_OFFSET; // offset from base I,Q mem 0x1FFC
	    num=1;
	    }
	  else
	    {
	      printf("Enter Offset from I,Q memory base :0x");
	      scanf("%x",&offset);
	      offset=check_offset(offset);
	      printf("Enter number of I,Q pairs to read :");
	      scanf("%d",&num);
	    }

	  if(selected_channel == ALLCH)
	    {  // 'ALL'
	      istart=1;
	      istop=6;
	    }
	  else
	    {
	      istart=selected_channel;
	      istop=istart+1;
	    }

	  for(ichan=istart; ichan< istop; ichan++)
	    {
              j=offset; // offset start value
	      base_iq_addr=get_IQ_BaseAddr(ichan); // use channel
	      printf("\nChannel %s:\n",channel_name(ichan));
	      for(i=0; i<num; i++)
		{
		  data = psmRegReadIQDM( myvme,  PSM_BASE, base_iq_addr, j, &I, &Q );
		  printf("     Offset 0x%x Read back  0x%x (%d bits) I=%d Q=%d\n",
			 j,data, nbits, I,Q);
		  j+=4; // next address in memory
		}
	    }
	  break;

	case ('f'):  //  Write I,Q pair(s)
          nbits=32;
	  show_selected_channel(&selected_channel);
          printf("Write Idle I,Q pair? y/n ");
          scanf("%s",cmd);
          cmd[0]=toupper(cmd[0]);
          if(strncmp(cmd,"Y",1)==0)
	    {
	      offset = IDLE_IQ_OFFSET; // offset from base I,Q mem  0x1FFC
	      num=0; // length
	    }
	  else
	    {
	      printf("Offset from I,Q memory base will be 0\n");
	      offset=0; // start at 0

	      printf("Load I,Q pairs from a file?");
	      scanf("%s",cmd);

	      cmd[0]=toupper(cmd[0]);
	      if(strncmp(cmd,"Y",1)==0)
		{ // loading from a file
		  printf("Default directory for loadfiles is \"%s\"\n",loadfiledir);
		  printf("Enter IQ filename e.g. %s :",  defaultIQfilename);
		  scanf("%s",filename);
		  sprintf(filepath,"%s/%s",loadfiledir,filename);
		  if(selected_channel == ALLCH)
		    {
		      istart=1;
		      istop=6;
		    }
		  else
		    {
		      istart=selected_channel;
		      istop=istart+1;
		    }

		  for(ichan=istart; ichan< istop; ichan++)
		    {
		      base_iq_addr=get_IQ_BaseAddr(ichan); // use channel to get base address

		      printf("Channel %s: Loading IQ DM from file %s at base_iq_address 0x%x \n",
			     channel_name(ichan),filepath, base_iq_addr);
                      // data in file will be converted to 2s comp if negative
		      status = psmLoadIQfile( myvme,  PSM_BASE, base_iq_addr, filepath, &num, &I, &Q );
		      if(status == SUCCESS)
			{
			  printf("Channel %s: Successfully loaded I,Q pairs file\n", channel_name(ichan));
			  printf("Channel %s: Number of I,Q pairs loaded: %d; first i,q pair was (%d,%d)\n",
				 channel_name(ichan),num,I,Q);

			  psmWriteIQLen( myvme, PSM_BASE,  ichan, num);
			}
		      else
			{
			  printf("Channel %s: Error loading I,Q pairs file\n", channel_name(ichan));
			  break;
			}
		    } // for
		  break; // done
		}
	    }

	  // load idle I,Q pair
	  while(1)
	    {
	      printf("Enter I,Q pair data to write (e.g. 0,-511) :");
	      scanf("%d,%d",&I,&Q);
	      if((I > 1023 || I< -511 ) || (Q > 1023 || Q < -511 ))
		printf("Value is out of range (10 bits only)\n");
	      else
		break;
	    }


	  if(selected_channel == ALLCH)
	    {  //
	      istart=1;
	      istop=6;
	    }
	  else
	    {
	      istart=selected_channel;
	      istop=istart+1;
	    }

	  for(ichan=istart; ichan< istop; ichan++)
	    {
	      psmLoadIdleIQ( myvme,  PSM_BASE,  ichan, I,Q );
	      //	      base_iq_addr=get_IQ_BaseAddr(ichan); // use channel
	      // data = psmRegWriteIQDM( myvme,  PSM_BASE, base_iq_addr,  offset, I,Q );
	      // if(pdd)printf("Channel %d Base_iq_addr=0x%x, Offset 0x%x Wrote I=%d Q=%d ; Read back  %d  (%d bits) \n",
	      //	    ichan, base_iq_addr,  offset,  I,Q,data, nbits);
	      // getIQpair(data, &II, &QQ); // get I,Q pair out of the 32-bit data


	      //  printf("Channel %s : wrote i,q pair as %d,%d; read back %d,%d\n",
	      //	     channel_name(ichan), I,Q,II,QQ);
	      // Check values match
	      //  if(check_IQ_match(I,II) && check_IQ_match(Q,QQ))  // writes a message if they don't match
	      //	printf("Values match\n");

	      psmWriteIQLen( myvme, PSM_BASE,  ichan, num); // write zero length
	    }


	  break;

        case ('P'):
	  psm(PSM_BASE);
	  break;

        case ('p'):
	  psmFreqSweepAddrPreset(myvme,PSM_BASE);
	  break;

	case ('g'):
	  printf("Select Gated (G) or PreGated (P) ?");
	  scanf("%s",cmd);
	  cmd[0]=toupper(cmd[0]);



	  if(strncmp(cmd,"P",1)==0)
	    {
	      offset= RF_PRE_GATED_OUTPUT_SELECT;
	      sprintf(string,"PreGated");
	    }
	  else if(strncmp(cmd,"P",1)==0)
	    {
	      offset= RF_GATED_OUTPUT_SELECT;
	      sprintf(string,"Gated");
	    }
	  else
	    {
	      printf("Illegal input %s. Expect either \"P\" or \"G\"\n",cmd);
	      break;
	    }

	  printf("Set RF %s output view port :\n",string);
	  printf("1-4 = select f1 channel 1-4 \n");
	  printf("5   = select channel f1  \n");
	  printf("6   = select Sum \n");
          printf("7   = read present value\n");
	  printf("Enter selection (1-7) :");
	  scanf("%d",&code);
	  if(code < 1 || code > 7)
	    {
	      printf("Illegal input %d\n",code);
	      break;
	    }

	  if(code < 7)
	    {
	      code--; // 0-5
	      data = psmRegWrite8(myvme, PSM_BASE, offset, (code & mask8));
	      //printf("Wrote %d, read back %d\n",code,data);
	    }
	  else
	    data = psmRegRead8(myvme, PSM_BASE, offset);
          data++;
	  if(data == 6)
	    printf("RF %s port: Sum of all channels is selected\n",string);
	  else
	    printf("RF %s port: %s is selected\n",string,channel_name(data));
	  break;



	case ('X'):
          return(SUCCESS);
          break;

	  // Test procedures

	case ('1'):

	  // disable all channels by setting gates to 0
	  selected_channel=ALLCH;
	  // these two handle ALLCH
	  psmWriteGateControl(myvme, PSM_BASE , selected_channel,FP_GATE_DISABLED ); // no gates
	  psmSetScaleFactor(myvme, PSM_BASE ,  selected_channel, 181); //  amplitude

	  printf("Enter Idle Frequency (Hz) :");
	  scanf("%d",&data);
	  data0=get_hex((unsigned int)data); // convert Hz to hex
	  data = psmRegWriteFreqDM( myvme,  PSM_BASE, IDLE_FREQ_OFFSET , data0 );
	  INT max;
	  printf("Enter max active channel (1-5) ?:");
	  scanf("%d",&max);
	  if(max > 5) max=5;
	  else if (max < 1) max=1;
	  printf("Max active channel is %d\n",max);

	  // INT I_defaults[]={511,  0,-511,   0, 511};
	  // INT Q_defaults[]={0,  511,   0,-511,   0};
	   INT I_defaults[]={0,0,0,0,0};
	   INT Q_defaults[]={511,511,511,511,511};

	  INT use_defaults=0;
	  if(max == 5)
	    {
	      printf("Enter f1 Reference Frequency (Hz) :");
	      scanf("%d",&freq_Hz);
	      offset =  F1_TUNING_FREQ;
	      psmWrite_reference_freq_Hz(myvme, PSM_BASE, offset, freq_Hz);
	      printf("Reference f1 will stay on always\n");
	    }

	  offset = IDLE_IQ_OFFSET; // offset from base I,Q mem  0x1FFC
	  printf("Read default values for I,Q pairs from file \"%s\" ? (y/n)?",iqdefault_file);
          scanf("%s",cmd);
          cmd[0]=toupper(cmd[0]);
	  if(strncmp(cmd,"Y",1)==0)
	    {
	      printf("Opening defaults file \n");
	      fin = fopen(iqdefault_file,"r");
	      if(fin==NULL)
		printf("Failure opening file %s\n",iqdefault_file);
	      else
		{
		  printf("Successfully opened file %s\n",iqdefault_file);
		  j=0;
		  while (fscanf(fin,"%d %d", &I,&Q) != EOF)
		    {
		      printf("j=%d I=%d Q=%d\n",j,I,Q);
		      I_defaults[j]=I;
		      Q_defaults[j]=Q;
		      j++;
		      if(j >= max)break;
		    }
		  fclose (fin);
		}
	    }

	  printf("Default values for I,Q pairs are ");
	  for(j=0; j<max; j++)
	    printf("%d,%d ",I_defaults[j],Q_defaults[j]);
	  printf("\nUse default values for I,Q pairs (y/n)?");
	  scanf("%s",cmd);
	  cmd[0]=toupper(cmd[0]);
	  if(strncmp(cmd,"Y",1)==0)
	    use_defaults=1;

	  for(ichan=1;ichan<max+1;ichan++)
	    {
	      if(ichan != 5) // reference
		{
		  printf("Enter time channel %d is enabled (ms)? :",ichan);
		  scanf("%d",&active_time_ms[ichan-1]);
		}
	      if(use_defaults)
		{
		  I=I_defaults[ichan-0];
		  Q=Q_defaults[ichan-0];
		}
	      else
		{
		retry:
		  printf("Enter Idle I,Q pair for Channel %d (e.g. 0,-511) : ?",ichan);
		  scanf("%d,%d",&I,&Q);
		  if((I > 1023 || I< -511 ) || (Q > 1023 || Q < -511 ))
		    {
		      printf("Value is out of range (10 bits only). Please try again\n");
		      goto retry;
		    }
		}

	      base_iq_addr=get_IQ_BaseAddr(ichan); // use channel
	      data = psmRegWriteIQDM( myvme,  PSM_BASE, base_iq_addr,  offset, I,Q );
	      if(pdd)printf("Channel %d Base_iq_addr=0x%x, Offset 0x%x Wrote I=%d Q=%d ; Read back  %d  (%d bits) \n",
			    ichan, base_iq_addr,  offset,  I,Q,data, nbits);
	      getIQpair(data, &II, &QQ); // get I,Q pair out of the 32-bit data


	      printf("Channel %s : wrote i,q pair as %d,%d; read back %d,%d\n",
		     channel_name(ichan), I,Q,II,QQ);
	      // Check values match
	      if(check_IQ_match(I,II) && check_IQ_match(Q,QQ))  // writes a message if they don't match
		printf("Values match\n");

	    } // ichan loop
	  printf("Now sending a frequency address reset\n");
	  psmFreqSweepAddrReset( myvme, PSM_BASE); // sets freq data length reg to 0 before performing reset (bug)

	  printf("Now sending a frequency strobe\n");
	  psmFreqSweepStrobe(myvme, PSM_BASE);



          psmTest1(myvme, PSM_BASE, max, active_time_ms);
          break;


	case ('2'):
	  // Loads IQ and DM from default files, load idle IQ, Freq issue mem addr preset and 1 strobe
	  psmVMEReset( myvme, PSM_BASE );
	  show_selected_channel(&selected_channel);

	  printf("Setting Gates Always and amplitude (scale factor) to 181\n");
	  // these two handle ALLCH
	  psmWriteGateControl(myvme, PSM_BASE , selected_channel, INTERNAL_GATE); //  gates always
	  psmSetScaleFactor(myvme, PSM_BASE ,  selected_channel, 181); //  amplitude

	  printf("Enter Idle Frequency (Hz) :");
	  scanf("%d",&data);
	  data0=get_hex((unsigned int)data); // convert Hz to hex
	  data = psmRegWriteFreqDM( myvme,  PSM_BASE, IDLE_FREQ_OFFSET , data0 );

	  if(selected_channel == ALLCH || selected_channel == F1)
	    {
	      printf("Enter f1 Reference Frequency (Hz) :");
	      scanf("%d",&freq_Hz);
	      offset =  F1_TUNING_FREQ;
	      psmWrite_reference_freq_Hz(myvme, PSM_BASE, offset, freq_Hz);

	    }


	  sprintf(filepath,"%s/%s",loadfiledir,defaultFreqfilename);
	  printf("Loading default frequency file %s\n",filepath);
	  psmLoadFreqFile( myvme, PSM_BASE, filepath , &nfreq, &first_freq); // loads length register, no preset
	  printf("Loaded frequency file %s . Number of frequencies=%d first frequency = 0x%x\n",
		 filepath, nfreq,first_freq);

	  sprintf(filepath,"%s/%s",loadfiledir,defaultIQfilename);
	  printf("Loading default iq file %s\n",filepath);
	  if(selected_channel == ALLCH)
	    {
	      istart=1;
	      istop=6;
	    }
	  else
	    {
	      istart=selected_channel;
	      istop=istart+1;
	    }
	  for(ichan=istart; ichan< istop; ichan++)
	    {
	      base_iq_addr=get_IQ_BaseAddr(ichan); // use channel to get base address

	      printf("Channel %s: Loading IQ DM from file %s at base_iq_address 0x%x \n",
		     channel_name(ichan),filepath, base_iq_addr);
	      // data in file will be converted to 2s comp if negative
	      status = psmLoadIQfile( myvme,  PSM_BASE, base_iq_addr, filepath, &num, &I, &Q ); // does not load the length
	      if(status == SUCCESS)
		{
		  printf("Channel %s: Successfully loaded I,Q pairs file\n", channel_name(ichan));
		  printf("Channel %s: Number of I,Q pairs loaded: %d; first i,q pair was (%d,%d)\n",
			 channel_name(ichan),num,I,Q);

		  psmWriteIQLen( myvme, PSM_BASE,  ichan, num);  // load the length
		}
	      else
		{
		  printf("Channel %s: Error loading I,Q pairs file\n", channel_name(ichan));
		  break;
		}


	      while(1)
		{
		  printf("Enter I,Q pair data to write for channel %s (e.g. 0,-511) :", channel_name(ichan) );
		  scanf("%d,%d",&I,&Q);
		  if((I > 1023 || I< -511 ) || (Q > 1023 || Q < -511 ))
		    printf("Value is out of range (10 bits only)\n");
		  else
		    break;
	    }
	      psmLoadIdleIQ( myvme,  PSM_BASE,  ichan, I,Q ); // load Idle IQ pair
	    } // for




	  /* preset Frequency Address register  */
	  printf("\nSending psmFreqSweepAddrPreset\n");
	  psmFreqSweepAddrPreset(myvme, PSM_BASE);

	  printf("\nReading Freq Sweep Address reg\n");
	  offset =  FREQ_SWEEP_ADDRS;
	  data=(psmRegRead16(myvme, PSM_BASE, offset )) &  ELEVEN_BIT_MASK;
	  printf("Frequency sweep address  =     0x%3.3x (%d)\n",data,data);


	  printf("Now sending a frequency strobe\n");
	  psmFreqSweepStrobe(myvme, PSM_BASE);

	  break;


	}
    }
  return SUCCESS;
}

#endif // MAIN_ENABLE
