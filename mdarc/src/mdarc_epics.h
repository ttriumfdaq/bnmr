/* mdarc_epics.h

 function prototypes


$Log: mdarc_epics.h,v $
Revision 1.1  2013/01/21 21:42:38  suz
initial VMIC version to cvs

Revision 1.1  2007/07/27 21:46:39  suz
initial version


*/
#ifndef mdarc_epics_header
#define mdarc_epics_header

#ifdef EPICS
INT init_epics(void);
INT open_epics_log(INT *pnum_epics); // prototype
INT open_epics_chan(char *name,  INT i);
INT read_epics_value(INT i);
void clear_epics_log(INT i);
void zero_epics_log(INT i);
void set_epics_constants(void);
INT get_epics_constants(char *Rname);
void epics_close(void);
#endif

#endif  // mdarc_epics_header
