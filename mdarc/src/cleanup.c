/********************************************************************\
  
  Name:         cleanup.c
  Created by:   Suzannah Daviel (from mdarc.c)
  
  Contents:  Cleanup BNMR saved files
  
  Previous revision history
  $Log: cleanup.c,v $
  Revision 1.5  2015/11/27 23:25:04  suz
  added ifdef YBOS

  Revision 1.4  2015/04/30 00:06:00  suz
  add an ifdef to merge with musr version

  Revision 1.3  2015/04/29 22:52:05  suz
  changes to cleanup help

  Revision 1.2  2015/04/29 22:32:15  suz
  add new beamlines for musr and add a range of run numbers to cleanup

  Revision 1.1  2013/01/21 21:42:38  suz
  initial VMIC version to cvs

  Revision 1.17  2007/07/27 20:13:54  suz
  added handles - possibly for c++ linking?

  add to cvs
 

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
#ifdef HAVE_YBOS
#include "ybos.h"  // not used
#endif
#ifdef MUSR
/* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "trii_fmt.h"  /* needed by MUSR in mdarc.h */ 
#endif
#include "mdarc.h" /* common header for mdarc,bnmr_darc & musr_darc */
#include "experim.h" 
#include "darc_files.h" 
#include "run_numbers.h"


#define MAX_PURGE_FILES  50
#define FAILURE 0

#ifndef MUSR
INT nH;
HNDLE hFS;
FIFO_ACQ_FRONTEND fifo_set;
#endif
INT setup_mdarc(); /* cd setup_hotlink in mdarc */
//void  hot_toggle (INT, INT, void * ); /* dummy here - should never be called */

HNDLE hDB;
INT rn;   // global run number to clean up
INT lrn;  // global last run number of range
INT status;

HNDLE hMDarc; /* handle for /equipment/<eqp_name>/mdarc */
#ifdef MUSR
MUSR_TD_ACQ_MDARC fmdarc;
#else
FIFO_ACQ_MDARC fmdarc;
#endif

char expt_name[HOST_NAME_LENGTH];

INT cleanup_init(void)
{
  INT   size;
  char  str[128];
  KEY   hKey;
  HNDLE hSet;
  HNDLE hArea;
  char save_dir[20];
  char archive_dir[20];  
  char filename[128],archived_filename[128]; 
  INT run_state;
  INT run_number;
  BOOL archived;
  INT jrn;

  printf ("Cleanup starting ... \n");
  
  cm_get_experiment_database(&hDB, NULL);


  /* Make sure we are not going to cleanup the current run ... */

//  get the CURRENT run number
    size = sizeof(run_number);
    status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"tr_start","key not found  /Runinfo/Run number");
      write_message1(status,"tr_start");
      return (status);
    }

  
    status = setup_mdarc();  /* obtain mdarc record */
    if(status != DB_SUCCESS) return(0);

    printf(" cleanup: purging & renaming saved files for run  %d\n",rn);
    printf("        (i.e. end-of-run procedure) \n");

  sprintf(archiver,"%s",fmdarc.archiver_task) ;  // archiver is in mdarc.h and passed to darc_files


  status=check_beamline();
  if (status != SUCCESS) return status; 



/*  Run cleanup procedure (same as at end of run) */
    
       
    if(debug)
    {
      printf("cleanup: saved_data_directory: %s\n",fmdarc.saved_data_directory);
      printf("cleanup: archived_data_directory: %s\n",fmdarc.archived_data_directory);
      printf("cleanup: archiver= %s   \n",fmdarc.archiver_task);
      if(lrn != rn)
	printf("cleanup: calling darc_rename with run number range %d to %d \n",rn,lrn);
      else
	printf("cleanup: calling darc_rename with run number %d   \n",rn);
 
    }
    if(debug_check)printf("debug_check is true\n");


    for(jrn=rn; jrn <= lrn; jrn++)
      {
	printf("\ncleanup: working on run %d\n", jrn);
	if (jrn == run_number)
	  {
	    /* get the run state to see if run is going */
	    size = sizeof(run_state);
	    status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
	    if (status != DB_SUCCESS)
	      {
		cm_msg(MERROR,"mdarc_cleanup","key not found /Runinfo/State");
		write_message1(status,"bnmr_darc");
		return (status);
	      }
	    if(run_state == 3)
	      {
		cm_msg(MERROR,"mdarc_cleanup","Run %d is still in progress; bad idea to clean it up now",run_number);
		return(0); // the current run will have the largest run number and therefore terminate the range.
	      }
	  }

	status = darc_rename_file(jrn,fmdarc.saved_data_directory, fmdarc.archived_data_directory, filename,
			      archived_filename, &archived);
	if (status == SUCCESS)
	  {
	    // darc_rename_file also archives file if appropriate
	    if(debug) printf("cleanup: success from darc_rename_file; saved file:%s\n",filename);
	    
	  }
	else
	  {
	    /* may be no versions to purge. Check for .msr and if found, archive it */
	    
	    if(!archived)
	      {
		printf("Cleanup: checking for a MUD  (.msr) file to Archive ...\n"); 
		if( jrn < 30000 || jrn >= 40000)
		  status = darc_archive_file(jrn, FALSE, fmdarc.saved_data_directory, fmdarc.archived_data_directory  );
		else
		  printf("cleanup: test runs are not archived\n");
	      }
	  }

      } // for loop

    return (status);
}

INT check_beamline(void)
{
  INT j;

  /* beamline needed for BNMR and MUSR to set directory for archive.txt
     and to check run number before archiving */

  // For all experiments now (BNM/QR or MUSR)
  strcpy(beamline,expt_name);  // expt_name and beamline are identical
  if(debug) printf("Got beamline from expt_name: %s\n",expt_name);

  
  for (j=0; j<strlen(beamline); j++)
      beamline[j] = toupper (beamline[j]); /* convert to upper case */
  if(debug) printf(" beamline:  %s, run number=%d\n",beamline,rn);

  if (strlen(beamline) <= 0 )
    {
      printf("check_beamline: no valid experiment name is supplied\n");
      return (DB_INVALID_PARAM);
    }

  
  /* check that the run number matches the beamline (real runs) */
  if(rn  >= MIN_M20C && rn <= MAX_M20C )        // M20C
    {
      if (strncmp(beamline,"M20C",4) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20C for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
       else
	printf("Run detected from Beamline M20C\n");
    }


  if(rn  >= MIN_M20D && rn <= MAX_M20D )        // M20D
    {
      if (strncmp(beamline,"M20D",4) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20D for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
       else
	printf("Run detected from Beamline M20D\n");
    }





  else if(  rn >= MIN_M15 &&  rn <= MAX_M15 )      // M15
    {
      if (strncmp(beamline,"M15",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M15 for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
      else
	printf("Run detected from Beamline M15\n");
      
				      
    }

  else if ( rn  >= MIN_M9A &&  rn <= MAX_M9A)
    {
      if (strncmp(beamline,"M9A",3) != 0)  // M9A
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M9A for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
      else 
	printf("Run detected from Beamline M9A\n");
      
    }

 else if ( rn  >= MIN_M9B &&  rn <= MAX_M9B)
    {
      if (strncmp(beamline,"M9B",3) != 0)  // M9B
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M9B for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
      else 
	printf("Run detected from Beamline M9B\n");
      
    }
  else if ( rn  >= MIN_DEV &&  rn <= MAX_DEV  )    // DEV
    {
      if (strncmp(beamline,"DEV",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = DEV for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
      else
	printf("dev run detected\n");
      
    }
   else if ( rn  >= MIN_BNMR &&  rn <= MAX_BNMR  )    // BNMR
    {
      if (strncmp(beamline,"BNMR",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNMR for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
    } 
   else if ( rn  >= MIN_BNQR &&  rn <= MAX_BNQR  )    // BNMR
    {
      if (strncmp(beamline,"BNQR",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNQR for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
    } 
  else if(  rn >= MIN_TEST &&  rn <= MAX_TEST  )   // test run (any beamline)
    {
      printf("Detected a test run\n");
    }
  else if(  rn >= MIN_M20 &&  rn <= MAX_M20  )   // OLD M20
    {
      cm_msg(MERROR,"check_beamline",
	     "Run number (%d) matches obsolete beamline (M20)\n",rn);
      return (DB_INVALID_PARAM);
    }
  else
    {
      cm_msg(MERROR,"check_beamline",
	     "Requested run number (%d) does not match any valid beamline",rn);
      return (DB_INVALID_PARAM);
    }
  return SUCCESS;
}



/*---- setup_mdarc  ---------------------------------------------------*/

INT setup_mdarc()
{
  char str[128];
  INT stat;

#ifdef MUSR
  MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);
#else
  FIFO_ACQ_MDARC_STR(acq_mdarc_str); 
#endif
  if(debug) printf("setup_mdarc starting\n");


  /* Create record for mdarc area */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  
  status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
  if (status != DB_SUCCESS)
    {
       /* mdarc may be running - it has hotlinks on some mdarc records
	 which prevents create record from working */
       if ( cm_exist("mdarc",FALSE)== CM_NO_CLIENT)
	 {
	/* mdarc is NOT running so create record should have worked */
	  cm_msg(MERROR,"setup_mdarc","Failed to create record for %s  (%d).", str,status);
	  write_message1(status,"setup_mdarc");
	  return(status);
	}
    }	  
  /* get the key hMDarc  */
 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_mdarc", "key %s not found (%d)", str,status);
    write_message1(status,"setup_mdarc");
    return (status);
  }



  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */





  
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_mdarc","Failed to retrieve %s record  (%d)",str);
    write_message1(status,"setup_mdarc");
    return(status);
  }
  return(status);
}  

 





/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT    status;
  DWORD  j, i, last_time_kb;
  HNDLE  hDB, hKey;
  char   host_name[HOST_NAME_LENGTH];
  char   ch;
  INT    msg;
  BOOL   cr=FALSE;
  BOOL   range=FALSE;
  char   clean_run[25];
  char   clean_last_run[25];
  //  char   dbg[25];
  int    bug;
  INT    l;


  /* set defaults for debug */
  debug = FALSE;
  debug_check = FALSE;

  /* set defaults for other globals */
   toggle = FALSE;  /* initial value */
   run_number = -1; /* global run number */

  
   cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
    

  /* initialize global variables */
#ifdef MUSR
  sprintf(eqp_name,"MUSR_TD_acq");
#else  // BNM/QR
  sprintf(eqp_name,"FIFO_acq");
#endif
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {

    if (argv[i][0] == '-'&& argv[i][1] == 'd')
      debug = debug_check = TRUE;
    else if  (argv[i][0] == '-')
      {
	if (i+1 >= argc || argv[i+1][0] == '-')
	  goto usage;
	if (strncmp(argv[i],"-e",2) == 0)
	  strcpy(expt_name, argv[++i]);
	else if (strncmp(argv[i],"-h",2)==0)
	  strcpy(host_name, argv[++i]);
	else if (strncmp(argv[i],"-q",2)==0)
	  strcpy(eqp_name, argv[++i]);
	else if (strncmp(argv[i],"-r",2)==0)
	  {
	    strcpy(clean_run, argv[++i]);
	    cr=TRUE;
	  }
	else if (strncmp(argv[i],"-l",2)==0)
	  {
	    strcpy(clean_last_run, argv[++i]);
	    range=TRUE;
	  }
      
    }
    else
    {
   usage:
#ifdef MUSR
      printf("\ncleanup: purges, renames (if necessary) and archives MUSR run files\n"); 
#else
      printf("\ncleanup: purges, renames (Type2) and archives (Types 1 & 2) BNMR/BNQR run files\n"); 
#endif
      printf("cleanup reads the archiver command and directory from the DAQ ODB\n\n");
      printf("usage: cleanup  [-h Hostname] [-e Experiment]  [-r Runnumber]  [-d ]  (debug)\n");
      printf("                or for range of run numbers: [-r First -l Last] \n");
      return 0;
    }
    
  }
  if(range)
    {
      if(!cr)
	{
	  printf("When supplying a range, use  -r and -l to specify first and last run of the range, e.g. -r 040054 -l 040065 \n");
	  return 0;
	}
      lrn =  atoi(clean_last_run);
      if (lrn <= 0)
	{
	  printf("Invalid last run number for cleanup; supplied number was %s\n",clean_last_run);
	  return 0;
	}

    }

  if(cr)
  {
    rn = atoi(clean_run);
    if (rn <= 0)
    {
      printf("Invalid run number for cleanup; supplied run number was %s\n",clean_run);
      return 0;
    }
    else
      {
	if(range)
	  { 
	    if(rn == lrn)
	        printf("About to run cleanup procedure for run %d\n",rn);
       
	    else if (rn > lrn)
	      {  // swap rn with lrn
		l=rn;
		rn=lrn;
		lrn=l;
	      }
	    printf("About to run cleanup procedure for range of runs %d to %d\n",rn,lrn);
	  }
	else
	  {
	    lrn=rn; // initialize lrn to rn
	    printf("About to run cleanup procedure for run %d\n",rn);
	  }
      }
  }
  else
  {
    printf("Supply run number for cleanup\n");
    return 0;
  }
 



  if (debug)
    printf("debug is TRUE\n");    
  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "mdarc_cleanup", 0);
  if (status != CM_SUCCESS)
    return 1;
  
  if(debug)
  cm_set_watchdog_params(TRUE, 0);
  
  /* turn off message display, turn on message logging */
  // cm_set_msg_print(MT_ALL, 0, NULL);
  /* turn on message display, turn on message logging */
  cm_set_msg_print(MT_ALL, MT_ALL, msg_print);  

  /* connect to the database */
//  cm_get_experiment_database(&hDB, &hKey);
  
  status = cleanup_init();


  cm_disconnect_experiment();

}














