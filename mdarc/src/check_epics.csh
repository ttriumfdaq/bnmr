#!/bin/csh 
#   (-x for debug)
# This script is executed before every run.
set num = `odb -e bnmr -c "ls '/Equipment/EpicsLog/Settings/n_epics_logged'"` 
echo "status= $status ; number of epics variables to be logged: $num "
exit
