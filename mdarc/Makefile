#
# Makefile for mdarc - MUSR TD/Integral and BNMR/BNQR  combined version
#
# Version for new Midas running with VMIC FE
#  64-bit machine
# 
#  Revision 1.23  2008/12/10 19:26:30  suz
#  link with efence and debug (Donald); use MHEADER to supply name of mheader file
#

# Make sure hostname is environment variable DAQ_HOST
# (set up in .cshrc)
# This file is to be built on the host computer (not VMIC).
ifeq ($(HOST),$(DAQ_HOST))
RH =1;
endif
ifeq ($(HOST),$(DAQ_HOST).triumf.ca)
RH =1;
endif

ifndef RH
wronghost::
	@echo "...";
	@echo "Wrong host ($(HOST)) !  Use  $(DAQ_HOST)";
	@echo "...";
endif


# The MIDASSYS should be defined prior the use of this Makefile
ifndef MIDASSYS
missmidas::
        @echo "...";
        @echo "Missing definition of environment variable 'MIDASSYS' !";
        @echo "...";
endif

ifndef LINUX
        @echo "...";
        @echo "Missing definition of environment variable 'LINUX' !";
        @echo "...";
endif

ifndef EPICS_BASE
        @echo "...";
        @echo "Missing definition of environment variable 'EPICS_BASE' !";
        @echo "...";
endif
ifndef EPICS_HOST_ARCH
        @echo "...";
        @echo "Missing definition of environment variable 'EPICS_HOST_ARCH' !";
        @echo "...";
endif

# location of MUD and CAMP in standard MUSR area
ifndef MUSR_DIR # set to /home/musrdaq/musr for cvs version (post-Ted)
missmd::
        @echo "...";
        @echo "Missing definition of environment variable 'MUSR_DIR' !";
        @echo "...";
endif

# all "long" changed to "int" so works on both 32 and 64 bit machines
#ifneq (,$(findstring _64,$(HOSTTYPE)))
#HOST64 = 1
#endif
#
ARCH = LINUX

# compilers  C and C++
# take out -m32
CC     = gcc -DOS_LINUX -pthread
CP = g++ 

MY_DIR = .

SRC_DIR     = $(MY_DIR)/src
OBJ_DIR     = $(MY_DIR)/lib
BIN_DIR     = $(MY_DIR)/bin
INSTALL_DIR =
WANT_EFENCE =

ifdef WANT_EFENCE
EFENCE  =  -lefence
endif

# includes

MIDAS_INC   = $(MIDASSYS)/include
TCL_INC     = /usr/include
LIBC_TW_INC = $(MUSR_DIR)/libc_tw/src
CAMP_INC    = $(MUSR_DIR)/camp/src
MUD_INC     = $(MUSR_DIR)/mud/src
MUDUTIL_INC = $(MUSR_DIR)/mud/util

#libraries
LIBC_DIR  = $(MUSR_DIR)/libc_tw/linux
MIDAS_LIB = $(MIDASSYS)/$(LINUX)/lib  
MIDAS_MFE = $(MIDASSYS)/$(LINUX)/lib/mfe.o  
TCL_LIB   = /usr/lib      
CAMP_LIB  = $(MUSR_DIR)/camp/linux
MUD_LIB   = $(MUSR_DIR)/mud/lib
TW_LIB    = $(MUSR_DIR)/libc_tw/linux

# 64-bit version
# Linux replaced by linux-x86_64/ in /home/midas/packages/epics/lib
EPICS_DIR   = $(EPICS_BASE)/lib/$(EPICS_HOST_ARCH)
EPICS_INC   = $(EPICS_BASE)/include
EPICS_DRV_DIR   =  $(MIDASSYS)/drivers/device
BEAMLINE_DIR    =

# BEAMLINE must be defined (e.g. in .login)
# It can be bnmr or bnqr or dev/m15/m20/m9b

ifeq ($(BEAMLINE),)
$(error Error - environment variable "BEAMLINE" is not defined)
endif

# check validity of beamline & assign TYPE as musr or bnmr
# so  bnmr or bnqr     -> type=bnmr
# and m15,m9b,m20,dev  -> type=musr

ifneq (,$(findstring bn,$(BEAMLINE)))
TYPE = bnmr
else
ifneq (,$(findstring m,$(BEAMLINE)))
TYPE = musr
else
ifneq (,$(findstring dev,$(BEAMLINE)))
TYPE = musr
else

$(error Error - environment variable "BEAMLINE" ($(BEAMLINE)) is not one of bnmr/bnqr/dev/m9b/m15/m20 (lower case))
endif

endif
endif

# bnmr/bnqr uses standard musrdaq area to access mud and camp
ifeq ($(TYPE),bnmr)
# bnmr and bnqr
BEAMLINE_DIR =../$(BEAMLINE)# include for experim.h
MY_SOURCES = $(SRC_DIR)/mdarcBnmr.c   $(SRC_DIR)/bnmr_darc.c $(SRC_DIR)/midbnmr_darc.c
MY_OBJS =  $(OBJ_DIR)/mdarcBnmr.o  $(OBJ_DIR)/bnmr_darc.o $(OBJ_DIR)/midbnmr_darc.o
# EPICS
EPICS_SRC       = ../../epics
EPICS_OBJS = $(OBJ_DIR)/connect.o $(OBJ_DIR)/mdarc_epics.o
EPICS_SOURCES = $(BEAMLINE_DIR)/connect.c  $(SRC_DIR)/mdarc_epics.c
EPICS_LIBS = $(EPICS_DIR)/libca.a $(EPICS_DIR)/libCom.a -lrt
EPICS_INCLUDES = -I $(EPICS_SRC) -I$(EPICS_INC) -I$(EPICS_DRV_DIR) -I$(EPICS_INC)/os/Linux
DRIVER        = epics_ca
MHEADER = mheaderBnmr
DRV_DIR = $(EPICS_DRV_DIR)
LINK = $(CP)
# EPICS access defined for BNMR/BNQR
#MY_DEFINES = -D EPICS_ACCESS -D RAND (rand for epics testing only)
MY_DEFINES = -D EPICS_ACCESS -D HAVE_SIS3820
#CAMP_CLIENT_NAME=camp_clnt.a  old pre-ted name
CAMP_CLIENT_NAME=libcampclnt.a
else
ifeq ($(TYPE),musr)
# dev,m9b,m15 or m20
# uses standard musrdaq area to build everything
BEAMLINE_DIR =/home/$(BEAMLINE)/($ONLINE)/musr
MY_DEFINES = -D MUSR
MY_SOURCES = $(SRC_DIR)/mdarcMusr.c  $(SRC_DIR)/bnmr_darc.c
MY_OBJS =  $(OBJ_DIR)/mdarcMusr.o  $(OBJ_DIR)/bnmr_darc.o
DRIVER = camacnul
MHEADER = mheader 
##DRV_DIR   = /midas/drivers/bus
DRV_DIR =  $(MIDASSYS)/drivers/bus
LINK = $(CC)
endif
endif
#
# CAMP direct access defined for all beamlines (needed by mheader)
DEFINES = -D CAMP_ACCESS $(MY_DEFINES)

#
# NOTE experim.h
# *** There must be no experim.h present in SRC_DIR  *** 
#    it must be present in the directory defined by 
#    EXPERIM_INC. To make experim.h, use "make" from odbedit
EXPERIM_INC = $(BEAMLINE_DIR)

#-------------------------------------------------------------------
# Hardware driver can be (camacnul, kcs2926, kcs2927, hyt1331)
#   needed for mheader that links with mfe.o

#--------------------------------------------------------------
INCLUDES = -I $(MIDAS_INC) -I $(EXPERIM_INC) -I $(SRC_DIR) -I $(LIBC_TW_INC) -I $(CAMP_INC) -I $(MUD_INC) \
	-I $(MUDUTIL_INC)
# -lm (include math library)
LIBRARIES   = $(CAMP_LIB)/$(CAMP_CLIENT_NAME) -lm -lutil -lnsl -lmidas -lc_tw -lmud -ldl -lreadline -lrt
LIBS =	 -L$(TW_LIB) -L$(MUD_LIB) -L$(CAMP_LIB) -L$(MIDAS_LIB)
FLAGS = -I. -I$(DRV_DIR) $(EPICS_INCLUDES)  $(INCLUDES)
#
# C compiler flags
DEBUG  = -g
CFLAGS = $(DEBUG) $(DEFINES) $(FLAGS)
#
# C++ compiler flags (EPICS libs is in C++)
CPFLAGS = -g $(FLAGS)
LDFLAGS =

# Targets
#
PROGS = $(BIN_DIR)/mdarc $(BIN_DIR)/cleanup $(BIN_DIR)/mheader

# Common between mdarc (BNMR and MUSR) and cleanup or mheader
COMMON_SOURCES =  $(SRC_DIR)/darc_files.c \
	$(SRC_DIR)/mdarc_subs.c  
COMMON_OBJS =  $(OBJ_DIR)/darc_files.o \
	    	$(OBJ_DIR)/mdarc_subs.o \
	$(LIBC_DIR)/c_utils.o $(LIBC_DIR)/timeval.o 
# c_utils.o and timeval.o are built using $(LIBC_DIR)Makefile

# mdarc's sources and objects
OBJS =  $(MY_OBJS) $(COMMON_OBJS) $(EPICS_OBJS)  $(OBJ_DIR)/$(DRIVER).o
SOURCES = $(MY_SOURCES) $(COMMON_SOURCES) $(EPICS_SOURCES)

# cleanup's sources and objects
OBJS_CL =  $(OBJ_DIR)/cleanup.o  $(COMMON_OBJS)
SOURCES_CL =  $(SRC_DIR)/cleanup.c  $(COMMON_SOURCES)

# mheader's sources and objects
OBJS_HE =  $(OBJ_DIR)/$(MHEADER).o $(OBJ_DIR)/$(DRIVER).o  $(OBJ_DIR)/check_camp.o $(COMMON_OBJS) $(EPICS_OBJS)
SOURCES_HE =  $(SRC_DIR)/$(MHEADER).c   $(SRC_DIR)/check_camp.c   $(DRV_DIR)/$(DRIVER).c $(COMMON_SOURCES) $(EPICS_SOURCES)



# Must clean or may use old objects compiled with(out) MUSR flag
all :   clean info $(BIN_DIR) $(OBJ_DIR) $(PROGS)

#####################################################################

#
# create library and binary directories
#
$(OBJ_DIR):
	@if [ ! -d  $(OBJ_DIR) ] ; then \
	 echo "Making directory $(OBJ_DIR)" ; \
         mkdir $(OBJ_DIR); \
	fi;

$(BIN_DIR):
	@if [ ! -d  $(BIN_DIR) ] ; then \
	 echo "Making directory $(BIN_DIR)" ; \
         mkdir $(BIN_DIR); \
	fi;

#
# applications binaries
#
# C++ for BNMR/BNQR (EPICS)  C for MUSR
$(BIN_DIR)/mdarc: $(SOURCES) $(OBJS) $(EFENCE)
	$(LINK)  -o $@ $(OBJS) $(EPICS_LIBS)  $(LIBS) $(LIBRARIES)

# C always (no epics)
$(BIN_DIR)/cleanup: $(SOURCES_CL) $(OBJS_CL) 
	$(CC)  $(CFLAGS) $(OSFLAGS) -o $@ $(OBJS_CL) $(LIBS) $(LIBRARIES)

# mheader: $(BIN_DIR)/mheader
$(BIN_DIR)/mheader:  $(MIDAS_MFE) $(SOURCES_HE) $(OBJS_HE)
	$(LINK) -o $@ $(OBJS_HE) $(EPICS_LIBS) $(MIDAS_MFE) $(LIBS) $(LIBRARIES) 


$(OBJ_DIR)/camacnul.o: $(DRV_DIR)/bus/camacnul.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $(OBJ_DIR)/camacnul.o $(DRV_DIR)/bus/camacnul.c $(LIBS)


$(OBJ_DIR)/epics_ca.o: $(EPICS_DRV_DIR)/epics_ca.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@


$(OBJ_DIR)/connect.o: $(BEAMLINE_DIR)/connect.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@
#
# library objects
#
$(OBJ_DIR)/%.o:$(SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) $(OSFLAGS) -o $@ $<

#$(OBJ_DIR)/%.o:$(SRC_DIR)/%.c
#	$(CC) -c $(CFLAGS) $(OSFLAGS) -o $@ $<




info :
	@echo " ******************************************************************";
	@echo " *   Building  mdarc, mheader and cleanup for beamline \"$(BEAMLINE)\" (type \"$(TYPE)\") * ";
	@echo " *   LINUX = \"$(LINUX)\"        *";
	@echo " *   using directory \"$(EXPERIM_INC)\" for experim.h                             *";
	@echo " *   and  $(MUSR_DIR) for camp/mud                       *";
	@echo " * beamline_dir = $(BEAMLINE_DIR)";
	@echo " * epics_sources = $(EPICS_SOURCES)";
	@echo " * epics_objs = $(EPICS_OBJS)";
	@echo " * drv_dir and driver = $(DRV_DIR) and $(DRIVER)";
	@echo " *   $(DRV_DIR)/$(DRIVER).c";
	@echo " * linker = $(LINK)";
	@echo " * Use make listing for listings in lib directory                             *"  
	@echo " ******************************************************************************";

clean :		
	rm -f $(BIN_DIR)/mdarc  $(BIN_DIR)/cleanup  $(BIN_DIR)/mheader $(OBJ_DIR)/*.o $(OBJ_DIR)/*.l


listing :
	 $(CC) -E -c $(CFLAGS) $(OSFLAGS)  $(SRC_DIR)/$(MHEADER).c > $(OBJ_DIR)/$(MHEADER).l
	 $(CC) -E -c $(CFLAGS) $(OSFLAGS)  $(SRC_DIR)/bnmr_darc.c > $(OBJ_DIR)/bnmr_darc.l
	 $(CC) -E -c $(CFLAGS) $(OSFLAGS)  $(SRC_DIR)/mdarcBnmr.c > $(OBJ_DIR)/mdarcBnmr.l
	 $(CC) -E -c $(CFLAGS) $(OSFLAGS)  $(SRC_DIR)/midbnmr_darc.c > $(OBJ_DIR)/midbnmr_darc.l

