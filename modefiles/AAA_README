AAA_README

NOTE: FOR NOW, ONLY EXPERT USERS SHOULD BE CHANGING DEFAULT FILES  !!!
------------------------------------------------------------------------

To check all modefiles, all modefiles must be listed in check_all
source check_all > c.tmp
grep  -B 2 -i "do not"  c.tmp            to see if any failed
grep -i success c.tmp                    all modefiles should be listed

-------------------------------------------------------------------------


[ All instructions refer to experiment bnmr. For bnqr, substitute bnqr for bnmr.]

This directory contains files of default settings of ODB parameters which are to be 
automatically loaded when the experimental mode (i.e. PPG Mode) is changed. 

For example, when mode "freq" is selected on the main status page of the browser, 
the file 1f_defaults.odb is to be loaded.

********************************************************************************
***  After editing these files, IT IS ESSENTIAL to run check_file.pl on them ***
***  to make sure that the key names exist in the odb.                       *** 
********************************************************************************

  The odbedit "load" command used to load the files will automatically create
  any keys that do not exist. This will almost certainly cause the DAQ system
  to fail with a Recordsize error at the start of the next run.

  If a spurious key is created by mistake, it must be deleted from the ODB 
  (using e.g. odbedit rm command ) before the run can start.

=========================================================
=   To check a file with check_file.pl, the command is  =
=                                                       =
=   cd ~bnmr/$ONLINE/modefiles                          =
=   check_file.pl  <filename>                           =
=                                                       =
=    e.g. check_file.pl  1f_defaults.odb                =
=========================================================
This perlscript (check_file.pl) invokes another perlscript, called mode_check.pl
to check the file.


To control which file of defaults gets loaded (or none):
----------------------------------------------------------
Provision has been included to load one of the following on changing mode:
   A. no default file
   B. a file of defaults
   C. a file of the user's choosing
 
Only an expert user should mess with this!

In the odb, in directory /scripts  there is a sub-directory for every supported mode.
Using odbedit in an xterm,

$ odbedit 
[local:bnmr:S]/>ls /Script/freq
cmd                             /home/bnmr/online/perl/change_mode.pl
include path                    /home/bnmr/online/perl
experiment name                 /experiment/name
select mode                     1f
mode file tag                   defaults
[local:bnmr:S]/>exit
$

The keys "select mode" and  "mode file tag" are combined to specify which file is to
be loaded when changing mode.  In this case, the file will be
~bnmr/$ONLINE/modefiles/1f_defaults.odb

To specify no file is to be loaded,  "mode file tag" is set to "none".
To specify some other file, say 1f_mytest.odb,  "mode file tag" must be set to "mytest"


To change or add/remove default settings to the file
-----------------------------------------------------
The files are in ASCII, in the format used by the odbedit "save" command, e.g. for
1f_defaults.odb

[/Equipment/FIFO_acq/Frontend/Hardware/PSM]
ALL profiles enabled = BOOL : n
one_f profile enabled = BOOL : y
three_f profile enabled = BOOL : n
five_f profile enabled = BOOL : n
fREF profile enabled = BOOL : n
quadrature modulation mode = BOOL : y
scale factor (def 181 max 255) = INT : 181

The first line is the DIRECTORY
The following lines are those KEYS in that directory that need default values.
Only those keys that require default values are specified. Any others will not be 
changed when the file is loaded.

To add keys in a second directory, add a line with the new directory, followed by
line(s) containing the key(s).
   
Edit the file, e.g.
$ cd ~bnmr/$ONLINE/modefiles
$ emacs 1f_defaults.odb

Changing/removing default settings is easy. Just edit the value, or delete the whole key.

Adding a new key or directory requires care. The safest way is to find the new 
key and/or directory in a recent odb saved file. You can create your own by 
using the odbedit command "save", in the directory of the key you wish to add.

e.g.
$ odbedit
$ cd "/equipment/fifo_acq/Frontend/hardware"
$ save temp.odb

Copy the keys you need from the file "temp.odb" into the default file.

Otherwise you can find all the information saved in the file ~bnmr/$ONLINE/bnmr/bnmr.odb, 
which is a saved version of the whole odb. 

Copy the new key and/or directory to the defaults file you are editing EXACTLY as it appears 
in the saved odb file.  Change the value as desired.

Save the file, and ALWAYS RUN check_file.pl (see above)  on it to check the 
validity of the keys.
