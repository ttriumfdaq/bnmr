#!/usr/bin/perl
use strict;

#   tunes.pl
#
#                                        Input Parameters:   
#                  include          expt   beamline     ppg_mode  tunename    action      newtunename
#                  directory                                                              (rename only)
# tunes.pl /home/bnmr/online/perl   bnmr   bnmr         2a        tunename    1/2/3/4/5   newtunename
#
#          action 1=load 2=save 3=delete 4=list  5=rename 9=abort
######### G L O B A L S #####################################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$TRUE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ( $inc_dir, $expt, $beamline, $ppg_mode, $tunename, $parameter, $newtunename ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "tunes"; # same as filename
our $outfile = "tunes.txt";
our $nparam = 6; # need 6 input parameters (7 for rename)
our $parameter_msg = "include_path,  experiment , beamline, ppg_mode, tunename, parameter";
#------------------------------------------------------
#
$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl";
require "$inc_dir/init_check.pl";


print FOUT    " tunes  starting with parameters:  \n";
print FOUT    "   Experiment = $expt;  beamline = $beamline; ppg mode = $ppg_mode   \n";
print FOUT    "   Tune name = $tunename;  parameter = $parameter \n";  

#
#          Check the parameters
#
unless ($EXPERIMENT =~ /bn[qm]r/i)  
{
    print_3($name, "FAILURE: Experiment type $EXPERIMENT not supported ",$MERROR,$DIE ) ;
}
unless ($ppg_mode) 
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print_3 ($name, "FAILURE: ppg mode not supplied. ",$MERROR,$DIE ) ;
}
unless ($ppg_mode=~/^[12]/)
{
    print_3 ($name, "Invalid PPG mode supplied ($ppg_mode)",$MERROR,1);
}

unless ($tunename)
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print_3 ($name, "FAILURE: Tune name not supplied. ",$MERROR,$DIE ) ;
}

unless ($parameter)
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print_3 ($name, "FAILURE: Parameter not supplied. ",$MERROR,$DIE ) ;
}
if($parameter==9)
{
    print_2 ($name, "parameter is 9, aborting",$CONT);
    exit;
}

unless ($parameter=~/^[12345]/)
{ 
    print_3 ($name, "Invalid parameter supplied ($parameter). ",$MERROR,$DIE ) ;
}

if($parameter==5)
{
   unless ($newtunename)
   {
       print "Invoke this perl script $name with the parameters:\n";
       print "   $parameter_msg , newtunename \n"; 
       print_3 ($name, "FAILURE: Parameter not supplied. ",$MERROR,$DIE ) ;
   }
}



# Path in ODB to input parameter area
my $online ="vmic_online";
my $tunesfilepath = "/home/$expt/$online/tunes/mode_$ppg_mode/";

print_2 ($name, "Tunes file path is $tunesfilepath",$CONT);

if ($parameter == 1)
{
    load_tune_file( $tunesfilepath, $tunename);
}
elsif ($parameter == 2)
{
    save_tune($tunesfilepath, $tunename, $ppg_mode );
}
elsif ($parameter == 3)
{
    delete_tune($tunesfilepath, $tunename, $ppg_mode );
}
elsif($parameter==4)
{
    find_tune_files($tunesfilepath, $ppg_mode);
}
elsif($parameter==5)
{
    rename_tune($tunesfilepath, $tunename, $ppg_mode, $newtunename);
}
else
{
    print_2 ($name, "illegal parameter value $parameter",$CONT);
}
exit;


sub load_tune_file($$)
{
    my  ($tunesfilepath, $tunename) = @_ ; # get the parameter(s)
    my  ($tunefile1, $tunefile2, $needpsm);
   my $status;

 
     print_2 ($name,"load_tune_file is starting with  ppg_mode $ppg_mode  and tune name $tunename",$CONT);

   $tunefile1 = $tunesfilepath.$tunename.".odb";
    print_2 ($name,"tunefile is  $tunefile1",$CONT);
    
# Check file exists
    unless (-f $tunefile1)  # includes directory
    { 
	print_2  ($name,"tune file $tunefile1 has not been found",$CONT);
	return ($FAILURE);  #No file found
    }
    
    $needpsm=0;
    unless($ppg_mode =~/^1[0cjn]/)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        print_2  ($name,"psm tunefile will be $tunefile2",$CONT);
	$needpsm=1;

	unless (-f $tunefile2)  # includes directory
	{ 
	    print_2($name,"cannot find tune file $tunefile2",$CONT);
	    #($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:last saved file from odb key $last_saved_filename has not been found " ) ;
	    #unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    return ($FAILURE); 
	}
    }


# Load tune file(s) into ODB
    print_2 ("$name","Loading tunefile $tunefile1 into odb", $CONT);
    ($status) = odb_cmd ( "load","$tunefile1" ,"","","") ;
    print_2 ($name,"after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status){  print_2($name, "loaded tune file $tunefile1",$CONT); }
    else { print_2 ($name,"$name: after odb_cmd, ANSWER=$ANSWER ",$CONT);}
    
    if($needpsm)
    {
	print_2 ("$name","Loading tunefile $tunefile2 into odb", $CONT);
	($status) = odb_cmd ( "load","$tunefile2" ,"","","") ;
	
	print_2 ($name,"after odb_cmd, COMMAND=$COMMAND ",$CONT);
	if  ($status){  print_2($name, "loaded tune file $tunefile2",$CONT); }
	else { print_2 ($name,"$name: after odb_cmd, ANSWER=$ANSWER ",$CONT);}
	
    }

    print_3 ($name,"tune \"$tunename\" has been loaded for mode $ppg_mode ",$MINFO,$CONT);
    return;
    
}

sub delete_tune($$$)
{
    my ($tunesfilepath, $tunename, $ppg_mode) = @_;  # get the parameter(s)
    my  ($tunefile1, $tunefile2, $needpsm);
    my ($ans,$status);

    print_2 ($name,"delete_tune_file is starting with  ppg_mode $ppg_mode and tune name $tunename",$CONT);
    $tunefile1 = $tunesfilepath.$tunename.".odb";
    
    print_2($name,"deleting tunefile  $tunefile1",$CONT);
    
# Check file exists
    unless (-f $tunefile1)  # includes directory
    { 
	print_3  ($name," tune file $tunefile1 has not been found. Cannot be deleted",$MINFO,$CONT ) ;
	return ($FAILURE);  #No file found
    }
    
    print "command is  rm -rf $tunefile1\n";
    $ans=`rm -rf $tunefile1`;
    if($!)
    { 
	print_3 ($name,"error from rm $tunefile1 :  $!", $MERROR,$DIE ) ;
	
    }
    else
    {print_2($name, "successfully removed file $tunefile1",$CONT);}
    
    
    $needpsm=0;
    unless($ppg_mode =~/^1[0cjn]/)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        print_2($name,"psm tunefile will be $tunefile2",$CONT);
	$needpsm=1;
	
	unless (-f $tunefile2)  # includes directory
	{ 
	    print_3  ($name," tune file $tunefile2 has not been found. Cannot be deleted",$MINFO,$CONT ) ;
	    return ($FAILURE);  #No file found   
	}
	
	
	print "command is  rm -rf $tunefile2\n";
	$ans=`rm -rf $tunefile2`;
        if($!)
        { 
	    print_3 ($name,"error from rm $tunefile2 :  $!", $MERROR,$DIE ) ; 
        }
        else
	{print_2($name, "successfully removed file $tunefile2",$CONT);}
}	
	

    print_3 ($name,"tune $tunename has been deleted for mode $ppg_mode",$MINFO,$CONT);

#  Update the array of tune names 
    print_2($name, "calling find_tune_files",$CONT);
	
    find_tune_files($tunesfilepath, $ppg_mode );
    return; # temp
    
    
    
}

sub save_tune($$$)
{
    my  ($tunesfilepath, $tunename, $ppg_mode ) = @_ ; # get the parameter(s)
    my ($tunefile1,$tunefile2, $status);
    my  $needpsm;

    print_2 ($name,"save_tune_file is starting with ppg_mode $ppg_mode and tune name $tunename",$CONT);

    $tunefile1 = $tunesfilepath.$tunename.".odb";

    print("tunefile will be $tunefile1 \n");

# Check that file does not exist
    if (-f $tunefile1)  # includes directory
    { 
	print_2  ($name," tune file $tunefile1 already exists",$CONT);
	return ($FAILURE); 
    }



    $needpsm=0;
    unless($ppg_mode =~/^1[0cjn]/)
    {
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        print("psm tunefile will be $tunefile2\n");
	$needpsm=1;
	
	if (-f $tunefile2)  # includes directory
	{ 
	    print_2  ($name,"tune file $tunefile2 already exists",$CONT);
	    return ($FAILURE); 
	}
    }
    else
    { print_2 ($name, "not saving psm for mode $ppg_mode",$CONT); }



    my $file=$tunesfilepath."temp.cmd";  # odb command file (temporary) to save parameter data

    open OUT, ">$file"  or die "cannot open file $file for writing: $! \n";
    print OUT "cd \"/Equipment/Fifo_acq/mode parameters/Mode $ppg_mode\"\n";
    print OUT "save $tunefile1\n";

    if($needpsm)
    {
	print OUT "cd \"/Equipment/Fifo_acq/frontend/hardware/psm\"\n";
        print OUT "save $tunefile2\n";
    }
    close OUT;

# Save data by executing temporary odb command file
    ($status) = odb_cmd ( "\@$file","" ,"","","") ;
    print_2 ($name,"$name: after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status){  print_2($name, "executed odb cmd file  $file",$CONT); }
    else 
    { 
        print_3 ($name,"error after executing odb command file $file, ANSWER=$ANSWER ",$MERROR,$CONT ) ;
        return ($FAILURE); 
    }

    
    print_3 ($name,"new tune $tunename has been created for mode $ppg_mode",$MINFO,$CONT);

#  Update the array of tune names ls
    find_tune_files($tunesfilepath, $ppg_mode );
    return;
}
    


sub find_tune_files($$)
{
    my  ($tunesfilepath, $ppg_mode) = @_ ; # get the parameter(s)
    my  $load_tunefile="tune_names";
    my  $tunes_path = "/tunes";
    
    my @files;
    my ($status,$len);
    my $pattern = "*.odb"; 
    
  print_2 ($name,"\nfind_tune_file is starting with ppg_mode $ppg_mode and tunesfilepath $tunesfilepath",$CONT);
    
    unless ( chdir ("$tunesfilepath"))
    {
	
        print_3 ($name,"FAILURE - cannot change directory to $tunesfilepath",$MINFO,$CONT);
	return ($FAILURE);
    }
    
    @files = glob($pattern); 
    $len = $#files; # array length  (no. files found = len+1
    
    print_2 ($name," len = $len; Number of files found = len+1",$CONT);
    print_2 ($name," files : @files",$CONT);
    if ($len == -1) 
    {
        print_3 ($name," no tune files found for ppg mode $ppg_mode ",$MINFO,$CONT);
	return($SUCCESS);
    }
    my $elem;
    my @tunes;
    
    
    foreach $elem (@files)
    {
	$elem=~s/.odb//;
	if($elem=~/_psm/){ next; }
	if($elem=~/$load_tunefile/){ next; }
	if($elem=~/^last/){ next; } # don't save "last" as a tune name
	print "elem: $elem\n";
	push @tunes, $elem;
	
    }
    
    print_2 ($name,"Tunes = @tunes ",$CONT);
    my $num_tunes = $#tunes + 1;
    print_2 ($name,"Number of tunes found is $num_tunes",$CONT);
    
    
    my $file=$tunesfilepath.$load_tunefile.".odb";  # file to be loaded into odb
    open OUT, ">$file"  or die "cannot open file $file for writing: $! \n";
    
    print OUT "[$tunes_path]\n";
    print OUT "ppg_mode = STRING : [4] $ppg_mode\n"; 
    print OUT "num_tunes = INT : $num_tunes\n";
    
    if ($num_tunes < 1)
    {   print OUT "tune_names = STRING : [32] none\n"; }
    elsif ($num_tunes < 2)
    {   print OUT "tune_names = STRING : [32] $tunes[0]\n"; }
    else
    {
	print OUT "tune_names = STRING[$num_tunes] :\n";
	foreach $elem (@tunes)
	{
	    print OUT "[32] $elem\n";
	}
    }
    close OUT;
    
# Load tune_names file into ODB
    ($status) = odb_cmd ( "load","$file" ,"","","") ;
    #print_2 ($name,"$name: after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status){  print_2($name, "loaded tune_names file $file",$CONT); }
    else { print_3 ($name,"$name:error after odb_cmd load $file, ANSWER=$ANSWER ",$MERROR,$CONT);}

    print_3 ($name,"list of saved tunes has been updated for mode $ppg_mode",$MINFO,$CONT);

    return;
}



sub rename_tune($$$$)
{
    my ($tunesfilepath, $tunename, $ppg_mode, $newtunename) = @_;  # get the parameter(s)
    my  ($tunefile1, $tunefile2, $needpsm);
    my  ($newtunefile1, $newtunefile2);
    my ($ans,$status);

    print_2 ($name,"rename_tune_file is starting with  ppg_mode $ppg_mode, tune name $tunename and new tune name $newtunename",$CONT);
    $tunefile1 = $tunesfilepath.$tunename.".odb";
    $newtunefile1 = $tunesfilepath.$newtunename.".odb";
    print_2($name,"renaming tunefile  $tunefile1 to $newtunefile1",$CONT);
    
# Check tune file exists
    unless (-f $tunefile1)  # includes directory
    { 
	print_3  ($name," tune file $tunefile1 has not been found. Cannot be renamed",$MINFO,$CONT ) ;
	return ($FAILURE);  #No file found
    }

# Check new filename does not exist
    if (-f $newtunefile1)  # includes directory
    { 
	print_3  ($name,"new tune filename $newtunefile1 already exists. Cannot rename $tunefile1",$MINFO,$CONT ) ;
	return ($FAILURE);  #New file found
    }

    $needpsm=0;
    unless($ppg_mode =~/^1[0cjn]/)
    {
        $needpsm=1;
	$tunefile2 = $tunesfilepath.$tunename."_psm.odb";   
        $newtunefile2 = $tunesfilepath.$newtunename."_psm.odb";   
        print_2($name,"renaming psm tunefile $tunefile2 to $newtunefile2",$CONT);
	$needpsm=1;
	
	unless (-f $tunefile2)  # includes directory
	{ 
	    print_3  ($name," tune file $tunefile2 has not been found. Cannot be renamed",$MINFO,$CONT ) ;
	    return ($FAILURE);  #No file found   
	}
   
	if (-f $newtunefile2)  # includes directory
	{ 
	    print_3  ($name," tune file $newtunefile2 already exists. Cannot rename $tunefile2",$MINFO,$CONT ) ;
	    return ($FAILURE);  #No file found   
	}
	
    } # end of needpsm

    print "command is  mv  $tunefile1 $newtunefile1\n";
    $ans=`mv $tunefile1 $newtunefile1`;
    if($!)
    { 
	print_3 ($name,"error from renaming $tunefile1 to $newtunefile1 :  $!", $MERROR,$DIE ) ;	
    }
    else
    { print_2($name, "successfully renamed file $tunefile1 to $newtunefile1",$CONT);}
   
    if($needpsm)
    {
	print "command is  mv $tunefile2 $newtunefile2\n";
	$ans=`mv $tunefile2 $newtunefile2`;
        if($!)
        { 
	    print_3 ($name,"error from mv $tunefile2  $newtunefile2 :  $!", $MERROR,$DIE ) ; 
        }
        else
	{print_2($name, "successfully renamed file $tunefile2 to $newtunefile2",$CONT);}
    }	
	

    print_3 ($name,"tune $tunename has been successfully renamed to $newtunename for mode $ppg_mode",$MINFO,$CONT);

#  Update the array of tune names 
    print_2($name, "calling find_tune_files",$CONT);
	
    find_tune_files($tunesfilepath, $ppg_mode );
    return; # temp
    
}
