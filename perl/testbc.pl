#!/usr/bin/perl -w

use warnings;
use strict;
use Config;   # Trap signals

my $a;
my $func="9*5**2";
my $b=eval($func);
print "b=$b\n";

#my $f1=40125000+100*x;
#my $f2=40120000-100*x;
#my $f3=40125000+3*10*x*x;
#my $f4=40120000; 
my $f1 = "20+2*x ";
my $f2 = "20-2*x ";
my $f3 = "10*x ";
my $f4 = "100 ";

my @array=($f1,$f2,$f3,$f4);
print "array= @array \n";
my $element;
# x=1 to 10, incr 2
my($start,$stop,$inc,$ans);
$start=10;
$stop=3;
$inc=-2;
print("start=$start  stop=$stop  inc=$inc \n");
if(($start == $stop) || ($inc == 0)) { print "start & stop cannot be equal or increment cannot be 0";}
if($start > $stop)
{
    if($inc > 0) { die "increment must be positive";}
}

if($start < $stop)
{
    if($inc < 0) { die "increment must be positive";}
}
my $ninc = abs( int(1+ ($stop - $start)/$inc)); # rounds down
print "ninc = $ninc\n";
if($ninc == 0 || $ninc > 20){ $ninc=20;}#  show first 20
printf("%s %3d\n","test format",$stop);
my $filename="cw_file.txt";
open FOUT, "> $filename" or die "Cannot open file $filename";
#print FOUT "$ninc    # number of increments\n";
print FOUT "# x start=$start  stop=$stop  incr=$inc   ninc=$ninc\n#\n"; 
print FOUT   "#Index IndVar             f1              f2              f3              f4  functions\n";
printf FOUT ("#  i    x     %15s %15s %15s %15s\n",$f1,$f2,$f2,$f4);

#for(my $i=$start; $i<=$stop; $i+=$inc)
my ($i,$x);
$i=$start;

for(my $i=0; $i<$ninc; $i++)
{
    $x = $start + $i * $inc;
    printf FOUT ("%4d %4d    ",$i,$x);
    for $element (@array)
    {
	$_ = $element;
	s/x/$x/g;
	$ans = eval($_);
	printf FOUT ("     %10d ",$ans);
    }
    print FOUT "\n";

}
print "Output file is $filename\n";
close FOUT;
exit;
