#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d

my ($result,$s);
my $pi=3.14159;
print "Examples:  \n";
print "   (3**2 + 4)*2 - 10 \n";
print "   5 *(log(10))\n";
print "   \$x=5; 40125000+100*\$x; \n";
print "  2 * sin(90 * \$pi/180)\n";
print " \nNote sin/cos etc. take value in radians\n\n";
print "Enter a line to eval (enter Cntrl C to exit):\n";
while (defined($s = <>)) {          # Read a line into $s
    $result = eval $s;              # Evaluate that line
    if ($@) {                       # Check for compile or run-time errors.
        print "Invalid string:\n $s";
    } else {
        print $result, "\n";
    }
}
exit;

