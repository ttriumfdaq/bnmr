#!/usr/bin/perl
use strict;

######### G L O B A L S ##################################################################################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$TRUE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################

sub find_tune_files($$);
sub print_2($$$);
sub print_3($$$$);

my $name="test_glob";	
my $ppg_mode="1f";
my $tunesfilepath = "/home/bnmr/vmic_online/tunes/mode_$ppg_mode/";
my $error_code=5;
my $savecurrenttune=  "_current"; # reserved filename to save current parameters  (used in action #8)
my $lasttune="_last"; # reserved filename to save last run's parameters

find_tune_files($tunesfilepath, $ppg_mode);
exit;

sub print_2($$$)
{
    my  ($name, $msg,$dummy) = @_ ; # get the parameter(s)
    print "$name : $msg \n";
    return;
}
sub print_3($$$$)
{
    my  ($name, $msg,$dummy1,$dummy2) = @_ ; # get the parameter(s)
    print "$name : $msg \n";
    if($dummy1 ==  $DIE)
    { die();}
    return;
}
sub find_tune_files($$)
{
    my  ($tunesfilepath, $ppg_mode) = @_ ; # get the parameter(s)
    my  $load_tunefile="tune_names";
    my  $tunes_path = "/tunes";
    
    my @files;
    my ($status,$len);
    my $pattern = "*.odb"; 
    my $namefile=$tunesfilepath.$load_tunefile.".odb";  # file to be loaded into odb
    
    my $elem;
    my $len;
    my @tunes;
    print_2 ($name,"\nfind_tune_file is starting with ppg_mode $ppg_mode and tunesfilepath $tunesfilepath",$CONT);
    
    unless ( chdir ("$tunesfilepath"))
    {
	
        print_3 ($name,"FAILURE - cannot change directory to $tunesfilepath",$MINFO,$CONT);
        set_done($error_code); # error
	return ($FAILURE);
    }
    opendir DH, "." or die "Could not open directory $tunesfilepath";
   # @files = glob($pattern);
    while($_ = readdir(DH))
    { print "$_\n"; 
      
      $elem=$_;
      unless($elem=~/odb$/){ next; }
      if($elem=~/_psm/){ next; }
	    $elem=~s/.odb//;
	   
	    if($elem=~/$load_tunefile/){ next; }

	    if($elem=~/^$lasttune/)
	    { 
		print_2 ($name, "found reserved name $lasttune",$CONT);
		next;  # don't save "_last" as a tune name (reserved)
	    }
   
	    if($elem=~/^$savecurrenttune/)
	    { 
		print_2 ($name, "found reserved name $savecurrenttune",$CONT);
		next;  # don't save  $savecurrenttune ("_current") as a tune name (temporary file)
	    }
     
	    print "*** working on elem: $elem\n";
            my $output = `ls -lt $elem.odb`;
            #print "output=$output \n";
            my @details = split / /, $output;
            my @date;
            push @date, $details[6];
            push @date, $details[5];
            push @date, $details[8];
            my $fulldate = join "",@date;
            print  "$elem $fulldate \n";
	    push @tunes, $elem;
    }
	
    print "tunes: @tunes\n";




    exit;


 
    $len = $#files; # array length  (no. files found = len+1
    
    print_2 ($name," len = $len; Number of files found = len+1",$CONT);
    print_2 ($name," files : @files",$CONT);
    if ($len == -1) 
    {   # no tune files
	print_3 ($name," no tune files found for ppg mode $ppg_mode ",$MINFO,$CONT);
	
	# open file tune_names.odb
	open OUT, ">$namefile"  or die "cannot open file $namefile for writing: $! \n";
	
	print OUT "[$tunes_path]\n";
	print OUT "ppg_mode = STRING : [4] $ppg_mode\n"; 
	print OUT "num_tunes = INT : 0\n";
    }
    else
    {   # there are tune files
	my $elem;
	my @tunes;
    
    
	foreach $elem (@files)
	{
	    $elem=~s/.odb//;
	    if($elem=~/_psm/){ next; }
	    if($elem=~/$load_tunefile/){ next; }

	    if($elem=~/^$lasttune/)
	    { 
		print_2 ($name, "found reserved name $lasttune",$CONT);
		next;  # don't save "_last" as a tune name (reserved)
	    }
   
	    if($elem=~/^$savecurrenttune/)
	    { 
		print_2 ($name, "found reserved name $savecurrenttune",$CONT);
		next;  # don't save  $savecurrenttune ("_current") as a tune name (temporary file)
	    }
	    print "elem: $elem\n";
	    push @tunes, $elem;
	    
	}
	
	print_2 ($name,"List of non-reserved Tunes = @tunes ",$CONT);
	my $num_tunes = $#tunes + 1;
	print_2 ($name,"Number of non-reserved tunes found is $num_tunes",$CONT);
	
	
	# open file tune_names.odb
	open OUT, ">$namefile"  or die "cannot open file $namefile for writing: $! \n";
	
	print OUT "[$tunes_path]\n";
	print OUT "ppg_mode = STRING : [4] $ppg_mode\n"; 
	print OUT "num_tunes = INT : $num_tunes\n";
	
	if ($num_tunes < 1)
	{   print OUT "tune_names = STRING : [32] none\n"; }
	elsif ($num_tunes < 2)
	{   print OUT "tune_names = STRING : [32] $tunes[0]\n"; }
	else
	{
	    print OUT "tune_names = STRING[$num_tunes] :\n";
	    foreach $elem (@tunes)
	    {
		print OUT "[32] $elem\n";
	    }
	}
    } # end of tune files present

    close OUT;
    
# Load tune_names file into ODB
##    ($status) = odb_cmd ( "load","$namefile" ,"","","") ;
    #print_2 ($name,"$name: after odb_cmd, COMMAND=$COMMAND ",$CONT);
##    if  ($status){  print_2($name, "loaded tune_names file $namefile",$CONT); }
##    else { print_3 ($name,"$name:error after odb_cmd load $namefile, ANSWER=$ANSWER ",$MERROR,$CONT);}

##    print_3 ($name,"list of saved tunes $namefile has been updated for mode $ppg_mode",$MINFO,$CONT);
##    set_done($done_code); # done
    return;
}

sub set_done($)
{
    return;
}



