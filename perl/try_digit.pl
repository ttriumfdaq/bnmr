#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#

use warnings;
use strict;
sub get_digits($);

my $name="try_digit";
my $bnmr_exp_num=" alarm message is blah";
my $val=" experiment number 2345\n alarm: some message";

unless ($val =~/\S/) # non-blank character
{ 
    die "send_ssh_cmd: reply is empty\n";    
}
if ($val =~ /experiment number/i)
{
    $val =~ s/experiment number//;
    $val =~ s/ *$//; #remove trailing spaces
    $val =~ s/^ *//; # and leading spaces
    #print "now val=\"$val\"\n";
}
else
{
    die  "send_ssh_cmd: illegal reply \"$val\". Could not find string \"experiment number\" \n";
}

my $num=get_digits($val);
my $bnqr_exp_num=$num+0; # a number

	
if( $bnqr_exp_num == 0)
{
    die  "send_ssh_cmd: illegal response for bnqr's experiment number\n";
} 
else
{
    print "bnqr experiment number is  $bnqr_exp_num\n";
}



$bnmr_exp_num =~ s/ *$//; #remove trailing spaces
$bnmr_exp_num  =~ s/^ *//; # and leading spaces

$num=get_digits($bnmr_exp_num);
$bnmr_exp_num=$num+0; # a number
	
if( $bnmr_exp_num == 0)
{
    die  "send_ssh_cmd: illegal response for bnmr's experiment number\n";
} 
else
{
    print "bnmr experiment number is  $bnmr_exp_num\n";
}
exit;


sub get_digits($)
{
    my $name="get_digits";
    my ($val)=@_;
    my @fields;
    my $string = $val; # original string

    if($val =~ /\D/) # any non-digit?
    {
	#print "$name: expect numerical response for experiment number ($val). Found a non-digit\n";
	# an ODB message may be appended to the response; appear to start with a <CR>
	
	@fields = split /\s+/, $val;
	$val=$fields[0];

	#print "after split, first element is \"$val\"\n";
	unless ($val =~/\S/) # non-blank character
	{ 
	    print "$name: expect numerical response for experiment number ($string). Found a non-digit\n"; 
	    return 0;
	}
  
	if($val =~ /\D/)
	{
	    print "$name: expect numerical response for experiment number (\"$string\"). Found a non-digit after split (\"$val\")\n"; 
	    return 0;
	}
	
    }
    return $val;
    
}
