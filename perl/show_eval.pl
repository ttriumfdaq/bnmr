#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# invoke this script with cmd e.g.
## previously - no y
##                  include_path     experiment  ppgmode   xstart xstop xinc      f1               f2            f3                 f4    
## show_eval.pl  /home/bnmr/online/perl  bnmr         1w      1     10    1  "11000000-75000*x" "empty" "11750000+25000*x" "12000000+75000*x" 

#                  include_path     experiment  ppgmode   xstart xstop xinc  yconst         f1         f2       f3           f4  
#  show_eval.pl  /home/bnmr/online/perl  bnmr         1w      1     10    1  11000000    "y-75000*x" "empty" "y+25000*x" "y+75000*x" 
#
# $Log: show_eval.pl,v $
#
# show_eval.pl does the same calculations as fcw.pl, but displays them in html file.
#              fcw.pl called by rf_config to calculate frequency table.
use warnings;
sub write_code($);
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
our $ONLINE; # used in check_online()
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts)
# init_check uses $inc_dir, $expt from the input parameters
# input parameters :
our ($inc_dir, $expt, $ppgmode, $xstart,$xstop, $xinc, $yconst, @farray ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "show_eval" ; # same as filename
our $outfile = "show_eval.txt"; # path will be added by file open
our $parameter_msg = "include path, experiment,  ppgmode, xstart, xstop, xinc, yconst, f1, f2, f3, f4";
our $nparam = 8;  # no. of input parameters (last is an array)
our $beamline = $expt; # beamline is not supplied. Same as $expt for bnm/qr
############################################################################
# local variables:
my ($transition, $run_state, $path, $key, $status);
my ($result,$j,$i,$k);
my $pi=3.14159;
my $hidden_path = "/custom/hidden/";
my $message;
my ($x,$y,$line,$ninc);
our @results;
my $htmlfile= "/home/".$expt."/online/custom/evaluate_frequency_table.html";
my $ans;

$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
# Init_check.pl checks:
#   one copy of this script running
#   no. of input parameters is correct
#   opens output file if above checks are passed
#
require "$inc_dir/init_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt, xtart=$xstart, xstop=$xstop, xinc=$xinc, yconst=$yconst  farray= @farray  \n";
$j=0;
for($i=0; $i<=$#farray; $i++)
 {   
     $j++;
     print FOUT " f0ch$j = \"$farray[$i]\"; \n";
 }
print FOUT "\n";

#print "Examples:  \n";
#print "   (3**2 + 4)*2 - 10 \n";
#print "   5 *(log(10))\n";
#print "   \$x=5; 40125000+100*\$x; \n";
#print "  2 * sin(90 * \$pi/180)\n";
#print " \nNote sin/cos etc. take value in radians\n\n";
#print "Enter a line to eval (enter Cntrl C to exit):\n";
#while (defined($s = <>)) {          # Read a line into $s
#    $result = eval $s;              # Evaluate that line
#    if ($@) {                       # Check for compile or run-time errors.
#        print "Invalid string:\n $s";
#    } else {
#        print $result, "\n";
#    }
#}

#

write_code(2); # set "/custom/hidden/cw eval status" to 2=perlscript started 
$ans = `rm -rf $htmlfile`;  # delete old temp file

open (FH, "> $htmlfile") or die $!;  # open html file
print "Opened $htmlfile\n";
    
print FH "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
print FH "<html><head><title>Mode 1w Frequency Table</title>\n";
print FH "\n<!-- This file created by show_eval.pl -->\n";
print FH "<script type=\"text/javascript\" src=\"mhttpd.js\">\n";
print FH "</script>\n";

print FH "<link rel=\"stylesheet\" type=\"text/css\" href=\"mhttpd.css\" > <!-- midas stylesheet (ignores title) --> \n";
print FH "<link type=\"text/css\" rel=\"stylesheet\" href=\"/CS/styles.css!\" title=\"Stylesheet\"> \n";
print FH "<link type=\"text/css\" rel=\"stylesheet\" href=\"/CS/common.css!\" title=\"Stylesheet\"> \n";

print FH "<body>\n";
print FH "<table style=\"width: 100%;\" border=\"1\" cellpadding=\"5\">\n";
print FH "<tbody>\n";
print FH "<form name=\"form1\" method=\"get\"  accept-charset=utf-8>\n";
print FH "<tr align=\"center\"><th id=title  class=\"header\" colspan=\"20\" >\n";
print FH "Frequency Table for PPG Mode: $ppgmode</th></tr>\n";
print FH "<tr align=\"center\"><th class=\"buttons\"  style=\"font-size:100%; font-weight:normal\">\n";
print FH "<input name=\"cmd\" value=\"Status\" type=\"submit\">\n";
print FH "<input name=\"cmd\" value=\"Messages\" type=\"submit\">\n";
print FH "<input type=\"button\" onClick=\"document.location.href='/CS/Parameters&'\" style=\"color:blue\" title=\"Show Run Parameters custom page\" value=\"RunParameters\"> \n";
print FH "</th></tr>\n";
print FH "</form>\n";
print FH "</table>\n";

my $datestring = localtime();
#print "Local date and time $datestring\n";
print FH "<h3><center>Last Evaluated at $datestring</center></h3>\n";
print FH "<p style=\"text-align:left; font-size:125%; font-weight:normal;\">\n";
print FH "<span style=\"font-weight:bold\">Input parameters:</span>\n";
print FH "Xstart=<span style=\"color:blue;\">$xstart</span>; &nbsp;\n";
print FH "Xstop=<span style=\"color:blue;\">$xstop</span>; &nbsp;\n";
print FH "Xinc=<span style=\"color:blue;\">$xinc</span>; \n";
print FH "Yconst=<span style=\"color:blue;\">$yconst</span>; \n";
print FH "<br><span style=\"font-weight:bold\">Input functions:</span>\n";
$j=0;
for($i=0; $i<=$#farray; $i++)
 {  
     $j++;
     print FH " <span style=\"font-weight:normal;color:navy;\">f0ch$j =</span>\n";
     print FH " <span style=\"font-style:italic;color:blue\"> \"$farray[$i]\";&nbsp; </span> \n";
 }
print FH "</p><br>\n";
 
if( $#farray < 3)
{ 
    $message= "Error - not enough input values; farray is too short";
    print FH "$message \n"; 
    odb_cmd ( "msg","$MERROR","","$name", "$message" ) ;
    goto end;
}

if($xinc == 0)
{ 
    $message="Error - xinc cannot be 0";
    print FH "$message\n";
    odb_cmd ( "msg","$MERROR","","$name", "$message" ) ;
    goto end;
}
if($xstart == $xstop)
{ 
    $message="Error - xstart cannot equal xstop=$xstop";
    print FH "$message\n" ;
    odb_cmd ( "msg","$MERROR","","$name", "$message" ) ;
    goto end;
}


$j=0;$ninc=0;

print FH "<table style=\"width: 100%;\" border=\"1\" cellpadding=\"1\" bgcolor=\"white\"> \n";
print FH '<tr><td class=\"title\" style="background-color:aliceblue;font-size:130%;font-weight:bold;" colspan=5>Evaluated Frequency Values</td></tr>';
print FH "\n";
print FH '<tr style="font-weight:bold"><td> Xvalue </td><td> f0ch1(Hz) </td><td> f0ch2(Hz) </td><td>  f0ch3(Hz) </td><td> f0ch4(Hz)</td></tr>';
print FH "\n";

$y = $yconst;  #substitute y if y is present in string  

for($x=$xstart;$x<=$xstop;$x+=$xinc)
{
    print FH "<tr><td>$x</td>\n";

    for($i=0;$i<4;$i++)
    {
	unless (  $farray[$i] eq "empty")
	{
	    $line = "x=$x; $farray[$i]";
	    #print "line=$line\n";
	    $line=~ s/x/\$x/g;
            #print "line=$line\n";
	    $line=~ s/y/$y/g;
	    #print "line=$line\n";
	    $result = eval $line;
	    if ($@) 
	    {                       # Check for compile or run-time errors.
		print FH "<td colspan=5>Invalid string:<br>$line\n";
		print FH "</td></tr></table>\n";
		close FH;
		exit;
	    } 
	    else 
	    {
		$results[$j]=$result;
		#	print "results[$j]=$results[$j]\n";
	    }
	}
	else 
	{ # empty
	    $result=0;
	    $results[$j]=0;
	}
	
	print FH "<td>$result</td>\n";
	$j++; # inc results index
	
    } # inner for $i
    print FH ("</tr>\n");
    $ninc++;
} # outer for
print FH ("<tr><td colspan=6 style=\"font-size:120%\">Number of increments: $ninc</td></tr>\n");
print FH "</table><br>\n";

end:
print FH "</body></html>\n";
close FH;
write_code(3); # set to 3 (perlscript done) or 9 (error) 
exit;

sub write_code($)
{
    my $value = shift;
    my $path = $hidden_path."cw eval status";
    my ($status,$key);
    ($status, $path, $key) = odb_cmd ( "set","$hidden_path","clicked ppgmode button","$value") ;
    
    unless ($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $hidden_path clicked ppgmode button true \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting clicked ppgmode button" ) ;
	die "$name: Failure setting $hidden_path clicked ppgmode button";
    }

    print "$name: set $path to $value\n"; 
    return; 
}

