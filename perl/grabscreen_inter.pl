#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
use warnings;
use strict;

#   input parameters 
#   if no paramters, uses $MIDAS_EXPT_NAME for elog and blank message file (default)
#   if one parameter, must be bnmr/bnqr. Can sent elog to the other experiment
#   if two parameters, must be 
#         1. bnmr/bnqr and 
#         2. filename of file containing a message to be added to the elog screengrab
#
#my ($experiment, $msg_filename) =@ARGV; #no input parameters
#my $len = $#ARGV +1; # array length
my $port='8089';
my ($HOSTNAME,$MIDAS_EXPERIMENT,$MIDAS_SERVER_HOST, $midas_experiment,$author,$subject,$system,$ans);
my ($default1,$default2,$default3);
$MIDAS_EXPERIMENT = $ENV{"MIDAS_EXPT_NAME"};
my ($experiment, $other_exp);
$HOSTNAME = $ENV{"HOSTNAME"};
unless($HOSTNAME =~/isdaq06/i)
{
    $HOSTNAME="isdaq06.triumf.ca";  # the elog daemon is running on isdaq06
}
$other_exp = "bnmr";
if ($MIDAS_EXPERIMENT =~ /bnmr/i)
{$other_exp = "bnqr";}
$experiment = $MIDAS_EXPERIMENT;

#my $message_file="~/scripts/blank.txt"; # default message file
my $message_file="temp.txt";
$default1= $MIDAS_EXPERIMENT." User";

print "  Send screengrab to elog for experiment \"$MIDAS_EXPERIMENT\" or \"$other_exp\" [$MIDAS_EXPERIMENT/$other_exp] <cr>=$MIDAS_EXPERIMENT ? ";
$ans=<STDIN>;
chomp $ans;
if($ans=~ /$other_exp/i)
{
    $experiment = $other_exp;
}


$MIDAS_EXPERIMENT = $experiment;
$MIDAS_EXPERIMENT = uc($MIDAS_EXPERIMENT);
$midas_experiment = lc($MIDAS_EXPERIMENT);
#print ("$MIDAS_EXPERIMENT, $midas_experiment = $MIDAS_EXPERIMENT, $midas_experiment\n");
print "Screengrab will go to elog for experiment:  $midas_experiment\n \n";

$default1= $midas_experiment." User";
$default2 = $midas_experiment."PicGrab";
$default3= "Picture Grab";

print "Answering <cr> to all questions will send the screengrab with default values:\n";
print "author= \"$default1\", system= \"$default2\", subject= \"$default3\" \n\n";

print "  Enter author's name [<cr> for default] ? ";
$author=<STDIN>;
chomp $author;
unless ($author)
{
    $author= $default1;
}
print "Author is: \"$author\" \n\n";


print "  Enter System  [<cr> for default] ? ";
$system=<STDIN>;
chomp $system;
unless ($system)
{
    $system= $default2;
}
print "System is: \"$system\" \n\n";


print "  Enter Subject [<cr> for default] ? ";
$subject=<STDIN>;
chomp $subject;
unless ($subject)
{
    $subject= $default3;
}
print "Subject is: \"$subject\" \n\n";
exit; #temp

print "Enter any comment to accompany screengrab - end with a blank line.\n";
#print "Opening file $message_file for write\n";
open FH, "> $message_file" or die $!;
while (1)
{
    $ans=<STDIN>;
    chomp $ans;
    unless ($ans){ last; }
    print FH "$ans\n";
}
close FH;

# temp - display temp file
#open FH, "$message_file" or die $!;
#my $linenum=1;
#while (<FH>)
#{
#    print $linenum++;
#    print ": $_";
#}


my $logbook = $MIDAS_EXPERIMENT."online";
my $pxw = $midas_experiment."2k14";


my $tempfile=`/bin/mktemp ~/images/grab.XXXXXX`;
if ($!) 
{
    die "Error making temporary image file : $! \n";
}
chomp $tempfile;
my $imgfile=$tempfile.".jpg";
#print "Importing $imgfile\n";
print "Use crosshairs for screengrab... \n";
`import $imgfile`;
  if ($!) {
	die "Error importing  file $imgfile : $! \n";
}


# elog for external elog
my $cmd=" /home/midas/packages/elog/elog -h $HOSTNAME  -p $port -l $logbook -u $midas_experiment $pxw  -a author=\"$author\" -a Type=\"ScreenGrab\" -a System=\"$system\" -a Subject=\"$subject\"   -f $imgfile -m $message_file ";


print  " command = $cmd \n";

`/home/midas/packages/elog/elog -h $HOSTNAME  -p $port -l $logbook -u $midas_experiment $pxw -a author="$author" -a Type="ScreenGrab" -a System="$system" -a Subject="$subject"   -f $imgfile -m $message_file `;

if ($!) {
    print  "Error sending command $cmd \n";
    die "Error return: $! ";
     }

$imgfile=$tempfile."*";

print "Removing $imgfile\n";
`/bin/rm $imgfile`;
if ($!) {
    die "Error removing  file $imgfile : $! \n";
}
exit;
