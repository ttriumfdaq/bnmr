#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d

use warnings;
use strict;
use Time::HiRes  qw (sleep);
# this routine cycles the log file monitor.log. 
#  monitor.log_5 is deleted, others are shifted up a number, monitor.log-> monitor.log_1
# oldest is monitor.log_5
# newest is monitor.log_1
# current is monitor.log
#

our $MONITOR_PATH = "/isdaq/data1/bnmr/mon/"; # the current year will be added to this
my $name="rename_logfile";

my $now=time();  # get present unix time 
my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now);
$year+=1900; # year since 1900
our $MONITOR_DIR = $MONITOR_PATH.$year."/";
my $logfilename="monitor";
my $logfile = $MONITOR_DIR.$logfilename.".log";
my $file;

my $i;
my @postfix = ("_1","_2","_3","_4","_5");
my $len= $#postfix ;
my $oldest_file=$logfile.$postfix[$len];

unless (-e $logfile)
{
    die "Cannot find $logfile\n";
}

if (-e $oldest_file)
{
    print "found $oldest_file; removing it\n";
    `rm  $oldest_file`;
    if( $!)
    {
	die  "$name: failure deleting file  $oldest_file  : $!\n";
    } 
}

while ($len >= 0)
{
    $oldest_file = $logfile.$postfix[$len];
    if (-e $oldest_file)
    {
	print "found $oldest_file; renaming it to  $logfile$postfix[$len+1]\n";
	`mv $oldest_file  $logfile$postfix[$len+1]`;
	if( $!)
	{
	    die  "$name: failure moving file  $oldest_file to $logfile$postfix[$len+1] : $!\n";
	} 
    }
    else
    {
	print "did not find $oldest_file\n";
    }
    $len--;
}
$i=3;
while ($i > 0)
{
    
    if (`/usr/sbin/lsof $logfile`)
    {
	print "$logfile is open (count $i)... sleeping 2s\n";
	sleep(2); # sleep 2s 
    }
    else
    {
	`mv $logfile $logfile$postfix[0]`;
	if( $!)
	{
	    die  "$name: failure renaming file  $logfile to  $logfile$postfix[0] : $!\n";
	} 
	print "renamed $logfile to  $logfile$postfix[0]\n";
	last;
    }
   
    $i--;
}    
exit;

sub rename_log_file()
{
    # time to rename the log file
    return;
}
