
//--------------------------------------------------------
int build_iq_table_psm3_unfinished(char *ppg_mode, char *channel_name)
//----------------------------------------------------
{
  /* build iq table for the psmIII; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  my_moduln_mode[16];
  char  str[256];
  char  astr[256];
  BOOL  I_zero; // temp flag
  INT  i,j,q,q0;
  INT Nf;
  

  char *moduln_modes[]={"ln-sech","hermite","wurst","unknown"};
  INT moduln_index; // index into moduln_modes
  INT my_debug=FALSE;

  const INT   Ncmx=4095; // value for PSMIII  
  const INT   Niq_min=1024;
  const INT   Niq_max=2048;
  const double max_mod_pulse = 419.32; // ms  Ncmax *2048 *50ns
  const double min_mod_pulse = 0.1024; // ms  1 *2048 *50ns

  INT n, Niq, Nwurst;
  double dNiq;
  
  double d_nu,d_nu_khz;
  double b;
  INT I,Q,f0_khz;
  float f1_khz; // Li

  // temp variables
  double x;
  
  double d_nu_min,d_nu_max;
  double factor;
  INT Nc1;
  double Tp,Tpc,MaxTpc,MinTpc,dNc1;
  double Tp_max,Tp_min;
  float phi, phi_radians;

  INT channel_index;

  BOOL set_const_i_file,  set_const_q_file;
  INT const_i, const_q;

  if(strncmp(ppg_mode,"2w",2)!=0)
    {
        cm_msg(MERROR,"build_iq_table_psm3_2w","ppg_mode must be 2w (not %s) for this routine. Use  build_iq_table_psm3  "
	       ,ppg_mode);
	return -1;
    }

  cm_msg(MINFO,"build_iq_table_psm3_2w","starting with channel %s  ",channel_name);
  if(get_channel_index(channel_name,&channel_index) != SUCCESS)
  {
    printf("build_iq_table_psm3_2w  : illegal channel_name %s\n",channel_name);
    cm_msg(MERROR,"build_iq_table_psm3_2w","illegal channel_name %s  ",channel_name);
    return -1;
  }
  printf("\n  build_iq_table_psm3_2w : working on channel_name \"%s\" (channel_index %d) (PSM III) \n",channel_name,channel_index);
  
  switch (channel_index)
    {
    case 0:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch1.iq_modulation.const_q__511__q__512_;
      phi=0;
      break;

    case 1:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch2.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch2.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch2.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch2.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f0ch2.iq_modulation.phase_correction__degrees_;
      break;

   case 2:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch3.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch3.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch3.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch3.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f0ch3.iq_modulation.phase_correction__degrees_;
      break;

   case 3:
      strncpy( my_moduln_mode, ppg.hardware.psm.f0ch4.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch4.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch4.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch4.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f0ch4.iq_modulation.phase_correction__degrees_;
      break;

    case 4:
      strncpy( my_moduln_mode, ppg.hardware.psm.f1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f1.iq_modulation.const_q__511__q__512_;
      phi =  ppg.hardware.psm.f1.iq_modulation.phase_correction__degrees_;
      break;
    default:
      printf("build_iq_table_psm3_2w : Error: channel_name %d i.e. \"%s\" is not supported\n",channel_index,channel_name);
      cm_msg(MERROR,"build_iq_table_psm3_2w","Error: channel_name %d i.e. \"%s\" is not supported ",channel_index,channel_name);
      return -1;
    }  

  f0_khz= ppg.hardware.psm.idle_freq__hz_ / 1000;
  f1_khz = ppg.input.e2w_li_precession_f1__khz_;
  q0 = ppg.input.e2w_q0__default_5_;
  Tp = (double)ppg.input.e2w_tp__ms_; //ms
  d_nu_khz = d_nu * pow(10,-3); // convert d_nu to khz
  Nf = ppg.input.e2w_freq_resolution_nf;
  Nwurst =  ppg.input.e2w_n__40_or_80_;

  phi_radians = phi*2*M_PI/360;

  printf("build_iq_table_psm3_2w: input params f0=%d khz f1=%f khz q0=%d Tp=%f ms d_nu_khz=%d Nf=%d\n", 
	 f0_khz, f1_khz, q0,Tp,d_nu_khz,Nf);

  /* Check the modulation mode (ln-sech or Hermite Wurst) */   
  sprintf( str,"%s", my_moduln_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen( str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  
  
  if (strncmp( str,moduln_modes[0],2) == 0)
    {
      moduln_index=0 ;
      printf("build_iq_table_psm3_2w: detected ln-sech modulation mode\n");
      b = 5.3;
      Tp_min = ( q0* b/(2 * M_PI))  * (d_nu_khz /(f1_khz * f1_khz)  ); //ms
      d_nu_min = 0.0512 * 2*M_PI* (f1_khz*f1_khz)/(q0*b); // kHz    minimum bandwidth
      d_nu_max = 419.4304*2*M_PI* (f1_khz*f1_khz)/(q0*b); // kHz  maximum bandwidth
    }
  else if  (strncmp( str,moduln_modes[1],2) == 0)
    {
      moduln_index = 1;
      printf("build_iq_table_psm3_2w: detected Hermite modulation mode\n");
      b = 2.5;
      Tp_min = 4.69/d_nu_khz;
      d_nu_min = 0.01118; // kHz  minimum bandwidth
      d_nu_max = 91.6; // kHz     maximum bandwidth
      
    }
  else if (strncmp(str,moduln_modes[2],2) == 0)
    {
      moduln_index = 2;
      printf("build_iq_table_psm3_2w: detected Wurst modulation mode\n");
      if(strncmp(ppg_mode,"2w",2)!=0)
	{
	  cm_msg(MINFO,"build_iq_table_psm3_2w","Wurst modulation mode is only supported by PPG Mode 2w");
	  printf("build_iq_table_psm3_2w: Wurst modulation mode is only supported by PPG Mode 2w");
	  return -1;
		   
	}
      b = 1;
      // assuming f1= 0.63 kHz; larger values will give higher max, smaller values a lower min
      
      Tp_min = ( q0* b/(2 * M_PI))  * (d_nu_khz /(f1_khz * f1_khz)  ); //ms
      d_nu_min = 0.0512 * 2*M_PI* (f1_khz*f1_khz)/q0; // kHz    minimum bandwidth
      d_nu_max = 419.4304*2*M_PI* (f1_khz*f1_khz)/q0; // kHz  maximum bandwidth
    }
  else
    {
      printf("build_iq_table_psm3_2w: Illegal modulation mode \"%s\"; only modes \"%s\", \"%s\" and  \"%s\" are presently supported \n",
	     str,  moduln_modes[0],  moduln_modes[1],  moduln_modes[2]);
      cm_msg(MERROR,"build_iq_table_psm3_2w", "Illegal modulation mode \"%s\"; only  modes \"%s\", \"%s\" and  \"%s\"are presently supported ",
	     str,moduln_modes[0],  moduln_modes[1],  moduln_modes[2] );
      return -1;
    }
  /* Ncmx  is written into ../output/psm by frontend
     We got the record for "output" */
  
  // if(debug) 
  printf("Ncmx from output record in odb is =%d\n", 
	 ppg.output.psm.max_cycles_iq_pairs__ncmx_); // note one value for all channels

  if(  ppg.output.psm.max_cycles_iq_pairs__ncmx_ != Ncmx)
    {
      cm_msg(MINFO,"build_iq_table_psm3_2w","Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Using Ncmx=%d",
	     ppg.output.psm.max_cycles_iq_pairs__ncmx_, Ncmx);
      
      printf("build_iq_table_psm3_2w: Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Expect %d. Continuing with Ncmx=%d\n",  ppg.output.psm.max_cycles_iq_pairs__ncmx_,Ncmx,Ncmx);
      
    }	


  if(Tp < Tp_min)
    {
      printf("build_iq_table_psm3_2w: calculated Tp_min= %f ms which is less than input value of Tp= %f ms\n",
	     Tp_min,Tp);
      return -1;
    }

  //  if (my_debug)
    {
      printf(" Modulation mode = \"%s\" \n", moduln_modes[moduln_index]);
      printf(" bandwidth (d_nu) = %lf Hz or %lf kHz\n",d_nu,d_nu_khz);
      printf(" b=%f; Ncmx = %d\n",b,Ncmx);
      printf(" d_nu_min = %.3f kHz  d_nu_max = %.3f   kHz\n",d_nu_min,d_nu_max);
    }

  /* check d_nu_khz is in range */

  if( (d_nu_khz < d_nu_min)  || (d_nu_khz > d_nu_max) )
    {
      cm_msg(MERROR, "build_iq_table_psm3_2w"," requested bandwidth (%.0fHz) is out of range for modulation mode  \"%s\" ",
	     d_nu,moduln_modes[moduln_index]);
	    
      cm_msg(MERROR, "build_iq_table_psm3_2w","bandwidth must be between %f and %f Hz",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3_2w : bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3_2w : requested bandwidth (%.0fHz) is out of range\n",d_nu);
      return -1;
    }
  
 




  //  Nc1= (INT) (0.5 + (Tp/(50*exp(-9)*2048)) ); // round up to nearest integer
  //  Niq =  (INT) (0.5 + (Tp/(50*exp(-9) * Nc1)));
  double f1,f2;
  // Tp is in ms 
  factor=  Tp/(50*pow(10,-6)*2048) ;
  Nc1 = (INT)(factor + 1); // next highest integer
  factor = Tp/(50*pow(10,-6) * Nc1);
  printf("Nc+1=%lf, rounded up Nc+1 = %d\n",factor, Nc1);
  Niq = (INT)(factor + 1); // next highest integer
  printf("Niq =%lf, rounded up Niq=%d\n",factor,Niq);


  Tpc = 50*pow(10,-6) * Nc1 * Niq; // ms
  printf("Tpc = %lf\n",Tpc);
  MaxTpc =  (4096 * 50 * pow(10,-6) * 2048) ; //=  419.4304 ms  
  MinTpc =  (1 * 50 * 1024 * pow(10,-6) ); // = 0.5012 ms
  printf("MinTpc=%fms MaxTpc=%fms\n",MinTpc,MaxTpc);
  if (Tpc < MinTpc )
    {
      printf("build_iq_table_psm3_2w: calculated Tpc (%f)ms is less than minimum value (%f)ms\n",Tpc,MinTpc);
      cm_msg(MERROR,"build_iq_table_psm3_2w","calculated Tpc (%f)ms is less than minimum value (%f)ms",Tpc,MinTpc);
    }
  else if (Tpc >= MaxTpc)
    {
      printf("build_iq_table_psm3_2w: calculated Tpc (%f)ms is greater than maximum value (%f)ms\n",Tpc,MaxTpc);
      cm_msg(MERROR,"build_iq_table_psm3_2w"," calculated Tpc (%f)ms is greater than maximum value (%f)ms",Tpc,MaxTpc);
    }
  if((Tpc < MinTpc) || (Tpc >= MaxTpc))
    {
      cm_msg(MERROR,"build_iq_table_psm3_2w", "Calculated Tpc (%f) is out of range",Tpc);
      printf("build_iq_table_psm3_2w: Calculated Tpc (%f) is out of range",Tpc);
      return -1;
    }

  if(Niq < Niq_min || Niq > Niq_max )
    {
      cm_msg(MERROR,"build_iq_table_psm3_2w", "Calculated Niq (%d) is out of range  %d < Niq < %d  ",Niq,Niq_min,Niq_max);
      printf("build_iq_table_psm3_2w: Calculated Niq (%d) is out of range  %d < Niq < %d \n ",Niq,Niq_min,Niq_max);
   
      return -1;
    }


  ppg.output.psm.num_cycles_iq_pairs__nc_[channel_index] = Nc1;
  ppg.output.psm.num_iq_pairs__niq_[channel_index] = Niq;

  dNc1   = (double) Nc1;
  dNiq = (double) Niq;

 
  printf("iq filename will be : %s%s_iq_%s.psm\n",ppg.input.cfg_path,ppg_mode,channel_name);
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,channel_name); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,channel_name); // debug file

  //if(my_debug)
    printf("build_iq_table_psm3_2w : about to open IQ table DEBUG file %s\n",str);

 

  /* If setting constant values, check they  are reasonable */
  I_zero=FALSE;
  printf("build_iq_table_psm3_2w : set_const_i_file = %d and set_const_q_file= %d \n",set_const_i_file,set_const_q_file );
  if(set_const_i_file)
    {
      if ( const_i == 0)
	{
	  I_zero=TRUE;
	  if(moduln_index ==1) // Hermite: all Q values are zero
	    {
	      cm_msg(MERROR,"build_iq_table_psm3_2w ",
		     "Modulating with constant I=0 will result in no output for Hermite since Hermite Q values are also zero");
	      printf("build_iq_table_psm3_2w : Constant I=0 not allowed for Hermite since Q values are zero (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       const_i);
      
    }
  
  if(set_const_q_file)
    {    
      if(moduln_index == 1  )
	{  // Hermite; all Q values are zero
	  cm_msg(MINFO,"build_iq_table_psm3_2w ",
		 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
		 const_q);
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( const_q ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"build_iq_table_psm3_2w ",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("build_iq_table_psm3_2w: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     const_q);
      
    }
  printf("build_iq_table_psm3_2w: calculated num IQ pairs=%d; pulsewidth Tpc =%f ms dwell_time=%f \n",
	      Niq,  Tpc, (float)(Tpc/Nf));

  dbgfile = fopen(astr,"w");
  if(dbgfile)
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Requested bandwidth(Hz)=%f\n",
	      d_nu);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tpc =%f ms dwell_time=%f \n",
	      Niq,  Tpc, (float)(Tpc/Nf));

      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);
 

  printf("build_iq_table_psm3_2w : about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into \"%s\" channel_name\n",channel_name);

  iqfile = fopen(str,"w");
  if (iqfile)    
    {
      n=1; 
      do
	{ 
	  double di,dq;
	 
	  /* ln-sech */
	  if(moduln_index == 0)
	    {  // sech = 1/cosh
	      x=b*2*(2*n-Niq-1)/(2*(Niq-1));
	      double sech_x = 1/cosh(x);
	      di = 511 * (1.01*sech_x-0.01)*cos(d_nu_khz*2*(M_PI/(4*5.3))*Tpc*log(sech_x)+ phi_radians);
	      dq = 511 * (1.01*sech_x-0.01)*sin(d_nu_khz*2*(M_PI/(4*5.3))*Tpc*log(sech_x)+ phi_radians);
	    }
	    
	  // Hermite
	  else if (moduln_index == 1)
	    {   
	      x=b*2*(2*n-Niq-1)/(2*(Niq-1));
	      di = 511 *(0.99* (1-pow(x,2))+0.01) * exp(-(pow(x,2)));
	      dq = 0;
	    }
	    // Wurst
	  else if (moduln_index == 2)
	    {   
	      x=b*2*(2*n-Niq-1)/(2*(Niq-1));
	      di = 511 * (1-pow(fabs(sin(M_PI*x/2)),Nwurst))*cos(d_nu_khz*2*(M_PI/8)*Tpc*x*x)+ phi_radians);
	      dq = 511 * (1-pow(fabs(sin(M_PI*x/2)),Nwurst))*sin(d_nu_khz*2*(M_PI/8)*Tpc*x*x)+ phi_radians);
	       
	    }

	  if(set_const_i_file)
	    I=const_i;
	  else	
	    {    
	      I = (int) (di + 0.5);  // closest integer

	      // Check I value when N=Niq/2
	      if(n==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm3_2w : Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	    i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	
	    //i= TwosComp_convert(I,0);
	    //printf("*** I=%d  after conversion i=%d 0x%x\n",I,i,i);

	  if(set_const_q_file)
	    Q=const_q;
	  else
	    Q = (int)(dq+0.5); // closest integer
	   

	    q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */
	    //q= TwosComp_convert(Q,0);
	    //printf("*** Q=%d  after conversion q=%d 0x%x\n",Q,q,q);
	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g \n",
		   n,I,Q,i,q,di,dq);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  n++;  
	}
      while ( n <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"build_iq_table_psm3_2w","Error opening iq file %s  ",str);
      printf("build_iq_table_psm3_2w : Error opening iq file %s \n",str);
      return -1;
    }
}
