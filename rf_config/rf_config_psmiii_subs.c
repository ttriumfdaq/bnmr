// TEMPORARY - these routines included in rf_config

#ifdef HAVE_PSMIII
/*------------------------------------------------------------------*/
char *get_channel(INT n)
{
#ifdef HAVE_PSMIII
  char *channel_name[]={"f0ch1","f0ch2","f0ch3","f0ch4","f1","all","unknown"};
 
  return (n<0 || n>MAX_CHANNELS) ? channel_name[4] : channel_name[n];
#endif // HAVE_PSMIII
  return "unknown";
}
/*------------------------------------------------------------------*/
INT check_psm_quad(char *ppg_mode)
{
  // called from tr_prestart
  INT n,status;
  char channel_name[5];
  BOOL quad,load_iq_file;
  //char str[80];

  /* PSMIII 
        Single Tone mode : modulated with IQ pair
        Quadrature mode:   load an IQ file, build the I,Q pairs table
  */
  for (channel_index=0; channel_index< MAX_CHANNELS; channel_index++) // cycle through channels 
    {
      quad=0;
      sprintf(channel_name, "%s", get_channel(channel_index));
      switch (channel_index)
	{
	case 0:
	  if(ppg.hardware.psm.f0ch1.channel_enabled) // if f0 ch1  is enabled
	    {
              quad = ppg.hardware.psm.f0ch1.quadrature_modulation_mode;
	      if(quad)
                 load_iq_file =  ppg.hardware.psm.f0ch1.iq_modulation/load_i_q_pairs_file;
	      else
		load_iq_file = 0; // single tone mode - will be modulated by a single i,q pair
              break;
	    }


	case 1:
	  if(ppg.hardware.psm.f0ch2.channel_enabled) // if f0 ch2 is enabled
	    {
              quad = ppg.hardware.psm.f0ch2.quadrature_modulation_mode;
	      if(quad)
                 load_iq_file =  ppg.hardware.psm.f0ch2.iq_modulation/load_i_q_pairs_file;
	      else
		load_iq_file = 0; // single tone mode - will be modulated by a single i,q pair
  
              break;
	    }
	 
	case 2:
	  if(ppg.hardware.psm.f0ch3.channel_enabled) // if f0 ch3 is enabled
	    {
              quad = ppg.hardware.psm.f0ch3.quadrature_modulation_mode;
	      if(quad)
                 load_iq_file =  ppg.hardware.psm.f0ch3.iq_modulation/load_i_q_pairs_file;
	      else
		load_iq_file = 0; // single tone mode - will be modulated by a single i,q pair
  
              break;
	    }

	case 3:
	  if(ppg.hardware.psm.f0ch4.channel_enabled) // if f0 ch4 is enabled
	    {
              quad = ppg.hardware.psm.f0ch4.quadrature_modulation_mode;
	      if(quad)
                 load_iq_file =  ppg.hardware.psm.f0ch4.iq_modulation/load_i_q_pairs_file;
	      else
		load_iq_file = 0; // single tone mode - will be modulated by a single i,q pair
  
              break;
	    }
	
	case 4:
	  if(ppg.hardware.psm.f1.channel_enabled) // if channel f1  is enabled
	    {
              quad = ppg.hardware.psm.f1.quadrature_modulation_mode;
	      if(quad)
                 load_iq_file =  ppg.hardware.psm.f1.iq_modulation/load_i_q_pairs_file;
	      else
		load_iq_file = 0; // single tone mode - will be modulated by a single i,q pair  
              break;
	    }
	 
	
	  default:
	    printf("check_psm_quad: channel %s (index %d)  is not enabled\n",channel_name,channel_index);
             goto next;
	}
      printf("check_psm_quad: working on channel %s \n",channel_name);
      if(quad)
	{  // quad mode is enabled 
	      
	  printf("check_psm_quad: quad mode is selected for channel \"%s\" (channel_index=%d) in  ppg_mode=%s\n",
		 channel_name,channel_index ,ppg_mode);


	  // for 2s  apparently
	  if(strncmp(ppg_mode,"2s",2)==0)
	    {
	      ppg.output.psm.num_cycles_iq_pairs__nc_[channel_index] =1; // buffer factor Nc
              cm_msg(MINFO,"rf_config","*** mode 2s writing  ppg.output.psm.num_cycles_iq_pairs__nc_ =1 for channel %d",
		     channel_index);
	    }
	  if(load_iq_file)
	    {
	      printf("check_psm_quad: calling build_iq_table_psm3 for channel  \"%s\" \n",channel_name); 
	      status =  build_iq_table_psm3(ppg_mode, channel_name);
	      if ( status == -1 )
		{
		  cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm2 for channel  \"%s\" ",
			 channel_name);
		  return status;
		}
	    } // end of build table
	} // end of quad mode for channel channel_index
      else
	printf("check_psm_quad: quad mode is NOT selected for channel %s\n",channel_name);
    next: continue;   
    } // end of for loop on all channels
  printf("check_psm_quad: returning 1 (success)\n");
  return SUCCESS;  // SUCCESS =1
}


INT check_psm_channel(void)
{
  // also checks gate ( check_psm_gate)
  INT amplitude_flag;
  printf("***   check_psm_channel: starting\n");
  /* called from check_psm_quad */
  
  amplitude_flag=0;
  /* check input parameters:  at least one channel is enabled, it has amplitude and fp gate enabled > 0 */
 
  if(ppg.hardware.psm.f0ch1.channel_enabled  && ppg.hardware.psm.f0ch1.scale_factor__def_181_max_255_ > 0 
     && ppg.hardware.psm.f0ch1.gate_control.gate_control == GATE_NORMAL_MODE)
    return SUCCESS;
  if(ppg.hardware.psm.f0ch2.channel_enabled && ppg.hardware.psm.f0ch2.scale_factor__def_181_max_255_ > 0
      && ppg.hardware.psm.f0ch2.gate_control.gate_control == GATE_NORMAL_MODE)
    return SUCCESS;
  if(ppg.hardware.psm.f0ch3.channel_enabled && ppg.hardware.psm.f0ch3.scale_factor__def_181_max_255_ > 0
     && ppg.hardware.psm.f0ch3.gate_control.gate_control == GATE_NORMAL_MODE )
    return SUCCESS;
  if(ppg.hardware.psm.f0ch4.channel_enabled && ppg.hardware.psm.f0ch4.scale_factor__def_181_max_255_ > 0
      && ppg.hardware.psm.f0ch4.gate_control.gate_control == GATE_NORMAL_MODE)
    return SUCCESS;
  if(ppg.hardware.psm.f1.channel_enabled && ppg.hardware.psm.f1.scale_factor__def_181_max_255_ > 0
     && ppg.hardware.psm.f1.gate_control.gate_control == GATE_NORMAL_MODE )
    return SUCCESS;

    
  printf("check_psm_channel: At least one PSM channel must be enabled, front panel gate selected and scale factor (amplitude) set > 0 \n");
  cm_msg(MERROR,"check_psm_channel",
	     "No PSM channel is enabled (i.e. enabled, front panel gates selected & scale factor (amplitude) set > 0) ");
  return -1; // FAILURE
}

INT  build_iq_table_psm3(char *ppg_mode, char *channel_name)
{
}
#endif PSMIII

