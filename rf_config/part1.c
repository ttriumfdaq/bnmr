
  freq_inc =  (DWORD)ppg.input.frequency_increment__hz_;
  if(freq_inc == 0)
    {
      cm_msg(MERROR,"build_f_table_psm","frequency increment cannot be 0");
      printf("build_f_table_psm: frequency increment cannot be 0 \n");
      return -1;
    }
 // use labs for long argument
 if ( labs(freq_inc) > labs( ppg.input.frequency_stop__hz_ - ppg.input.frequency_start__hz_ ))
   { 
     cm_msg(MERROR,"build_f_table_psm","frequency increment (%ld) is too large",freq_inc);
     printf("build_f_table_psm: frequency increment (%ld) is too large \n",freq_inc);
     return -1;
   }


  /* Modify so this works with start < stop value */
  if( ((freq_inc > 0) && 
       ( ppg.input.frequency_stop__hz_ <  ppg.input.frequency_start__hz_)) ||
      ((freq_inc < 0) && 
       ( ppg.input.frequency_start__hz_ <  ppg.input.frequency_start__hz_)) )
	    freq_inc *= -1;

 printf("build_f_table_psm: freq start = %u stop = %u incr = %ld\n",
	(DWORD)ppg.input.frequency_start__hz_, 
	(DWORD)ppg.input.frequency_stop__hz_,
	freq_inc);

 // use labs for long argument
 f_ninc = (  labs( (DWORD)ppg.input.frequency_stop__hz_ -  (DWORD)ppg.input.frequency_start__hz_)
	     / labs(freq_inc) ) +1;

 printf("no. increments %d  freq_inc = %ld\n",f_ninc,freq_inc);
 if( f_ninc < 1)
    { 
      cm_msg(MERROR,"build_f_table_psm","too few frequency increments");
      printf("build_f_table_psm: too few frequency increments\n");
      return -1;
    }


  /*Create frequency table */
  
  i = 0;
  freq  = (double) ppg.input.frequency_start__hz_;
  ftemp = freq/finc +0.5 ;// round up ftemp is type double
  freqx = (unsigned long int)ftemp; // freq start value in hex

  //  fstep =  (double) ppg.input.frequency_increment__hz_;
  fstep = (double) freq_inc; // direction may have been reversed above
  ftemp = fstep/finc +0.5 ;// round up
  fstepx = (unsigned long int)ftemp; // step value in hex
  
  fstop = (double) ppg.input.frequency_stop__hz_;
  ftemp = fstop/finc +0.5 ;// round up
  fstopx = (long int)ftemp; // stop value in hex
  
  /* calculate the number of steps needed */
  j = 1 + (int)(fstop-freq)/fstep;
  
  printf("start freq=%.1fHz; step by=%.1fHz  stop at %.1fHz\n",freq,fstep,fstop);
  printf("freqx = %lu (0x%lx) ;  fstepx = %lu (0x%lx); fstopx = %lu (0x%lx)\n",
	 freqx,freqx,fstepx,fstepx,fstopx,fstopx);

  //printf("Number of steps needed %d should agree with f_ninc=%d\n",j,f_ninc);
  
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,".psm");
  if(strncmp("1",ppg_mode,1)==0) /* look for type 1 */
    hz=TRUE; /* values in table should be in hz */
  else
    hz = FALSE;

  printf("build_f_table_psm: about to open frequency table file %s\n",str);
  if(hz)
    printf("   freq values will be in Hz\n");
  else 
    printf("   freq values will be in Hex format\n");

  if ((freqfile = fopen(str,"w")) != NULL)
    {
      for(i=0; i<j; i++)
	{
	  /* print some extra stuff for debugging including freq in Hz */
	  if(my_debug)
	    {
	      if(i==0)
		{
		  printf(" 0x%lx  (%uHz) %d; stop=%uHz; step=%uHz; %d steps \n",
			 freqx,(unsigned int)freq, i,
			 (unsigned int)ppg.input.frequency_stop__hz_,
			 (unsigned int)freq_inc,j);
		}
	      else
		printf("0x%x  (%uHz) %d \n",
		       (unsigned int)freqx,(unsigned int)freq,i);
	    }

	  if(hz)
	    sprintf(str,"%lu",(unsigned long int)freq);
	  else
	    sprintf(str,"%lx",freqx);
	  fputs(str,freqfile);
          fputs("\n",freqfile);
	  freqx = freqx + fstepx;
	  freq = (double)freqx  * finc +0.5; // debugging, convert to Hz       
	}
      fclose(freqfile);
      ppg.output.num_frequency_steps =  j;
      ftemp = (double)(freqx - fstepx) ; // calculate last value
      freq = ftemp * finc + 0.5 ; // round up
    
      printf("num freq steps = %d\n",j);    
      ppg.output.frequency_stop__hz_ =  (DWORD)freq;
      printf("Freq stop in hz = %d\n",ppg.input.frequency_stop__hz_);
      printf("Actual freq stop will be %f or %lu\n",freq,(unsigned long int)freq);
      return j;
    }
  else
    {
      cm_msg(MERROR,"Build_f_table_psm","Error opening frequency file %s ",str);
      printf("Build_f_table_psm: Error opening frequency file %s ",str);
      return -1;
    }
}
