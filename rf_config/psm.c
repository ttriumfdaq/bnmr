#else  /* standard PSM */

//--------------------------------------------------------
int build_iq_table_psm(char *ppg_mode, char *profile )
//----------------------------------------------------
{
  /* build an iq table for the psm; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  str[256];
  char  astr[256];
  char  fstr[12];
  BOOL I_zero;

  INT status, size, j,i,q;

  INT my_debug=FALSE;
  INT N, Ncic,Niq;
  double dNcic, dNtiq, dNiq, dn;
  double dNc, dNtiqtemp;

  double d_nu, d_w_req, dw;
  double a,phi,tmp,tmp2;
  INT I,Q;
  double Tp;

  // temp variables
  double x,y,z;
  double d_const;
  int  actual_bandwidth__hz_;

  double d_w_max,d_w_min,d_nu_min,d_nu_max;
  double alpha, A; /* modulation params */
  double d_Ntiqtemp;
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */

  INT Ntiqtemp, Ntiq, Ncmx, Nc;
  INT pro_index;

  if(get_index=(profile,&pro_index) != SUCCESS)
  {
    cm_msg(MERROR,"build_iq_table_psm  :","illegal profile %s",profile);
    return -1;
  }
  printf("\n  build_iq_table_psm: working on profile \"%s\" (profile index %d) (PSM II) \n",profile,pro_index);
  
  /* Check the modulation mode (ln-sech or Hermite) */    
  sprintf(str,"%s",ppg.hardware.psm.iq_modulation.moduln_mode__ln_sech_hermite_[pro_index]);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen(str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  

  if (strncmp(str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm: detected ln-sech modulation mode\n");
      alpha = ppg.output.psm.amplitude_mod_param__alpha_[pro_index] = 5;
      A     = ppg.output.psm.linewidth_mod_param__a_[pro_index]     = 0.1;
    }
  else if (strncmp(str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm: detected Hermite modulation mode\n");
      alpha =  ppg.output.psm.amplitude_mod_param__alpha_[pro_index] = 2.2;
      A =      ppg.output.psm.linewidth_mod_param__a_[pro_index]     = 0.39714;
    }
  else
    {
      printf("build_iq_table_psm: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported\n",
	     ppg.hardware.psm.iq_modulation.moduln_mode__ln_sech_hermite_[pro_index]);
      return -1;
    }


  size = sizeof(Ncmx);


  /* Ncmx  is a hardware param read from PSM by frontend_init (febnmr) and written into odb
     in the output directory, so that rf_config can use it.  We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", 
		   ppg.output.psm.max_cycles_iq_pairs__ncmx_); // Ncmx note one value for all profiles
  Ncmx =  ppg.output.psm.max_cycles_iq_pairs__ncmx_; // note one value for all profiles
  
  if(Ncmx < 1 || Ncmx > 32)
    {
      printf("build_iq_table_psm: Illegal value of param Ncmx read from odb (%d), (0<Ncmx<32) \n",Ncmx);
      if(Ncmx == 0)
	{
	  printf("Ncmx is supposed to be filled by the frontend reading value from PSM (bnmr_init)\n");
	  printf("Default value of 32 will be used\n");
	  cm_msg(MINFO,"rf_config","Ncmx has not been filled by the frontend...using default Ncmx=32");
	  ppg.output.psm.max_cycles_iq_pairs__ncmx_ =  Ncmx = 32;  // note one value for all profiles
	}
      else
	return -1;
    }
  if(my_debug)
    printf("Input params: A=%f ; alpha = %f; Ncmx = %d\n",A,alpha,Ncmx);
  
  d_w_max = ( pow(10,7)* alpha ) / (A * 512.0);  // rad/s
  if(my_debug)
    { // for checking calc.
      x = d_w_max/128;
      printf(" d_w_min = %f / Ncmx\n", x);
    }
  d_w_min = d_w_max /(128 * Ncmx); // rad/s
  d_nu_min = d_w_min/(2*M_PI); // Hz
  d_nu_max = d_w_max/(2*M_PI); // Hz
								   
  //  if (my_debug)
    {
      printf(" d_w_min  = %.3f  d_w_max  = %.3g  rad/s\n",d_w_min,d_w_max);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }

 
  d_nu = (double)  ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_[pro_index];

  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      printf("build_iq_table_psm: requested bandwidth (%dHz) is out of range\n",
	     ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_[pro_index]);
      printf("        bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      return -1;
    }

  d_w_req= 2 * M_PI * d_nu; 

  printf("build_iq_table_psm: requested bandwidth =%d Hz \n",
	 ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_[pro_index]);
  printf("build_iq_table_psm: requested bandwidth d_nu=%1e Hz;   d_w_req = %.2f rad/s\n",d_nu, d_w_req);


  d_const = ( alpha * 2 * pow(10,7)) / A ; // this is 10**9 for A=0.1 and alpha=5
  if(my_debug)
    printf("d_const = %.2e; expected value = 10**9 \n",d_const);

  Ntiqtemp = (INT) ( d_const/d_w_req );   // nearest smallest integer

  if( (Ntiqtemp < 1024)  || ( Ntiqtemp >= 129024 * Ncmx ))
    {
      printf("build_iq_table_psm: preliminary no. of IQ points (Ntiqtemp=%d) is out of range\n",
	     Ntiqtemp);
      printf("      Ntiqtemp must be between 1024 and %d \n",( 129024 * Ncmx));
      return -1;
    }
   
  // select Nc, Ncic from the table of Ntiqtemp values
  Nc=Ncic=0;
  status = get_IQ_params(Ntiqtemp, &Nc, &Ncic);
  if(status== -1)
    return -1;


  printf("Build_iq_table_psm: Ntiqtemp=%d, Nc =%d, Ncic=%d\n",Ntiqtemp,Nc,Ncic);
  if(Nc > Ncmx)
    {
      printf("build_iq_table_psm: Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d\n",
	     Ntiqtemp,Nc,Ncmx);
      return -1;
    }

  ppg.output.psm.cic_interpoln_rate__ncic_[pro_index] = Ncic;
  ppg.output.psm.num_cycles_iq_pairs__nc_[pro_index] = Nc;


  dNcic = (double) Ncic;
  dNc   = (double) Nc;
  dNtiqtemp = (double) Ntiqtemp;


  /* calculate Niq */
  tmp = dNtiqtemp/(dNcic * dNc);
  Niq = (int) (1+ tmp);  // closest larger integer
  Ntiq= Niq * Nc * Ncic;

  if(my_debug)
    printf("build_iq_table_psm: dNcic = %.1f ; dNc = %.1f;  dNtiqtemp = %.1f; tmp=%.1f  \n",
	   dNcic,dNc,dNtiqtemp,tmp);

  printf("build_iq_table_psm: Calculated Niq = %d ; Ntiq = %d \n",Niq,Ntiq);

  if(Niq < 512 || Niq > 2048)
    {
      printf("Consistency check failure; Niq=%d, must be between 512 and 2048\n",Niq);
      return -1;
    }
  if(Ntiq >= 129024 * Ncmx)
    {
      printf("Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d\n",
	     Ntiq,129024*Ncmx);
      return -1;
    }


  ppg.output.psm.num_iq_pairs__niq_[pro_index] = Niq;
  dNtiq = (double) Ntiq;
  dNiq = (double) Niq;

  /* d_const = 10**9   */
  dw = d_const /dNtiq  ; /* rad/sec */
  ppg.output.psm.bandwidth__rad_per_sec_[pro_index] = dw;

  x=dw/(2* M_PI);

  /* no longer output "actual bandwidth" as new calculation
     delivers the requested bandwidth */
  actual_bandwidth__hz_ = (int)(x +0.5); // as a check
  
  Tp =  pow(10,-7) * dNtiq; // pulse width (sec)
  printf("Tp = %.3e sec\n",Tp);
  ppg.output.psm.pulse_width__msec_[pro_index] =  Tp * pow(10,3); // pulse width (ms)
  
  printf("\n **  IQ Output parameters: *** \n");
  printf("No. IQ pairs Niq= %d;  Ntiq= %d;    Ncic=%d\n",Niq,Ntiq,Ncic);
  printf("output bandwidth dw=%.2f rad/s or %d Hz;   pulse width Tp=%.3e sec or %.3f ms \n\n",
	 dw, actual_bandwidth__hz_, Tp, ppg.output.psm.pulse_width__msec_[pro_index]);



  N=1;
  dn=1.0;

  printf("iq filename will be : %s%s_iq_%s.psm\n",ppg.input.cfg_path,ppg_mode,profile);
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,profile); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,profile); // debug file

  if(my_debug)printf("build_iq_table_psm: about to open IQ table DEBUG file %s\n",str);

  /* If setting constant values, check they  are reasonable 
     For standard PSM,  
     ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_[pro_index] and
     ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_[pro_index] are both 0.

*/
  I_zero=FALSE;
  if(ppg.hardware.psm.iq_modulation.set_constant_i_value_in_file[pro_index])
    {
      if ( ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_[pro_index] ==0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	         cm_msg(MERROR,"Build_iq_table_psm",
		     "Modulating with constant I=0 will result in no output for Hermite since Q=0 also");
	      printf("Build_iq_table_psm: Constant I=0 not allowed for Hermite since Q=0 (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_[pro_index]);
      
    }
  if(ppg.hardware.psm.iq_modulation.set_constant_q_value_in_file[pro_index])
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	    cm_msg(MINFO,"build_iq_table_psm",
	  	 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
	   ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_[pro_index]);
	  printf("Build_iq_table_psm: no effect from modulating with constant Q=%d  since Q=0 always for Hermite\n",	 ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_[pro_index]);
		 
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_[pro_index] ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"Build_iq_table_psm",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("Build_iq_table_psm: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_[pro_index]);
      
    }
  

  if(my_debug)printf("build_iq_table_psm: about to open IQ table DEBUG file %s\n",str);

  if ( dbgfile = fopen(astr,"w"))
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Req bandwidth(Hz)=%d; output bandwidth(Hz) =%d\n",
	      ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_[pro_index],
	      actual_bandwidth__hz_);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms ; Ncic=%d;\n",
	      Niq,  ppg.output.psm.pulse_width__msec_[pro_index], Ncic);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);

  printf("build_iq_table_psm: about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into \"%s\" profile\n",profile);
  if (iqfile = fopen(str,"w"))    
    {
      do
	{ 
	  double di,dq;
	  /* ln-sech */
	  if(mod_func == 0)
	    {
	      // sech = 1/cosh
	      z=(dw*Tp/10.0)  * ((dn/dNiq) - 0.5);
	      a = 1.0/cosh (z);
	      phi = 5.0 * log(a);
	      di= 511*a*cos(phi) ;    
	      dq= 511*a*sin(phi) ;
	    }
	  else
	    {  /* mod_func = 1, Hermite */
	      z = A * dw * Tp *  ((dn/dNiq) - 0.5) ;
	      a = pow(z,2);
	      di = 511 * (1.0 -0.957 * a) * exp(-a);
	      dq = 0;
	    }

	  if(ppg.hardware.psm.iq_modulation.set_constant_i_value_in_file[pro_index])
	    I=ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_[pro_index];
	  else
	    {
	      I = (int) (di + 0.5);  // closest integer
 
	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm: Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	  i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	    
	  if(ppg.hardware.psm.iq_modulation.set_constant_q_value_in_file[pro_index])
	    Q=ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_[pro_index];
	  else
	    Q = (int)(dq+0.5); // closest integer

	  q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */

	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g  a=%g  phi=%g \n",
		   (int)dn,I,Q,i,q,di,dq,a,phi);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;
	  dn=(float)N;
	  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"Build_iq_table_psm","Error opening iq file %s ",str);
      printf("Build_iq_table_psm: Error opening iq file %s ",str);
      return -1;
    }
}

