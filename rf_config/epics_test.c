/* test program  epics_test.c

Read helicity

 */


#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "cadef.h"


#include "epics_IDs.h"
#ifndef LOCAL
#define LOCAL static
#endif

INT read_Epics_chan(INT chan_id,  float * pvalue);
INT write_Epics_chan(INT chan_id,  float *pvalue);
INT getline(char *line, INT max);



INT getline(char *line, INT max)
{
  if (fgets(line,max,stdin) == NULL)
    return 0;
  else
    return strlen(line);
}

void list(void)
{
  printf("Names of Epics channels (R=read W=write):\n\n");
  printf("helicity    R : %s, %s  %s\n",RhelOn,RhelOff,RhelSwitch);
  printf("helicity    W : %s, %s\n",WhelOn,WhelOff); 
  printf("shutter     R : %s, %s\n",RshutOn,RshutOff);
  printf("shutter     W : %s, %s\n",WshutOn,WshutOff);
  printf("pol         R : %s, %s\n",RpolOn,RpolOff); 
  printf("pol         W : %s, %s\n\n",RpolOn,RpolOff); 

  printf("dual chan   R : %s\n", REnableSwitching);
  printf("dual chan   R : %s, %s, %s, %s\n", RbnmrPeriod,  RbnqrPeriod,  RbnmrDelay,  RbnqrDelay);

  printf("\nNaCell  R,W : %s, %s\n", NaRead_name, NaWrite_name);
  printf("Dye Laser R,W : %s, %s\n", LaRead_name,LaWrite_name);
  printf("Field     R,W : %s, %s\n\n", FdRead_name,FdWrite_name);

  printf("POL HV bias R : %s, %s, %s\n",R_ITW, R_ITE, R_IOS );  
}


int main(void)
{
  INT status;
  float value=-1.0;
  char Name[80];
  char str[32];
  INT max=79;
  INT len, i,j,chid;
  BOOL write;
  float fval;

  chid = -1;
  printf("\nepics_test is a program that allows you to read from or write to a single Epics channel\n");
  printf("  NOTE: Some EPICS channels are read-only or write-only, and to \n");
  printf("       successfully write to an Epics variable, you must have write permission\n\n"); 
  printf("\n for a list of Epics channels used by BNMR/BNQR/POL, enter \"List\" \n"); 

  j=1;
  while(j)
    {
      printf("\nEnter name of Epics channel (<CR> to exit):");
      len=getline(Name,max);
      if(len> 1)
	Name[len-1]='\0';
      else
	return 0;

      for(i=0; i<len; i++)
	Name[i]=toupper(Name[i]);

      printf("Epics channel:\"%s\"\n",Name);
      if(strncmp(Name,"LIST",4)==0)
	{
	  list();
	}
      else
	j=0;
    }

  printf("Enter value to write, or <CR> if reading:");
  if(getline(str,max) > 1)
    {
      write=TRUE;
      sscanf(str,"%f",&fval);
      printf("Value to write to EPICS channel \"%s\" is \"%f\"\n",Name,fval);
    }
  else
    {
      printf("Reading from EPICS channel \"%s\" \n",Name);
      write=FALSE;
    }


  // Get EPICS ID 
  status=caGetSingleId (Name, &chid );
  if(status==-1)
    {
      printf("epics_test: Bad status after caGetID for channel %s\n",Name);
      caExit(); /* clear any channels that are open */
      return 0;
    }

  
  printf("caGetId returns chan_id for channel %s) as %d \n",
	 Name,chid);

  if (caCheck(chid))
    printf("caCheck says channel %s is connected\n",Name);
  else
    printf("caCheck says channel %s is NOT connected\n",Name);

  
  if(!write)
    {

      /* now read the channel */

      value = -1;
      status = read_Epics_chan(chid,  &value);
      printf("\nepics_test: after reading Epics channel %d, value = %f  (%d)\n",Name,value,status);

    }
  else
    {
      /* now write to EPICS channel */
      value = 0;
      //      printf("\n Writing %f to Epics channel %s\n",fval,Name);
  
      status = write_Epics_chan(chid, &fval);
      printf("\nepics_test: after writing %f to Epics channel %s,   status = %d\n",fval,Name,status);
    
    }

  
  
  caExit();
  return;
}


INT read_Epics_chan(INT chan_id, float *pvalue)
{
  INT status;
  float fval;
  
  if(chan_id == -1)
  {
    printf("read_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caRead(chan_id,&fval);
  if(status==-1)
  {
    printf("read_Epics_chan: Bad status after caRead for %s\n", ca_name((chid)chan_id));
    return(FE_ERR_HW);
  }
  else
    printf("read_Epics_chan: Read value for %s as %8.3f\n", ca_name((chid)chan_id),fval);

  *pvalue=fval;
  printf("read_Epics_chan for %s returning value = %f \n",ca_name((chid)chan_id),*pvalue);
  return(DB_SUCCESS);
}


/*********************************************************************/
INT write_Epics_chan(INT chan_id,  float *pvalue)
/*********************************************************************/  
{
  INT status;
  INT d7=1;


  if(d7)printf("Write_Epics_chan starting with chan_id=%d and value = %f\n",
	       chan_id ,*pvalue);
 
  if(chan_id == -1)
  {
    printf("write_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caWrite(chan_id,pvalue);
  if(d7)printf("caWrite returns status (%d)\n",status);

  if(status==-1)
  {
    printf("write_Epics_chan: Bad status after caWrite for %s\n",ca_name( (chid)chan_id) );
    return(FE_ERR_HW);
  }
  
  return(DB_SUCCESS);
}

