/* epics_check.c

included by rf_config

Read helicity

 */
char RhelSwitch[]="ILE2:POLSW2:STATLOC";/* 0=DAQ has control 1=Epics has control */
/* 
--------------------------------------------
EPICS names of dual channel switching modes
--------------------------------------------
*/
char RbnmrPeriod[]="BNMR:BNQRSW:BNMRPERIOD";
char RbnqrPeriod[]="BNMR:BNQRSW:BNQRPERIOD";
char RbnmrDelay[]="BNMR:BNQRSW:BNMRDELAY";
char RbnqrDelay[]="BNMR:BNQRSW:BNQRDELAY";
char REnableSwitching[]="BNMR:BNQRSW:ENBSWITCHING";

INT read_Epics_chan(INT chan_id,  float * pvalue);
/* INT write_Epics_chan(INT chan_id,  float *pvalue); */

INT d7=1;


INT epics_check(BOOL hel)
{
  /* hel=1 for checking helicity switch position only */

  INT i,status;
  float value=-1.0;
  

  // Get ID for  Helicity Switch
  Rchid_helSwitch=-1;
  status=caGetSingleId (RhelSwitch, &Rchid_helSwitch );
  if(status==-1)
    {
      printf("epics_check: Bad status after caGetID for status of Helicity Switch \n");
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }

  if(d7)
    printf("caGetId returns chan_id for  status HEL SWITCH (%s) as %d \n",
	   RhelSwitch,Rchid_helSwitch);

 
  if(d7)printf("Calling read_Epics_chan for helSwitch\n");
  status = read_Epics_chan(Rchid_helSwitch,  &value);
  i=(INT)value;
  printf("\n epics_check: helicity switch value is %d (switch control: 1=EPICS 0=DAQ)\n",i);
  if( ppg.hardware.enable_helicity_flipping)
    {
      if(i != 0)
	{
	  cm_msg(MERROR,"epics_check",
		 "ERROR - DAQ does not have control of helicity switch; cannot flip helicity");
	  caExit(); /* epics finished */
	  return FE_ERR_HW;
	}
      else
	cm_msg(MINFO,"epics_check",
	       "INFO: DAQ controls EPICS helicity switch; helicity can be flipped");
    }
  else
    {
      if (i != 1)
	  cm_msg(MINFO,"epics_check",
		 "WARNING - Flipping not enabled; DAQ should NOT have control of helicity switch; ");
    }

  if(d7)printf("epics_check: disconnecting Epics channel for Helicity Switch \n");
  ca_clear_channel((chid)Rchid_helSwitch); /* we don't need this channel any more */
  Rchid_helSwitch = -1;

  if(hel)
    {
      caExit(); /* epics finished */
      return SUCCESS; /* checked helicity switch only */
    }

      
  /* Read the Epics Enable Switching param for dual channel mode */

  Rchid_EnableSwitching = -1;

  status = caGetSingleId(REnableSwitching, &Rchid_EnableSwitching );
  if(status==-1)
    {
      printf("epics_check: Bad status after caGetID for status of \"%s\" to Enable Switching \n", REnableSwitching);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }



  if(d7)printf("epics_check: caGetId returns chan_id for \"Enable Switching\" (\"%s\") as %d \n",
	 REnableSwitching,Rchid_EnableSwitching);
 
  if(d7)printf("Calling read_Epics_chan for Dual channel EnableSwitching\n");
  status = read_Epics_chan(Rchid_EnableSwitching,  &value);
  i=(INT)value;
  printf("epics_check: dual channel switch value is %d (switch control: 1=Dual Channel Mode 0=Single Channel Mode)\n",i);

  ppg.output.epics_dual_channel_switch=i;
  if(d7)printf("epics_check: disconnecting Epics channel for Enable Switching \n");
  ca_clear_channel((chid)Rchid_EnableSwitching); /* we don't need this channel any more */
  Rchid_EnableSwitching = -1;

  if(i==0)
    {
      /* Single channel mode is enabled in Epics */
      cm_msg(MINFO,"epics_check","Single Channel Mode is enabled in Epics");
      if(ppg.hardware.enable_dual_channel_mode)
	{
	  cm_msg(MERROR,"epics_check","Dual channel Mode is enabled in the ODB; mismatch with EPICS\n");
	  return DB_INVALID_PARAM;
	}
      cm_msg(MINFO,"epics_check","EPICS switch setting is consistent with Single Channel Mode");
      return SUCCESS;  /* Single Channel Mode: other values are irrelevent */
    }

/* 
          Dual Channel Mode: 
*/
  cm_msg(MINFO,"epics_check","Dual Channel Mode is enabled in Epics");

  if(!ppg.hardware.enable_dual_channel_mode)
    {
      cm_msg(MERROR,"epics_check","Single channel Mode is enabled in the ODB; mismatch with EPICS\n");
      return DB_INVALID_PARAM;
    }
  cm_msg(MINFO,"epics_check","EPICS switch setting is consistent with Dual Channel Mode");

  /*
    read the rest of the Epics dual channel mode parameters 
  */  
  
  Rchid_BnmrDelay=Rchid_BnmrPeriod=Rchid_BnqrDelay=Rchid_BnqrPeriod=-1;
 

  /* get parameters and write them into odb output area */

  /*  BNMR Period */
  status = caGetSingleId(RbnmrPeriod, &Rchid_BnmrPeriod );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" (BNMR Period) \n", RbnmrPeriod);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNMR Period\" (\"%s\") as %d \n",
	 RbnmrPeriod,Rchid_BnmrPeriod);
 
  if(d7)printf("Calling read_Epics_chan for BNMR Period\n");
  status = read_Epics_chan(Rchid_BnmrPeriod,  &value);
  printf("GetEpicsParams: BNMR Period is %f \n",value);

  ppg.output.epics_bnmr_beam_period=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNMR Period \n");
  ca_clear_channel((chid)Rchid_BnmrPeriod); /* we don't need this channel any more */
  Rchid_BnmrPeriod = -1;


  /*  BNMR Delay */
  status = caGetSingleId(RbnmrDelay, &Rchid_BnmrDelay );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\"  (BNMR delay) \n", RbnmrDelay);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNMR Delay\" (\"%s\") as %d \n",
	 RbnmrDelay,Rchid_BnmrDelay);
 
  if(d7)printf("Calling read_Epics_chan for BNMR Delay\n");
  status = read_Epics_chan(Rchid_BnmrDelay,  &value);
  printf("GetEpicsParams: BNMR Delay is %f \n",value);

  ppg.output.epics_bnmr_beam_delay=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNMR Delay \n");
  ca_clear_channel((chid)Rchid_BnmrDelay); /* we don't need this channel any more */
  Rchid_BnmrDelay = -1;


 

  /*  BNQR Period */
  status = caGetSingleId(RbnqrPeriod, &Rchid_BnqrPeriod );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" (BNQR Period) \n", RbnqrPeriod);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNQR Period\" (\"%s\") as %d \n",
	 RbnqrPeriod,Rchid_BnqrPeriod);
 
  if(d7)printf("Calling read_Epics_chan for BNQR Period\n");
  status = read_Epics_chan(Rchid_BnqrPeriod,  &value);
  printf("GetEpicsParams: BNQR Period is %f \n",value);

  ppg.output.epics_bnqr_beam_period=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNQR Period \n");
  ca_clear_channel((chid)Rchid_BnqrPeriod); /* we don't need this channel any more */
  Rchid_BnqrPeriod = -1;


  /*  BNQR Delay */
  status = caGetSingleId(RbnqrDelay, &Rchid_BnqrDelay );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\"  (BNQR Delay) \n", RbnqrDelay);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNQR Delay\" (\"%s\") as %d \n",
	 RbnqrDelay,Rchid_BnqrDelay);
 
  if(d7)printf("Calling read_Epics_chan for BNQR Delay\n");
  status = read_Epics_chan(Rchid_BnqrDelay,  &value);
  printf("GetEpicsParams: BNQR Delay is %f \n",value);

  ppg.output.epics_bnqr_beam_delay=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNQR Delay \n");
  ca_clear_channel((chid)Rchid_BnqrDelay); /* we don't need this channel any more */
  Rchid_BnqrDelay = -1;
 
  caExit();
  return SUCCESS;
}


INT read_Epics_chan(INT chan_id, float *pvalue)
{
  INT status;
  float fval;
  
  if(chan_id == -1)
  {
    printf("read_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caRead(chan_id,&fval);
  if(status==-1)
  {
    printf("read_Epics_chan: Bad status after caRead for %s\n", ca_name((chid)chan_id));
    return(FE_ERR_HW);
  }
  else
    printf("read_Epics_chan: Read value for %s as %8.3f\n", ca_name((chid)chan_id),fval);

  *pvalue=fval;
  printf("read_Epics_chan for %s returning value = %f \n",ca_name((chid)chan_id),*pvalue);
  return(DB_SUCCESS);
}


/*********************************************************************/
INT write_Epics_chan(INT chan_id,  float *pvalue)
/*********************************************************************/  
{
  INT status;
  INT d7=1;


  if(d7)printf("Write_Epics_chan starting with chan_id=%d and value = %f\n",
	       chan_id ,*pvalue);
 
  if(chan_id == -1)
  {
    printf("write_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caWrite(chan_id,pvalue);
  if(d7)printf("caWrite returns status (%d)\n",status);

  if(status==-1)
  {
    printf("write_Epics_chan: Bad status after caWrite for %s\n",ca_name( (chid)chan_id) );
    return(FE_ERR_HW);
  }
  
  return(DB_SUCCESS);
}
