//--------------------------------------------------------
int build_iq_table_psm3(char *ppg_mode, char *channel_name)
//----------------------------------------------------
{
  /* build iq table for the psmIII; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  moduln_mode[16];
  char  str[256];
  char  astr[256];
  BOOL  I_zero; // temp flag
  INT status, size, j,i,q;
  
  INT my_debug=FALSE;
  INT N, Ncic,Niq;
  double dNiq;
  
  double d_nu;
  INT I,Q;
  double Tp,Tpc,MaxTpc,MinTpc,dNC1;
  
  // temp variables
  double x;
  int  actual_bandwidth__hz_;
  
  double d_nu_min,d_nu_max;
  double factor;
  const   INT u = 5;
  double b;
  INT Nc1;
  
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */  
  INT Ncmx;  
  INT channel_index;

  BOOL set_const_i_file,  set_const_q_file;
  INT const_i, const_q, req_bw;

  cm_msg(MINFO,"build_iq_table_psm3","starting with channel %s  ",channel_name);
  if(get_channel_index(channel_name,&channel_index) != SUCCESS)
  {
    printf("build_iq_table_psm3  : illegal channel_name %s\n",channel_name);
    cm_msg(MERROR,"build_iq_table_psm3","illegal channel_name %s  ",channel_name);
    return -1;
  }
  printf("\n  build_iq_table_psm3 : working on channel_name \"%s\" (channel_index %d) (PSM III) \n",channel_name,channel_index);
  
  switch (channel_index)
    {
    case 0:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch1.iq_modulation.const_q__511__q__512_;
      req_bw =  ppg.hardware.psm.f0ch1.iq_modulation.requested_bandwidth__hz_;
      break;

    case 1:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch2.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch2.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch2.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch2.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch2.iq_modulation.const_q__511__q__512_;
      req_bw =  ppg.hardware.psm.f0ch2.iq_modulation.requested_bandwidth__hz_;
      break;

   case 2:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch3.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch3.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch3.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch3.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch3.iq_modulation.const_q__511__q__512_;
      req_bw =  ppg.hardware.psm.f0ch3.iq_modulation.requested_bandwidth__hz_;
      break;

   case 3:
      strncpy( moduln_mode, ppg.hardware.psm.f0ch4.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f0ch4.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f0ch4.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f0ch4.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f0ch4.iq_modulation.const_q__511__q__512_;
      req_bw =  ppg.hardware.psm.f0ch4.iq_modulation.requested_bandwidth__hz_;
      break;

    case 4:
      strncpy( moduln_mode, ppg.hardware.psm.f1.iq_modulation.moduln_mode__ln_sech_hermite_,15);
      d_nu = (double)  ppg.hardware.psm.f1.iq_modulation.requested_bandwidth__hz_;
      set_const_i_file = ppg.hardware.psm.f1.iq_modulation.set_constant_i_value_in_file;
      const_i = ppg.hardware.psm.f1.iq_modulation.const_i__511__i__512_;
      set_const_q_file = ppg.hardware.psm.f1.iq_modulation.set_constant_q_value_in_file;
      const_q = ppg.hardware.psm.f1.iq_modulation.const_q__511__q__512_;
      req_bw =  ppg.hardware.psm.f1.iq_modulation.requested_bandwidth__hz_;
      break;
    default:
      printf("build_iq_table_psm3 : Error: channel_name %d i.e. \"%s\" is not supported\n",channel_index,channel_name);
      cm_msg(MERROR,"build_iq_table_psm3","Error: channel_name %d i.e. \"%s\" is not supported ",channel_index,channel_name);
      return -1;
    }  

  /* Check the modulation mode (ln-sech or Hermite) */   
  sprintf( str,"%s", moduln_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen( str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  
  
  if (strncmp( str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm3: detected ln-sech modulation mode\n");
      b = 5.2983;
      factor =  b * u / M_PI;  // 8.4325
      d_nu_min = 40; // Hz
      d_nu_max = 164688; // Hz
    }
  else if  (strncmp( str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm3: detected Hermite modulation mode\n");
      b = 2.5;
      factor =  4 * sqrt (-log(.5)) * b / M_PI; // 2.64
      d_nu_min = 12.6; // Hz
      d_nu_max = 51562; // Hz
    }
  else
    {
      printf("build_iq_table_psm3: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported \n",
	     moduln_mode);
      cm_msg(MERROR,"build_iq_table_psm3", "Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported ",
	     moduln_mode);
      return -1;
    }

 
  size = sizeof(Ncmx);


  /* Ncmx  is written into ../output/psm by frontend
     We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", 
		   ppg.output.psm.max_cycles_iq_pairs__ncmx_); // note one value for all channels
  Ncmx =  ppg.output.psm.max_cycles_iq_pairs__ncmx_; // note one value for all channels
 
  // Ncmx is 1023 for PSMIII
  if(Ncmx != 1023)
    {
      cm_msg(MINFO,"build_iq_table_psm3","Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Using Ncmx=1023",Ncmx);
      printf("build_iq_table_psm3: Unexpected value of param Ncmx read from ODB ../output/psm for PSMIII (%d). Expect 1023. Continuing with Ncmx=1023\n",Ncmx);
  // Set to 1024 and continue
    Ncmx = 1023;
    }								   
  //  if (my_debug)
    {
      printf("build_iq_table_psm3:  for Modulation mode \"%s\" :\n",moduln_mode);
      printf(" b=%f; factor=%f;  Ncmx = %d\n",b,factor,Ncmx);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }

  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      cm_msg(MERROR, "build_iq_table_psm3"," requested bandwidth (%.0fHz) is out of range ",
	     d_nu);
      cm_msg(MINFO, "build_iq_table_psm3","bandwidth must be between %f and %f Hz",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3 : bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      printf("build_iq_table_psm3 : requested bandwidth (%.0fHz) is out of range\n",d_nu);
      return -1;
    }
  
  Tp = factor/d_nu * pow(10,-3);   // d_nu in kHz, Tp in ms
  // check Tp in limits - what are the limits?

  Nc1= (INT) (0.5 + (Tp/(50*exp(-9)*2048)) ); // round up to nearest integer
  Niq =  (INT) (0.5 + (Tp/(50*exp(-9) * Nc1)));
  Tpc = 50*exp(-9) * Nc1 * Niq;

  MaxTpc =  (2048 * 50 * exp(-9) * 2048);
  MinTpc =  (1 * 50 * exp(-9) * 2048);
  if (Tpc < MinTpc )
    printf("build_iq_table_psm3: calculated Tpc (%f) is less than minimum value (%f)\n",Tpc,MinTpc);
  else if (Tpc >= MaxTpc)
    printf("build_iq_table_psm3: calculated Tpc is greater than maximum value (%f)\n",Tpc,MaxTpc);
  if((Tpc < MaxTpc) || (Tpc >= MaxTpc))
    {
      cm_msg(MERROR,"build_iq_table_psm3", "Calculated Tpc (%f) is out of range",Tpc);
      return -1;
    }

  if(Niq <1024 || Niq > 2048)
    {
      cm_msg(MERROR,"build_iq_table_psm3", "Calculated Niq (%f) is out of range 1023 < Niq <= 2048 ",Niq);
      return -1;
    }

  ppg.output.psm.num_cycles_iq_pairs__nc_[channel_index] = Nc1;
  ppg.output.psm.num_iq_pairs__niq_[channel_index] = Niq;

  dNc1   = (double) Nc1;
  dNiq = (double) Niq;

 
  printf("iq filename will be : %s%s_iq_%s.psm\n",ppg.input.cfg_path,ppg_mode,channel_name);
  sprintf(str,"%s%s_iq_%s.psm",ppg.input.cfg_path,ppg_mode,channel_name); // IQ pair file
  sprintf(astr,"%s%s_iq_%s.dbg",ppg.input.cfg_path,ppg_mode,channel_name); // debug file

  if(my_debug)printf("build_iq_table_psm3 : about to open IQ table DEBUG file %s\n",str);

 

  /* If setting constant values, check they  are reasonable */
  I_zero=FALSE;
  printf("build_iq_table_psm3 : set_const_i_file = %d and set_const_q_file= %d \n",set_const_i_file,set_const_q_file );
  if(set_const_i_file)
    {
      if ( const_i == 0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	      cm_msg(MERROR,"build_iq_table_psm3 ",
		     "Modulating with constant I=0 will result in no output for Hermite since Q=0 also");
	      printf("build_iq_table_psm3 : Constant I=0 not allowed for Hermite since Q=0 (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       const_i);
      
    }
  
  if(set_const_q_file)
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	  cm_msg(MINFO,"build_iq_table_psm3 ",
		 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
		 const_q);
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( const_q ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"build_iq_table_psm3 ",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("build_iq_table_psm3: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     const_q);
      
    }
  
  dbgfile = fopen(astr,"w");
  if(dbgfile)
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Req bandwidth(Hz)=%d; output bandwidth(Hz) =%d\n",
	      req_bw,
	      actual_bandwidth__hz_);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms ; Ncic=%d;\n",
	      Niq,  ppg.output.psm.pulse_width__msec_[channel_index], Ncic);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);
 

  printf("build_iq_table_psm3 : about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into \"%s\" channel_name\n",channel_name);

  iqfile = fopen(str,"w");
  if (iqfile)    
    {
      N=1; 
      do
	{ 
	  double di,dq;
	 
	  /* ln-sech */
	  if(mod_func == 0)
	    {  // sech = 1/cosh
	      x=b*2*(2*N-Niq-1)/(2*(Niq-1));
	      double sech_x = i/cosh(x);
	      di = 511*sech_x * cos(u*log(sech_x));
	      dq = 511*sech_x * sin(u*ln(sech_x));
	    }
	    
	  else
	    {  /* mod_func = 1, Hermite */	 
	      x=b*2*(2*N-Niq-1)/(2*(Niq-1));
	      di = 511 * (1-pow(x,2)) * exp(-(pow(x,2)));
	      dq = 0;
	    }

	  if(set_const_i_file)
	    I=const_i;
	  else	
	    {    
	      I = (int) (di + 0.5);  // closest integer

	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm3 : Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	  i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	    
	  if(set_const_q_file)
	    Q=const_q;
	  else
	    Q = (int)(dq+0.5); // closest integer
	   

	  q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */


	  if(my_debug)
	    printf("N=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g \n",
		   N,I,Q,i,q,di,dq);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"build_iq_table_psm3","Error opening iq file %s  ",str);
      printf("build_iq_table_psm3 : Error opening iq file %s \n",str);
      return -1;
    }
}
// end of PSMIII specific
