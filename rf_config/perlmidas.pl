# perlmidas.pl 
use strict;
use warnings;
##############################################################
sub MIDAS_env
# set up proper MIDAS environment...
##############################################################
{
    our ($MIDAS_HOSTNAME,$MIDAS_EXPERIMENT,$ODB_SUCCESS,$DEBUG);
    our ($CMDFLAG_HOST, $CMDFLAG_EXPT);

    $ODB_SUCCESS=0;

    $MIDAS_HOSTNAME = $ENV{"MIDAS_SERVER_HOST"};
    if (defined($MIDAS_HOSTNAME) &&   $MIDAS_HOSTNAME ne "")
    {
        $CMDFLAG_HOST = "-h $MIDAS_HOSTNAME";
    }
    else
    {
        $MIDAS_HOSTNAME = "";
        $CMDFLAG_HOST = "";
    }

    $MIDAS_EXPERIMENT = $ENV{"MIDAS_EXPT_NAME"};
    if (defined($MIDAS_EXPERIMENT) &&   $MIDAS_EXPERIMENT ne "")
    {
        $CMDFLAG_EXPT = "-e ${MIDAS_EXPERIMENT}";
    }
    else
    {
        $MIDAS_EXPERIMENT = "";
        $CMDFLAG_EXPT = "";
    }

}
##############################################################
sub MIDAS_sendmsg
##############################################################
{
# send a message to odb message logger
    my ($name, $message) =  @_;

    our ($MIDAS_HOSTNAME,$MIDAS_EXPERIMENT,$ODB_SUCCESS,$DEBUG);
    our ($CMDFLAG_HOST, $CMDFLAG_EXPT);
    our ($COMMAND, $ANSWER);

    my $status;
    my $host="";
    my $dquote='"';
    my $squote="'";
    my $command="${dquote}msg ${name} ${squote}${message}${squote}${dquote}";
    print "name=$name, message=$message\n";
    print "command is: $command \n";

    $COMMAND ="`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c ${command}`";
    $ANSWER=`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c ${command}`;
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
    if($DEBUG)
    {
        print "command: $COMMAND\n";
        print " answer: $ANSWER\n";
    }

    if($status != $ODB_SUCCESS) 
    { # this status value is NOT the midas status code
        print "send_message:  Failure status returned from odb msg (status=$status)\n";
    }
    return;
}
sub strip
{
# removes / from end of string, // becomes /
    my $string=shift;
    $string=~ (s!//!/!g);
    $string=~s!/$!!;
    print "strip: now \"$string\"\n";
    return ($string);
}
sub MIDAS_varset
##############################################################
{
# set a value of an odb key
    my ($key, $value) =  @_;

    our ($MIDAS_HOSTNAME,$MIDAS_EXPERIMENT,$ODB_SUCCESS,$DEBUG);
    our ($CMDFLAG_HOST, $CMDFLAG_EXPT);
    our ($COMMAND, $ANSWER);

    my $status;
    my $host="";
    my $dquote='"';
    my $squote="'";
    my $command="${dquote}set ${squote}${key}${squote} ${squote}${value}${squote}${dquote}";
    print "key=$key, new value=${value}\n";
    print "command is: $command \n";

    $COMMAND ="`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c command`";
    $ANSWER=`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c $command `;
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
    if($DEBUG)
    {
        print "command: $COMMAND\n";
        print " answer: $ANSWER\n";
    }

    if($status != $ODB_SUCCESS) 
    { # this status value is NOT the midas status code
        print "send_message:  Failure status returned from odb msg (status=$status)\n";
    }
    return;
}

sub MIDAS_varget
##############################################################
{
# set a value of an odb key
    my ($key) =  @_;

    our ($MIDAS_HOSTNAME,$MIDAS_EXPERIMENT,$ODB_SUCCESS,$DEBUG);
    our ($CMDFLAG_HOST, $CMDFLAG_EXPT);
    our ($COMMAND, $ANSWER);

    my $status;
    my $host="";
    my $dquote='"';
    my $squote="'";
    my $command="${dquote}ls -v ${squote}${key}${squote}${dquote}";
    print "key=$key\n";
    print "command is: $command \n";
    
    $COMMAND ="`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c command`";
    $ANSWER=`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c $command `;  
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
    if($DEBUG)
    {
        print "command: $COMMAND\n";
        print " answer: $ANSWER\n";
    }

    if($status != 0) 
    { # this status value is NOT the midas status code
        print "send_varset  Failure status returned from odb msg (status=$status)\n";
    }
    return $ANSWER;
}

sub MIDAS_dirlist
##############################################################
{
# return a directory list of directory given by odb key
    my ($key) =  @_;

    our ($MIDAS_HOSTNAME,$MIDAS_EXPERIMENT,$ODB_SUCCESS,$DEBUG);
    our ($CMDFLAG_HOST, $CMDFLAG_EXPT);
    our ($COMMAND, $ANSWER);

    my $status;
    my $host="";
    my $dquote='"';
    my $squote="'";
    my $command="${dquote}ls ${squote}${key}${squote}${dquote}";
    print "key=$key\n";
    print "command is: $command \n";
    
    $COMMAND ="`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c command`";
    $ANSWER=`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c $command `;  
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
    if($DEBUG)
    {
        print "command: $COMMAND\n";
        print " answer: $ANSWER\n";
    }

    if($status != 0) 
    { # this status value is NOT the midas status code
        print "send_varset  Failure status returned from odb msg (status=$status)\n";
    }
    return $ANSWER;
}
sub MIDAS_startrun
##############################################################
{
# start MIDAS run
    my ($key) =  @_;

    our ($MIDAS_HOSTNAME,$MIDAS_EXPERIMENT,$ODB_SUCCESS,$DEBUG);
    our ($CMDFLAG_HOST, $CMDFLAG_EXPT);
    our ($COMMAND, $ANSWER);

    our ($SCANLOG_FH);

    my $status;
    my $host="";
    my $dquote='"';
    my $squote="'";
    my $command="${dquote}start now${dquote}";
    print "command is: $command \n";

    #sleep(10);

    $COMMAND ="`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c ${command}`";
    $ANSWER=`odbedit ${CMDFLAG_EXPT} ${CMDFLAG_HOST} -c ${command}`;
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
    if($DEBUG)
    {
        print "command: $COMMAND\n";
        print " answer: $ANSWER\n";

        #print $SCANLOG_FH "status: $status\n";
        #print $SCANLOG_FH "command: $COMMAND\n";
        #print $SCANLOG_FH " answer: $ANSWER\n";

    }

    if($status != 0)
    { # this status value is NOT the midas status code
        print "startrun:  Failure status returned from odb msg (status=$status)\n";
        print $SCANLOG_FH " answer: $ANSWER\n";

    }
    return $ANSWER;
}   
1;
