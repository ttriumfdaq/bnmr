#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
int TwosComp_convert(int value, int twos_comp);
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
int pdd=0;

int main(void)
{
  char cmd[]="hallo";
  int result,data,i;
  while ( 1 )
    {
      printf("\nEnter value to convert to 2s complement?  ");
      scanf("%d",&i);
      printf("Converting %d into 2s comp..\n",i);
      result = TwosComp_convert(i, 0);
      printf("result=%d \n",result);


      printf("Converting result %d back from 2s comp..\n",result);
      result = TwosComp_convert(result, 1);
      printf("Now result=%d \n",result);
      if(i != result)
      printf("** Converter problem!! **\n");


    }
}

int TwosComp_convert(int value, int twos_comp)
{
 /*  Convert data into twos complement and back (10 bit data)
     twos_comp = FALSE -> converts data into 2s Compliment
     twos_comp = TRUE  -> converts data out of 2s Compliment
 
   Register is 10 bits.  
     Valid input data can be from -511 to +512 to convert to 2s compliment.
     2's compliment data is from 0 to 1023 

            Example:
     Input        2s compliment
     512             512   0x200
     511             513   0x201
     480             544
      30             994
       1            1023
       0               0
      -1               1
     -30              30
    -254             254
    -511             511   

  */

  int IQ,iq;
  
  if(! twos_comp)
    {
      /* Convert data to twos_comp */
      IQ=value;
     
      if(IQ > 512 || IQ < -511) 
	{
	  printf("TwosComp_convert: Data is out of range for 10 bits (-512<IQ<512) \n");
	  return -1;    ; 
	}
      if(IQ > 0)
	iq=IQ;
      else
	{
          IQ*=-1;
	  iq=(~IQ +1) & I_Q_DATA_MASK ;
	}
      if(pdd)
	printf("*** TwosComp_convert: Data (%d 0x%x) converted to 2's complement and masked = %d 0x%x\n",IQ,IQ,iq,iq);
      return iq;
    }

  else
    {
      /* Convert data back from twos_comp */
      iq=value;
      if(iq>1023 || iq < 0)
	{
	  printf("TwosComp_convert: 2s complement data (%d) is out of range for 10 bits (-1<iq<1024) \n",iq);
	  return -1024; 
	}
	
       if(iq<=511)
      //	IQ=~(iq)+1;
	 IQ=iq;
      else
	{
	  IQ=~(iq-1);
	  IQ = IQ & I_Q_DATA_MASK ;
	  IQ*=-1;
	}
      if(pdd)
	printf("TwosComp_convert: Data (%d 0x%x) converted back from 2's complement = %d 0x%x\n",iq,iq,IQ,IQ);
      return IQ;
    }
}

